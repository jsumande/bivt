<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class NewsValidator extends Validator{


	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }

	}

	public function validateId($atts,$val) {

		global $wpdb;


		$checkNews = $wpdb->get_var("SELECT COUNT(*) FROM newsitems WHERE id = $val");


		if($checkNews==0) {
			$this->setErrorMessage($atts,'Please make sure that ID exists.');
			return false;

		}


	}


	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
	    }


	}


}