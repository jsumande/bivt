<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class CourseValidator extends Validator{

	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }
	}

	public function validateCourseName($atts,$val) {

		global $wpdb;

		if($val!=''){

			if(isset($_POST['id'])){
				$id = intval($_POST['id']);
				$checkCourse = $wpdb->get_var("SELECT COUNT(*) FROM courses WHERE name = '$val' AND id <> $id");
			}else{
				$checkCourse = $wpdb->get_var("SELECT COUNT(*) FROM courses WHERE name = '$val'");
			}

			if($checkCourse>0) {
				$this->setErrorMessage($atts,'That course name is already existed. Please choose another course name.');
				return false;

			}
		}
		



	}


	public function validateCategory($atts,$val) {

		global $wpdb;


		$editedVal = explode(',', $val);

		$categories = $wpdb->get_results("SELECT id,name FROM categories");

		$validCategories = [];
		if(count($categories)>0){

			foreach($categories as $category){
				array_push($validCategories,$category->id);
			}
		}
		$correctCategory = 0;
		foreach($editedVal as $v){
			if(!in_array($v, $validCategories)){
				$correctCategory = 1;
			}
		}

		if($correctCategory==1){
			$this->setErrorMessage($atts,'Please make sure that the category you selected existed.');
			return false;
		}

			
		
	}



	public function validateTeacher($atts,$val) {

		global $wpdb;

		$editedVal = explode(',', $val);

		$teachers = $wpdb->get_results("SELECT docents.id FROM wp_users LEFT JOIN users_groups ON users_groups.user_id=wp_users.ID LEFT JOIN docents ON docents.user_id =wp_users.ID WHERE users_groups.group_id='5'");

		$all_valid = true;
		foreach ($editedVal as $v) {
			$valid_teacher = false;
			foreach ($teachers AS $key=>$teacher) {
				$valid_teacher = $valid_teacher || $teacher->id == $v;
			}
			$all_valid = $all_valid && $valid_teacher;
		}

		if(!$all_valid){
			$this->setErrorMessage($atts,'Deze lijst van docenten is niet geldig!');
			return false;
		}
		
	}




	public function validateId($atts,$val){

		global $wpdb;

		$checkCourse = $wpdb->get_var("SELECT COUNT(*) FROM courses WHERE id = $val");

		if($checkCourse==0) {
			$this->setErrorMessage($atts,'Please make sure that ID exists.');
			return false;

		}

	}
	public function validateStatus($atts,$val) {


 		$validStatus = ['close','open'];
 		if(!in_array($val,$validStatus)) {

 			$this->setErrorMessage($atts,'Please make sure that the status is valid.');
			return false;
 		}

	}



	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
	    }


	}
}