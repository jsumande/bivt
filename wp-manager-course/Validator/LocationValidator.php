<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class LocationValidator extends Validator{


	public function validateStatus($atts,$val) {

		$validStatus = ['open','close'];


		if(!in_array($val, $validStatus)){

			$this->setErrorMessage($atts,'Please make sure you enter a valid status.');
			return false;
		}

	}

	public function validateLocation($atts,$val) {

		global $wpdb;

		$checkLocation = $wpdb->get_row("SELECT * FROM locations WHERE id = {$val}");

		if(count($checkLocation)==0){
			$this->setErrorMessage($atts,'Please make sure you enter a valid location.');
			return false;
		}

	}


	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }

	}

	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
	    }

	}
}