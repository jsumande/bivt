<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class AttendanceValidator extends Validator{

	public function validateStatus($atts,$val) {


 		$validStatus = ['absent','present'];
 		if(!in_array($val,$validStatus)) {

 			$this->setErrorMessage($atts,'Een student moet afwezig of aanwezig zijn.');
			return false;
 		}

	}
}