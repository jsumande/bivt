<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class MaterialsValidator extends Validator{


	public function validateFileSize($atts,$val) {
		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }

	}

	public function validateId($atts, $val) {

		global $wpdb;
		$checkId = $wpdb->get_var("SELECT COUNT(*) FROM materials WHERE id = $val");

		if( $checkId == 0 ) {
			$this->setErrorMessage( $atts, 'Please make sure that ID exists.');
			return false;
		}

	}


	public function validateFileUpload($atts,$val) {

		$check = $_FILES[$atts]['type'];
	    if($check == false) {
	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid file.');
			return false;
	    }

	}

	public function validateFile($atts,$val) {
	
	
		$check = getimagesize($_FILES[$atts]['tmp_name']);
		if($check == false) {
	
			$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
		}
	
	
	}
	
	public function validateStatus($atts,$val){
	
		$validStatus = ['open','close'];
	
	
		if(!in_array($val, $validStatus)){
	
			$this->setErrorMessage($atts,'Please make sure you enter a valid status.');
			return false;
		}
	
	}

}