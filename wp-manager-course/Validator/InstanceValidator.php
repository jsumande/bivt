<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class InstanceValidator extends Validator{


	public function validateInstance($atts,$val) {

		
		global $wpdb;

		$checkModule = $wpdb->get_row("SELECT * FROM instances WHERE id = {$val}");

		if(count($checkModule)==0){
			$this->setErrorMessage($atts,'Please make sure you enter a valid instance');
			return false;
		}

		
	}


	public function validateFull($atts,$val){

		if($val==0 || $val==1){
		}else{
			$this->setErrorMessage($atts,'Please make sure you enter a valid full.');
			return false;
		}


	}

	public function validateStatus($atts,$val){

		$validStatus = ['open','close'];


		if(!in_array($val, $validStatus)){

			$this->setErrorMessage($atts,'Please make sure you enter a valid status.');
			return false;
		}

	}



	public function validateModule($atts,$val) {

		
		global $wpdb;

		$checkModule = $wpdb->get_row("SELECT * FROM modules WHERE id = {$val}");

		if(count($checkModule)==0){

			$this->setErrorMessage($atts,'Please make sure you enter a valid module.');
			return false;
		}

		
	}
}
