<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class TeacherValidator extends Validator{

	public function matchPassword($atts,$val){


		global $wpdb;


		
		if(strcmp($_POST['dataPass']['password'], $val)!==0){
			$this->setErrorMessage($atts,'Password and Confirm pasword do not match.');
			return false;
		}


	}


	public function minPassword($atts,$val){


		global $wpdb;

		if($val!=''){
			if(strlen($val)<8){
				$this->setErrorMessage($atts,'Password should be at least 8 characters.');
				return false;

			}
		}
		
		

	}



	public function validateUserName($atts,$val){


		global $wpdb;

		if($val!=''){

			$id = get_current_user_id();


			if(isset($id)){
				$checkUser = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}users WHERE user_login = '$val' AND id <> $id");
			}else{
				$checkUser = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}users WHERE user_login = '$val'");
			}

			if($checkUser>0) {
				$this->setErrorMessage($atts,'That username is already taken. Please choose another username.');
				return false;

			}
		}

	}
	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }
	}

	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
	    }


	}
	
}