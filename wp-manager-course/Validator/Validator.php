<?php

namespace wp_manager_course\Validation;

class Validator {

	private $errorMessages = [];


	public function __construct() {

	}

	public function errors()
	{
		return $this->errorMessages;

	}

	public function setErrorMessage($atts,$message) {
		if (isset($this->errorMessages[$atts])) {
			$this->errorMessages[$atts] .= $message . '<br/>';
		} else {
			$this->errorMessages[$atts] = $message . '<br/>';
		}
	}


	public function required($atts,$val){

		$val = trim($val);

		if($val=='') {
			if($atts=='datum-add'){
				$this->setErrorMessage($atts,'Datum is een verplicht veld.');
			}else{
				$this->setErrorMessage($atts,ucwords(preg_replace('/_/',' ',$atts)) . ' is een verplicht veld.');
			}
			return false;
			
		}

	}

	public function make($request,$validations) {

		foreach($validations as $key=>$validation) {
			if(!isset($request[$key])) {
				if ($request) {
					$this->errorMessages[$key] .= $key . ' is een verplicht veld.<br/>';
				}
				//array_push($errorMessages, [$key=>$key. ' is required.']);
			}else{
				foreach($validation as $execute) {
					$this->$execute($key,$request[$key]);
				}
				
			}
		}
		//return $this;

	}

	public function fails() {
		if($this->errorMessages==[]) {
			return false;
		}else{
			return true;
		}
	}



}