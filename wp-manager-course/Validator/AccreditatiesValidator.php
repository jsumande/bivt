<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class AccreditatiesValidator extends Validator{

	public function validateModule($atts,$val){

		global $wpdb;

		$validModule = 0;

		$countModules = $wpdb->get_var("SELECT COUNT(*) FROM modules");

		//modules
		if($countModules>0){
			$getModules = $wpdb->get_results("SELECT * FROM modules",ARRAY_A);
			foreach($getModules as $getModule) {
				if($getModule['id']==$val)
					$validModule = 1;
			}
		}

		if($validModule==0) {
			$this->setErrorMessage($atts,'Please enter a valid module.');
			return false;
		}

	}



	public function validateId($atts,$val) {

		global $wpdb;


		$checkAccreditaty = $wpdb->get_var("SELECT COUNT(*) FROM accreditaties WHERE id = $val");


		if($checkAccreditaty==0) {
			$this->setErrorMessage($atts,'Please make sure that ID exists.');
			return false;


		}


	}
	public function validateOrganization($atts,$val){

		global $wpdb;

		$validOrganization = 0;

		$countOrganizations = $wpdb->get_var("SELECT COUNT(*) FROM professional_organizations");

		//modules
		if($countOrganizations>0){
			$getOrganizations = $wpdb->get_results("SELECT * FROM professional_organizations",ARRAY_A);
			foreach($getOrganizations as $getOrganization) {
				if($getOrganization['id']==$val)
					$validOrganization = 1;
			}
		}

		if($validOrganization==0) {
			$this->setErrorMessage($atts,'Please enter a valid professional organization.');
			return false;
		}

	}

	public function validateTill($atts,$val) {


		$pattern = '/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/';

		if(!preg_match($pattern, $val) || strlen($val)!=10) {
			$this->setErrorMessage($atts,'Please enter a valid valid untill.');
			return false;

		}

	}



	public function validateFrom($atts,$val) {


		$pattern = '/[0-9]{4}\-[0-9]{2}\-[0-9]{2}/';

		if(!preg_match($pattern, $val) || strlen($val)!=10) {
			$this->setErrorMessage($atts,'Please enter a valid valid from.');
			return false;

		}

	}


}