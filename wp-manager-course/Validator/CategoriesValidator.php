<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class CategoriesValidator extends Validator{

 	public function validateStatus($atts,$val) {


 		$validStatus = ['close','open'];
 		if(!in_array($val,$validStatus)) {

 			$this->setErrorMessage($atts,'Please make sure that the status is valid.');
			return false;
 		}

	}


	public function validateCategoryName($atts,$val) {



		global $wpdb;


		if($val!=''){
			$checkCategory = $wpdb->get_var("SELECT COUNT(*) FROM categories WHERE name = '$val'");


			if($checkCategory>0) {
				$this->setErrorMessage($atts,'That category name is already existed. Please choose another category name.');
				return false;


			}
		}
		



	}
	public function validateId($atts,$val) {

		global $wpdb;


		$checkCategory = $wpdb->get_var("SELECT COUNT(*) FROM categories WHERE id = $val");


		if($checkCategory==0) {
			$this->setErrorMessage($atts,'Please make sure that ID exists.');
			return false;

		}


	}
 
	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry the filesize is too high.');
			return false;
        }

	}




	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Please make sure that you upload a valid image file.');
			return false;
	    }


	}
}