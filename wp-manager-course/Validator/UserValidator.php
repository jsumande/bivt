<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class UserValidator extends Validator{


	public function validateEmail($atts,$val) {

		global $wpdb;

		$checkEmail = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}users WHERE user_email ='$val'");

		if($val=='') {
			// Error already reported by 'required'
		} else {
			if($checkEmail>0){
				$this->setErrorMessage($atts,'Dit email adres is al geregistreerd. Kies voor inloggen.');
				return false;
			}else{
				if (filter_var($val, FILTER_VALIDATE_EMAIL) === false) {

				  	$this->setErrorMessage($atts,'Dit email adres is ongeldig.');
					return false;
				}
			}
		} 
	}


	public function validateGroups ($atts, $val) {
	
		global $wpdb;

		// All valid group IDs from DB
		$all_groups = $wpdb->get_results("SELECT id FROM groups");
		
		// Groups selected by user
		$groups = explode(',', $val);
		
		
		$valid = sizeof($groups) > 0;
		foreach ($groups AS $value) {
			// Check whether each supplied group is a database group
			$found = false;
			foreach ($all_groups AS $k=>$v) {
				$found = $found || $value==$v->id;
			}
			$valid = $valid && $found;
		}
		
		if (!$valid) {
			$this->setErrorMessage($atts, "Er is minstens een ongeldige groep geselecteerd.");
			return false;
		}
		
	}

	public function validateUrl($atts,$val) {

		$array = get_headers($val);
		$string = $array[0];
		if(!strpos($string,"200") && $val != "")
		{
			$this->setErrorMessage($atts, "Je hebt ongeldige URL opgegeven");
			return false;
		}
	
		
	}
	public function validateMinPassword($atts,$val) {

		if( strlen($_POST['password']) < 8)
		{
			$this->setErrorMessage($atts, "Wachtwoord moet minstens 8 tekens bevatten");
			return false;
		}
	
		
	}
	public function validatePassword($atts,$val) {

		if($_POST['password'] != $_POST['confirm_password'])
		{
			$this->setErrorMessage($atts, "Wachtwoord niet overeen");
			return false;
		}
	
		
	}

	public function validateFileSize($atts,$val) {

		                                                                       
		if ($_FILES[$atts]['size'] > 800000) {
			$this->setErrorMessage($atts,'Sorry het bestand is te hoog.');
			return false;
        }

	}


	public function validateFile($atts,$val) {


		$check = getimagesize($_FILES[$atts]['tmp_name']);
	    if($check == false) {

	    	$this->setErrorMessage($atts,'Zorg ervoor dat u een geldig image-bestand te uploaden.');
			return false;
	    }
	}



}