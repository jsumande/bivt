<?php


namespace wp_manager_course;

use wp_manager_course\Validation\Validator;

class RelatedDaysValidator extends Validator{

	public function validateDate($atts,$val) {


		if (false === strtotime($val)) {
			$this->setErrorMessage($atts,'Voer een geldige datum in');
			return false;
		}


	}


	public function validateDay($atts,$val) {

		global $wpdb;

		$checkDay= $wpdb->get_row("SELECT * FROM days WHERE id = {$val}");

		if(count($checkDay)==0){
			$this->setErrorMessage($atts,'Please make sure that you enter a valid day.');
			return false;
		}


	}




	public function validateInstance($atts,$val) {

		global $wpdb;

		$checkInstance= $wpdb->get_row("SELECT * FROM instances WHERE id = {$val}");

		if(count($checkInstance)==0){
			$this->setErrorMessage($atts,'Please make sure that you enter a valid instance.');
			return false;
		}


	}



	public function validateModule($atts,$val) {

		global $wpdb;

		$checkModule = $wpdb->get_row("SELECT * FROM modules WHERE id = {$val}");

		if(count($checkModule)==0){
			$this->setErrorMessage($atts,'Please make sure that you enter a valid module.');
			return false;
		}


	}


	public function validateDocent($atts,$val) {

		global $wpdb;

		$checkModule = $wpdb->get_row("SELECT * FROM docents WHERE id = {$val}");

		if(count($checkModule)==0){
			$this->setErrorMessage($atts,'Please make sure that you enter a valid docent.');
			return false;
		}


	}


	public function validateLocation($atts,$val) {

		global $wpdb;

		$checkLocation = $wpdb->get_row("SELECT * FROM locations WHERE id = {$val}");

		if(count($checkLocation)==0){
			$this->setErrorMessage($atts,'Please make sure that you enter a valid location.');
			return false;
		}


	}



}