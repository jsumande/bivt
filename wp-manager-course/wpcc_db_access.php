<?php

function wpcc_manager_course_install() {
	global $wpdb;

	$wpUser = $wpdb->get_row("SELECT * FROM ". $wpdb->prefix. "users");
	//Add column if not present.
	if(!isset($wpUser->user_code)){
	    $wpdb->query("ALTER TABLE ". $wpdb->prefix. "users ADD 	user_code varchar(255) NOT NULL");
	}
	if(!isset($wpUser->modified)){
	    $wpdb->query("ALTER TABLE ". $wpdb->prefix. "users ADD 	modified datetime NOT NULL");
	}

	$rows = $wpdb->get_results("SELECT * FROM users");
	foreach( $rows as $row ) {
		$username = $row->user_name;
		$password = '12345';
		$email = $row->user_email;
		$user_id = wp_create_user( $username, $password, $email );

		$user_code = $row->user_code;
		$created = $row->created;
		$modified = $row->modified;
		$user_status = $row->user_status;
		$users_id = $row->id;
		
		$wpdb->update( 
			$wpdb->prefix. "users", 
			array( 
				'user_code' => $user_code,
				'user_registered' => $created,
				'modified' => $modified,
				'user_status' => $user_status,
				'ID' =>$users_id, 
			
			), 
			array( 'ID' => $user_id )
		);
		$u = new WP_User( $users_id );
		$u->set_role( 'bivt' );

	}


}

register_activation_hook(__FILE__,'wpcc_manager_course_install');

function wpcc_manager_course_uninstall() {
	global $wpdb;
}
?>