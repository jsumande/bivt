<?php
function wpcc_manager_course_generate_page() {

	$page = get_page_by_name('Admin');
	if ( empty($page) ) {
		wp_insert_post(array(
		'post_title'     => 'Admin',
		'post_name'      => 'admin',
		'post_content'   => '[bivt logged="admin"]',
		'post_status'    => 'publish',
		'post_author'    => 1, // or "1" (super-admin?)
		'post_type'      => 'page',
		'menu_order'     => 1,
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		));
	}
	$page = get_page_by_name('Teacher');
	if ( empty($page) ) {
		wp_insert_post(array(
		'post_title'     => 'Teacher',
		'post_name'      => 'teacher',
		'post_content'   => '[bivt logged="teacher"]',
		'post_status'    => 'publish',
		'post_author'    => 1, // or "1" (super-admin?)
		'post_type'      => 'page',
		'menu_order'     => 1,
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		));
	}
	$page = get_page_by_name('Student');
	if ( empty($page) ) {
		wp_insert_post(array(
		'post_title'     => 'Student',
		'post_name'      => 'student',
		'post_content'   => '[bivt logged="student"]',
		'post_status'    => 'publish',
		'post_author'    => 1, // or "1" (super-admin?)
		'post_type'      => 'page',
		'menu_order'     => 1,
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		));
	}
	$page = get_page_by_name('News');
	if ( empty($page) ) {
		wp_insert_post(array(
		'post_title'     => 'news',
		'post_name'      => 'news',
		'post_content'   => '[bivt page="news"]',
		'post_status'    => 'publish',
		'post_author'    => 1, // or "1" (super-admin?)
		'post_type'      => 'page',
		'menu_order'     => 1,
		'comment_status' => 'closed',
		'ping_status'    => 'closed',
		));
	}

}

function get_page_by_name($pagename)
{
	$pages = get_pages();
	foreach ($pages as $page) if ($page->post_name == $pagename) return $page;
	return false;
}