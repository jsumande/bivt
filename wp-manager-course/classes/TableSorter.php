<?php

namespace wp_manager_course;

class TableSorter {
	
	private $_query;
	private $_limit;
	private $_unsort;
	private $_pagenum;
	private $_max_count;
	
	private $_data;
	
	const SORT_2 = false;
	const SORT_3 = true;
	
	public function __construct($query, $unsort=true, $max = -1, $limit=-1) {
		$this->_query = $query;
		$this->_limit = $limit;
		$this->_unsort = $unsort;
		
		$this->_pagenum = isset($_GET['pagenum']) ? $_GET['pagenum'] : 1;
		
		$this->_data = $this->retrieveData ();
		$this->_max_count = $max;

		if (($this->_limit > -1) && ($this->_limit < $this->_max_count)) {
			 $this->_data = array_slice($this->_data, 0, $this->_limit, true); 
		}		
	}
	
	public function getSortLink ($item,$title) {
	
		$page_query_string = $_SERVER['QUERY_STRING'];
		
		$pos = strpos($page_query_string,'&sort=');
		if ($pos !== false) {
			$page_query_string = substr($page_query_string, 0, $pos);
		}
		
		$page_query_string .= $this->getSortString($item, $this->getNextSortState());
	
		return '<a style="color:white !important;" href="' . get_permalink() .'?' . $page_query_string.'">'.$title.'</a>';	
	}

	public function getData () {
		return $this->_data;
	}
	
	public function getNumPage () {
		return ($this->limit > -1) ? ceil($this->_max_count / $this->_limit) : 1;
	}
	
	public function getCurrentPage () {
		return $this->_pagenum;
	}
	
	public function getPaginationLinks () {
		return paginate_links( array(
				'base' => add_query_arg( 'pagenum', '%#%' ),
				'format' => '',
				'current' => $this->_pagenum,
				'total' => ceil ($this->_max_count / $this->_limit),
				'type'  => 'array',
				'prev_next'   => TRUE,
				'prev_text'    => __('Vorige'),
				'next_text'    => __('Volgende'),
		));
	}
	
	public function getPaginationPattern () {		
		return '/current/';
	}
	
	protected function getSortString ($item, $state) {
		return ($state != SORT_NATURAL) ? '&sort=' . $item . '&order=' . ($state == SORT_ASC ? 'ASC' : 'DESC') : '';
	}
	
	protected function getSortStatement ($state) {
		return ($state != SORT_NATURAL) ? ' ORDER BY ' . $this->getSortItem() . ' ' . ($state == SORT_ASC ? 'ASC' : 'DESC') : '';
	}
	
	protected function getLimitStatement () {
		return ($this->_limit > 0) ? ' LIMIT ' . (($this->_pagenum -1) * $this->_limit) .',' . $this->_limit : '';	
	}
	
	protected function getSortState () {
		return (isset($_GET['order']) ? (($_GET['order'] == 'ASC') ? SORT_ASC : SORT_DESC) : SORT_NATURAL);
	}
	
	protected function getNextSortState () {
		switch ($this->getSortState()) {
			case SORT_ASC:
				return SORT_DESC;
			case SORT_DESC:
				return $this->_unsort ? SORT_NATURAL : SORT_ASC;
			default:
				return SORT_ASC;
		}
	}
	
	protected function getSortItem () {
		return isset($_GET['sort']) ? $_GET['sort'] : 'id';
	}
	
	protected function retrieveData () {
		global $wpdb;
		
		$sort_order_string = $this->getSortStatement($this->getSortState());
		
		$limit_string = $this->getLimitStatement();

		return $wpdb->get_results($this->_query . $sort_order_string . $limit_string);
		
	}
}
