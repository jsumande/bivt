<?php

namespace wp_manager_course;

use \WP_User;
use \WP_Error;

class Student extends WP_User {
	
	public $attendance_days;
	
	public function __construct ($id) {
		parent::__construct($id);
		
		$this->attendance_days = null;
	}
	
	public function getAttendanceDays () {
		if (null == $this->attendance_days) {
				$this->retrieveAttendanceDays ();
		}
		
		return $this->attendance_days;
	}
	
	protected function retrieveAttendanceDays () {
		global $wpdb;
		
		$query = "SELECT id, day_id, remarks, attended FROM `attendance_lists` WHERE student_id ='" . $this->ID . "'";
		$attendance = $wpdb->get_results($query);
		
		foreach ($attendance AS $k=>$v) {
			$this->attendance_days[$v->day_id] = (object) [
					'id' => $v->id,
					'attended' => (isset($v->attended) ? $v->attended > 0 : false), 
					'remarks' => (isset($v->remarks) ? $v->remarks : ''),	
			];
		}
		
		return (sizeof($results) == 0) ?  new WP_Error( 'no-attendance-data', __( "Could not retrieve attendance data", "wp_manager_course" ), $query ) : $results;
	}
}

?>
