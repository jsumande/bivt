<?php 

/*
Plugin name: WP Manager Course
Plugin URI:  //
Description: A plugin for BivT Manager Course.
Author: Tinotech Team
Author URI: //
Version: 1.0
*/


/*************
Global
**************/
include("wpcc_db_access.php");
include("wpcc-manager-admin.php");
include("wpcc-manager-teacher.php");
include("wpcc-manager-student.php");

include_once ( __DIR__ . '/classes/Student.php');

use wp_manager_course\Student;


$currentPageCustom = '';


function bivt_activate() {
	wpcc_manager_course_install();
}
register_activation_hook(   __FILE__, 'bivt_activate' );

function bivt_deactivate() {
	wpcc_manager_course_uninstall();
}
register_uninstall_hook(    __FILE__, 'bivt_deactivate' );


/*************
	Global
**************/


/*
** Admin Custom CSS
*/

function wpcc_css_scripts() {

    wp_register_style( 'prefix-style', plugins_url('/assets/css/styles.css', __FILE__) );
    wp_enqueue_style( 'prefix-style' ); 
    
	wp_enqueue_style( 'bootstrap-css', plugins_url( '/assets/css/bootstrap.min.css', __FILE__) );
	wp_enqueue_style( 'font-awesome-css', plugins_url( '/assets/css/font-awesome.min.css', __FILE__) );

    wp_register_style( 'dataTables-bootstrap-css', plugins_url('/assets/css/dataTables.bootstrap.min.css', __FILE__) );
    wp_enqueue_style( 'dataTables-bootstrap-css' ); 
    wp_register_style( 'buttons-bootstrap-css', plugins_url('/assets/css/buttons.bootstrap.min.css', __FILE__) );
    wp_enqueue_style( 'buttons-bootstrap-css' );     
    wp_register_style( 'multiple-select-css', plugins_url('/assets/css/select2.min.css', __FILE__) );
    wp_enqueue_style( 'multiple-select-css' ); 

	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/css/font-awesome.min.css' );

	wp_register_style('bootstrap-fonts-css',plugins_url('/assets/css/font-awesome.min.css',__FILE__));
    //wp_register_style('bootstrap-fonts-css', '//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');

    //wp_register_style( 'jquery-ui-css','//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css');
    wp_register_style( 'jquery-ui-css',plugins_url('/assets/css/jquery-ui.css',__FILE__));


    wp_enqueue_style( 'jquery-ui-css'); 

    wp_register_style( 'masonry-docs-css', plugins_url('/assets/css/masonry-docs.css', __FILE__) );
    wp_enqueue_style( 'masonry-docs-css' ); 

	wp_enqueue_style('fancy-box-css', plugins_url('/assets/js/fancybox/jquery.fancybox.css?v=2.1.5', __FILE__));
	wp_enqueue_style('fancy-box-thumb-css', plugins_url('/assets/css/jquery.fancybox-thumbs.css?v=1.0.7', __FILE__));
	wp_enqueue_style( 'jquery-confirm-css', plugins_url('/vendors/jquery-confirm-v3.0.1/css/jquery-confirm.css',__FILE__) );
}

function wpcc_js_scripts() {


	//wp_enqueue_script( 'jquery-ui-js',plugins_url('/assets/js/jquery-ui.js', __FILE__),array('jquery'));
	//wp_enqueue_script('jquery-custom','//code.jquery.com/jquery-1.9.1.js');
	
    //wp_enqueue_script( 'prefix-js', plugins_url('/assets/js/main.js', __FILE__),array('jquery'),false,true);
   
    wp_enqueue_script( 'bootstrap-js', plugins_url('/assets/js/bootstrap.min.js', __FILE__) );
    wp_enqueue_script( 'jquery-dataTables-js', plugins_url('/assets/js/jquery.dataTables.min.js', __FILE__),array('jquery') );
    wp_enqueue_script( 'datatables-bootstrap-js', plugins_url('/assets/js/dataTables.bootstrap.min.js', __FILE__) );
    wp_enqueue_script( 'dataTables-buttons-js', plugins_url('/assets/js/dataTables.buttons.min.js', __FILE__) );
    wp_enqueue_script( 'buttons-bootstrap-js', plugins_url('/assets/js/buttons.bootstrap.min.js', __FILE__) );

    wp_enqueue_script( 'tinymce-js','//cdn.tinymce.com/4/tinymce.min.js',array('jquery'));
    
    wp_register_script( 'custom-new-js',plugins_url('/assets/js/custom-new.js',__FILE__));
    wp_register_script( 'prefix-admin-js',plugins_url('/assets/js/wpcc-admin.js', __FILE__));

    wp_localize_script('custom-new-js','ajax_object_here', array( 'ajax_url_here' => admin_url( 'admin-ajax.php' )));
    
    wp_localize_script( 'prefix-admin-js', 'ajax_object_here', array( 'ajax_url_here' => admin_url( 'admin-ajax.php' )));
   	// wp_enqueue_script( 'prefix-admin-js', plugins_url('/assets/js/wpcc-admin.js', __FILE__),array('jquery','jquery-ui-js'),false,true);
   
    wp_localize_script( 'prefix-teacher-js', 'ajax_object_here', array( 'ajax_url_here' => admin_url( 'admin-ajax.php' ),'loading_image'=>plugins_url('wp-manager-course/assets/images/loading.gif')));

    wp_localize_script( 'prefix-student-js', 'ajax_object_here', array( 'ajax_url_here' => admin_url( 'admin-ajax.php' )));

    wp_enqueue_script( 'masonry-js-jquery',plugins_url('/assets/js/jquery.min-karaan.js', __FILE__));
    //wp_enqueue_script( 'masonry-js-jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js');

	wp_enqueue_script( 'masonry-js', '//unpkg.com/masonry-layout@4.1/dist/masonry.pkgd.js',array('jquery'));

	wp_enqueue_script('fancy-box-js', plugins_url('/assets/js/fancybox/jquery.fancybox.pack.js?v=2.1.5', __FILE__));
	wp_enqueue_script('fancy-box-thumbs-js', plugins_url('/assets/js/fancybox/jquery.fancybox.pack.js?v=2.1.5', __FILE__));

	wp_enqueue_script( 'multiple-select-js', plugins_url('/assets/js/select2.min.js', __FILE__), array('jquery') );
	wp_enqueue_script( 'easing-js', plugins_url('/assets/js/easing.js', __FILE__));
	wp_enqueue_script( 'velocity-min-js', plugins_url('/assets/js/velocity.min.js', __FILE__));
	
	wp_localize_script( 'custom-new-js', 'page_properties', array('home_url'=>get_home_url()));
	wp_enqueue_script('custom-new-js',plugins_url('/assets/js/custom-new.js',__FILE__),array('jquery'),false,true);

	wp_enqueue_script( 'jquery-ui-js',plugins_url('/assets/js/jquery-ui-needed.js',__FILE__),array('jquery'));
	//wp_enqueue_script( 'jquery-ui-js','//code.jquery.com/ui/1.12.1/jquery-ui.js',array('jquery'));
	wp_enqueue_script( 'prefix-admin-js', plugins_url('/assets/js/wpcc-admin.js', __FILE__),array('jquery','jquery-ui-js'),false,true);
    
	wp_enqueue_script('jquery-confirm-js',plugins_url('/vendors/jquery-confirm-v3.0.1/js/jquery-confirm.js',__FILE__),array('jquery'),false,true);
	wp_enqueue_script( 'prefix-teacher-js', plugins_url('/assets/js/wpcc-teacher.js', __FILE__),array('jquery','jquery-ui-js'),false,true);
	wp_enqueue_script( 'prefix-student-js', plugins_url('/assets/js/wpcc-student.js', __FILE__),array('jquery','jquery-ui-js'),false,true);
	wp_enqueue_script( 'prefix-js', plugins_url('/assets/js/main.js', __FILE__),array('jquery','jquery-ui-js'),false,true);


}


/**** Register Settings ****/

function wp_manager_course_settings()
{
	register_setting('wp-manager-course-group', 'wp-manager-course-setting');
}
add_action('admin_init', 'wp_manager_course_settings');

/**** SHORTCODE ****/

function wp_bivt_shortcode( $atts ) {
	global $current_user;
	$user_roles = $current_user->roles;
	$url =  home_url();
	$path = dirname(__FILE__);
	$currentpage = $_SERVER['REQUEST_URI'];
	if ( !isset($user_roles[0]) && isset($atts['page']) && $atts['page'] != 'news'  && $atts['page'] != 'registration' ) {
	     echo'<script> window.location="'.$url.'"; </script> ';
	}
	if ( isset($atts['logged']) ) {
	    $attr = $atts['logged'];
	    switch ($attr) {
	    	case 'admin':

	    		if ( $user_roles[0] == 'bivt' || $user_roles[0] == 'administrator') {
	    			return bivt_admin_template();
	    		} 
	    		break;

	    	case 'student':
	    		if ( $user_roles[0] == 'bivt' || $user_roles[0] == 'administrator' ) {
	    			return bivt_student_template();
	    		}
	    		break;

	    	case 'teacher':
	    		if ( $user_roles[0] == 'bivt' || $user_roles[0] == 'administrator') {
	    			return bivt_teacher_template();
	    		}

	    		break;	    	

	    	default:
	    		echo "Invalid Shortcode!";
	    		break;
	    }	
	}
	if ( isset($atts['page']) && $atts['page'] == 'news' ) { 
		include $path . '/custom-template/pages/index.php';
	} else if ( isset($atts['page']) && $atts['page'] == 'registration' ) { 
		include $path . '/custom-template/pages/registration.php';
	} 
	else if ( isset($atts['page']) && $atts['page'] == 'courses' ) {
		include $path . '/custom-template/pages/courses.php';
	}
}
add_shortcode('bivt', 'wp_bivt_shortcode');

add_filter( 'wp_nav_menu_items', 'wp_bivt_dashboard_menu', 10, 2 );

function wp_bivt_dashboard_menu ( $items, $args ) {
	if ($args->theme_location == 'primary') {
		$items .= wp_bivt_nav_shortcode(null);
	}
	return $items;
}


function wp_bivt_nav_shortcode( $atts ) {
	global $current_user, $wpdb;
	$user_id = get_current_user_id();
	$rows = $wpdb->get_results("SELECT * FROM users_groups WHERE user_id = $user_id");
	$user_roles = $current_user->roles;
	$nav = '';

	if ( $user_id > 1   ) {
		//Show menu based on current user login role
		foreach ($rows as $row) {
			switch ( $row->group_id ) {
				case 1:
					//admin
					$nav .= '<li><a href="'.get_site_url().'/admin/?action=student_overview">Admin</a></li>';
					break;
				case 3:
					//Student
				$nav .= '<li><a href="'.get_site_url().'/student/?action=profile&user_id='.$user_id.'&edit=true">Student</a></li>';
					break;
				case 5:
					//teacher
				$nav .= '<li><a href="'.get_site_url().'/teacher/?action=profile&user_id='.$user_id.'&edit=true">Teacher</a></li>';
					break;			
				default:
					# code...
					break;
			}
		}


	}

	if ( $user_id == 1   ) {

		$nav .= '<li><a href="'.get_site_url().'/admin/?action=student_overview">Admin</a></li>';
		$nav .= '<li><a href="'.get_site_url().'/student/?action=profile&user_id='.$user_id.'&edit=true">Student</a></li>';
		$nav .= '<li><a href="'.get_site_url().'/teacher/?action=profile&user_id='.$user_id.'&edit=true">Teacher</a></li>';

	}

	if ( $user_id == 0){
		$nav .= '<li><a href="'.get_site_url().'/login"><span class="menu-item-text"><span class="menu-text">Login</span></span></a></li>';
	} else {
		$nav .= '
				<li class="">
					<a href="'.wp_logout_url(home_url()).'">Logout</span></a>
				</li>
				';
	}

	$menu_lists	='';
	
	if ( $user_id !== 0   ) {

		$menu_lists	.= '<li class="dropdown">
							    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="menu-item-text"><span class="menu-text">Dashboard</span></span> <b class="caret"></b></a>
							  <ul class="dropdown-menu">
							    '.$nav.'
							  </ul>
							</li>';
		} else {
		$menu_lists = '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-login">' .
				'<a href="'.get_site_url().'/login">' .
				'<span class="menu-item-text"><span class="menu-text">Login</span></span></a></li>';
	}

	return $menu_lists;
}



function init() {

 // ADD USER ROLE
    add_role('bivt', 'Bivt', array(
        'read' => false, // True allows that capability
        'edit_posts' => false,
        'delete_posts' => false, // Use false to explicitly deny
    ));

    add_action('wp_enqueue_scripts','wpcc_css_scripts',1);

   // add_action('wp_head', 'wpcc_css_scripts');
    add_action('wp_footer', 'wpcc_js_scripts');

	// LIMIT ACCESS ON BACKEND
	global $current_user; 
	$user_roles = $current_user->roles;

	if (isset($user_roles[0]) && $user_roles[0] == 'bivt' ) {
		global $current_user, $wpdb;
		$user_id = get_current_user_id();
		$check = $wpdb->get_row("SELECT * FROM users_groups WHERE user_id = $user_id AND group_id = 1");

		if (count($check) == 0){
		    show_admin_bar(false);
		    $currentpage = $_SERVER['REQUEST_URI'];

		    if ( !isset($_GET['action']) ) {
			    if ( strpos($currentpage, '/wp-admin/profile.php') !== false || strpos($currentpage, '/wp-admin/') !== false   || strpos($currentpage, '/wp-admin/index.php') !== false && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX )) {
				    if ( is_user_logged_in() )
				     	wp_redirect( home_url() ); 
				    else
				     	wp_redirect( wp_login_url() );			    
				    exit;
				}
		  	}
		 }
	}


	//search general function
	if(isset($_GET['search-general'])){

	?>
		<script type="text/javascript">
			var searchGeneral = "<?= trim($_GET['search-general']); ?>";
		</script>
	<?php
	}

}
add_action('init', 'init');

function bivt_default_page () {
	return '/';
}

add_filter('login_redirect', 'bivt_redirect_login');

function bivt_redirect_login () {
	$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
	list($addr, $redirect) = explode("redirect_to=", $referer);

	return isset($redirect) ? $redirect : '/';
}

function searchRows($rows,$checkValue){


	$getResults = [];

	if(is_array($rows)){

		//convert first object array into pure array
		//$keyFinds = ['name','slug','introduction','description','goal','target_audience','literature','programme','meta_description'];

		foreach ($rows as $key => $value) {
			
			if((isset($value->name) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->name))) || (isset($value->slug) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->slug))) || (isset($value->introduction) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->introduction))) || (isset($value->description) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->description))) || (isset($value->goal) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->goal))) || (isset($value->target_audience) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->target_audience))) || (isset($value->literature) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->literature))) || (isset($value->programme) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->programme))) || (isset($value->meta_description) && preg_match('/'.strtolower($checkValue).'/', strtolower($value->meta_description)))){
				array_push($getResults, $value);
			}

		}
		
	}

	return $getResults;

}



function set_html_content_type() {
	return 'text/html';
}

function emailSettings($phpmailer){
    $phpmailer->isSMTP(); 
	$phpmailer->Host = 'mail.nl.tinotech.eu';
    $phpmailer->SMTPAuth = true;
    $phpmailer->Port = 587;
    $phpmailer->Username = 'no-reply@nl.tinotech.eu';
    $phpmailer->Password = '7cgPxyQA';
    $phpmailer->SMTPSecure = 'tls';
    $phpmailer->FromName = "Bivt";
}

add_action( 'phpmailer_init','emailSettings');

function sendInvoiceEmail(){

	$response = ['status'=>'not ok','product_id'=>0];

	$count = count(WC()->cart->get_cart());

	if($count==1){

		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			$response['product_id'] = $product_id;
		}
	}


	if($response['product_id']!=0){
		
		global $wpdb;
		$getInvoice = $wpdb->get_row("SELECT modules.name AS module_name,modules.introduction AS module_introduction,modules.price AS module_price,modules.level AS module_level,instances.id AS instance_id FROM modules LEFT JOIN instances ON modules.id = instances.module_id WHERE modules.post_id = ".$response['product_id']);
		$getFirstDay = $wpdb->get_row("SELECT datum,locations.name AS location_name FROM days LEFT JOIN locations ON days.location_id = locations.id WHERE days.instance_id = ".$getInvoice->instance_id." ORDER BY days.datum ASC");
			
		if(count($getFirstDay)>0){
			$getInvoice->datum = $getFirstDay->datum;
			$getInvoice->location_name = $getFirstDay->location_name;
		}
		

		if(count($getInvoice)>0){

			$subject = 'Jouw inschrijving bij BivT van' . $getInvoice->datum;
			$body = "<p>Beste " . "<NAAM>" . "</p>";
			$body .= "<p></p>";
			$body .= "<p>Hartelijk dank voor je inschrijving bij BivT, hieronder vindt je details van deze inschrijving:</p>";
			$body .= "<p></p>";
			$body .= "<p>Inschrijvingsnummer: " . "<Ordernummer>" . "</p>";	
			$body .= "<p>Onderdeel: ".$getInvoice->module_name."</p>";
			$body .= "<p>Begindatum: ".$getInvoice->datum."</p>";
			$body .= "<p>Locatie: ".$getInvoice->location_name."</p>";
			$body .= "<p>Prijs: ".$getInvoice->module_price."</p>";
			$body .= "<p>Betaalmethod: ". "<Betaalmethode>"."</p>";
			$body .= "<p></p>";
			$body .= "<p>Meer informatie over alle lesdagen en locaties vind je op de website als je inlogt. Je vindt dit bij de optie: rooster. Noteer de dagen en locaties in je agenda!</p>";
			$body .= "<p></p>";
			$body .= "<p>Binnen enkele werkdagen zal je de definitieve factuur ontvangen via de mail. Mocht dit niet het geval zijn, neem dan contact met ons op.</p>";
			$body .= "<p></p>";
			$body .= "<p>Uiterlijk 1 week voor aanvang van de lesdagen ontvang je een bericht ter herinnering aan deelname.</p>";
			$body .= "<p></p>";
			$body .= "<p>Betalinggegevens: " . (isset($_GET['payment']) ? $_GET['payment'] : 'Onbekend') . "</p>";
			$body .= "<p></p>";
			$body .= "<p>Klant details</p>";
			$body .= "<p></p>";
			$body .= "<p>Voornaam: "."<Voornaam>"."</p>";
			$body .= "<p>Achternaam: "."<Achternaam>"."</p>";
			$body .= "<p>Email: "."<Email>"."</p>";
			$body .= "<p>Telefoon: " . "<Telefoon>" . "</p>";
			$body .= "<p>Beroepsvereninging: " . "<Beroepsvereniging>" ."</p>";
			$body .= "<p>Lidnummer: " . "<Lidnummer>" . "</p>";
			$body .= "<p></p>";
			$body .= "<p>Factuuradres:</p>";
			$body .= "<p>Bedrijf: " . "<Bedrijf>" . "</p>";
			$body .= "<p></p>Contactpersoon: " . "<Contactpersoon>" . "</p>";
			$body .= "<p>Adres: " . "<Adres>" . "</p>";
			$body .= "<p>Postcode: " . "<Postcode>" . "</p>";
			$body .= "<p>Plaats: " . "<Plaats>" . "</p>";
			$body .= "<p>Land: " . "<Land>" . "</p>";
			$body .= "<p></p>";
			$body .= "<p>Met vriendelijke groet,</p>";
			$body .= "<p></p>";
			$body .= "<p>Secretariaat BivT</p>";
			$body .= "<p></p>";
			$body .= '<p>E: <a href="mailto:info@bivt.nl">info@bivt.nl</a></p>';
			$body .= '<p>W: <a href="http://www.bivt.nl">www.bivt.nl</a></p>';
			$body .= "<p>T: 0251-222210</p>";
								
			/* Email Functionality */

			add_filter( 'wp_mail_content_type','set_html_content_type');
			$headers = 'From: Bivt - Invoice Number <no-reply@nl.tinotech.eu>' . "\r\n";
			$to = (isset($_GET['email'])) ? $_GET['email'] : '';

			$mailSetting = array('to'=>$to,
								'subject'=>$subject,
								'message'=>$body,
								'headers'=>$headers);
			if($to!=''){
				if(wp_mail($to, $mailSetting['subject'],$mailSetting['message'],$mailSetting['headers'])){
					$response['status'] = 'ok';	
				}
				
				
			}

			remove_filter( 'wp_mail_content_type', 'set_html_content_type');
			/* Email Functionality */

		}


	}
	echo json_encode($response);
	wp_die();
}

add_action( 'wp_ajax_bivt_send_invoice_email', 'sendInvoiceEmail' );
add_action( 'wp_ajax_nopriv_bivt_send_invoice_email', 'sendInvoiceEmail' );



function sendSampleEmail(){

	$response = array('status'=>'not ok');

	add_filter( 'wp_mail_content_type','set_html_content_type');
	$headers = 'From: Bivt - Invoice Number <no-reply@nl.tinotech.eu>' . "\r\n";
	$to = 'kenjos75@yahoo.com';
	$subject = 'Bivt- Invoice Number';
	$body = 'sample message';

	$mailSetting = array('to'=>$to,
						'subject'=>$subject,
						'message'=>$body,
						'headers'=>$headers);
	if($to!=''){
		if(wp_mail($to, $mailSetting['subject'],$mailSetting['message'],$mailSetting['headers'])){
			$response['status'] = 'ok';	
		}
		
		
	}

	remove_filter( 'wp_mail_content_type', 'set_html_content_type');

	echo json_encode($response);
	
	wp_die();


}
add_action( 'wp_ajax_send_sample_email', 'sendSampleEmail' );
add_action( 'wp_ajax_nopriv_send_sample_email', 'sendSampleEmail' );


function customOrderButtonScript() { ?>
    <script type="text/javascript">

    	jQuery(document).ready(function(){

    		jQuery('body').on('click','#place_order',function(e){

				var getPayment = (jQuery('select[name="mollie-payments-for-woocommerce_issuer_mollie_wc_gateway_ideal"]').val()=='') ? 'ideal' : null;
				getPayment = (getPayment || (jQuery('select[name="mollie-payments-for-woocommerce_issuer_mollie_wc_gateway_banktransfer"]').val()=='')) ? 'banktransfer' : null;				
				if(null != getPayment){
					e.preventDefault();
					var thisHere = this;
					var state = jQuery(this).attr('state');

					if(state==0){

						jQuery('#place_order_inner_parent').append('<div id="place_order_protective"></div>');
						jQuery(this).attr('disabled','disabled');
						jQuery(this).attr('state',1);
						var getEmail = jQuery.trim(jQuery('#billing_email').val());

						(function repeat(){

							jQuery.get(ajax_object_here.ajax_url_here,{action: 'bivt_send_invoice_email',email: getEmail, payment: getPayment},function(response){

								if(response==''){
									repeat();
								}else{
									jQuery(thisHere).attr('state',0);
									jQuery('#place_order_protective').remove();
									if(response.status=='ok'){
										window.location.href = page_properties.home_url;
										//jQuery('form.woocommerce-checkout').submit();
										//jQuery(thisHere).submit();
									}else{
										console.log(response);
										jQuery.alert('An error occured while processing your request.');
									}
								}

							},'json');
						})();
						
					}
				}
				
				


				
			});

    		
    	});
    </script>
<?php       
}
add_action( 'woocommerce_checkout_after_order_review', 'customOrderButtonScript' );



function wp_bivt_nav_news_shortcode() {

	global $wpdb;
	$news = '';
	
	if(isset($_GET['search-general'])){

		//$getResults = $wpdb->get_row()

		$searchQuery = trim($_GET['search-general']);

		//$category = wp_bivt_category_query($tab);
		//$rows = wp_bivt_modules_query('totaal');


		//search if there is match entry
		$coursesConditions ="AND (";
		$coursesKeys = array('c.name','c.slug','c.introduction','c.SBU','c.meeting_days');
		$limit = count($coursesKeys) - 1;
		for($x = 0;$x<count($coursesKeys);$x++) {
			if($x==$limit){
				$coursesConditions .=$coursesKeys[$x]." LIKE '%".$searchQuery."%'";
			}else{
				$coursesConditions .=$coursesKeys[$x]." LIKE '%".$searchQuery."%' OR ";
			}
			
		}
		$coursesConditions .=')';
		
		$modulesConditions ="AND (";
		$modulesKeys = array('m.name','m.slug','m.introduction','m.SBU','m.meeting_days');
		$limit = count($modulesKeys) - 1;
		for($x = 0;$x<count($modulesKeys);$x++) {
			if($x==$limit){
				$modulesConditions .=$modulesKeys[$x]." LIKE '%".$searchQuery."%'";
			}else{
				$modulesConditions .=$modulesKeys[$x]." LIKE '%".$searchQuery."%' OR ";
			}
			
		}
		$modulesConditions .=')';
		
		$pagesConditions ="AND (";
		$pagesKeys = array('post_title','post_content');
		$limit = count($pagesKeys) - 1;
		for($x = 0;$x<count($pagesKeys);$x++) {
			if($x==$limit){
				$pagesConditions .=$pagesKeys[$x]." LIKE '%".$searchQuery."%'";
			}else{
				$pagesConditions .=$pagesKeys[$x]." LIKE '%".$searchQuery."%' OR ";
			}
			
		}
		$pagesConditions .=')';
		$coursesResults = $wpdb->get_results("SELECT c.name,c.slug,c.introduction ,c.picture,c.SBU,c.meeting_days,c.post_id,GROUP_CONCAT(DISTINCT c.name SEPARATOR '/') as categories 
											FROM courses AS c 
											LEFT JOIN categories_courses AS cc ON c.id = cc.course_id 
											LEFT JOIN categories AS cat ON cc.category_id = cat.id
											LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id= c.post_id
											WHERE c.post_id <> 0 ".$coursesConditions." GROUP BY c.name");


		$modulesResults = $wpdb->get_results("SELECT m.name,m.slug,m.introduction ,m.picture,m.SBU,m.meeting_days,m.post_id,GROUP_CONCAT(DISTINCT c.name SEPARATOR '/') as categories 
											FROM modules AS m
											LEFT JOIN categories_modules AS cm ON m.id = cm.module_id
											LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id = m.post_id
											LEFT JOIN categories AS c ON cm.category_id = c.id 
											WHERE m.post_id <> 0 ".$modulesConditions." GROUP BY m.name");




		$pagesResults = $wpdb->get_results("SELECT post_title,post_content,guid FROM {$wpdb->prefix}posts WHERE post_type ='page' ".$pagesConditions);

		//$modulesResults = searchRows($rows['modules'],$searchQuery);
		//$coursesResults = searchRows($rows['courses'],$searchQuery);
		//$pagesResults = searchRows($rows['others'],$searchQuery);


		$hasResult = 0;

		if(count($coursesResults)>0 || count($modulesResults)>0 || count($pagesResults)>0){
			$hasResult = 1;
		}

		ob_start();

	?>
		<?php if($hasResult==0) : ?>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h4>No result found for "<?= $_GET['search-general']; ?>"</h4>
				</div>
			</div>
		</div>
		<?php else : ?>
		<div class="panel-group" id="accordion">
			<?php if(count($coursesResults)>0) : ?>
			<div class="panel panel-default">
				<div class="panel-heading">
	            	<a data-toggle="collapse" data-parent="#accordion" href="#courses">
	                    <h4 class="panel-title">
	                        <span class="glyphicon glyphicon-file">
	                        </span> <?= count($coursesResults); ?> Results found for "<?= $_GET['search-general']; ?>" in Courses
	                    </h4>
	                </a>
	            </div>
	            <div id="courses" class="panel-collapse collapse">
	                <div class="panel-body">
	                	<?php foreach($coursesResults as $key=>$val) : ?>
						<div class="row">
							<div class="col-md-12">
								<strong><a target="_blank" href="<?= get_home_url() . '/news/?page='.$val->slug.'&post_id='.$val->post_id; ?>"><?= $val->name; ?></a></strong>
							</div>
						</div>
						<?php endforeach; ?>
	                </div>
	            </div>
	        </div>
	    	<?php endif; ?>
	    	<?php if(count($modulesResults)>0) : ?>
			<div class="panel panel-default">
				<div class="panel-heading">
	            	<a data-toggle="collapse" data-parent="#accordion" href="#modules">
	                    <h4 class="panel-title">
	                        <span class="glyphicon glyphicon-file">
	                        </span> <?= count($modulesResults); ?> Results found for "<?= $_GET['search-general']; ?>" in Modules
	                    </h4>
	                </a>
	            </div>
	            <div id="modules" class="panel-collapse collapse">
	                <div class="panel-body">
	                	<?php foreach($modulesResults as $key=>$val) : ?>
						<div class="row">
							<div class="col-md-12">
								<strong><a target="_blank" href="<?= get_home_url() . '/news/?page='.$val->slug.'&post_id='.$val->post_id; ?>"><?= $val->name; ?></a></strong>
							</div>
						</div>
						<?php endforeach; ?>
	                </div>
	            </div>
	        </div>
	    	<?php endif; ?>
	    	<?php if(count($pagesResults)>0) : ?>
			<div class="panel panel-default">
				<div class="panel-heading">
	            	<a data-toggle="collapse" data-parent="#accordion" href="#pages">
	                    <h4 class="panel-title">
	                        <span class="glyphicon glyphicon-file">
	                        </span> <?= count($pagesResults); ?> Results found for "<?= $_GET['search-general']; ?>" in Pages
	                    </h4>
	                </a>
	            </div>
	            <div id="pages" class="panel-collapse collapse">
	                <div class="panel-body">
	                	<?php foreach($pagesResults as $key=>$val) : ?>
						<div class="row">
							<div class="col-md-12">
								<strong><a target="_blank" href="<?= $val->guid; ?>"><?= $val->post_title; ?></a></strong>
							</div>
						</div>
						<?php endforeach; ?>
	                </div>
	            </div>
	        </div>
	    	<?php endif; ?>
	    </div>
		<?php endif; ?>
	<?php
		return ob_get_clean();


	}else{

		$tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'nieuw';

		$category = wp_bivt_category_query($tab);
		$rows = wp_bivt_modules_query($tab);
		$tab1 = '';
		$tab2 = '';
		$tab3 = '';
		$tab4 = '';
		$tab5 = '';
		$tab6 = '';
		switch ($tab) {
			
			case 'next':
				$tab1 = 'class="show-all act"';
				break;
			case 'nieuw':
				$tab2 = 'class="show-all act"';
				break;
			case 'bijscholing':
				$tab3 = 'class="show-all act"';
				break;
			case 'mpsbk':
				$tab4 = 'class="show-all act"';
				break;
			case 'praktijkdagen':
				$tab5 = 'class="show-all act"';
				break;
			case 'totaal':
				$tab6 = 'class="show-all act"';
				break;					
			default:
				$tab2 = 'class="show-all act"';
				break;
		}

		if ($tab == 'totaal') {
			$news .= '
				<div class="inner-container">			
					<h3>Courses</h3>
				</div>
			';
			$news .='
				<div class="grid wf-container with-ajax loading-effect-scale-up iso-container description-on-hover hover-style-two effect-sarah dt-isotope cont-id-0 iso-item-ready" data-padding="2px" data-cur-page="1" data-width="750px" data-columns="5" data-cont-id="0" style="position: relative;">
				<div class="grid-sizer"></div>
				';
			$news .= wp_bivt_html_news($rows['courses']);
			$news .='
				</div>
				';

			$news .= '
				<div class="inner-container">			
					<h3>Modules</h3>
				</div>
			';

			$news .='
				<div class="grid wf-container with-ajax loading-effect-scale-up iso-container description-on-hover hover-style-two effect-sarah dt-isotope cont-id-0 iso-item-ready" data-padding="2px" data-cur-page="1" data-width="750px" data-columns="5" data-cont-id="0" style="position: relative;">
				<div class="grid-sizer"></div>
				';
			$news .= wp_bivt_html_news($rows['modules']);
			
		} else {
			$news .='
				<div class="grid wf-container with-ajax loading-effect-scale-up iso-container description-on-hover hover-style-two effect-sarah dt-isotope cont-id-0 iso-item-ready" data-padding="2px" data-cur-page="1" data-width="750px" data-columns="5" data-cont-id="0" style="position: relative;">
				<div class="grid-sizer"></div>
				';

			$news .= wp_bivt_html_news($rows);
		}

		$news .='
				</div>
				';

	$description = strip_tags($category->description);
	
	return '
		<div class="bivt-nav-news">
			<div class="inner-container">
				<div class="clabel">
					<h3>'.$category->name.'</h3>
				</div>
				<p style="text-align: justify;">
				'.$description.'
				</p>
			</div>
			<div class="filter">
			    <div class="filter-categories">
			    	<a '.$tab1.' href="'.get_site_url().'/?tab=next">Binnenkort</a>
			    	<a '.$tab2.' href="'.get_site_url().'/">Nieuw</a>
			    	<a '.$tab3.' href="'.get_site_url().'/?tab=bijscholing">Bijscholing</a>
			    	<a '.$tab4.' href="'.get_site_url().'/?tab=mpsbk">MPsBK</a>
			    	<a '.$tab5.' href="'.get_site_url().'/?tab=praktijkdagen">Praktijkdagen</a>
			    	<a '.$tab6.' href="'.get_site_url().'/?tab=totaal">Totaal</a>
				</div>
			</div>

		<div class="row filter-display-product">
	            '.$news.'
	        </div>

		';
	}

	
}

add_shortcode('bivt-nav-locatie', 'wp_bivt_nav_locatie_shortcode');







function wp_bivt_news_page(){

	global $wpdb;

	$pagenum = (isset($_GET['pagenum'])) ? $_GET['pagenum'] : 1;

	$limit = 10; // number of rows in page
	$offset = ( $pagenum - 1 ) * $limit;

	$total = $wpdb->get_var( "SELECT COUNT(id) FROM newsitems");

	$num_of_pages = ceil( $total / $limit );

	$getNews =$wpdb->get_results("SELECT * FROM newsitems ORDER BY modified DESC LIMIT $offset,$limit");

	ob_start();

?>
	<?php if(count($getNews)>0) : ?>
	<div class="container">
	<?php foreach($getNews as $v) : ?>
	<?php

		$str = $v->picture;

		$result = explode('/', $str);

		$getLast = count($result) - 1;

		$limit = $getLast - 1;

		$getInitial = '';
		//get initial directory
		for($x=0;$x<=$limit;$x++)
		{
			$getInitial .=$result[$x] . '/';
		}

		$result = explode('.',$result[$getLast]);

		$v->picture_thumbnail = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

	?>
	<div class="row">
		<div class="col-md-2">
			<div class="news-picture">
				<?php if($v->picture!='' && file_exists(plugin_dir_path(__FILE__) . $v->picture)) : ?>
				<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive img-rounded" />
				<?php else : ?>
				<img src="<?= plugins_url('wp-manager-course/assets/images/noImage.jpg'); ?>" class="img-responsive img-rounded" />
				<?php endif; ?>
			</div>
		</div>
		<div class="col-md-6">
			<p style="margin-top:10px;"><a href="#"><?= $v->title; ?></a></p>
			<p>
				<?php
					$getContentLength = strlen($v->content);
				?>
					<?php
						$getContent = strip_tags(substr($v->content,0,100)) . '...'; 
						if($getContent!='...'){
					?>
					<?= $getContent; ?>
					<?php } ?>
			</p>
			<div class="well">
				<p>
					<?= date('F d, Y',strtotime($v->modified)); ?> |<span style="padding-left:5px;"><a href="#">View More</a></span>
				</p>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
	<?php

		$page_links = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'current' =>$pagenum,
            'total' => $num_of_pages,
            'type'  => 'array',
            'prev_next'   => TRUE,
			'prev_text'    => __('Previous'),
			'next_text'    => __('Next'),
		));

		$pattern = '/current/';

		if( is_array( $page_links ) ) {

            echo '<ul class="pagination">';
            foreach ( $page_links as $page_link ) {
            	if(preg_match($pattern, $page_link,$matches)){
            		echo "<li class='active'>$page_link</li>";
				}else{
					echo "<li>$page_link</li>";
				}
            }
           echo '</ul>';
		}
	?>
	</div>
	<?php endif; ?>
<?php
	
	return ob_get_clean();
}
add_shortcode('bivt-news-page', 'wp_bivt_news_page');


class CategoryCustom {
	public $name,$description;

	public function __construct(){
	}

}

function wp_bivt_category_query($tab) {
	global $wpdb;
	
	$category = new CategoryCustom;
	switch ($tab) {
		case 'next':
			$category->name = 'Aankomende Scholingen';
			$category->description = 'Hieronder vind je alle aankomende bij en nascholingen die gepland zijn. Onder meer info kun je ook vinden op welke locaties de dagen gepland zijn.';
			return $category;
			break;
		case 'totaal':
			$category->name = 'Totaal';
			$category->description = 'Dit is het totaal overzicht van alle producten aangeboden door het BivT.';
			return $category;
			break;
		case '':
			return $wpdb->get_row("SELECT * FROM `categories` AS c WHERE c.slug = 'nieuw'");
			break;
		default:
			$query = $wpdb->get_row("SELECT * FROM `categories` AS c WHERE c.slug = '".$tab."'");

			if(!isset($query)) {
				return $wpdb->get_row("SELECT * FROM `categories` AS c WHERE c.slug = 'nieuw'");
			} else {
				return $query;
			}
			
			break;
	}
}

function wp_bivt_modules_query($tab) {
	global $wpdb;

	switch ($tab) {
		case 'next':
			$queries = $wpdb->get_results("SELECT m.* FROM `days` AS d 
											LEFT JOIN instances AS i ON i.id = d.instance_id 
											LEFT JOIN modules AS m ON i.module_id = m.id 
											LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id= m.post_id
											WHERE d.datum > DATE(NOW()) AND m.status = 'open' AND m.post_id <> 0 AND m.id IS NOT NULL GROUP BY i.module_id");
			return $queries;
			break;
		case 'totaal':
			$modules = $wpdb->get_results("SELECT m.name,m.slug,m.introduction ,m.picture,m.SBU,m.meeting_days,m.post_id,GROUP_CONCAT(DISTINCT c.name SEPARATOR '/') as categories 
											FROM modules AS m
											LEFT JOIN categories_modules AS cm ON m.id = cm.module_id
											LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id = m.post_id
											LEFT JOIN categories AS c ON cm.category_id = c.id 
											WHERE m.status = 'open' AND m.post_id <> 0 GROUP BY m.name");

			$courses = $wpdb->get_results("SELECT c.name,c.slug,c.introduction ,c.picture,c.SBU,c.meeting_days,c.post_id,GROUP_CONCAT(DISTINCT c.name SEPARATOR '/') as categories 
											FROM courses AS c 
											LEFT JOIN categories_courses AS cc ON c.id = cc.course_id 
											LEFT JOIN categories AS cat ON cc.category_id = cat.id
											LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id= c.post_id
											WHERE c.post_id <> 0 GROUP BY c.name");


			$queries['modules'] = $modules;
			$queries['courses'] = $courses;
			return $queries;
			break;
		default:

			$tab = isset($_GET['tab']) ? $_GET['tab'] : 'nieuw';
			return $wpdb->get_results("SELECT m.* FROM `modules` AS m 
									   LEFT JOIN categories_modules AS cm ON m.id = cm.module_id 
						               LEFT JOIN categories AS c ON c.id = cm.category_id
									   LEFT JOIN wp_postmeta AS postmeta ON postmeta.post_id = m.post_id
									   WHERE m.status = 'open' AND m.post_id<>0 AND c.slug = '".$tab."' GROUP BY m.id");
			break;
	}
}

function wp_bivt_html_news($rows) {

	$news = '';
		foreach ($rows as $row) {

			$introduction = substr(strip_tags($row->introduction),0,300);

			if ( !isset($row->introduction)  ) {
				$introduction = substr(strip_tags($row->description),0,300);
			}
			
			$news .=  '
					    <div class="grid-item" data-name="'.$row->name.'">
					    	<article class="post post-23 dt_portfolio type-dt_portfolio status-publish has-post-thumbnail hentry dt_portfolio_category-08-photo-video dt_portfolio_category-20">
					    	<figure class="rollover-project forward-post this-ready">
					    			
						    		<img class="iso-lazy-load preload-me height-ready iso-layzr-loaded" src="'.plugins_url($row->picture, __FILE__).'">

									<figcaption class="rollover-content click-ready">
										<a href="'.get_site_url().'/news/?page='.$row->slug.'&post_id='.$row->post_id.'" title="'.$row->name.'" rel="bookmark">
									    <div class="rollover-content-container">
									        <p>'.limit_text($introduction, 20).'</p>
									        <div class="entry-meta portfolio-categories"><span class="category-link">
									        	<p>'.$row->meeting_days.' dgn | '.$row->SBU.' SBU</p>
									        </div>								        
									    </div>
									    <a href="'.get_site_url().'/news/?page='.$row->slug.'&post_id='.$row->post_id.'" title="'.$row->name.'" rel="bookmark">
									</figcaption>
									
					    	</figure>
					    	</article>
					        <h5 class="entry-title" style="text-align:center;padding: 5px;">
					        	<a href="'.get_site_url().'/news/?page='.$row->slug.'&post_id='.$row->post_id.'" title="'.$row->name.'" rel="bookmark">'.$row->name.'</a>
					        </h3>
					  	</div>

					';
		}
	return $news;
}

function wp_bivt_nav_locatie_shortcode() {
	global $wpdb;

	$location = '';

	$tab1 = '';
	$tab2 = '';
	$tab3 = '';
	$tab4 = '';
	$tab5 = '';
	$tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'nieuw';
	switch ($tab) {
		case 'planningperlocatie':
			$tab2 = 'class="show-all act"';
			$rows = $wpdb->get_results("SELECT * from locations WHERE status='open'");
		break;
		default:
			$tab1 = 'class="show-all act"';
			$rows = $wpdb->get_results("SELECT * from locations WHERE status='open'");
		break;
	}

	if ( isset($_GET['locatie']) ){
		$page = $_GET['locatie'];

		$row = $wpdb->get_row("SELECT * from locations WHERE id=$page AND status='open'");
		$location .= '<strong>'.$row->name.'</strong><br>';
		$location .= $row->address.'<br>';
		$location .= $row->parking.'<br>';
		$location .= $row->comments.'<br>';

		

	} else {
			if ( $tab == 'planningperlocatie' ) {
				$getLocations = $wpdb->get_results("SELECT id, name FROM `locations` ORDER BY name ASC");
				$counter = 0;
				$url =  home_url().'/news/?page=';
				$location .='
							<div class="inner-container">
								<p>Hier een overzicht van de locaties en de bijscholingen die daar gepland zijn.<p><br>
							</div>
							<div class="container">
							    <div class="row">
							        <div class="col-md-12">
							            <div class="panel-group" id="accordion">
							';
							foreach ($getLocations as $getLocation) {
								$counter = $counter + 1;
							$location .='
										<div class="panel panel-default">
											<div class="panel-heading">
						                    	<a data-toggle="collapse" data-parent="#accordion" href="#collapse_'.$counter.'">
							                        <h4 class="panel-title">
							                            <span class="glyphicon glyphicon-file">
							                            </span> '.$getLocation->name.'
							                        </h4>
							                       </a>
						                    </div>
							                    <div id="collapse_'.$counter.'" class="panel-collapse collapse">
							                        <div class="panel-body">
										';
								$getdays = $wpdb->get_results("SELECT days.datum as datum, modules.name as name, modules.meeting_days as meeting, modules.slug as slug FROM days 
															LEFT JOIN instances ins ON ins.id = days.instance_id
															LEFT JOIN modules ON modules.id = ins.module_id
															WHERE days.location_id = ".$getLocation->id." AND days.datum > NOW() AND modules.name IS NOT NULL");
								if ( empty($getdays) ) {
									$location .='<div class="row"><div class="col-md-12"><strong>Op dit moment zijn er geen reeksen gepland op deze locatie</strong></div></div>';
								}
								foreach ($getdays as $getday) {
									$dayofweek = '';
									$datum ='';
									$fullDate ='';
									if ( $getday->datum != NULL ){
										$datum = new DateTime($getday->datum);
										$dayofweek =  $datum->format('l');
										$fullDate = $datum->format('m-d-Y');
									}
									
								$location .='				<div class="row">';
								$location .='

								                                <div class="col-md-4">
								                                    	<strong><p>Naam:</p></strong>
								                                        <a href="' .$url. $getday->slug .'">' .$getday->name. '</a>

								                                </div>
								                                <div class="col-md-4">

								                                    	<strong><p>Contactdagen:</p></strong>
								                                		'.$getday->meeting.' dag(en)

								                                </div>
								                                <div class="col-md-4">
								                                    	<strong><p>Begint op:</p></strong>
								                                		'. $dayofweek." ".$fullDate.' 

								                                </div>
											';
							$location .='				</div>';
								}
				$location .='
							                        </div>
							                    </div>
							            </div>				
				';
							}

				$location .='
							                
							            </div>
							        </div>
							    </div>
							</div>


				';

			} else {

				$location .='
						<div class="grid wf-container with-ajax loading-effect-scale-up iso-container description-on-hover hover-style-two effect-sarah dt-isotope cont-id-0 iso-item-ready" data-padding="2px" data-cur-page="1" data-width="750px" data-columns="5" data-cont-id="0" style="position: relative;">
						<div class="grid-sizer"></div>
						';
						//'.plugins_url($row->picture, __FILE__).'

				foreach ($rows as $row) {
							$location .=  '
							    <div class="grid-item" data-name="'.$row->name.'">
							    	<article class="post post-23 dt_portfolio type-dt_portfolio status-publish has-post-thumbnail hentry dt_portfolio_category-08-photo-video dt_portfolio_category-20">
							    	<figure class="rollover-project forward-post this-ready">
								    		<img class="iso-lazy-load preload-me height-ready iso-layzr-loaded" src="'.randPic().'">
											<figcaption class="rollover-content click-ready">
											    <div class="rollover-content-container">
											        <p>'.$row->address.'</p>
											        <div class="entry-meta portfolio-categories"><span class="category-link">
											        <p>'.limit_text($row->parking,15).'</p>
											            </a>
											        </div>
											    </div>
											</figcaption>
										
							    	</figure>
							    	</article>
							        <h5 class="entry-title" style="text-align:center;padding: 5px;">
							        	<a href="'.get_site_url().'/locatie/?locatie='.$row->id.'" title="'.$row->name.'" rel="bookmark">'.$row->name.'</a>
							        </h3>
							  	</div>

							';
				}
				$location .='
							</div>
							';
			}
	}
	return '
		<div class="bivt-nav-locatie">

			<div class="filter">
			    <div class="filter-categories">
			    	<a '.$tab1.' href="'.get_site_url().'/locatie">Adressen</a>
			    	<a '.$tab2.' href="'.get_site_url().'/locatie/?tab=planningperlocatie">Planning per locatie</a>
				</div>
			</div>
		</div>

		<div class="filter-display-product"> '.$location .' </div>

	';
}

add_shortcode('bivt-nav-news', 'wp_bivt_nav_news_shortcode');
function get_user_role() {
	global $current_user, $wpdb;
	$user_id = get_current_user_id();
	$rows = $wpdb->get_results("SELECT group_id FROM users_groups WHERE user_id = $user_id");
	$get_user_role = 0;

    foreach( $rows as $row ) {
    	switch ( $row->group_id ) {
    		case 1:
					$get_user_role = 1;
    			break;
    		
    		case 3: 
    				$get_user_role = 3;
    			break;

    		case 5:
    				$get_user_role = 5;
    			break;

    	}
    }

    return $get_user_role;
}

add_shortcode('bivt-accreditaties-page', 'wp_bivt_nav_accreditaties_shortcode');
function wp_bivt_nav_accreditaties_shortcode() {
	global $wpdb;

	if (isset($_GET['id'])) {
		$id = $_GET['id'];
	 	$js = "<script>
	 			jQuery(window).load(function(){
	 				jQuery('#content > p:first-child').css('display','none');
	 			});
	 				
	 		</script>";
 		$accreditaties = '';
		$data = $wpdb->get_row("SELECT name, comments FROM professional_organizations WHERE id=$id");
		
		$rows = get_accreditaties_modules($id);
		$location = wp_bivt_html_news($rows);

		return '<div class="clabel"> <h3>'. $data->name. '</h3> </div><p>' . $data->comments. '</p><div class="row filter-display-product"><div class="grid wf-container with-ajax loading-effect-scale-up iso-container description-on-hover hover-style-two effect-sarah dt-isotope cont-id-0 iso-item-ready" data-padding="2px" data-cur-page="1" data-width="750px" data-columns="5" data-cont-id="0" style="position: relative; height: 281.781px;">
				<div class="grid-sizer"></div>'.$location .'</div></div></div>'. $js;
		

	} else {
		$accreditaties = '';
		$rows = $wpdb->get_results("SELECT pro.id, pro.name FROM `professional_organizations` AS pro LEFT JOIN accreditaties as acc ON acc.professional_organization_id = pro.id GROUP BY acc.professional_organization_id ORDER BY acc.professional_organization_id, pro.id ASC");
		foreach ($rows as $row) {
			$accreditaties .=  "<li><a href='".get_permalink()."/?id=".$row->id."' clas=''>". $row->name ."</a></li>";
		}

		//return "<div class='accre-label'>Name</div><ul class='accreditaties-menu'>". $accreditaties ."</ul>";
		return '
		grid wf-container
			<div class="vc_row wpb_row vc_inner vc_row-fluid">
			    <div class="wpb_column vc_column_container vc_col-sm-6">
			        <div class="vc_column-inner ">
			            <div class="wpb_wrapper">
			                <div class="standard-arrow bullet-top">
			                    <ul class="accreditaties-menu">
			                        ' . $accreditaties . '
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>


		';
	}

}

function get_accreditaties_modules($accreditaties_id) {
	global $wpdb;
	$modules = $wpdb->get_results("SELECT po.name,m.* FROM `accreditaties` AS a 
									LEFT JOIN modules AS m ON m.id = a.module_id 
									LEFT JOIN professional_organizations AS po ON po.id = a.professional_organization_id
									WHERE po.id = ".$accreditaties_id." AND m.id IS NOT NULL");

	return $modules;
}

add_shortcode('bivt-courses', 'wp_bivt_courses_shortcode');
function wp_bivt_courses_shortcode() {
	global $wpdb;
	$categories = $wpdb->get_results("SELECT id,name FROM categories");

	$teachers = $wpdb->get_results("SELECT DISTINCT(docents.user_id) AS duserid,docents.name AS name,user_nicename,users_groups.user_id AS uid FROM {$wpdb->prefix}users LEFT JOIN users_groups ON {$wpdb->prefix}users.ID = users_groups.user_id LEFT JOIN docents ON users_groups.user_id = docents.user_id WHERE users_groups.group_id = 5");
	//$teachers = $wpdb->get_results("SELECT docents.name AS name,user_nicename,users_groups.user_id AS uid FROM users_groups LEFT JOIN {$wpdb->prefix}users ON users_groups.user_id = {$wpdb->prefix}users.ID LEFT JOIN docents ON users_groups.user_id = docents.user_id WHERE users_groups.group_id = 5");

	//$teachers = $wpdb->get_results("SELECT docents.name AS name,user_nicename,users_groups.user_id AS uid FROM users_groups LEFT JOIN docents ON users_groups.user_id = docents.user_id LEFT JOIN {$wpdb->prefix}users ON docents.user_id = {$wpdb->prefix}users.ID WHERE users_groups.group_id = 5");

	ob_start();
?>	

		<button class="btn btn-link add-courses" data-toggle="modal" data-target="#add_courses">
    		<span class="glyphicon glyphicon-plus bivt-plus-icon"> Add Course item </span>
    	</button>

		<div class="modal fade" id="add_courses" tabindex="-1" role="dialog" aria-labelledby="courses" aria-hidden="true">
		    <div class="modal-dialog modal-lg modal-80p">
		        <div class="modal-content">
		            <!-- Modal Header -->
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">
		                       <span aria-hidden="true">×</span>
		                       <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title" id="courses">
		                    Courses
		                </h4>
		            </div>
		            
		            <!-- Modal Body -->
		            <div class="modal-body">
					    <form id="AddCourses" class="form-horizontal" method="POST">

					        <div class="form-group form-group-sm">
					            <!-- left column -->
					            <div class="col-sm-6">

					            	<div class="form-group">
				                    	<label class="col-sm-2 control-label" for="name" >Name</label>
				                    	<div class="col-sm-10">
				                    		<input type="text" class="form-control required" name="name" id="name" placeholder="Name" required/>
				                    	</div>
				                  	</div>

					           		<div class="form-group">
				                    	<label  class="col-sm-2 control-label" for="introduction">Introduction</label>
				                    	<div class="col-sm-10">
				                    		<textarea class="form-control required mceEditor" rows="6" name="introduction" id="introduction" required></textarea>
				                    	</div>
				                  	</div>				                  	

					                <div class="form-group">
				                    	<label class="col-sm-2 control-label" for="meta_description" >Meta Description</label>
				                    	<div class="col-sm-10">
				                    		<textarea class="form-control required mceEditor" rows="6" name="meta_description" id="meta_description" required></textarea>
				                    	</div>
				                  	</div>	

				                  	<div class="form-group">
				                    	<label class="col-sm-2 control-label" for="meta_keywords" >Meta Keywords</label>
				                    	<div class="col-sm-10">
				                    		<textarea class="form-control required mceEditor" rows="6" name="meta_keywords" id="meta_keywords" required></textarea>
				                    	</div>
				                  	</div>
					            </div>

					            <!-- right column -->
					            <div class="col-sm-6">

					            	<div class="form-group">
				                    	<label class="col-sm-2 control-label" for="conditions" >Conditions</label>
				                    	<div class="col-sm-10">
				                    		<input type="text" class="form-control required" name="conditions" id="conditions" placeholder="Add Conditions" required/>
				                    	</div>
					                </div>					                
					                
									<div class="form-group">
										<label class="col-sm-2 control-label" for="description" >Description</label>
										<div class="col-sm-10">
											<textarea class="form-control required mceEditor" name="description" id="description" required value="<?= $v->description ?>" ></textarea>
										</div>
									</div>

					                <div class="form-group">
					                    <label for="sessions" class="col-sm-2 control-label">Sessions</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required" name="sessions" id="sessions" placeholder="Sessions" required/>			                        
					                    </div>			                    
					                </div>

					                <div class="form-group">
					                    <label for="meeting_days" class="col-sm-2 control-label">Meeting Days</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required" name="meeting_days" id="meeting_days" placeholder="Meeting Days" required/>		                        
					                    </div>			                    
					                </div>

					                <div class="form-group">
					                    <label for="new_bol_require" class="col-sm-2 control-label">Price</label>
					                    <div class="col-sm-4">
					                        <input type="text" class="form-control required" name="price" id="price" placeholder="$0.00" required/>	
					                    </div>
					                    <label for="new_pod_require" class="col-sm-2 control-label">SBU</label>
					                    <div class="col-sm-4">
					                        <input type="text" class="form-control required" name="sbu" id="sbu" placeholder="0" required/>	
					                    </div>
					                </div>

					                <div class="form-group">
					                    <label for="incasso" class="col-sm-2 control-label">Incasso</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required" name="incasso" id="incasso" placeholder="Incasso" required/>			                        
					                    </div>			                    
					                </div>

					                <div class="form-group">
					                    <label for="status" class="col-sm-2 control-label">Status</label>
					                    <div class="col-sm-4">
					                        <select class="form-control" name="status" id="status">
					                            <option value="open">Open</option>
					                            <option value="close" selected="selected">Close</option>
					                        </select>
					                    </div>
					                </div>
					                <div class="form-group form-group-sm" style="display:none;">
										<!-- left column -->
										<div class="col-sm-12">
											<div class="form-group">
												<label  class="col-sm-2 control-label" for="picture_preview">Picture Preview</label>
												<div class="col-sm-10">
													<img class="picture_preview img-responsive col-md-6" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-sm">
										<!-- left column -->
										<div class="col-sm-12">
											<div class="form-group">
												<label  class="col-sm-2 control-label" for="picture">Picture</label>
												<div class="col-sm-10">
													<input type="file" class="form-control required" name="picture" id="picture" required/>
												</div>
											</div>
										</div>
									</div>
					                <div class="form-group">
					                    <label for="category" class="col-sm-2 control-label">Assign Category</label>
					                    <div class="col-sm-10">
					                        <select class="form-control cat-edit-multiple" multiple="multiple" name="category[]" multiple id="category">
					                        	<?php if(count($categories)>0) : ?>
												<?php foreach ($categories as $k => $v) : ?>
												<option value="<?= $v->id; ?>"><?= $v->name; ?></option>
												<?php endforeach; ?>
												<?php else : ?>
												<option value=0>No available category found</option>
												<?php endif; ?>
					                        </select>
					                    </div>
					                </div>
					                <div class="form-group">
					                    <label for="category" class="col-sm-2 control-label">Assign Teacher</label>
					                    <div class="col-sm-10">
					                        <select class="form-control cat-edit-multiple" multiple="multiple" name="teacher[]" multiple id="teacher">
					                        	<?php if(count($teachers)>0) : ?>
												<?php foreach ($teachers as $k => $v) : ?>
												<option value="<?= $v->uid; ?>"><?= $v->name; ?></option>
												<?php endforeach; ?>
												<?php else : ?>
												<option value=0>No available teacher found</option>
												<?php endif; ?>
					                        </select>
					                    </div>
					                </div>

					            </div>
					        </div>
					      <!-- Modal Footer -->
				            <div class="modal-footer">
				                <button id="CoursesSubmit" state=0 type="button" class="btn btn-default">Add Course</button>
				            </div><!-- End Modal Footer -->
					      
					    </form>
					</div> <!-- End modal body div -->
				</div> <!-- End modal content div -->
			</div> <!-- End modal dialog div -->
		</div> <!-- End modal div -->
	<?php
		return ob_get_clean();
}

add_shortcode('bivt-materials', 'wp_bivt_materials_shortcode');
function wp_bivt_materials_shortcode() 
{
	global $wpdb;
	$materials = '';
	$id = '';

	$results = $wpdb->get_results('SELECT * FROM modules');


	$materials = '
		<button class="btn btn-link" data-toggle="modal" data-target="#add_materials">
			<span class="glyphicon glyphicon-plus bivt-plus-icon"> Add Materials </span>
		</button>

		<div class="modal fade" id="add_materials" tabindex="-1" role="dialog" aria-labelledby="materials" aria-hidden="true">
		    <div class="modal-dialog modal-lg modal-80p">
		        <div class="modal-content">
		            <!-- Modal Header -->
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal">
		                       <span aria-hidden="true">×</span>
		                       <span class="sr-only">Close</span>
		                </button>
		                <h4 class="modal-title" id="materials">
		                    Materials
		                </h4>
		            </div>
		            
		            <!-- Modal Body -->
		            <div class="modal-body">
					    <form id="AddMaterials" class="form-horizontal" method="POST">
					        <div class="form-group form-group-sm">
					            <!-- left column -->
					            <div class="col-sm-6">

					                <div class="form-group">
				                    	<label class="col-sm-2 control-label" for="name" >Name</label>
				                    	<div class="col-sm-10">
				                    		<input type="text" class="form-control required" name="name" id="name" placeholder="Name" required/>
				                    	</div>
				                  	</div>
					                
					                <div class="form-group">
				                    	<label class="col-sm-2 control-label" for="file" >File</label>
				                    	<div class="col-sm-10">
				                    		<input type="file" class="form-control required" name="file" id="file" required/>
				                    	</div>
				                  	</div>

				                  	<div class="form-group">
					                    <label for="status" class="col-sm-2 control-label">Module ID</label>
					                    <div class="col-sm-4">
					                        <select class="form-control" name="module_id" id="module_id">';
											foreach ($results as $k => $v) {
												$materials .= '<option value="'. $v->id .'">'. $v->id .'</option>';
											}					                        	
		$materials .= '					                        								                        							                        	
					                        </select>
					                    </div>
					                </div>					               
					            </div>

					            <!-- right column -->
					            <div class="col-sm-6">
					                <div class="form-group">
					                    <label for="file_type" class="col-sm-2 control-label">File Type</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required" name="file_type" id="file_type" disabled required/>			                        
					                    </div>              
					                </div>

					                <div class="form-group">
					                    <label for="original_creator" class="col-sm-2 control-label">Original Creator</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required" name="original_creator" id="original_creator" required/>		                        
					                    </div>			                    
					                </div>

					                <div class="form-group">
					                    <label for="status" class="col-sm-2 control-label">Status</label>
					                    <div class="col-sm-4">
					                        <select class="form-control" name="status" id="status">
					                            <option value="open">Open</option>
					                            <option value="close">Close</option>
					                        </select>
					                    </div>
					                </div>
					            </div>
					        </div>

					      <!-- Modal Footer -->
				            <div class="modal-footer">
				                <button type="button" class="btn btn-default MaterialSubmit">Add Materials</button>
				            </div><!-- End Modal Footer -->
					      
					    </form>
					</div> <!-- End modal body div -->
				</div> <!-- End modal content div -->
			</div> <!-- End modal dialog div -->
		</div> <!-- End modal div -->
	';

	return $materials;
}


/* Start Edit   * /
/* Kenneth */
/* 7-28-16 */

add_shortcode('bivt-news', 'wp_bivt_news_shortcode');
function wp_bivt_news_shortcode() {

	$news = '<button class="btn btn-default" data-toggle="modal" data-target="#add_news">
				Nieuwsbericht toevoegen
    		</button>
			<div class="modal fade" id="add_news" tabindex="-1" role="dialog" aria-labelledby="news" aria-hidden="true">
			    <div class="modal-dialog modal-lg modal-80p">
			        <div class="modal-content">
			            <!-- Modal Header -->
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal">
			                       <span aria-hidden="true">×</span>
			                       <span class="sr-only">Close</span>
			                </button>
			                <h4 class="modal-title" id="news">
			                    News
			                </h4>
			            </div>
			            
			            <!-- Modal Body -->
			            <div class="modal-body">
						    <form id="AddNews" class="form-horizontal" method="POST">

						        <div class="form-group form-group-sm">
						            <!-- left column -->
						            <div class="col-sm-10">
						           		<div class="form-group">
					                    	<label  class="col-sm-2 control-label" for="title">Title</label>
					                    	<div class="col-sm-10">							                    		
					                    		<input type="text" class="form-control" name="title" id="title" placeholder="title"/>
					                    	</div>
					                  	</div>

						                <div class="form-group">
					                    	<label class="col-sm-2 control-label" for="content" >Content</label>
					                    	<div class="col-sm-10">
					                     		<textarea class="form-control mceEditor" rows="6" name="content" id="content" placeholder="Content"></textarea>
					                    	</div>
					                  	</div>
						                <div class="form-group form-group-sm" style="display:none;">
											<!-- left column -->
											<div class="col-sm-12">
												<div class="form-group">
													<label  class="col-sm-2 control-label" for="picture_preview">Picture Preview</label>
													<div class="col-sm-10">
														<img class="picture_preview img-responsive col-md-6" />
													</div>
												</div>
											</div>
										</div>
										<div class="form-group form-group-sm">
											<!-- left column -->
											<div class="col-sm-12">
												<div class="form-group">
													<label  class="col-sm-2 control-label" for="picture">Picture</label>
													<div class="col-sm-10">
														<input type="file" class="form-control required" name="picture" id="picture" required/>
													</div>
												</div>
											</div>
										</div>

						            </div>
						        </div>
						      <!-- Modal Footer -->
					            <div class="modal-footer">
					                <button state=0 id="NewsSubmit" type="button" class="btn btn-default">Add News</button>
					            </div><!-- End Modal Footer -->
						      
						    </form>
						</div> <!-- End modal body div -->
					</div> <!-- End modal content div -->
				</div> <!-- End modal dialog div -->
			</div> <!-- End modal div -->';

	return $news;
}



add_shortcode('bivt-categories', 'wp_bivt_categories_shortcode');
function wp_bivt_categories_shortcode() {


	$categories= '';


	global $wpdb;


	$categories = '
		<button class="btn btn-default" data-toggle="modal" data-target="#add_categories">
    		Categorie toevoegen
    	</button>


    	<div class="modal fade" id="add_categories" tabindex="-1" role="dialog" aria-labelledby="categories" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-80p">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							   <span aria-hidden="true">×</span>
							   <span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="categories">
							Categorie
						</h4>
					</div>
					<!-- Modal Body -->
					<div class="modal-body">
						<form id="AddCategories" class="form-horizontal" method="POST">
							<div class="form-group form-group-sm">
								<!-- left column -->
								<div class="col-sm-12">
									<div class="form-group">
										<label  class="col-sm-2 control-label" for="name">Naam</label>
										<div class="col-sm-10">
											<input type="text" class="form-control required" name="name" id="name" placeholder="Naam" required/>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-sm">
		                    	<label class="col-sm-2 control-label" for="description" >Omschrijving</label>
		                    	<div class="col-sm-10">
		                    		<textarea class="form-control required mceEditor" rows="6" name="description" placeholder="Omschrijving" id="description" required></textarea>
		                    	</div>
				            </div>
				            <div class="form-group form-group-sm">
								<div class="form-group">
									<label class="col-sm-2 control-label" for="type" >Type</label>
									<div class="col-sm-10">
										<select class="form-control" name="type" id="type">
											<option value="vak">vak</option>
											<option value="vorm">vorm</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="meta_description" >Meta Omschrijving</label>
								<div class="col-sm-10">
									<textarea class="form-control required" name="meta_description" id="meta_description" required>
									</textarea> 
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label" for="meta_keyword" >Meta Keywords</label>
								<div class="col-sm-10">
									<textarea class="form-control required" name="meta_keywords" id="meta_keywords" required>
									</textarea> 
								</div>
							</div>
						<div class="form-group form-group-sm">
								<div class="col-sm-12">
									<div class="form-group">
										<label  class="col-sm-2 control-label" for="status">Status</label>
										<div class="col-sm-10">
											<select name="status" id="status" class="form-control required">
												<option value="close">afgesloten</option>
												<option value="open">open</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-sm" style="display:none;">
								<!-- left column -->
								<div class="col-sm-12">
									<div class="form-group">
										<label  class="col-sm-2 control-label" for="picture_preview">Voorbeeld afbeelding</label>
										<div class="col-sm-10">
											<img class="picture_preview img-responsive col-md-6" />
										</div>
									</div>
								</div>
							</div>
							<div class="form-group form-group-sm">
								<!-- left column -->
								<div class="col-sm-12">
									<div class="form-group">
										<label  class="col-sm-2 control-label" for="picture">Afbeelding</label>
										<div class="col-sm-10">
											<input type="file" class="form-control required" name="picture" id="picture" required/>
										</div>
									</div>
								</div>
							</div>
						  	<!-- Modal Footer -->
							<div class="modal-footer">
								<button id="CategorySubmit" type="button" state=0 class="btn btn-default">Categorie toevoegen</button>
							</div><!-- End Modal Footer --> 
						</form>
					</div> <!-- End modal body div -->
				</div> <!-- End modal content div -->
			</div> <!-- End modal dialog div -->
		</div> <!-- End modal div -->
	';

	return $categories;
}



add_shortcode('bivt-accreditaties', 'wp_bivt_accreditaties_shortcode');
function wp_bivt_accreditaties_shortcode() {

	$modules = '';

	global $wpdb;

	$countModules = $wpdb->get_var("SELECT COUNT(*) FROM modules");
	$countOrganizations = $wpdb->get_var("SELECT COUNT(*) FROM professional_organizations");

	$optionModules = '';
	$optionOrganizations = '';

	//modules
	if($countModules>0){
		$getModules = $wpdb->get_results("SELECT * FROM modules",ARRAY_A);
		foreach($getModules as $getModule) {
			$optionModules .='<option value="'.$getModule['id'].'">'.$getModule['name'].'</option>';
		}
	}
	else
		$optionModules = '<option value=0>No Modules found.</option>';

	//organizations
	if($countOrganizations>0){
		$getOrganizations = $wpdb->get_results("SELECT * FROM professional_organizations",ARRAY_A);
		foreach($getOrganizations as $getOrganization) {
			$optionOrganizations .='<option value="'.$getOrganization['id'].'">'.$getOrganization['name'].'</option>';
		}
	}
	else
		$optionOrganizations = '<option value=0>No Professional Organization found.</option>';




	$modules = '
		<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_accreditaties">
			Accreditatie toevoegen
		</button>		
					
    	<div class="modal fade" id="add_accreditaties" tabindex="-1" role="dialog" aria-labelledby="modules" aria-hidden="true">
			<div class="modal-dialog modal-lg modal-80p">
				<div class="modal-content">
					<!-- Modal Header -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							   <span aria-hidden="true">×</span>
							   <span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="acrreditaties">
							Acreditaties
						</h4>
					</div>
					
					<!-- Modal Body -->
					<div class="modal-body">
						<form id="AddAccreditaties" class="form-horizontal" method="POST">
							<div class="form-group form-group-sm">
								<!-- left column -->
								<div class="col-sm-12">
									<div class="form-group">
										<label  class="col-sm-2 control-label" for="points">Points</label>
										<div class="col-sm-10">
											<input type="text" class="form-control required" name="points" id="points" placeholder="Name" required/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="valid_from" >Valid from</label>
										<div class="col-sm-10">
											<input type="text" class="form-control required date-type" name="valid_from" id="valid_from" placeholder="valid_from" required/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="valid_till" >Valid until</label>
										<div class="col-sm-10">
											<input type="text" class="form-control required date-type" name="valid_till" id="valid_till" placeholder="valid_till" required/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="module_id" >Module</label>
										<div class="col-sm-10">
											<select class="form-control required" name="module_id">
											'.$optionModules.'
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="professional_organization_id" >Organization</label>
										<div class="col-sm-10">
											<select class="form-control required" name="professional_organization_id">
											'.$optionOrganizations.'
											</select>
										</div>
									</div>
								</div>
							</div>
						  	<!-- Modal Footer -->
							<div class="modal-footer">
								<button id="AccreditatySubmit" type="button" state=0 class="btn btn-default">Add Accreditaty</button>
							</div><!-- End Modal Footer -->
						  
						</form>
					</div> <!-- End modal body div -->
				</div> <!-- End modal content div -->
			</div> <!-- End modal dialog div -->
		</div> <!-- End modal div -->
	';

	return $modules;
}

add_shortcode('bivt-modules', 'wp_bivt_modules_shortcode');
function wp_bivt_modules_shortcode() {
	global $wpdb;
	$courses = $wpdb->get_results('SELECT id, name FROM courses');
	$categories = $wpdb->get_results('SELECT id, name FROM categories');
	$teachers = $wpdb->get_results ("SELECT docents.id, docents.name FROM docents ORDER BY name");
	
	ob_start();
?>

	<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add_module" onclick="$('#add_module .groups-edit-multiple').select2({ allowClear: true });$('#add_module .cat-edit-multiple').select2({ allowClear: true })">
		Module toevoegen
	</button>		
	
	<div class="modal fade" id="add_module" tabindex="-1" role="dialog" aria-labelledby="modules" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-80p">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						   <span aria-hidden="true">×</span>
						   <span class="sr-only">Sluiten</span>
					</button>
					<h4 class="modal-title" id="courses">
						Nieuwe module
					</h4>
				</div>
				
				<!-- Modal Body -->
				<div class="modal-body">
					<form id="AddModules" class="form-horizontal" method="POST">

						<div class="form-group form-group-sm">
							<!-- left column -->
							<div class="col-sm-6">

								<div class="form-group">
									<label class="col-sm-2 control-label" for="name" >Naam</label>
									<div class="col-sm-10">
										<input type="text" class="form-control required" name="name" id="name" placeholder="Name" required/>
									</div>
								</div>
							
								<div class="form-group">
									<label  class="col-sm-2 control-label" for="introduction">Introductie</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" rows="6" name="introduction" id="introduction" required></textarea>
									</div>
								</div>									
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="description" >Omschrijving</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="description" id="description" required></textarea>
									</div>
								</div>		                  	

								<div class="form-group">
									<label class="col-sm-2 control-label" for="conditions" >Voorwaarden</label>
									<div class="col-sm-10">
										<input type="text" class="form-control required" name="conditions" id="conditions" placeholder="Add Conditions" required/>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="programme" >Programma</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="programme" id="programme" required></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="meta_description" >Meta omschrijving</label>
									<div class="col-sm-10">
										<textarea class="form-control required" rows="6" name="meta_description" id="meta_description" required></textarea>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-2 control-label" for="meta_keywords" >Meta Keywords</label>
									<div class="col-sm-10">
										<textarea class="form-control required" rows="6" name="meta_keywords" id="meta_keywords" required></textarea>
									</div>
								</div>								   
							</div>

							<!-- right column -->
							<div class="col-sm-6">
								<div class="form-group">
									<label for="sessions" class="col-sm-2 control-label">Sessies</label>
									<div class="col-sm-10">
										<input type="text" class="form-control required" name="sessions" id="sessions" placeholder="Sessions" required/>			                        
									</div>			                    
								</div>

								<div class="form-group">
									<label for="meeting_days" class="col-sm-2 control-label">Lesdagen</label>
									<div class="col-sm-10">
										<input type="text" class="form-control required" name="meeting_days" id="meeting_days" placeholder="Meeting Days" required/>		                        
									</div>			                    
								</div>

								<div class="form-group">
									<label for="new_bol_require" class="col-sm-2 control-label">Prijs</label>
									<div class="col-sm-4">
										<input type="text" class="form-control required" name="price" id="price" placeholder="$0.00" required/>	
									</div>
									<label for="new_pod_require" class="col-sm-2 control-label">SBU</label>
									<div class="col-sm-4">
										<input type="text" class="form-control required" name="sbu" id="sbu" required/>	
									</div>
								</div>
								
								<div class="form-group">
									<label for="level" class="col-sm-2 control-label">Niveau</label>
									<div class="col-sm-4">
										<input type="text" class="form-control required" name="level" id="level" required/>	
									</div>
									<label for="exam" class="col-sm-2 control-label">Examen</label>
									<div class="col-sm-4">
										<input type="text" class="form-control required" name="exam" id="exam" required/>	
									</div>
								</div>

								<div class="form-group">
									<label for="incasso" class="col-sm-2 control-label">Incasso</label>
									<div class="col-sm-10">
										<input type="text" class="form-control required" name="incasso" id="incasso" placeholder="Incasso" required/>			                        
									</div>			                    
								</div>
								
								<div class="form-group">
									<label for="preliminary_knowledge" class="col-sm-2 control-label">Vereiste voorkennis</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="preliminary_knowledge" id="preliminary_knowledge" required></textarea>			                        
									</div>			                    
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label" for="goal" >Doel</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="goal" id="goal" required></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="target_audience" >Doelgroep</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="target_audience" id="target_audience" required></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-2 control-label" for="literature" >Literatuur</label>
									<div class="col-sm-10">
										<textarea class="form-control required mceEditor" name="literature" id="literature" required></textarea>
									</div>
								</div>

								<div class="form-group">
									<label for="status" class="col-sm-2 control-label">Status</label>
									<div class="col-sm-4">
										<select class="form-control" name="status" id="status">
											<option value="open">Open</option>
											<option value="close" selected="selected">Gesloten</option>
										</select>
									</div>
								</div>
								<div class="form-group form-group-sm" style="display:none;">
									<!-- left column -->
									<div class="col-sm-12">
										<div class="form-group">
											<label  class="col-sm-2 control-label" for="picture_preview">Voorbeeld afbeelding</label>
											<div class="col-sm-10">
												<img class="picture_preview img-responsive col-md-6" />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group form-group-sm">
									<!-- left column -->
									<div class="col-sm-12">
										<div class="form-group">
											<label  class="col-sm-2 control-label" for="picture">Afbeelding</label>
											<div class="col-sm-10">
												<input type="file" class="form-control required" name="picture" id="picture" required/>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
								    <label for="status" class="col-sm-2 control-label">Scholing</label>
								    <div class="col-sm-10">
										<select class="groups-edit-multiple" multiple="multiple" name="course[]" id="course">
										<?php foreach ($courses as $k => $v): ?>
										<option value="<?= $v->id; ?>"><?= $v->name; ?></option>
										<?php endforeach; ?>
										</select>
								    </div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label" for="category" >Categorieën</label>
									<div class="col-sm-10">
										<select class="cat-edit-multiple" multiple="multiple" name="category[]" id="category">
										<?php foreach ($categories as $category) : ?> 
										<option value="<?= $category->id; ?>"><?= $category->name; ?></option>;
										<?php endforeach; ?>
										</select>
									</div>
								</div>	
								<div class="form-group">
									<label for="teacher" class="col-sm-3 control-label"> Docent</label>
									<div class="col-sm-9">
										<select class="form-control cat-edit-multiple" multiple="multiple" name="teacher" id="teacher">
										<?php
											foreach ( $teachers as $teacher ) {
										?>
												<option value="<?= $teacher->did; ?>"><?= $teacher->name ?></option>
										<?php
										}
										?>
										</select>
									</div>
								</div>
								
							</div>
						</div>
					  <!-- Modal Footer -->
						<div class="modal-footer">
							<button state=0 id="ModuleSubmit" type="button" class="btn btn-default">Module toevoegen</button>
						</div><!-- End Modal Footer -->
					</form>
				</div> <!-- End modal body div -->
			</div> <!-- End modal content div -->
		</div> <!-- End modal dialog div -->
	</div> <!-- End modal div -->
<?php
	return ob_get_clean();
}


function bivt_admin_template()
{
	global $current_user, $wpdb;
	$user_id = get_current_user_id();

	$row = $wpdb->get_row("SELECT * FROM users_groups WHERE user_id = $user_id AND group_id = 1");
	$url =  home_url();
	$path = dirname(__FILE__);
		if (  $user_id != 1 && $row == NULL || !is_user_logged_in() ) {
		     echo'<script> window.location="'.$url.'"; </script> ';
		}

	$action = (isset($_GET['action'])) ? $_GET['action'] : '';
	$path = dirname(__FILE__);

	switch ( $action ) {
		case 'student_overview':
			include $path . '/custom-template/admin/wpcc-admin-users.php';				
			break;
		case 'student_groups':
			include $path . '/custom-template/admin/wpcc-admin-student-groups.php';
			break;
		case 'student_enrolments':
			include $path . '/custom-template/admin/wpcc-admin-student-enrolments.php';
			break;
		case 'export':
			include $path . '/custom-template/admin/wpcc-admin-export.php';
			break;
		case 'student_info':
			include $path . '/custom-template/admin/wpcc-admin-student-info.php';
			break;
		case 'teacher_overview':
			include $path . '/custom-template/admin/wpcc-admin-users.php';
			break;
		case 'teacher_invoices':
			include $path . '/custom-template/admin/wpcc-admin-teacher-invoices.php';
			break;  
		case 'users_overview':
			include $path . '/custom-template/admin/wpcc-admin-users.php';
			break;
		case 'user_profile':
			include $path . '/custom-template/admin/wpcc-admin-user-profile.php';
			break;
		case 'courses_courses':
			include $path . '/custom-template/admin/wpcc-admin-courses.php';
			break;
		case 'courses_modules':
			include $path . '/custom-template/admin/wpcc-admin-modules.php';
			break;
		case 'courses_categories':
			include $path . '/custom-template/admin/wpcc-admin-cats.php';
			break;
		case 'planning_instances':
			include $path . '/custom-template/admin/wpcc-admin-instances.php';
			break;
		case 'planning_schedule':
			include $path . '/custom-template/admin/wpcc-admin-schedule.php';
			break;
		case 'instance_menu':
			include $path . '/custom-template/admin/wpcc-admin-instances-view.php';
			break;
		case 'enrolment':
			include $path . '/custom-template/admin/wpcc-admin-enrolment.php';
			break;
					
		case 'materials':
			include $path . '/custom-template/admin/wpcc-admin-materials.php';
			break; 
			
		case 'assignments':
			include $path . '/custom-template/admin/wpcc-admin-assignments.php';
			break;
		case 'import':
			include $path . '/custom-template/admin/wpcc-admin-import.php';
			break;
		case 'news':			
			if (isset($_GET['action']) && isset($_GET['newsitems'])) {
				include $path . '/custom-template/admin/wpcc-admin-header.php';
				include $path . '/custom-template/pages/newsitems.php';
			} else {
				include $path . '/custom-template/admin/wpcc-admin-news.php';
			}	
			break;
		case 'locations':

			include $path . '/custom-template/admin/wpcc-admin-locs.php';
			break;

		case 'accreditations':
			if (isset($_GET['action']) && isset($_GET['accreditaties_id'])) {
				include $path . '/custom-template/admin/wpcc-admin-header.php';
				include $path . '/custom-template/pages/accreditaties.php';
			} else {
				include $path . '/custom-template/admin/wpcc-admin-acc.php';
			}
			break;

		case 'settings_menu':
			include $path . '/custom-template/admin/wpcc-admin-settings.php';
			break;

		case 'materials_menu':
			include $path . '/custom-template/admin/wpcc-admin-materials.php';
			break;
		
		default:
			# code...
			break;
	}

}

function bivt_teacher_template()
{
	global $current_user, $wpdb;
	$user_id = get_current_user_id();

	$row = $wpdb->get_row("SELECT * FROM users_groups WHERE user_id = $user_id AND group_id = 5");
	$url =  home_url();
	$path = dirname(__FILE__);
		if (  $user_id != 1 && $row == NULL || !is_user_logged_in() ) {
		     echo'<script> window.location="'.$url.'"; </script> ';
		}

	$action = $_GET['action'];
	$path = dirname(__FILE__);

	switch ( $action ) {

		case 'profile':
			include $path . '/custom-template/admin/wpcc-admin-user-profile.php';
			break;
		
		case 'courses':
			if (isset($_GET['action']) && isset($_GET['courses'])) {
				include $path . '/custom-template/teacher/wpcc-teacher-header.php';
				include $path . '/custom-template/pages/courses.php';
			} else {
				include $path . '/custom-template/teacher/wpcc-teacher-courses.php';
			}
			break;
			
		case 'schedule':
			include $path . '/custom-template/teacher/wpcc-teacher-schedule.php';
			break;

		case 'homework':
			include $path . '/custom-template/teacher/wpcc-teacher-homework.php';
			break;

		case 'materials':
			include $path . '/custom-template/admin/wpcc-admin-materials.php';
			break;
				
		case 'assignments':
			include $path . '/custom-template/admin/wpcc-admin-assignments.php';
			break;
			
		default:
		# code...
			break;
	}

}

function bivt_student_template()
{
	global $current_user, $wpdb;
	$user_id = get_current_user_id();

	$row = $wpdb->get_row("SELECT * FROM users_groups WHERE user_id = $user_id AND group_id = 3");
	$url =  home_url();
	$path = dirname(__FILE__);
		if (  $user_id != 1 && $row == NULL || !is_user_logged_in() ) {
		     echo'<script> window.location="'.$url.'"; </script> ';
		}

	$action = $_GET['action'];
	$path = dirname(__FILE__);

	switch ( $action ) {
		case 'profile':
			include $path . '/custom-template/admin/wpcc-admin-user-profile.php';
			break;

		case 'financial':
			include $path . '/custom-template/student/wpcc-student-financial.php';
			break;

		case 'portfolio':
			include $path . '/custom-template/student/wpcc-student-portfolio.php';
			break;
		
		case 'portfolio-item':
			include $path . '/custom-template/student/wpcc-student-portfolio-item.php';
			break;
			
		case 'schedule':
			include $path . '/custom-template/student/wpcc-student-schedule.php';
			break;
			
		default:
			# code...
			break;
	}

}

function GetBetween($var1="",$var2="",$pool){
	$temp1 = strpos($pool,$var1)+strlen($var1);
	$result = substr($pool,$temp1,strlen($pool));
	$dd=strpos($result,$var2);

	if($dd == 0){
		$dd = strlen($result);
	}

	return substr($result,0,$dd);
}

function randomStrings() {
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function randPic(){
	$rand = rand(1, 3);
	$string='';
	switch ($rand) {
		case 1:
				$string= "//s3-us-west-2.amazonaws.com/s.cdpn.io/82/submerged.jpg";
			break;
		case 2:
				$string= "//s3-us-west-2.amazonaws.com/s.cdpn.io/82/orange-tree.jpg";
			break;
		case 3:
				$string= "//s3-us-west-2.amazonaws.com/s.cdpn.io/82/drizzle.jpg";
			break;	
		default:
			# code...
			break;
	}
	return $string;
}

function limit_text($text, $limit) {
    $strings = $text;
      if (strlen($text) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          if(sizeof($pos) >$limit)
          {
            $text = substr($text, 0, $pos[$limit]) . '...';
          }
          return $text;
      }
      return $text;
    }
    
/**
 * 
 * @return mixed  string| null Role part of permalink (e.g. 'admin', 'teacher'), null if not found
 */    
function get_perma_role () {
	$permalink_parts = explode('/', get_permalink());
	return sizeof($permalink_parts > 2) ? $permalink_parts[sizeof($permalink_parts)-2] : null;
}

/**
 * Redirect the page to the supplied URL, even if headers are already loaded.
 * @param string $url
 */
function bivt_redirect($url)
{
	$string = '<script type="text/javascript">';
	$string .= 'window.location = "' . $url . '"';
	$string .= '</script>';

	echo $string;
}

/**
 * Retrieve a list of Student that enrolled for the instance given by the parameter
 * @param int $instance_id ID of the instance for which enrolled students should be found
 * @return Student[] Array of Student found
 */
function get_students_by_instance_id($instance_id) {
	global $wpdb;

	$query_results = $wpdb->get_results("SELECT oim2.meta_value AS user_id FROM wp_woocommerce_order_itemmeta AS oim1 LEFT JOIN wp_woocommerce_order_itemmeta AS oim2 ON oim1.order_item_id = oim2.order_item_id WHERE oim1.meta_key = '_instance_id' AND oim1.meta_value='" . $instance_id . "' AND oim2.meta_key='_useraccount'");
	$results = [];
	foreach ($query_results AS $k=>$v) {
		$results[$v->user_id] = new Student($v->user_id);
	}
	return $results;
}

/**
 * Retrieve a list of Student that enrolled for the day given by the parameter
 * @param int|string $day_id ID of the day for which enrolled studens should be found
 * @return Student[] Array of Student found
 */
function get_students_by_day_id ($day_id) {
	global $wpdb;

	$query_results = $wpdb->get_results("SELECT oim2.meta_value AS user_id FROM wp_woocommerce_order_itemmeta AS oim1 LEFT JOIN wp_woocommerce_order_itemmeta AS oim2 ON oim1.order_item_id = oim2.order_item_id WHERE oim1.meta_key = '_days_instance' AND INSTR(oim1.meta_value,'" .$day_id . "') AND oim2.meta_key='_useraccount'");
	$results = [];
	foreach ($query_results AS $k=>$v) {
		$results[$v->user_id] = new Student($v->user_id);
	}
	return $results;
}


/**
 * Retrieve a list of Student that enrolled for the day given by the parameter, including
 * the attendance
 * @param int|string $day_id ID of the day for which enrolled studens should be found
 * @return Student[] Array of Student found
 */
function get_attendance_by_day_id ($day_id) {
	$results = get_students_by_day_id($day_id);
	foreach ($results AS $k=>$v) {
		$v->getAttendanceDays ();
	}
	return $results;
}
/**
  * Retrieve a list of modules sorted alphabetically by name
*/ 
function get_all_modules() {
	global $wpdb;

	$query_results = $wpdb->get_results("SELECT * FROM modules ORDER BY name");

	return $query_results;
}
