<?php
/**
 * News Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */


add_action('init','add_registration',1);

function add_registration(){

	global $wpdb;
	if ( isset($_GET['token_key']) && isset($_GET['AddCartId']) && isset($_GET['user_id'])) {

		$user  = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."users WHERE user_activation_key = '".$_GET['token_key']."'"  );
		$login = wp_signon(array('user_login'=>$user->user_login,'user_password'=>$_GET['generic']));

		$url = home_url() .'/cart/?AddCartId=' . $_GET['AddCartId'] . '&quantity=1&user_id='.$_GET['user_id'].'&days='.$_GET['days'] ;
		echo "<script>window.location = '" . $url ."';</script>";
	}
}

add_action( 'wp_ajax_bivt_visitor_add', 'bivt_visitor_add' );
add_action( 'wp_ajax_nopriv_bivt_visitor_add', 'bivt_visitor_add' );


function bivt_import_xls() {

	        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/xml/';

	        if(!file_exists($targetDirectory))
	            mkdir($targetDirectory, 0777, true);
			
	        //upload the image
			$imageFileType = pathinfo(basename($_FILES['xls']['name']),PATHINFO_EXTENSION);

	        $newFileName = sprintf('%s.%s',date('Ymd_His'),$imageFileType);

			$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);
			move_uploaded_file($_FILES['xls']['tmp_name'],$targetFile);

			echo json_encode(['success'=>1]);
			wp_die();

}

add_action( 'wp_ajax_bivt_import_xls', 'bivt_import_xls' );
add_action( 'wp_ajax_nopriv_bivt_import_xls', 'bivt_import_xls' );

function sett_html_content_type() {
	return 'text/html';
}

function repeatRegistration($originalUsername,$createUsername,$index,$postArray){
	
	global $wpdb;

	$checkUsername = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}users WHERE user_login='{$createUsername}'");
	if(count($checkUsername)>0){

		$createUsername = $originalUsername;
		$createUsername=$createUsername.$index;
		$index++;
		repeatRegistration($originalUsername,$createUsername,$index,$postArray);
		
	}else{

		$hash = md5( rand(0,1000) );
		$randomStrings = randomStrings();
		$wpdb->insert(
		'wp_users', 
			array(
				'user_login' => $createUsername,
				'user_pass' => wp_hash_password( $randomStrings ),
				'user_nicename' => $postArray['email'],
				'user_email' => $postArray['email'],
				'display_name' => $postArray['email'],
				'user_registered' => date('Y-m-d h:i:s'),
				'user_activation_key' => $hash,
				'user_status' => 0,
				'modified' => date('Y-m-d h:i:s')
			)
		);


		$user_id_role = new WP_User($wpdb->insert_id);
		$user_id_role->set_role('bivt');
		$url = home_url() .'/registration/?AddCartId='.$postArray['AddCartId'].'&quantity=1&token_key='.$hash.'&days='.$postArray['days']. '&user_id='.$wpdb->insert_id.'&generic='.$randomStrings;


		$wpdb->insert(
		    'users_groups',
		    array(
		      'user_id' => $wpdb->insert_id,
		      'group_id' => 3 // Student
		    )
		);


		$to = (isset($postArray['email'])) ? $postArray['email'] : '';
		$subject = 'Bivt - registratie';
		$body = "<p>Beste gebruiker". ", </p><br><br>";
		$body .= "<p>Hartelijk dank voor je interesse en registratie bij BivT, deze is nodig om je in te schrijven voor ons aanbod. Om de registratie af te ronden moet je op onderstaande link klikken:</p><br>";
		$body .= "<p>".$url." </p><br>";
		$body .= "<p>Hierna kun je je inschrijving(en) afronden.</p><br>";
		$body .= "<p>Je gebruikersnaam is: ".$postArray['email']."</p><br>";
		$body .= "<p>Je (voorlopige) wachtwoord is: ".$randomStrings."</p>";
		$body .= "<p>Mochten er vragen zijn, neem dan gerust contact met ons op via " .'<a hrf="mailto:info@bivt.nl">info@bivt.nl</a>' . " of ons telefoonnummer: 0251-222210.</p><br>";
		$body .= "<p>Met vriendelijke groet,</p><br>";
		$body .= "<p>BivT</p><br>";
		

		/*
		wp_mail( $to, $subject, $body);
		*/

		add_filter( 'wp_mail_content_type','set_html_content_type');
		$headers = 'From: Bivt - Registration <no-reply@nl.tinotech.eu>' . "\r\n";

		$mailSetting = array('to'=>$to,
							'subject'=>$subject,
							'message'=>$body,
							'headers'=>$headers);

		$response['status'] = 'ok';
		if($to!=''){
			$blacklist = array(
					'127.0.0.1',
					'::1'
			);
			
			if(!in_array($_SERVER['REMOTE_ADDR'], $blacklist)){
				wp_mail($to, $mailSetting['subject'],$mailSetting['message'],$mailSetting['headers']);
			}
			else {
				$response['status'] = 'ok';
				$response['errors'] = 'Cannot send mail from local host';
			}
		}

		remove_filter( 'wp_mail_content_type', 'set_html_content_type');

		return $response;
		
		
	}


}
function bivt_visitor_add() {
	
	global $wpdb; // this is how you get access to the database

	$response = ['status'=>'not ok','errors'=>''];

	parse_str($_POST['form'],$_POST);

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');


	$u = new \wp_manager_course\UserValidator();


	$u->make($_POST,[
		'email' => ['required','validateEmail']
	]);


	if($u->fails()){
		$response['errors'] = $u->errors();
		
	}else{

		$createUsername = trim(isset($_POST['email']) ? $_POST['email'] : "Nieuwe gebruiker");
		$originalUsername = $createUsername;
		$index = 1;

		$response = repeatRegistration($originalUsername,$createUsername,$index,$_POST);
	}

	echo json_encode($response);

	wp_die();
	
}


add_action( 'wp_ajax_bivt_admin_users_create', 'bivt_admin_users_create' );
add_action( 'wp_ajax_nopriv_bivt_admin_users_create', 'bivt_admin_users_create' );

function bivt_admin_users_create (){

	global $wpdb;
	
	if(isset($_POST)) {
	
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');
		
		$cv = new \wp_manager_course\UserValidator();
		$dataValidation = [
				'email' => ['required','validateEmail'],
				'group' => ['required', 'validateGroups'],
		];		
		
		$cv->make($_POST,$dataValidation);
	
		// Groups selected by user
		$groups = explode(',', $_POST['group']);

		$all_groups = $wpdb->get_results("SELECT id FROM groups");
		
		// Groups selected by user
		$groups = explode(',', $_POST['group']);
		
		$msg = '';
		$valid = sizeof($groups) > 0;
		foreach ($groups AS $value) {
			$msg .= 'Checking ' . $value;
			// Check whether each supplied group is a database group
			$found = false;
			foreach ($all_groups AS $k=>$v) {
				$msg .= ' against ' . $v->id;
				$found = $found || $value==$v->id;
			}
			$valid = $valid && $found;
		}
		
		
		if($cv->fails() || sizeof($groups) < 1) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
					'groups'=>$groups,
					'valid'=>$valid,
					'all_groups'=>$all_groups,
					'msg'=>$msg,
			]);
			die ();
		}
	
		$table = 'wp_users';
		$data = null;
	
		$data = [
				'user_login' => $_POST['email'],
				'user_email' => $_POST['email'],
				'display_name' => $_POST['email'],
				'role' => 'bivt',
		];
		
		$user_id = wp_insert_user ($data);
		if (is_wp_error($user_id)) {
			echo json_encode([
					'success'=>0,
					'errors'=>['Error creating WP_User'],
					'details'=>$user_id->get_error_message(),
			]);
			die ();				
		}

		// Create the group entries for this user
		foreach ($groups AS $g) {
			$wpdb->insert ('users_groups', ['user_id' => $user_id, 'group_id' => $g]);
		}
		
		$data = ['ID' => $user_id];
		$user_id = wp_update_user($data);	// Trigger an e-mail to be sent
		if (is_wp_error($user_id)) {
			echo json_encode([
					'success'=>0,
					'errors'=>['Error update WP_User after creation'],
					'details'=>$user_id->get_error_message(),
			]);
			die ();
		}

	}
	echo json_encode([
			'success'=>1,
			'errors'=>[],
			'data'=>$_POST,
			'table'=>$table,
			'table_data'=>$data,
			'format'=>$format,
			'where'=>$where,
	]);
	die ();	
}

add_action( 'wp_ajax_bivt_delete_location', 'bivt_delete_location' );
add_action( 'wp_ajax_nopriv_bivt_delete_location', 'bivt_delete_location' );

function bivt_delete_location(){

	$response = ['status'=>'ok','errors'=>''];
	$locationId = absint($_GET['id']);

	global $wpdb;

	$checkLocation = $wpdb->get_row("SELECT * FROM locations WHERE id = ".$locationId,ARRAY_A);

	if(count($checkLocation)>0){

		//get thumbnail link and picture to delete
		if($checkLocation['picture']!='')
		{
			$str = $checkLocation['picture'];

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';

			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);

			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkLocation['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}
			$wpdb->delete('locations',array('id'=>$locationId),'%d');
			$response['status'] = 'ok';

		}

	}

	echo json_encode($response);
	wp_die();

}





add_action( 'wp_ajax_bivt_delete_changerequest', 'bivt_delete_changerequest' );
add_action( 'wp_ajax_nopriv_bivt_delete_changerequest', 'bivt_delete_changerequest' );


function bivt_delete_changerequest(){

	$response = ['status'=>'ok','errors'=>''];

	$requestId = abs($_GET['id']);

	global $wpdb;

	$checkRequest = $wpdb->get_row("SELECT * FROM changerequests WHERE id = {$requestId}");

	if(count($checkRequest)>0){
		$wpdb->delete('changerequests',array('id'=>$requestId),'%d');
	}else{
		$response['status'] = 'not ok';
	}
	
	echo json_encode($response);

	wp_die();
}




add_action( 'wp_ajax_bivt_delete_related_day', 'bivt_delete_related_day' );
add_action( 'wp_ajax_nopriv_bivt_delete_related_day', 'bivt_delete_related_day' );


function bivt_delete_related_day(){

	$response = ['status'=>'ok','errors'=>''];

	$dayId = abs($_GET['id']);

	global $wpdb;

	$checkDay = $wpdb->get_row("SELECT * FROM days WHERE id = ".$dayId);


	if(count($checkDay)>0){

		$wpdb->delete('days',array('id'=>$dayId),'%d');
		$wpdb->delete('docents_days',array('day_id'=>$dayId),'%d');
		$response['status'] = 'ok';
	}


	echo json_encode($response);



	wp_die();
}



add_action( 'wp_ajax_bivt_add_location', 'bivt_add_location' );
add_action( 'wp_ajax_nopriv_bivt_add_location', 'bivt_add_location' );

function bivt_add_location(){

	if(isset($_POST))
	{
		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/LocationValidator.php');

		$lv = new \wp_manager_course\LocationValidator();

		$dataValidation = [
			'name' => ['required'],
			'address' => ['required'],
			'status' => ['required','validateStatus']
		];

		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];

		$lv->make($_POST,$dataValidation);

		if($lv->fails())
			echo json_encode(['errors'=>$lv->errors()]);
		else
		{
			global $wpdb;

			//begin upload scripts here
	        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/location_pictures/';

	        if(!file_exists($targetDirectory))
	            mkdir($targetDirectory, 0777, true);
			

	        //upload the image
			$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
	        //do not delete in case it will be used

	        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

	        // $newFileName = md5(time()) . '.' . $imageFileType;
			//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
			$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

			//if successfully uploaded the file, create now the thumbnail
			if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
			{

				//upload too the thumbnail picture
				include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

				$thumb = new easyphpthumbnail;

				$thumb->Chmodlevel = '0777';
				
				$thumb->Thumblocation = $targetDirectory;
				
				$thumb->Thumbprefix = 'thumb_';
				
				$thumb -> Createthumb($targetFile,'file');

			}

			//insert now the news item
			$wpdb->insert(
			'locations', 
			array(
				'name' => $_POST['name'],
				'address' => $_POST['address'],
				'status' => $_POST['status'],
				'picture' => '/files/uploads/location_pictures/' . $newFileName
			), 
			array(
				'%s',
				'%s',
				'%s',
				'%s'
			)
			);

			echo json_encode(['success'=>1]);
		}

	} 

	wp_die();
}










add_action( 'wp_ajax_bivt_edit_location', 'bivt_edit_location' );
add_action( 'wp_ajax_nopriv_bivt_edit_location', 'bivt_edit_location' );

function bivt_edit_location(){

	if(isset($_POST))
	{
		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/LocationValidator.php');

		$lv = new \wp_manager_course\LocationValidator();

		$dataValidation = [
			'location_id' => ['required','validateLocation'],
			'name' => ['required'],
			'address' => ['required'],
			'status' => ['required','validateStatus']
		];


		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];


		$lv->make($_POST,$dataValidation);

		if($lv->fails())
			echo json_encode(['errors'=>$lv->errors()]);
		else
		{
			global $wpdb;

			$id = $_POST['location_id'];

			$dataUpdate = [
				'name' => $_POST['name'],
				'address' => $_POST['address'],
				'status' => $_POST['status']
			];


			if($_POST['picture']!='')
			{


				$checkLocation = $wpdb->get_row("SELECT picture FROM locations WHERE id = $id",ARRAY_A);

				//get thumbnail link and picture to delete
				if($checkLocation['picture']!='')
				{
					$str = $checkLocation['picture'];

					$result = explode('/', $str);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);

					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkLocation['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}
				}
				//start
				//begin upload scripts here
		        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/location_pictures/';

		        if(!file_exists($targetDirectory))
		            mkdir($targetDirectory, 0777, true);
				
		        //upload the image
				$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
		        //do not delete in case it will be used

		        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

		        // $newFileName = md5(time()) . '.' . $imageFileType;
				//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
				$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

				//if successfully uploaded the file, create now the thumbnail
				if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
				{

					//upload too the thumbnail picture
					include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

					$thumb = new easyphpthumbnail;

					$thumb->Chmodlevel = '0777';
					
					$thumb->Thumblocation = $targetDirectory;
					
					$thumb->Thumbprefix = 'thumb_';
					
					$thumb -> Createthumb($targetFile,'file');

				}
				//end
				$dataUpdate['picture'] = '/files/uploads/location_pictures/' . $newFileName;

			}

			$wpdb->update(
			'locations', 
			$dataUpdate,
			array( 'id' => $id )
			);

			echo json_encode(['success'=>1]);
		}

	} 

	wp_die();
}




add_action( 'wp_ajax_bivt_edit_related_day', 'bivt_edit_related_day' );
add_action( 'wp_ajax_nopriv_bivt_edit_related_day', 'bivt_edit_related_day' );



function bivt_edit_related_day(){



	$response = ['status'=>'not ok','errors'=>''];

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/RelatedDaysValidator.php');


	$form = (isset($_POST['form'])) ? $_POST['form'] : '';

	parse_str($form,$_POST);

	$rd = new \wp_manager_course\RelatedDaysValidator();

	$rd->make($_POST,[
		'datum' => ['required','validateDate'],
		'instance_id'=>['required','validateInstance'],
		'location_id' => ['required','validateLocation'],
		'docent_id' => ['required','validateDocent'],
		'day_id' => ['required','validateDay']
	]);

	if($rd->fails()){
		$response['errors'] = $rd->errors();
		
	}else{

		$response['status']  = 'ok';
		global $wpdb;


		$wpdb->update(
		'days',
		array(
			'datum' => date('Y-m-d h:i:s',strtotime($_POST['datum'])),
			'instance_id' => $_POST['instance_id'],
			'location_id' => $_POST['location_id'],
			'modified' => date('Y-m-d h:i:s')

		),
		array('id'=>$_POST['day_id']),
		array(
		'%s',
		'%d',
		'%d',
		'%s'
		),
		array('%d')
		);
		


		$wpdb->update(
		'docents_days',
		array(
			'docent_id'=>$_POST['docent_id']
		),
		array('day_id'=>$_POST['day_id']),
		array(
			'%d'
		),
		array('%d')
		);


	}

	echo json_encode($response);

	wp_die();


}


add_action( 'wp_ajax_bivt_edit_instance', 'bivt_edit_instance' );
add_action( 'wp_ajax_nopriv_bivt_edit_instance', 'bivt_edit_instance' );


function bivt_edit_instance(){

	$response = ['status'=>'not ok','errors'=>''];

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/InstanceValidator.php');

	$form = (isset($_POST['form'])) ? $_POST['form'] : '';
	parse_str($form,$_POST);


	$iv = new \wp_manager_course\InstanceValidator();

	$iv->make($_POST,[
		'instance_id' => ['required','validateInstance'],
		'module_id' => ['required','validateModule'],
		'group' => ['required'],
		'full' => ['required','validateFull'],
		'status' => ['required','validateStatus'],
		'seats_available' => ['required'],
	]);

	if($iv->fails()){
		$response['errors'] = $iv->errors();
		$response['results'] = $_POST;
		
	}else{

		global $wpdb;
		$response['status'] = 'ok';


		$updates = $wpdb->update(
		'instances',
		array(
			'module_id' => $_POST['module_id'],
			'group' => $_POST['group'],
			'full' => $_POST['full'],
			'status' => $_POST['status'],
			'group_size' => $_POST['seats_available'],
			'modified' => date('Y-m-d H:i:s')

		),
		array('id'=>$_POST['instance_id'])
		);
		

		$response['wpdb_errors'] = $wpdb->last_error;
		$response['statuss'] = $updates;

	}
	echo json_encode($response);
	wp_die();
}




add_action( 'wp_ajax_bivt_add_instance', 'bivt_add_instance' );
add_action( 'wp_ajax_nopriv_bivt_add_instance', 'bivt_add_instance' );
function bivt_add_instance(){
	parse_str($_POST['form'],$_POST);
	$response = ['status'=>'not ok','errors'=>''];
	var_dump($_POST);

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/InstanceValidator.php');



	$rd = new \wp_manager_course\InstanceValidator();

	$rd->make($_POST,[
		'module_id' => ['required','validateModule'],
		'group' => ['required'],
		'full' => ['required','validateFull'],
		'status' => ['required','validateStatus'],
		'seats_available' => ['required'],
	]);

	if($rd->fails()){
		$response['errors'] = $rd->errors();
		$response['results'] = $_POST;
		
	}else{

		global $wpdb;

		$response['status'] = 'ok';

//INSERT INTO `bivt_db`.`instances` (`id`, `created`, `modified`, `module_id`, `comments`, `group`, `full`, `status`) VALUES (NULL, '2016-12-28 00:00:00', '2016-12-14 00:00:00', '12', NULL, 'asdfasdf', '0', 'open');



		$reso = $wpdb->insert(
		'instances',
		array(
			'module_id' => $_POST['module_id'],
			'group' => $_POST['group'],
			'comments' => '',
			'full' => $_POST['full'],
			'status' => $_POST['status'],
			'group_size' => $_POST['seats_available'],
			'created' => date('Y-m-d h:i:s'),
			'modified' => date('Y-m-d h:i:s')
		)
		);


	}
	echo json_encode($response);
	wp_die();
}



add_action( 'wp_ajax_bivt_add_related_days', 'bivt_add_related_days' );
add_action( 'wp_ajax_nopriv_bivt_add_related_days', 'bivt_add_related_days' );

function bivt_add_related_days(){

	$response = ['status'=>'not ok','errors'=>''];

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/RelatedDaysValidator.php');

	$form = (isset($_POST['form'])) ? $_POST['form'] : '';

	parse_str($form,$_POST);

	$rd = new \wp_manager_course\RelatedDaysValidator();

	$rd->make($_POST,[
		'datum-add' => ['required','validateDate'],
		'instance_id'=>['required','validateInstance'],
		'location_id' => ['required','validateLocation'],
		'docent_id' => ['required','validateDocent']
	]);




	if($rd->fails()){
		$response['errors'] = $rd->errors();
		$response['results'] = $_POST;
		
	}else{

		global $wpdb;
		$response['status'] = 'ok';


		$wpdb->insert(
		'days',
		array(
			'datum' => date('Y-m-d h:i:s',strtotime($_POST['datum-add'])),
			'instance_id' =>$_POST['instance_id'],
			'location_id' =>$_POST['location_id'],
			'created' => date('Y-m-d h:i:s'),
			'modified' => date('Y-m-d h:i:s')
		), 
		array( 
			'%s',
			'%d',
			'%d',
			'%s',
			'%s'
		)
		);
		

		$dayId = $wpdb->insert_id;

		$wpdb->insert(
		'docents_days',
		array(
			'docent_id'=>$_POST['docent_id'],
			'day_id' =>$dayId,
		),
		array(
			'%d',
			'%d'
		)
		);


	}
	echo json_encode($response);

	wp_die();
}


add_action( 'wp_ajax_bivt_admin_news', 'bivt_admin_add_news' );
add_action( 'wp_ajax_nopriv_bivt_admin_news', 'bivt_admin_add_news' );
function bivt_admin_add_news() {
	
	global $wpdb; // this is how you get access to the database

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/NewsValidator.php');


	$nv = new \wp_manager_course\NewsValidator();

	
	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

	$nv->make($_POST,[
		'title' => ['required'],
		'content' => ['required'],
		'picture' => ['validateFile','validateFileSize']
	]);


	if($nv->fails()) {
		echo json_encode(['errors'=>$nv->errors()]);
	}
	else
	{
		global $wpdb;


		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/newsitem_pictures/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}

		//insert now the news item
		$wpdb->insert(
		'newsitems', 
		array(
			'title' => $_POST['title'],
			'author' => 0,
			'date' => date('Y-m-d'),
			'slug' => strtolower(str_replace(' ', '-',$_POST['title'])),
			'content' => $_POST['content'],
			'picture' => '/files/uploads/newsitem_pictures/' . $newFileName,
			'created' => date('Y-m-d h:i:s'),
			'modified' => date('Y-m-d h:i:s')
		), 
		array( 
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
		) 
		);

		echo json_encode(['success'=>1]);
	}

	wp_die();

}

add_action( 'wp_ajax_bivt_edit_news', 'bivt_admin_edit_news' );
add_action( 'wp_ajax_nopriv_bivt_edit_news', 'bivt_admin_edit_news' );


function bivt_admin_edit_news() {

	if(isset($_POST))
	{

		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/NewsValidator.php');

		$nv = new \wp_manager_course\NewsValidator();


		$dataValidation = [
			'id' => ['required','validateId'],
			'title' => ['required'],
			'content' => ['required']
		];

		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];

		$nv->make($_POST,$dataValidation);

		if($nv->fails())
			echo json_encode(['errors'=>$nv->errors()]);
		else
		{
			global $wpdb;
			
			$id = $_POST['id'];

			$dataUpdate = [
				'title' => $_POST['title'],
				'content' => $_POST['content'],
				'modified' => date('Y-m-d h:i:s')
			];


			if($_POST['picture']!='')
			{

				$id = $_POST['id'];

				$checkNews = $wpdb->get_row("SELECT picture FROM newsitems WHERE id = $id",ARRAY_A);

				//get thumbnail link and picture to delete
				if($checkNews['picture']!='')
				{
					$str = $checkNews['picture'];

					$result = explode('/', $str);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);


					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkCategory['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];


					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}
				}
				//start

				//begin upload scripts here
		        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/newsitem_pictures/';

		        if(!file_exists($targetDirectory))
		            mkdir($targetDirectory, 0777, true);
				

		        //upload the image
				$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
		        //do not delete in case it will be used

		        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

		        // $newFileName = md5(time()) . '.' . $imageFileType;
				//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
				$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

				//if successfully uploaded the file, create now the thumbnail
				if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
				{

					//upload too the thumbnail picture
					include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

					$thumb = new easyphpthumbnail;

					$thumb->Chmodlevel = '0777';
					
					$thumb->Thumblocation = $targetDirectory;
					
					$thumb->Thumbprefix = 'thumb_';
					
					$thumb -> Createthumb($targetFile,'file');

				}
				//end

				$dataUpdate['picture'] = '/files/uploads/newsitem_pictures/' . $newFileName;
				
			}

			$wpdb->update(
			'newsitems', 
			$dataUpdate,
			array( 'id' => $id )
			);

			echo json_encode(['success'=>1]);
		}

	}

	wp_die();

}

add_action( 'wp_ajax_bivt_admin_dnews', 'bivt_admin_delete_news' );
add_action( 'wp_ajax_nopriv_bivt_admin_dnews', 'bivt_admin_delete_news' );
function bivt_admin_delete_news() {
	global $wpdb;

	$id = intval($_POST['id']);

	$checkNews = $wpdb->get_var("SELECT COUNT(*) FROM newsitems WHERE id = $id");

	if($checkNews>0)
	{

		$checkNews = $wpdb->get_row("SELECT picture FROM newsitems WHERE id = $id",ARRAY_A);

		//get thumbnail link and picture to delete
		if($checkNews['picture']!='')
		{
			$str = $checkNews['picture'];

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);


			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkNews['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];


			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}
			
		}

		//delete now the category item in the table
		$wpdb->delete( 'newsitems' , array( 'id' => $id ));
	}



}


/**
 * Insert product details for WooCommerce
 */

function bivt_add_product($post_id, $price) {
	add_post_meta($post_id, '_sku', $post_id);
	add_post_meta($post_id, '_stock_status', 'instock');
	add_post_meta($post_id, '_visibility','visible');
	add_post_meta($post_id, '_regular_price', $price);
	add_post_meta($post_id, '_price', $price);
	add_post_meta($post_id, '_virtual', 'yes');
}


/**
 * Update product details for WooCommerce
 */

function bivt_update_product($post_id, $price) {
	update_post_meta($post_id, '_sku', $post_id);
	update_post_meta($post_id, '_stock_status', 'instock');
	update_post_meta($post_id, '_visibility','visible');
	update_post_meta($post_id, '_regular_price', $price);
	update_post_meta($post_id, '_price', $price);
	update_post_meta($post_id, '_virtual', 'yes');
}


/**
 * Courses Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_acourses', 'bivt_admin_add_courses' );
add_action( 'wp_ajax_nopriv_bivt_admin_acourses', 'bivt_admin_add_courses' );

function bivt_admin_add_courses() 
{
	global $wpdb; // this is how you get access to the database;
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
 	include( plugin_dir_path( __FILE__ ) . 'Validator/CourseValidator.php');

 	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';
 	$publish = (isset($_POST['status']) && $_POST['status'] == 'open' ? 'publish' : '');
	
	$cv = new \wp_manager_course\CourseValidator();
	
	$cv->make($_POST,[
		'name' => ['required','validateCourseName'],
		'introduction'=> ['required'],
		'description' => ['required'],
		'category' => ['required','validateCategory'],
		'teacher' => ['required','validateTeacher'],
		//'sbu' => ['required'],
		'price' => ['required'],
		'meeting_days' => ['required'],
		'sessions' => ['required'],
		//'incasso' => ['required'],
		'conditions' => ['required'],
		'status' => ['required','validateStatus'],
		'meta_description' => ['required'],
		'meta_keywords' => ['required'],
		'picture' => ['validateFile','validateFileSize']
	]);

	if($cv->fails()) {
		echo json_encode([
				'success'=>0,
				'errors'=>$cv->errors(),
				'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
		]);
	}
	else
	{

		global $wpdb;

		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/course_pictures/';

        if(!file_exists($targetDirectory)) {
            if (!mkdir($targetDirectory, 0777, true)) {
            	echo json_encode([
            			'succes'=>0,
            			'errors'=>"Directory $targetDirectory could not be created",
            			'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
            	]);
            	die();
            }
        }
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		} else {
			$msg = "Could not upload " . $_FILES['picture']['tmp_name'];
			echo json_encode([
					'success'=>0,
					'errors'=>$msg,
					'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
				]);
			die();
		}

		//insert data in the posts
		$wpdb->insert(
			'wp_posts',
			array(
				'post_author' 			=> get_current_user_id(),
				'post_date'				=> date('Y-m-d h:i:s'),
				'post_date_gmt'			=> date('Y-m-d h:i:s'),
				'post_content'			=> $_POST['description'],
				'post_title'			=> str_replace(' ', '-',$_POST['name']),
				'post_excerpt'			=> $_POST['introduction'],
				'post_status'			=> $publish,
				'comment_status'		=> '',
				'ping_status'			=> '',
				'post_password'			=> '',
				'post_name'				=> str_replace(' ', '-',$_POST['name']), 
				'to_ping'				=> '/files/uploads/course_pictures/' . $newFileName,
				'pinged'				=> '',
				'post_modified'			=> date('Y-m-d h:i:s'),
				'post_modified_gmt'		=> date('Y-m-d h:i:s'),
				'post_content_filtered'	=> '',
				'post_parent'			=> 0,
				'guid'					=> '',
				'menu_order'			=> 0,
				'post_type'				=> 'product',
				'post_mime_type'		=> '',
				'comment_count'			=> 0
			)
		);

		$price = $_POST['price'];
		$inherit_post_id = $wpdb->insert_id;
		
		bivt_add_product($inherit_post_id, $price);

		$counter_post = $wpdb->insert_id;
		$wpdb->insert(
			'courses', 
			array(
				'name' => $_POST['name'],
				'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
				'introduction' => $_POST['introduction'],
				'description' => $_POST['description'],
				'sbu' => $_POST['sbu'],
				'price' => $_POST['price'],
				'meeting_days' => $_POST['meeting_days'],
				'sessions' => $_POST['sessions'],
				'incasso' => $_POST['incasso'],
				'conditions' => $_POST['conditions'],
				'status' => $_POST['status'],
				'meta_description' => $_POST['meta_description'],
				'meta_keywords' => $_POST['meta_keywords'],
				'picture' => '/files/uploads/course_pictures/' . $newFileName,
				'created' => date('Y-m-d h:i:s'),
				'modified' => date('Y-m-d h:i:s'),
				'post_id' => $counter_post
			)
		);

		$courseId = $wpdb->insert_id;

		$categories = explode(',',$_POST['category']);
		$teachers = explode(',', $_POST['teacher']);

		foreach($categories as $category){

			$wpdb->insert( 
				'categories_courses', 
				array(
					'course_id' => $courseId,
					'category_id' => $category
				)
			);
		}

		foreach($teachers as $teacher){

			$wpdb->insert( 
				'docents_courses', 
				array(
					'course_id' => $courseId,
					'docent_id' => $teacher
				)
			);
		}

		echo json_encode([
				'success'=>1,
				'errors'=>[],
				'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
		]);		
	}
	wp_die();
}


add_action( 'wp_ajax_bivt_admin_ecourses', 'bivt_admin_edit_courses' );
add_action( 'wp_ajax_nopriv_bivt_admin_ecourses', 'bivt_admin_edit_courses' );
function bivt_admin_edit_courses() {

	if(isset($_POST))
	{

		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/CourseValidator.php');

		$cv = new \wp_manager_course\CourseValidator();
		$dataValidation = [
			'name' => ['required'],
			'introduction' => ['required'],
			'meta_description' => ['required'],
			'meta_keywords' => ['required'],
			'conditions' => ['required'],
			'description' => ['required'],
			'sessions' => ['required'],
			'meeting_days' => ['required'],
			'price' => ['required'],
			'category' => ['required','validateCategory'],
			'teacher' => ['required','validateTeacher'],
			//'sbu' => ['required'],
			//'incasso' => ['required'],
			'status' => ['required','validateStatus'],
			'post_id' => ['required'],
		];

		//var_dump($_POST);exit;


		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];


		$cv->make($_POST,$dataValidation);

		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
			]);
		} else {
			global $wpdb;
			
			$id = $_POST['course_id'];

			$dataUpdate = [
				'name' => $_POST['name'],
				'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
				'introduction' => $_POST['introduction'],
				'meta_description' => $_POST['meta_description'],
				'meta_keywords' => $_POST['meta_keywords'],
				'conditions' => $_POST['conditions'],
				'description' => $_POST['description'],
				'sessions' => $_POST['sessions'],
				'meeting_days' => $_POST['meeting_days'],
				'price' => $_POST['price'],
				'SBU' => $_POST['sbu'],
				'incasso' => $_POST['incasso'],
				'status' => $_POST['status'],
				'modified' => date('Y-m-d h:i:s')
			];


			if($_POST['picture']!='')
			{

				$id = $_POST['id'];

				$checkCourse = $wpdb->get_row("SELECT picture FROM courses WHERE id = $id",ARRAY_A);

				//get thumbnail link and picture to delete
				if($checkCourse['picture']!='')
				{
					$str = $checkCourse['picture'];

					$result = explode('/', $str);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);

					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkCourse['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}
					
				}
				//start

				//begin upload scripts here
		        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/course_pictures/';

		        if(!file_exists($targetDirectory))
		            mkdir($targetDirectory, 0777, true);
				

		        //upload the image
				$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
		        //do not delete in case it will be used

		        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

		        // $newFileName = md5(time()) . '.' . $imageFileType;
				//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
				$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

				//if successfully uploaded the file, create now the thumbnail
				if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
				{

					//upload too the thumbnail picture
					include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

					$thumb = new easyphpthumbnail;

					$thumb->Chmodlevel = '0777';
					
					$thumb->Thumblocation = $targetDirectory;
					
					$thumb->Thumbprefix = 'thumb_';
					
					$thumb -> Createthumb($targetFile,'file');

				}
				//end

				$dataUpdate['picture'] = '/files/uploads/course_pictures/' . $newFileName;
				
			}



			$wpdb->update(
			'courses', 
			$dataUpdate,
			array( 'id' => $id )
			);

			bivt_update_product($_POST['post_id'], $_POST['price']);
				
			$categories = $_POST['category'];
			$teachers = $_POST['teacher'];


			$wpdb->delete( 'categories_courses',  array( 'course_id' => $id ));
			$wpdb->delete( 'docents_courses',  array( 'course_id' => $id ));


			$categories = explode(",",$categories);
			foreach ($categories as $category) {
				$wpdb->insert( 
					'categories_courses', 
					array( 
						'category_id' 	=> $category,
						'course_id' 		=> $id
					)
				);
			}


			$teachers = explode(",",$teachers);
			foreach ($teachers as $teacher) {
				$wpdb->insert(
					'docents_courses', 
					array( 
						'docent_id' 	=> $teacher,
						'course_id' 		=> $id
					)
				);
			}

			echo json_encode([
					'success'=>1,
					'errors'=>[],
					'course_id'=>isset($_POST['course_id']) ? $_POST['course_id'] : -1,
			]);
		}

	} 

	wp_die();


}

add_action( 'wp_ajax_bivt_admin_dcourses', 'bivt_admin_delete_courses' );
add_action( 'wp_ajax_nopriv_bivt_admin_dcourses', 'bivt_admin_delete_courses' );


function bivt_admin_delete_courses() {

	global $wpdb;

	$id = intval($_POST['id']);

	$checkCourse = $wpdb->get_var("SELECT COUNT(*) FROM courses WHERE id = $id");

	if($checkCourse>0)
	{
		$checkCourse = $wpdb->get_row("SELECT picture FROM courses WHERE id = $id",ARRAY_A);

		//get thumbnail link and picture to delete
		if($checkCourse['picture']!='')
		{
			$str = $checkCourse['picture'];

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);

			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkCourse['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}
			
		}
		//delete now the course item in the table
		$wpdb->delete( 'courses' , array( 'id' => $id ));


		$wpdb->delete( 'categories_courses', array( 'course_id' => $id ));
		$wpdb->delete( 'docents_courses', array( 'course_id' => $id ));
	}
}

/**
 * Users Functionality
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_adusers', 'bivt_admin_add_users' );
add_action( 'wp_ajax_nopriv_bivt_admin_adusers', 'bivt_admin_add_users' );
function bivt_admin_add_users() {
	global $wpdb; // this is how you get access to the database

	parse_str($_POST['form']);
	$groups = $_POST['groups'];
	$created = current_time('mysql');
	$modified =  date("Y-m-d H:i:s");
	$password = '12345';
	//$password = randomStrings();
	$newuser = wp_create_user( $username, $password, $email );
	$user_id_role = new WP_User($newuser);
	$user_id_role->set_role('bivt');
	$wpdb->update(
		$wpdb->prefix.'users', 
		array( 
			'display_name' 			=> $fullname,  
			'user_status' 			=> $status,
			'user_registered' 		=> $created,
			'modified' 				=> $modified	
		),
		array( 'id' => $newuser )
	);

	foreach ($groups as $group) {
			$wpdb->insert( 
				'users_groups', 
				array( 
					'user_id' 		=> $newuser,
					'group_id' 		=> $group
				)
			);
	}
}


add_action( 'wp_ajax_bivt_admin_vedit_user', 'bivt_admin_view_edit_user' );
add_action( 'wp_ajax_nopriv_bivt_admin_vedit_user', 'bivt_admin_view_edit_user' );
function bivt_admin_view_edit_user() {
	global $wpdb;
	$id = $_POST['id'];

	$result = $wpdb->get_row( "SELECT * FROM wp_users WHERE ID = $id", ARRAY_A);

	echo json_encode($result);

	wp_die();
}
 
add_action( 'wp_ajax_bivt_admin_vgroups_edit_user', 'bivt_admin_view_groups_edit_user' );
add_action( 'wp_ajax_nopriv_bivt_admin_vgroups_edit_user', 'bivt_admin_view_groups_edit_user' );
function bivt_admin_view_groups_edit_user() {
 	global $wpdb; // this is how you get access to the database
 	$id = $_POST['id'];
 
 	$results = $wpdb->get_results( "SELECT g.id FROM `groups` as g LEFT JOIN `users_groups` ug ON ug.group_id = g.id WHERE ug.user_id = $id", ARRAY_A);
 	$arraya = array();
 	foreach( $results as $result ) {
 		//$array[] = $result->NAME;
 		array_push($arraya, $result['id']);
 	}
 	echo json_encode($arraya);
 	wp_die(); // this is required to terminate immediately and return a proper response
 }

add_action( 'wp_ajax_bivt_admin_euser', 'bivt_admin_edit_user' );
add_action( 'wp_ajax_nopriv_bivt_admin_euser', 'bivt_admin_edit_user' );
function bivt_admin_edit_user() {
	global $wpdb;
	parse_str($_POST['form']);
	$modified =  date("Y-m-d H:i:s");
	$output='';
	$groups = $_POST['groups'];
	$wpdb->update(
		$wpdb->prefix.'users', 
		array( 
			'user_login' 			=> $username,
			'display_name' 			=> $fullname,
			'user_status'			=> $status,
			'modified' 				=> $modified		
		),
		array( 'id' => $id )
	);
	$wpdb->delete( 'users_groups',  array( 'user_id' => $id )  );
	foreach ($groups as $group) {
			$wpdb->insert( 
				'users_groups', 
				array( 
					'user_id' 		=> $id,
					'group_id' 		=> $group
				)
			);
	}

}

add_action( 'wp_ajax_bivt_admin_duser', 'bivt_admin_delete_user' );
add_action( 'wp_ajax_nopriv_bivt_admin_duser', 'bivt_admin_delete_user' );
function bivt_admin_delete_user() {
	global $wpdb;
	$id = $_POST['id'];
	$wpdb->delete( $wpdb->prefix.'users' , array( 'ID' => $id ) );
}

/**
 * USERS AREA - Groups
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_aduser_group', 'bivt_admin_add_user_group' );
add_action( 'wp_ajax_nopriv_bivt_admin_aduser_group', 'bivt_admin_add_user_group' );
function bivt_admin_add_user_group() {
	global $wpdb; 

	parse_str($_POST['form']);
	$created = current_time('mysql');
	$modified =  date("Y-m-d H:i:s");
	$password = randomStrings();
	$newuser = wp_create_user( $username, $password, $email );

	$wpdb->insert( 
		'groups', 
		array( 
			'name' 			=> $name,
			'created' 		=> $created,
			'modified' 		=> $modified
		)
	);
}

add_action( 'wp_ajax_bivt_admin_veuser_group', 'bivt_admin_view_user_group' );
add_action( 'wp_ajax_nopriv_bivt_admin_veuser_group', 'bivt_admin_view_user_group' );
function bivt_admin_view_user_group() {
	global $wpdb;
	$id = $_POST['id'];

	$result = $wpdb->get_row( "SELECT * FROM wp_users WHERE ID = $id", ARRAY_A);

	echo json_encode($result);

	wp_die(); 
}

/**
 * Modules Functionality
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_modules', 'bivt_admin_add_modules' );
add_action( 'wp_ajax_nopriv_bivt_admin_modules', 'bivt_admin_add_modules' );

function bivt_admin_add_modules() {


	global $wpdb; // this is how you get access to the database

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/ModuleValidator.php');


	$mv = new \wp_manager_course\ModuleValidator();

	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

	
	
	//$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';


	$mv->make($_POST,[
		'name' => ['required','validateModuleName'],
		'introduction'=> ['required'],
		'description' => ['required'],
		'conditions' => ['required'],
		'programme' => ['required'],
		'meta_description' => ['required'],
		'meta_keywords' => ['required'],
		'sessions' => ['required'],
		'meeting_days' => ['required'],
		'price' => ['required'],
		'sbu' => ['required'],
		'level' => ['required'],
		'exam' => ['required'],
		//'incasso' => ['required'],
		'preliminary_knowledge' => ['required'],
		'goal' => ['required'],
		'target_audience' => ['required'],
		'literature' => ['required'],
		'status' => ['required','validateStatus'],
		'picture' => ['validateFile','validateFileSize'],
		'teacher' => ['required', 'validateTeacher'],
	]);

	if($mv->fails()) {
		echo json_encode(['errors'=>$mv->errors()]);
	}
	else
	{

		global $wpdb;

		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/module_pictures/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}
		$publish = (isset($_POST['status']) && $_POST['status'] == 'open' ? 'publish' : '');
		$wpdb->insert(
		   'wp_posts',
		   array(
		    'post_author'    => get_current_user_id(),
		    'post_date'    => date('Y-m-d h:i:s'),
		    'post_date_gmt'   => date('Y-m-d h:i:s'),
		    'post_content'   => $_POST['description'],
		    'post_title'   => str_replace(' ', '-',$_POST['name']),
		    'post_excerpt'   => $_POST['introduction'],
		    'post_status'   => $publish,
		    'comment_status'  => '',
		    'ping_status'   => '',
		    'post_password'   => '',
		    'post_name'    => str_replace(' ', '-',$_POST['name']), 
		    'to_ping'    => '',
		    'pinged'    => '',
		    'post_modified'   => date('Y-m-d h:i:s'),
		    'post_modified_gmt'  => date('Y-m-d h:i:s'),
		    'post_content_filtered' => '',
		    'post_parent'   => 0,
		    'guid'     => '',
		    'menu_order'   => 0,
		    'post_type'    => 'product',
		    'post_mime_type'  => '',
		    'comment_count'   => 0
		   )
		  );

		  $counter_post = $wpdb->insert_id;
		  $price = $_POST['price'];
		  $inherit_post_id = $counter_post;		  

		  $wpdb->insert(
		   'wp_posts',
		   array(
		    'post_author'    => get_current_user_id(),
		    'post_date'    => date('Y-m-d h:i:s'),
		    'post_date_gmt'   => date('Y-m-d h:i:s'),
		    'post_content'   => '',
		    'post_title'   => str_replace(' ', '-',$_POST['name']),
		    'post_excerpt'   => $_POST['introduction'],
		    'post_status'   => 'inherit',
		    'comment_status'  => 'open',
		    'ping_status'   => 'closed',
		    'post_password'   => '',
		    'post_name'    => str_replace(' ', '-',$_POST['name']), 
		    'to_ping'    => '',
		    'pinged'    => '',
		    'post_modified'   => date('Y-m-d h:i:s'),
		    'post_modified_gmt'  => date('Y-m-d h:i:s'),
		    'post_content_filtered' => '',
		    'post_parent'   => $inherit_post_id,
		    'guid'     => 'http://localhost/bivt/wp-content/plugins/wp-manager-course/files/uploads/module_pictures/' . $newFileName,
		    'menu_order'   => 0,
		    'post_type'    => 'attachment',
		    'post_mime_type'  => 'image/jpeg',
		    'comment_count'   => 0
		   )
		  );
		
		$postWpost = $wpdb->insert_id;
		bivt_add_product($postWpost, $price);
				
		$wpdb->insert(
		'modules', 
		array(
			'name' => $_POST['name'],
			'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
			'introduction'=> $_POST['introduction'],
			'description' => $_POST['description'],
			'conditions' => $_POST['conditions'],
			'programme' => $_POST['programme'],
			'meta_description' => $_POST['meta_description'],
			'meta_keywords' => $_POST['meta_keywords'],
			'sessions' => $_POST['sessions'],
			'meeting_days' => $_POST['meeting_days'],
			'price' => $_POST['price'],
			'SBU' => $_POST['sbu'],
			'level' => $_POST['level'],
			'exam' => $_POST['exam'],
			'incasso' => $_POST['incasso'],
			'preliminary_knowledge' => $_POST['preliminary_knowledge'],
			'goal' => $_POST['goal'],
			'target_audience' => $_POST['target_audience'],
			'literature'=> $_POST['target_audience'],
			'status' => $_POST['status'],
			'picture' => '/files/uploads/module_pictures/' . $newFileName,
			'created' => date('Y-m-d h:i:s'),
			'modified' => date('Y-m-d h:i:s'),
			'post_id' => $counter_post
		), 
		array( 
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%d',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
		)
		);
		parse_str($_POST['form']);
		$courses = $_POST['course'];

		$module_id = $wpdb->get_row("SELECT id FROM modules ORDER BY id DESC LIMIT 0 , 1" );

		$courses = explode(",",$courses);
		foreach ($courses as $course) {
				$wpdb->insert( 
					'courses_modules', 
					array( 
						'module_id' 		=> $module_id->id,
						'course_id' 		=> $course
					)
				);
		}


		$categories = $_POST['category'];
		$categories = explode(",",$categories);
		foreach ($categories as $category) {
				$wpdb->insert( 
					'categories_modules', 
					array( 
						'module_id' 		=> $module_id->id,
						'category_id' 		=> $category
					)
				);
		}
	
		$teachers = $_POST['teacher'];
		$teachers = explode(",",$teachers);
		foreach ($teachers as $teacher) {
			$wpdb->insert(
					'docents_modules',
					array(
							'module_id' 		=> $module_id->id,
							'docent_id' 		=> $teacher
					)
				);
		}
	
		echo json_encode(['success'=>1]);

		
	}

	wp_die();

}

add_action( 'wp_ajax_bivt_admin_emodules', 'bivt_admin_edit_modules' );
add_action( 'wp_ajax_nopriv_bivt_admin_emodules', 'bivt_admin_edit_modules' );
function bivt_admin_edit_modules() {

	if(isset($_POST))
	{
		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/ModuleValidator.php');
		
		$mv = new \wp_manager_course\ModuleValidator();

		$dataValidation = [
			'id' => ['required','validateId'],
			'name' => ['required','validateModuleName'],
			'introduction'=> ['required'],
			'description' => ['required'],
			'conditions' => ['required'],
			'programme' => ['required'],
			'meta_description' => ['required'],
			'meta_keywords' => ['required'],
			'sessions' => ['required'],
			'meeting_days' => ['required'],
			'price' => ['required'],
			'sbu' => ['required'],
			'level' => ['required'],
			'exam' => ['required'],
			//'incasso' => ['required'],
			'preliminary_knowledge' => ['required'],
			'goal' => ['required'],
			'target_audience' => ['required'],
			'literature' => ['required'],
			'status' => ['required','validateStatus'],
			'teacher' => ['required', 'validateTeacher'],
		];


		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];


		$mv->make($_POST,$dataValidation);

		if($mv->fails())
			echo json_encode(['errors'=>$mv->errors()]);
		else
		{
			global $wpdb;
			
			$id = $_POST['id'];

			$dataUpdate = [
				'name' => $_POST['name'],
				'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
				'introduction'=> $_POST['introduction'],
				'description' => $_POST['description'],
				'conditions' => $_POST['conditions'],
				'programme' => $_POST['programme'],
				'meta_description' => $_POST['meta_description'],
				'meta_keywords' => $_POST['meta_keywords'],
				'sessions' => $_POST['sessions'],
				'meeting_days' => $_POST['meeting_days'],
				'price' => $_POST['price'],
				'SBU' => $_POST['sbu'],
				'level' => $_POST['level'],
				'exam' => $_POST['exam'],
				'incasso' => $_POST['incasso'],
				'preliminary_knowledge' => $_POST['preliminary_knowledge'],
				'goal' => $_POST['goal'],
				'target_audience' => $_POST['target_audience'],
				'literature' => $_POST['literature'],
				'status' => $_POST['status'],
				'modified' => date('Y-m-d h:i:s'),
				'post_id' => $_POST['post_id'],
			];
			
			bivt_update_product($_POST['post_id'], $_POST['price']);
				
			if($_POST['picture']!='')
			{

				$id = $_POST['id'];

				$checkModule = $wpdb->get_row("SELECT picture FROM modules WHERE id = $id",ARRAY_A);

				//get thumbnail link and picture to delete
				if($checkModule['picture']!='')
				{
					$str = $checkModule['picture'];

					$result = explode('/', $zstr);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);

					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkModule['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}
					
				}
				//start

				//begin upload scripts here
		        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/module_pictures/';

		        if(!file_exists($targetDirectory))
		            mkdir($targetDirectory, 0777, true);
				

		        //upload the image
				$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
		        //do not delete in case it will be used

		        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

		        // $newFileName = md5(time()) . '.' . $imageFileType;
				//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
				$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

				//if successfully uploaded the file, create now the thumbnail
				if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
				{

					//upload too the thumbnail picture
					include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

					$thumb = new easyphpthumbnail;

					$thumb->Chmodlevel = '0777';
					
					$thumb->Thumblocation = $targetDirectory;
					
					$thumb->Thumbprefix = 'thumb_';
					
					$thumb -> Createthumb($targetFile,'file');

				}
				//end

				$dataUpdate['picture'] = '/files/uploads/module_pictures/' . $newFileName;
				
			}

			$wpdb->update(
			'modules', 
			$dataUpdate,
			array( 'id' => $id )
			);
			
			
			
			$courses = $_POST['course'];
			$teachers = $_POST['teacher'];
			$id = $_POST['id'];

			$wpdb->delete( 'courses_modules',  array( 'module_id' => $id )  );

			$courses = explode(",",$courses);
			foreach ($courses as $course) {
					$wpdb->insert( 
						'courses_modules', 
						array( 
							'module_id' 		=> $id,
							'course_id' 		=> $course
						)
					);
			}

			$categories = $_POST['category'];
			$wpdb->delete( 'categories_modules',  array( 'module_id' => $id )  );

			$categories = explode(",",$categories);
			foreach ($categories as $category) {
					$wpdb->insert( 
						'categories_modules', 
						array( 
							'category_id' 		=> $category,
							'module_id' 		=> $id
						)
					);
			}

			$wpdb->delete( 'docents_modules',  array( 'module_id' => $id )  );
				
			$teachers = explode(",",$teachers);
			foreach ($teachers as $teacher) {
				$wpdb->insert(
						'docents_modules',
						array(
								'module_id' 		=> $id,
								'docent_id' 		=> $teacher
						)
						);
			}
				
				
			echo json_encode(['success'=>1]);
		}
	} 

	wp_die();
}

add_action( 'wp_ajax_bivt_admin_dmodules', 'bivt_admin_delete_modules' );
add_action( 'wp_ajax_nopriv_bivt_admin_dmodules', 'bivt_admin_delete_modules' );

function bivt_admin_delete_modules() {

	global $wpdb;

	$id = intval($_POST['id']);

	$checkModule = $wpdb->get_var("SELECT COUNT(*) FROM modules WHERE id = $id");

	if($checkModule>0)
	{
		$checkModule = $wpdb->get_row("SELECT picture FROM modules WHERE id = $id",ARRAY_A);

		//get thumbnail link and picture to delete
		if($checkModule['picture']!='')
		{
			$str = $checkModule['picture'];

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);

			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkModule['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];

			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}
			
		}
		//delete now the module item in the table
		$wpdb->delete( 'modules' , array( 'id' => $id ));
		$wpdb->delete( 'categories_modules' , array( 'module_id' => $id ));
		$wpdb->delete( 'courses_modules' , array( 'module_id' => $id ));
		
	}
}


/**
 * Instaces
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_dinstances', 'bivt_admin_dinstances' );
add_action( 'wp_ajax_nopriv_bivt_admin_dinstances', 'bivt_admin_dinstances' );



function bivt_admin_dinstances() {

	global $wpdb;
	$response = ['status'=>'ok','errors'=>''];
	
	$id = $_GET['id'];
	$checkInstance = $wpdb->get_row("SELECT * FROM instances WHERE id = {$id}",ARRAY_A);

	if(count($checkInstance)>0){

		$wpdb->delete('instances',array( 'id' => $id ));
	}else{
		$response['status'] = 'not ok';
	}

	echo json_encode($response);

	wp_die();

} 

/**
 * Materials Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_materials', 'bivt_admin_materials' );
add_action( 'wp_ajax_nopriv_bivt_admin_materials', 'bivt_admin_materials' );
function bivt_admin_materials() {
	
	$_POST['files'] = (isset($_FILES['files'])) ? $_FILES['files'] : null;
	$material_id = isset($_POST['material_id']) ? $_POST['material_id'] : null;
	
	if(isset($_POST)) {
	
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/MaterialsValidator.php');
	
		$cv = new \wp_manager_course\MaterialsValidator();
		$dataValidation = [
				'material_id'=> ['required'],
				'module_id' => ['required'],
				'name' => ['required'],
				'type' => ['required'],
				'status' => ['required'],
				'user_id' => ['required'],
				'available_on_day' => ['required'],
		];
		if ($material_id == 0 || $_POST['files'] != null) {
			$dataValidation['files'] = ['validateFileSize'];		
		}
	
		$cv->make($_POST,$dataValidation);

		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
			]);
			die ();
		}
	
		$newFileName = null;
		
		if ($_POST['files']) {
			//begin upload scripts here
			$targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/materials/';
			
			if(!file_exists($targetDirectory)) {
				if (!mkdir($targetDirectory, 0777, true)) {
					echo json_encode([
							'succes'=>0,
							'errors'=>"Directory $targetDirectory could not be created",
							'data'=>$_POST,
					]);
					die();
				}
			}
			
			
			//upload the image
			$newFileName = $_FILES['files']['name'];
			$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);
			
			//if successfully uploaded the file, create now the thumbnail
			if(move_uploaded_file($_FILES['files']['tmp_name'],$targetFile))
			{			
			} else {
				$msg = "Could not upload " . $_FILES['files']['tmp_name'];
				echo json_encode([
						'success'=>0,
						'errors'=>$msg,
						'data'=>$_POST,
				]);
				die();
			}
				
		}
		
		
		global $wpdb;
		$table = 'materials';
		$data = null;
		$format = null;
		$where = null;

		if($material_id == 0) {
			$data = [
				'created' => date('Y-m-d h:i:s'),						
				'modified' => date('Y-m-d h:i:s'),
				'name' => $_POST['name'],
				'available_on_day' => $_POST['available_on_day'],
				'file' => '/files/uploads/materials/' . $newFileName,
				'type' => $_POST['type'],
				'module_id' => $_POST['module_id'],
				'status' => $_POST['status'],
				'original_creator' => $_POST['user_id'],
				'last_modifier' => $_POST['user_id'],
			];
			$format = ['%s','%s','%s','%d', '%s', '%s', '%d', '%s', '%d', '%d'];
			$wpdb->insert($table, $data,$format);
		} else {
			$data = [
					'modified' => date('Y-m-d h:i:s'),
					'name' => $_POST['name'],
					'available_on_day' => $_POST['available_on_day'],
					'type' => $_POST['type'],
					'module_id' => $_POST['module_id'],
					'status' => $_POST['status'],
					'last_modifier' => $_POST['user_id'],
			];
			$format = ['%s','%s','%d','%s', '%d', '%s', '%d'];
			$where = ['id' => $material_id];
			if ($_POST['files']) {
				$data[0]['file'] = '/files/uploads/materials/' . $newFileName;
				array_push($format, '%s');
			}
			$wpdb->update($table, $data, $where, $format);
		}
	}
	echo json_encode([
			'success'=>1,
			'errors'=>[],
			'data'=>$_POST,
			'table'=>$table,
			'table_data'=>$data,
			'format'=>$format,
			'where'=>$where,
	]);
	die ();
}


/**
 * Assignments Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_assignments', 'bivt_admin_assignments' );
add_action( 'wp_ajax_nopriv_bivt_admin_assignments', 'bivt_admin_assignments' );
function bivt_admin_assignments() {
	
	$_POST['files'] = (isset($_FILES['files'])) ? $_FILES['files'] : null;
	$assignment_id = isset($_POST['assignment_id']) ? $_POST['assignment_id'] : null;
	
	if(isset($_POST)) {
	
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/AssignmentsValidator.php');
	
		$cv = new \wp_manager_course\AssignmentsValidator();
		$dataValidation = [
				'assignment_id'=> ['required'],
				'module_id' => ['required'],
				'subject' => ['required'],
				'content' => ['required'],
				'status' => ['required'],
				'docent_id' => ['required'],
				'available_on_day' => ['required'],
		];
		if ($assignment_id == 0 || $_POST['files'] != null) {
			$dataValidation['files'] = ['validateFileSize'];		
		}
	
		$cv->make($_POST,$dataValidation);

		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
			]);
			die ();
		}
	
		$newFileName = null;
		
		if ($_POST['files']) {
			//begin upload scripts here
			$targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/assignments/';
			
			if(!file_exists($targetDirectory)) {
				if (!mkdir($targetDirectory, 0777, true)) {
					echo json_encode([
							'succes'=>0,
							'errors'=>"Directory $targetDirectory could not be created",
							'data'=>$_POST,
					]);
					die();
				}
			}
			
			//upload the image
			$newFileName = $_FILES['files']['name'];
			$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);
			
			//if successfully uploaded the file, create now the thumbnail
			if(move_uploaded_file($_FILES['files']['tmp_name'],$targetFile))
			{			
			} else {
				$msg = "Could not upload " . $_FILES['files']['tmp_name'];
				echo json_encode([
						'success'=>0,
						'errors'=>$msg,
						'data'=>$_POST,
				]);
				die();
			}
				
		}
		
		
		global $wpdb;
		$table = 'assignments';
		$data = null;
		$format = null;
		$where = null;

		if($assignment_id == 0) {
			$data = [
				'created' => date('Y-m-d h:i:s'),						
				'modified' => date('Y-m-d h:i:s'),
				'text_subject' => $_POST['subject'],
				'text_content' => $_POST['content'],
				'day_number' => $_POST['available_on_day'],
				'file' => '/files/uploads/assignments/' . $newFileName,
				'module_id' => $_POST['module_id'],
				'status' => $_POST['status'],
				'docent_id' => $_POST['docent_id'],
			];
			$format = ['%s','%s','%s','%s', '%d', '%s', '%d', '%s', '%d'];
			$wpdb->insert($table, $data,$format);
		} else {
			$data = [
				'modified' => date('Y-m-d h:i:s'),
				'text_subject' => $_POST['subject'],
				'text_content' => $_POST['content'],
				'day_number' => $_POST['available_on_day'],
				'module_id' => $_POST['module_id'],
				'status' => $_POST['status'],
				'docent_id' => $_POST['docent_id'],
			];
			$format = ['%s','%s','%s', '%d', '%d', '%s', '%d'];
			$where = ['id' => $assignment_id];
			if ($_POST['files']) {
				$data[0]['file'] = '/files/uploads/assignments/' . $newFileName;
				array_push($format, '%s');
			}
			$wpdb->update($table, $data, $where, $format);
		}
	}
	echo json_encode([
			'success'=>1,
			'errors'=>[],
			'data'=>$_POST,
			'table'=>$table,
			'table_data'=>$data,
			'format'=>$format,
			'where'=>$where,
	]);
	die ();
}


/**
 * Accreditaties Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_daccreditaties', 'bivt_admin_delete_accreditaties' );
add_action( 'wp_ajax_nopriv_bivt_admin_daccreditaties', 'bivt_admin_delete_accreditaties' );
function bivt_admin_delete_accreditaties() {

	global $wpdb;

	$id = intval($_POST['id']);

	$checkAccreditaty = $wpdb->get_var("SELECT COUNT(*) FROM accreditaties WHERE id = $id");

	if($checkAccreditaty>0)
	{
		$wpdb->delete( 'accreditaties' , array( 'id' => $id ));
	}

}


add_action( 'wp_ajax_bivt_admin_accreditaties', 'bivt_admin_add_accreditaties' );
add_action( 'wp_ajax_nopriv_bivt_admin_accreditaties', 'bivt_admin_add_accreditaties' );
function bivt_admin_add_accreditaties() {

	if(isset($_POST['dataPass']))
	{
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/AccreditatiesValidator.php');

		$av = new \wp_manager_course\AccreditatiesValidator();


		$av->make($_POST['dataPass'],[
			'points' => ['required'],
			'professional_organizational_id' => ['required','validateOrganization'],
			'module_id' => ['required','validateModule'],
			'valid_from' => ['required','validateFrom'],
			'valid_till' => ['required','validateTill']
		]);


		if($av->fails()) {
			echo json_encode(['errors'=>$av->errors()]);
		}
		else
		{

			global $wpdb;

			$wpdb->insert(
			'accreditaties', 
			array(
				'Points' => $_POST['dataPass']['points'], 
				'valid_from' => $_POST['dataPass']['valid_from'],
				'valid_till' => $_POST['dataPass']['valid_till'],
				'module_id' => $_POST['dataPass']['module_id'],
				'professional_organization_id' => $_POST['dataPass']['professional_organizational_id'],
				'created' => date('Y-m-d h:i:s'),
				'modified' => date('Y-m-d h:i:s')
			), 
			array( 
				'%s', 
				'%s',
				'%s',
				'%d',
				'%d',
				'%s',
				'%s'
			) 
			);

			echo json_encode(['success'=>1,'insert_id'=>$wpdb->insert_id]);
		}

	} 




	wp_die();
}

/**
 * Categories Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_admin_dcategories', 'bivt_admin_delete_categories' );
add_action( 'wp_ajax_nopriv_bivt_admin_dcategories', 'bivt_admin_delete_categories' );

function bivt_admin_delete_categories()
{
	global $wpdb;

	$id = intval($_POST['id']);

	$checkCategory = $wpdb->get_var("SELECT COUNT(*) FROM categories WHERE id = $id");

	if($checkCategory>0)
	{

		$checkCategory = $wpdb->get_row("SELECT picture FROM categories WHERE id = $id",ARRAY_A);

		//get thumbnail link and picture to delete
		if($checkCategory['picture']!='')
		{
			$str = $checkCategory['picture'];

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);


			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkCategory['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];


			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}
			
		}

		//delete now the category item in the table
		$wpdb->delete( 'categories' , array( 'id' => $id ));
	}

}






//responsible for removing picture set in the category via AJAX call
add_action( 'wp_ajax_bivt_admin_remove_picture_ajax', 'bivt_admin_remove_picture_ajax' );
add_action( 'wp_ajax_nopriv_bivt_admin_remove_picture_ajax', 'bivt_admin_remove_picture_ajax' );
function bivt_admin_remove_picture_ajax()
{

	$response = ['status'=>'not ok'];

	if(isset($_POST['action']) && isset($_POST['table']) && isset($_POST['id']))
	{
		$id = absint($_POST['id']);
		global $wpdb;

		if($_POST['table']=='categories' || $_POST['table']=='newsitems' || $_POST['table']=='courses' || $_POST['table']=='modules' || $_POST['table']=='locations')
		{
			$checkTable = $wpdb->get_row("SELECT picture FROM ".$_POST['table']." WHERE id = $id",ARRAY_A);

			if(count($checkTable)>0)
			{
				//get thumbnail link and picture to delete
				if($checkTable['picture']!='')
				{
					$str = $checkTable['picture'];

					$result = explode('/', $str);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);


					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkTable['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];


					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}


					//updates now the table
					$wpdb->update(
						$_POST['table'], 
						array( 
							'picture' => ''	
						),
						array( 'id' => $id )
					);

					$response['status'] = 'ok';
					
				}
			}
		}

		echo json_encode($response);
	}


	wp_die();


}



add_action( 'wp_ajax_bivt_admin_edit_categories', 'bivt_admin_edit_categories' );
add_action( 'wp_ajax_nopriv_bivt_admin_edit_categories', 'bivt_admin_edit_categories' );

function bivt_admin_edit_categories()
{


	if(isset($_POST))
	{


		$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';



		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/CategoriesValidator.php');

		$av = new \wp_manager_course\CategoriesValidator();
		$dataValidation = [
			'id' => ['required','validateId'],
			'name' => ['required'],
		//	'slug' => ['required'],
			'description' => ['required'],
			'type' => ['required'],
			'meta_description' => ['required'],
			'meta_keyword' => ['required'],
			'status' => ['required','validateStatus']
		];

		if($_POST['picture']!='')
			$dataValidation['picture'] = ['validateFile','validateFileSize'];

		$av->make($_POST,$dataValidation);

		if($av->fails())
			echo json_encode(['errors'=>$av->errors()]);
		else
		{
			global $wpdb;
			
			$id = $_POST['id'];

			$dataUpdate = [
				'name' => $_POST['name'],
				'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
				'description' =>$_POST['description'],
				'type' => $_POST['type'],
				'meta_description' =>$_POST['description'],
				'meta_keywords' =>$_POST['meta_keyword'],
				'status' => $_POST['status'],
				'modified' => date('Y-m-d h:i:s')
			];


			if($_POST['picture']!='')
			{

				$id = $_POST['id'];

				$checkCategory = $wpdb->get_row("SELECT picture FROM categories WHERE id = $id",ARRAY_A);

				//get thumbnail link and picture to delete
				if($checkCategory['picture']!='')
				{
					$str = $checkCategory['picture'];

					$result = explode('/', $str);

					$getLast = count($result) - 1;

					$limit = $getLast - 1;

					$getInitial = '';
					//get initial directory
					for($x=0;$x<=$limit;$x++)
					{
						$getInitial .=$result[$x] . '/';
					}

					$result = explode('.',$result[$getLast]);


					$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


					$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$checkCategory['picture'],get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];


					//delete main picture and corresponding thumbnail picture
					foreach($targetFiles as $targetFile)
					{
						if(file_exists($targetFile))
						{
							unlink($targetFile);
						}
					}
					
				}
				//start

				//begin upload scripts here
		        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/category_pictures/';

		        if(!file_exists($targetDirectory))
		            mkdir($targetDirectory, 0777, true);
				

		        //upload the image
				$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
		        //do not delete in case it will be used

		        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

		        // $newFileName = md5(time()) . '.' . $imageFileType;
				//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
				$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

				//if successfully uploaded the file, create now the thumbnail
				if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
				{

					//upload too the thumbnail picture
					include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

					$thumb = new easyphpthumbnail;

					$thumb->Chmodlevel = '0777';
					
					$thumb->Thumblocation = $targetDirectory;
					
					$thumb->Thumbprefix = 'thumb_';
					
					$thumb -> Createthumb($targetFile,'file');

				}
				//end

				$dataUpdate['picture'] = '/files/uploads/category_pictures/' . $newFileName;
				
			}

			$wpdb->update(
			'categories', 
			$dataUpdate,
			array( 'id' => $id )
			);

			echo json_encode(['success'=>1]);
		}

	} 

	wp_die();

}


add_action( 'wp_ajax_bivt_admin_edit_accreditaties', 'bivt_admin_edit_accreditaties' );
add_action( 'wp_ajax_nopriv_bivt_admin_edit_accreditaties', 'bivt_admin_edit_accreditaties' );

function bivt_admin_edit_accreditaties()
{
	if(isset($_POST['dataPass']))
	{
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/AccreditatiesValidator.php');

		$av = new \wp_manager_course\AccreditatiesValidator();

		$av->make($_POST['dataPass'],[
			'id' => ['required','validateId'],
			'points' => ['required'],
			'professional_organizational_id' => ['required','validateOrganization'],
			'module_id' => ['required','validateModule'],
			'valid_from' => ['required','validateFrom'],
			'valid_till' => ['required','validateTill']
		]);


		if($av->fails()) {
			echo json_encode(['errors'=>$av->errors()]);
		}
		else
		{

			global $wpdb;

			$wpdb->update(
			'accreditaties', 
			array(
				'Points' => $_POST['dataPass']['points'], 
				'valid_from' => $_POST['dataPass']['valid_from'],
				'valid_till' => $_POST['dataPass']['valid_till'],
				'module_id' => $_POST['dataPass']['module_id'],
				'professional_organization_id' => $_POST['dataPass']['professional_organizational_id'],
				'modified' => date('Y-m-d h:i:s')
			),
			array( 'id' => $_POST['dataPass']['id'] )
			);


			echo json_encode(['success'=>1]);
		}

	} 

	wp_die();


}


add_action( 'wp_ajax_bivt_admin_categories', 'bivt_admin_add_categories' );
add_action( 'wp_ajax_nopriv_bivt_admin_categories', 'bivt_admin_add_categories' );



function bivt_admin_add_categories()
{

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/CategoriesValidator.php');


	$cv = new \wp_manager_course\CategoriesValidator();

	
	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

	$cv->make($_POST,[
		'name' => ['required','validateCategoryName'],
		//'slug' => ['required'],
		'description' => ['required'],
		'type' => ['required'],
		'meta_description' => ['required'],
		'meta_keyword' => ['required'],
		'status' => ['required','validateStatus'],
		'picture' => ['validateFile','validateFileSize']
	]);

	if($cv->fails()) {
		echo json_encode(['errors'=>$cv->errors()]);
	}
	else
	{
		global $wpdb;


		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/category_pictures/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}




		$wpdb->insert(
		'categories', 
		array(
			'name' => $_POST['name'], 
			'slug' => strtolower(str_replace(' ', '-',$_POST['name'])),
			'description' => $_POST['description'],
			'type' => $_POST['type'],
			'meta_description' =>$_POST['meta_description'],
			'meta_keywords' => $_POST['meta_keyword'],
			'status' => $_POST['status'],
			'picture' => '/files/uploads/category_pictures/' . $newFileName,
			'created' => date('Y-m-d h:i:s'),
			'modified' => date('Y-m-d h:i:s')
		), 
		array( 
			'%s', 
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s',
			'%s'
		) 
		);

		echo json_encode(['success'=>1]);
	}


	wp_die();
}



