var $ = jQuery;
// on scroll, 
$(window).on('scroll',function(){
	if ( $('.parallax').length > 0 ) {
		var mainbottom = $('.parallax').offset().top + $('.parallax').height()-88;
	    // we round here to reduce a little workload
	    stop = Math.round($(window).scrollTop());
	    if (stop > mainbottom) {
	        $('#masthead').addClass('header-master');
	    } else {
	        $('#masthead').removeClass('header-master');
	   	}
	}
	
});

$(window).load(function() {
	if ( $('.spinner-loader').length > 0 ) {
		$('.spinner-loader').remove();
	}

	if($('.woocommerce-message a').length > 0){
		$('.woocommerce-message a').hide();
	}
	$('[customTooltip="tooltip"]').tooltip(); 

});