
/*
var teacher = (function () {
    
    var init = function () {
        $(document).ready(function () {
            
            /*$("#SubmitProfile").on('click', function () {
                var form = $('form#FormProfile').serialize(),
                    bio_ifr = $('#bio_ifr'),
                    bio_ifr_body = $('#tinymce[data-id="bio"]', bio_ifr.contents()).html();

                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_teacher_profile', form : form, bio_ifr_body: bio_ifr_body }, function(data) {
                    window.location.reload();
                }, 'json');
            }); */
            


       /*     $('body').on('change','input[name="upload_picture"]',function(e){


                e.preventDefault();
                var thisHere = this;

                var request = ($(this).attr('request')) ? $(this).attr('request') : '';
                var requestid = ($(this).attr('requestid')) ? $(this).attr('requestid') : '';

                if(request=='profile-save'){

                    //change the image to loading state
                    $(this).parent().find('img').attr('src',ajax_object_here.loading_image);

                    var pictureData =  (document.querySelector('input[name="upload_picture"]').files[0]) ?document.querySelector('input[name="upload_picture"]').files[0] : '';

                    var formData = new FormData();
                    
                    formData.append('picture',pictureData);
                    formData.append('action','bivt_teacher_profile_upload_image');












                }




                










            });


            $('body').on('click','.teacher-button',function(e){

                e.preventDefault();

                var thisHere = this;

                var requestid = ($(this).attr('requestid')) ? $(this).attr('requestid') : '';
                var request = ($(this).attr('request')) ? $(this).attr('request') : '';


                if(request=='profile-save'){

                    var state = ($(this).attr('state')) ? $(this).attr('state') : 2;


                    if(state==0){


                        var buttonValue = $(this).val();

                        $(this).attr('state',1);
                        $(this).attr('disabled','disabled');
                        $(this).val('Saving Changes...');

                        //clear errors classes first
                        $('input[name="name"]').parent().removeClass('has-error');
                        $('input[name="name"]').parent().find('span.help-block').remove();

                        $('input[name="username"]').parent().removeClass('has-error');
                        $('input[name="username"]').parent().find('span.help-block').remove();

                        $('textarea[name="bio"]').parent().removeClass('has-error');
                        $('textarea[name="bio"]').parent().find('span.help-block').remove();
                        
                        $('input[name="password"]').parent().removeClass('has-error');
                        $('input[name="password"]').parent().find('span.help-block').remove();
                        
                        $('input[name="confirm_password"]').parent().removeClass('has-error');
                        $('input[name="confirm_password"]').parent().find('span.help-block').remove();

                        $('input[name="upload_picture"]').parent().removeClass('has-error');
                        $('input[name="upload_picture"]').parent().find('span.help-block').remove();


                        var bio = $('#tinymce[data-id="bio"]',$('#io_ifr').contents()).html();

                        var formData = new FormData();
                        formData.append('name',$('input[name="name"]').val());
                        formData.append('bio',bio);
                        formData.append('username',$('input[name="username"]').val());
                        formData.append('action', 'bivt_teacher_profile');


                    }



                    

                }


            });
                
        });
    }

    return {
        init: init
    };
    
})();

teacher.init();

*/


/* Batman Code */

var Teacher = {};
(function(module){

    module.updateProfile = function(thisHere,buttonValue) {

        //clear errors classes first
        $('input[name="name"]').parent().removeClass('has-error');
        $('input[name="name"]').parent().find('span.help-block').remove();

        $('input[name="username"]').parent().removeClass('has-error');
        $('input[name="username"]').parent().find('span.help-block').remove();

        $('textarea[name="bio"]').parent().removeClass('has-error');
        $('textarea[name="bio"]').parent().find('span.help-block').remove();
        
        $('input[name="password"]').parent().removeClass('has-error');
        $('input[name="password"]').parent().find('span.help-block').remove();
        
        $('input[name="confirm_password"]').parent().removeClass('has-error');
        $('input[name="confirm_password"]').parent().find('span.help-block').remove();

        $('input[name="upload_picture"]').parent().removeClass('has-error');
        $('input[name="upload_picture"]').parent().find('span.help-block').remove();


    
        var bio = $('#tinymce[data-id="bio"]',$('#bio_ifr').contents()).html();

        var dataGet = {
            'name' : $('input[name="name"]').val(),
            'username' : $('input[name="username"]').val(),
            'bio' : bio,
            'password' : $('input[name="password"]').val(),
            'confirm_password' : $('input[name="confirm_password"]').val()

        };


         $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_teacher_profile', dataPass: dataGet}, function(data) {

            $(thisHere).attr('state',0);
            $(thisHere).removeAttr('disabled');
            $(thisHere).val(buttonValue);

            data = $.parseJSON(data);
            if(data.errors){
                
                if(data.errors['name']){

                    $('input[name="name"]').parent().addClass('has-error');
                    $('input[name="name"]').parent().append('<span class="help-block"><strong>'+data.errors['name']+'</strong></span>');
                }

                if(data.errors['username']){

                    $('input[name="username"]').parent().addClass('has-error');
                    $('input[name="username"]').parent().append('<span class="help-block"><strong>'+data.errors['username']+'</strong></span>');
                }

                if(data.errors['bio']){

                   $('textarea[name="bio"]').parent().addClass('has-error');
                    $('textarea[name="bio"]').parent().append('<span class="help-block"><strong>'+data.errors['bio']+'</strong></span>');
                }

                if(data.errors['password']){

                    $('input[name="password"]').parent().addClass('has-error');
                    $('input[name="password"]').parent().append('<span class="help-block"><strong>'+data.errors['password']+'</strong></span>');
                }

                if(data.errors['confirm_password']){

                    $('input[name="confirm_password"]').parent().addClass('has-error');
                    $('input[name="confirm_password"]').parent().append('<span class="help-block"><strong>'+data.errors['confirm_password']+'</strong></span>');
                }

            }

            if(data.success){
                window.location.reload();
            }


        });


    }

    module.updatePicture = function(thisHere) {


        var tempPicture = $(thisHere).parent().find('img').attr('src');
        $(thisHere).parent().find('img').attr('src',ajax_object_here.loading_image);
        


        var pictureData =  (document.getElementById('upload_picture').files[0]) ? document.getElementById('upload_picture').files[0] : '';

        var formData = new FormData();
        
        formData.append('picture',pictureData);
        formData.append('action','teacher_profile_update_image');
        

        $.ajax({

          url: ajax_object_here.ajax_url_here + '?action=request',

          data: formData,

          processData: false,

          contentType: false,

          type: 'POST',

          success: function(response){

            response = $.parseJSON(response);

            if(response.success==1){

                $(thisHere).parent().find('img').attr('src',response.picture_thumbnail);

            }

          }
        });

    }


})(Teacher);



$(document).ready(function(){

    $('body').on('change','input[name="upload_picture"]',function(e){

        e.preventDefault();
        var thisHere = this;

        var request = ($(this).attr('request')) ? $(this).attr('request') : '';
        var requestid = ($(this).attr('requestid')) ? $(this).attr('requestid') : '';

        if(request=='profile-save'){
            //change the image to loading state
            Teacher.updatePicture(this);
        }
    });

    /*$("#SubmitProfile").on('click', function () {
        var form = $('form#FormProfile').serialize(),
            bio_ifr = $('#bio_ifr'),
            bio_ifr_body = $('#tinymce[data-id="bio"]', bio_ifr.contents()).html();

        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_teacher_profile', form : form, bio_ifr_body: bio_ifr_body }, function(data) {
            window.location.reload();
        }, 'json');
    });*/


    
    $('body').on('click','.teacher-button',function(e){

        e.preventDefault();

        var thisHere = this;

        var requestid = ($(this).attr('requestid')) ? $(this).attr('requestid') : '';
        var request = ($(this).attr('request')) ? $(this).attr('request') : '';


        if(request=='profile-save'){

            var state = ($(this).attr('state')) ? $(this).attr('state') : 2;

            if(state==0){

                var buttonValue = $(this).val();

                $(this).attr('state',1);
                $(this).attr('disabled','disabled');
                $(this).val('Saving Changes...');

                //update profile
                Teacher.updateProfile(this,buttonValue);

            }
        }


    });

});

/* End Batman Code */