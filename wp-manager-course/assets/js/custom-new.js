/* Kenneth Sanctuary */

jQuery(document).ready(function(){

	tinyMCE.init({
		selector: '.mceEditor',
		plugins: 'lists advlist',
		toolbar: 'undo redo | bold italic underline | outdent indent | bullist numlist',
		menubar: false,
		
		setup: function (editor) {

			editor.on('change', function () {
				tinymce.triggerSave();
			});


// Convenient for testing:
//			editor.on('init', function () {
//				this.setContent('The init function knows on which editor its called - this is for ' + editor.id);
//			 });
		}
	});
	
	
	var searchValue = '';
	function listen(selector,property,value){

		var firstChange = 0,secondChange = 0;

		var t =setInterval(function(){
			var getValue = jQuery(selector).css(property);
			if(getValue==value){
				if(firstChange==0){
					firstChange = 1;
					secondChange = 0;

					searchValue = jQuery('#search-general').val();
					searchValue = (searchValue==undefined) ? '' : searchValue;

					if(typeof searchGeneral!="undefined")
						searchValue = (searchValue=='' ) ? searchGeneral : searchValue; 

					jQuery('#search-general-container').remove();
					jQuery('#phantom > div > div.widget-box').append('<div id="search-general-container" style="position: relative;z-index: 1000;margin-top: 95px;margin-left: -23px;padding-bottom: 10px;"><form method="GET" action="'+page_properties.home_url+'"><input type="text" id="search-general" name="search-general" value="'+searchValue+'" class="form-control" placeholder="Search..." style="border-radius: 0px;width: 0px;padding:0px;display:none;"><button isHidden=1 class="btn btn-default general-search-btn" type="submit" style="display:inline-block;height: 34px;border-radius: 0px;margin-top: -3px;margin-left: -6px;"><i class="fa fa-search" style="border-radius: 0px;"></i></button></form></div>');
				}
				
				

			}else{
				if(secondChange==0){
					secondChange = 1;
					firstChange = 0;

					searchValue = jQuery('#search-general').val();
					searchValue = (searchValue==undefined) ? '' : searchValue;
					
					if(typeof searchGeneral!="undefined")
						searchValue = (searchValue=='' ) ? searchGeneral : searchValue; 


					jQuery('#phantom > div > div.widget-box').find('#search-general-container').remove();
					jQuery('.header-bar').append('<div id="search-general-container" style="position: relative;z-index: 1000;margin-top: 95px;margin-left: -23px;padding-bottom: 10px;"><form method="GET" action="'+page_properties.home_url+'"><input type="text" id="search-general" name="search-general" value="'+searchValue+'" class="form-control" placeholder="Search..." style="border-radius: 0px;width: 0px;padding:0px;display:none;"><button isHidden=1 class="btn btn-default general-search-btn" type="submit" style="display:inline-block;height: 34px;border-radius: 0px;margin-top: -3px;margin-left: -6px;"><i class="fa fa-search" style="border-radius: 0px;"></i></button></form></div>');	
				}
				
			}

		},50);

		

	}

	listen('#phantom','opacity',1);
	
	jQuery('body').on('keydown','#search-general',function(e) {
	 	if(e.which==13){
	 		jQuery(this).parent().submit();
	 	}
	});


	

	jQuery('body').on('click','.general-search-btn',function(e){

		e.preventDefault();

		var isHidden = jQuery(this).attr('isHidden');

		if(isHidden==1){
			jQuery('#search-general').css('display','inline-block');
			jQuery(this).attr('isHidden',0);
			jQuery('#search-general').animate({
		    width: '165px'
		  	}, 300,function(){
		  		jQuery('#search-general').css('padding','7px 15px 7px 15px');
		  	});
		}else{
			if(jQuery('#search-general').length>0 && jQuery.trim(jQuery('#search-general').val())!=''){
				jQuery(this).parent().submit();
			}else{
				jQuery(this).attr('isHidden',1);
				jQuery('#search-general').css('padding','0px');
				jQuery('#search-general').animate({
			    width: '0px',
			  	}, 300,function(){
			  		jQuery('#search-general').css('display','none');
			  	});
			}
		}

	});
	
	
	//all transaction button goes here edit/delete/add button
	jQuery('body').on('click','.transaction-button',function(e){

		e.preventDefault();
		var state = jQuery(this).attr('state');


		if(state==0){
			
			var thisHere = this;
			var getValue = jQuery(this).html();
			jQuery(this).attr('state',1);
			var transaction = jQuery(this).attr('transaction');
			var table = jQuery(this).attr('table');
			
			console.log ("table: " + table + " transaction: " + transaction);

			if(table=='related_day' && transaction=='add'){

				jQuery(this).html('Adding Related Day...');
				var form = jQuery('#add-day').serialize();

				var fields = ['datum-add','location-id','docent-id','instance-id'];

				jQuery.each(fields,function(key,value){
					jQuery('#add-day').find('#'+value).parent().parent().removeClass('has-error');
					jQuery('#add-day').find('#'+value).parent().find('span.help-block').remove();
					
				});

				(function repeat(){

					jQuery.post(ajax_object_here.ajax_url_here,{form: form,action: 'bivt_add_related_days'},function(response){
						
						if(response==''){
							repeat();
						}else{

							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.status=='ok'){
								window.location.reload();
							}
							if(response.errors!=''){
								jQuery.each(response.errors,function(key,value){

									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;

									});
									jQuery('#add-day').find('#'+key).parent().parent().addClass('has-error');
									jQuery('#add-day').find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});

							}
						}
						
					},'json');
				})();



			}else if(table=='instance' && transaction=='edit'){



				var id = parseInt(jQuery(this).attr('rowid'));
				jQuery(this).html('Editing Instance...');
				var form = jQuery('#edit-instance-'+id+' > form').serialize();
				var fields = ['module_id','status','group','full','instance_id'];


				jQuery.each(fields,function(key,value){
					jQuery('#edit-instance-'+id).find('#'+value).parent().parent().removeClass('has-error');
					jQuery('#edit-instance-'+id).find('#'+value).parent().find('span.help-block').remove();
				});


				(function repeat(){
					jQuery.post(ajax_object_here.ajax_url_here,{form: form,action: 'bivt_edit_instance'},function(response){
						if(response==''){
							repeat();
						}else{
							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.status=='ok'){
								window.location.reload();
							}
							if(response.errors!=''){
								

								jQuery.each(response.errors,function(key,value){

									jQuery('#edit-instance-'+id).find('#'+key).parent().parent().addClass('has-error');
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									jQuery('#edit-instance-'+id).find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});
							}
						}
						
					},'json');
				})();

			}else if(table=='related_day' && transaction=='edit'){

				var id = parseInt(jQuery(this).attr('rowid'));


				jQuery(this).html('Editing Related Day...');
				var form = jQuery('#edit-day-'+id+' > form').serialize();

				var fields = ['datum','location_id','docent_id','instance_id'];

				jQuery.each(fields,function(key,value){
					jQuery('#edit-day-'+id).find('#'+value).parent().parent().removeClass('has-error');
					jQuery('#edit-day-'+id).find('#'+value).parent().find('span.help-block').remove();
					
				});

				(function repeat(){
					jQuery.post(ajax_object_here.ajax_url_here,{form: form,action: 'bivt_edit_related_day'},function(response){
						if(response==''){
							repeat();
						}else{

							console.log(response);
							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.status=='ok'){
								window.location.reload();
							}
							if(response.errors!=''){
								
								jQuery.each(response.errors,function(key,value){
									jQuery('#edit-day-'+id).find('#'+key).parent().parent().addClass('has-error');

									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									jQuery('#edit-day-'+id).find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});

							}
						}
						
					},'json');
				})();
			}else if(table=='instance' && transaction=='delete'){

				var id = parseInt(jQuery(this).attr('rowid'));
				jQuery.confirm({
				    title: 'Verwijderen',
				    content: 'Weet u zeker dat u deze groep wilt verwijderen?',
				    buttons: {
				        confirm: function () {
				            (function repeat(){

				            	jQuery.get(ajax_object_here.ajax_url_here,{id: id,action: 'bivt_admin_dinstances'},function(response){

				            		if(response==''){
				            			repeat();
				            		}else{
				            			jQuery(this).attr('state',0);
				            			if(response.status=='ok'){
				            				jQuery.alert('De groep is succesvol verwijderd.');
				            				jQuery('#instance-'+id).fadeOut('slow');
				            			}else{
				            				jQuery.alert('An error occured while trying to delete. Please refresh the page and try to delete again.');
				            			}
				            		}

				            	},'json');

				            })();
				        },
				        cancel: function () {
				            
				        }
				    }
				});
				jQuery(this).attr('state',0);

				
			}else if(table=='location' && transaction=='delete'){


				var id = parseInt(jQuery(this).attr('rowid'));
				jQuery.confirm({
				    title: 'Verwijderen',
				    content: 'Weet u zeker dat u deze locatie wilt verwijderen?',
				    buttons: {
				        confirm: function () {
				            (function repeat(){

				            	jQuery.get(ajax_object_here.ajax_url_here,{id: id,action: 'bivt_delete_location'},function(response){

				            		if(response==''){
				            			repeat();
				            		}else{
				            			jQuery(this).attr('state',0);
				            			if(response.status=='ok'){
				            				jQuery.alert('De locatie is succesvol verwijderd.');
				            				jQuery('#location-'+id).fadeOut('slow');
				            			}else{
				            				jQuery.alert('An error occured while trying to delete. Please refresh the page and try to delete again.');
				            			}
				            		}

				            	},'json');

				            })();
				        },
				        cancel: function () {
				            
				        }
				    }
				});

				jQuery(this).attr('state',0);

				

			}else if(table=='instance' && transaction=='add'){

				jQuery(this).html('Adding Instance...');
				var form = jQuery('#add-instance > form').serialize();
				var fields = ['module_id','status','group','full'];

				jQuery.each(fields,function(key,value){
					jQuery('#add-instance').find('#'+value).parent().parent().removeClass('has-error');
					jQuery('#add-instance').find('#'+value).parent().find('span.help-block').remove();

				});

				(function repeat(){
					jQuery.post(ajax_object_here.ajax_url_here,{form: form,action: 'bivt_add_instance'},function(response){
						
						jQuery(thisHere).html(getValue);
						if(response.status=='ok'){
							window.location.reload();
						}
						if(response.errors!=''){
							
							jQuery.each(response.errors,function(key,value){

								jQuery('#add-instance').find('#'+key).parent().parent().addClass('has-error');
								var accumErrors = '';
								jQuery.each(response.errors[key],function(key2,value2){
									accumErrors +=value2;
								});

								jQuery('#add-instance').find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
							});

						}
						
						
					},'json');
				})();

				
			}else if(table=='courses' && (transaction=='add' || transaction=='edit')){
				// Which row to edit? -1 for adding
				var id = parseInt(jQuery(this).attr('rowId'));

				// Add or edit?
				var selectorName = null;
				var mceTag = null;
				if (transaction == 'add') {
					jQuery(this).html('Opleiding toevoegen');
					selectorName = "#add-course";
					mceTag = "-add-course";
				} else {
					jQuery(this).html('Opleiding wijzigen');
					selectorName = "#edit-course-" + id;
					mceTag = "-edit-course-" + id;
				}
				
				// Clear error fields
				var fields = ['name','introduction','meta_description','meta_description', 'meta_keywords','conditions','description',
								'sessions','meeting_days','price','sbu','incasso','status','picture','category','teacher'];
				jQuery.each(fields,function(key,value){
					jQuery(selectorName).find('#'+value).parent().parent().removeClass('has-error');
					jQuery(selectorName).find('#'+value).parent().find('span.help-block').remove();

				});

				// Prepare load data info 
				var tinymceFields = ['introduction', 'description'];
				
				var formArray = jQuery(selectorName + ' > form').serializeArray();
				var formData = new FormData (jQuery(selectorName)) ;
				
				// Set action to take
				formData.append('action', (transaction=='add' ? 'bivt_admin_acourses' : 'bivt_admin_ecourses'));
				
				// Load all fields, except pictures
				var teacher = [], category = [];
				jQuery.each(formArray, function(key,value){

					if(jQuery.inArray(value.name, tinymceFields) == -1) {
						if(value.name=="teacher"){
							teacher.push(value.value);
						}else if(value.name=="category") {
							category.push(value.value);
						} else {
							formData.append(value.name, value.value);
						}
					} else {
						formData.append(value.name, tinymce.get(value.name+mceTag).getContent());
					}
				});
				
				formData.append('teacher',teacher);
				formData.append('category',category);
				
				// Load the picture, if any
				var fileArray = jQuery(selectorName + " #picture").prop("files");
				if (fileArray && fileArray.length > 0) {
					formData.append('picture', jQuery(selectorName + " #picture").prop("files")[0]);
				}
				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success: function(response){
			        	  console.log (response);
			          	if(response==''){
			          		repeat();
			          	}else{
			          		jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							console.log(response);
							if(response.success==1){
								window.location.reload();
							}
							if(response.errors!=''){
								console.log (response.errors);
								
								jQuery.each(response.errors,function(key,value){

									// Figure out whether action is 'add' or 'edit'
									var selectorName = null;
									if (response.course_id == "") {
										selectorName = "#add-course";
									} else {
										selectorName = "#edit-course-" + response.course_id;
									}
									
									// Update fields for errors
									jQuery(selectorName).find('#'+key).parent().parent().addClass('has-error');
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									jQuery(selectorName).find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});

							}
			          	}
			          }
			        });

				})();

				
			}else if(table=='location' && transaction=='add'){

				jQuery(this).html('Adding Location...');

				var form = jQuery('#add-location > form').serializeArray();
				var fields = ['name','address','status','picture'];

				jQuery.each(fields,function(key,value){

					jQuery('#add-location').find('#'+value).parent().parent().parent().removeClass('has-error');
					jQuery('#add-location').find('#'+value).parent().find('span.help-block').remove();

				});

				var fm = new FormData();
				var pictureData =  (jQuery('#add-location').find('#picture')[0].files[0]) ? jQuery('#add-location').find('#picture')[0].files[0] : '';
				fm.append('action','bivt_add_location');
				fm.append('picture',pictureData);

				jQuery.each(form,function(key,value){
					if(value.name!='address')
						fm.append(value.name,value.value);
				});
				fm.append('address',tinyMCE.activeEditor.getContent());


				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: fm,
			          processData: false,
			          contentType: false,
			          type: 'POST',
			          success: function(response){
			          	if(response==''){
			          		repeat();
			          	}else{

			          		jQuery(thisHere).attr('state',0);
			          		jQuery(thisHere).html(getValue);
			          		response = jQuery.parseJSON(response);

				          	if(response.success==1){
				          		window.location.reload();
				          	}
				          	if(response.errors!=''){
								jQuery.each(response.errors,function(key,value){
									jQuery('#add-location').find('#'+key).parent().parent().parent().addClass('has-error');
									var accumErrors = '';
									
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									jQuery('#add-location').find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});
							}
			          	}

			          }
			      	});
				})();
			}else if(table=='changerequests_view' && transaction=='delete'){


				var id = parseInt(jQuery(this).attr('rowid'));
				
				jQuery.confirm({
				    title: 'Delete Confirmation',
				    content: 'Are you sure you want to delete this item?',
				    buttons: {
				        confirm: function () {
				            
				            (function repeat(){

				            	jQuery.get(ajax_object_here.ajax_url_here,{id: id,action: 'bivt_delete_changerequest'},function(response){
				            		if(response==''){
				            			repeat();
				            		}else{
				            			jQuery(this).attr('state',0);
				            			if(response.status=='ok'){
				            				window.location.reload();
				            			
				            			}else{
				            				jQuery.alert('An error occured while trying to delete. Please refresh the page and try to delete again.');
				            			}
				            		}


				            	},'json');

				            })();
				        },
				        cancel: function () {
				            
				        }
				    }
				});
				jQuery(this).attr('state',0);
				

			}else if(table=='changerequests' && transaction=='delete'){

				var id = parseInt(jQuery(this).attr('rowid'));
				

				jQuery.confirm({
				    title: 'Delete Confirmation',
				    content: 'Are you sure you want to delete this item?',
				    buttons: {
				        confirm: function () {
				            
				            (function repeat(){

				            	jQuery.get(ajax_object_here.ajax_url_here,{id: id,action: 'bivt_delete_changerequest'},function(response){
				            		if(response==''){
				            			repeat();
				            		}else{
				            			jQuery(this).attr('state',0);
				            			if(response.status=='ok'){
				            				jQuery.alert('Item was successfully deleted.');
				            				jQuery('#change-request-'+id).fadeOut('slow');
				            			}else{
				            				jQuery.alert('An error occured while trying to delete. Please refresh the page and try to delete again.');
				            			}
				            		}


				            	},'json');

				            })();
				        },
				        cancel: function () {
				            
				        }
				    }
				});
				jQuery(this).attr('state',0);
				
			}else if(table=='related_day' && transaction=='delete'){
				
				var id = parseInt(jQuery(this).attr('rowid'));
				
				jQuery.confirm({
				    title: 'Delete Confirmation',
				    content: 'Are you sure you want to delete this item?',
				    buttons: {
				        confirm: function () {
				            
				            (function repeat(){

				            	jQuery.get(ajax_object_here.ajax_url_here,{id: id,action: 'bivt_delete_related_day'},function(response){

				            		if(response==''){
				            			repeat();
				            		}else{
				            			jQuery(this).attr('state',0);
				            			if(response.status=='ok'){
				            				jQuery.alert('Item was successfully deleted.');
				            				jQuery('#related-day-'+id).fadeOut('slow');
				            				
				            			}else{
				            				jQuery.alert('An error occured while trying to delete. Please refresh the page and try to delete again.');
				            			}
				            		}


				            	},'json');

				            })();



				        },
				        cancel: function () {
				            
				        }
				    }
				});
				jQuery(this).attr('state',0);

				
			}else if(table=='location' && transaction=='edit'){

				var id = parseInt(jQuery(this).attr('rowid'));

				jQuery(this).html('Editing Location...');

				var form = jQuery('#edit-location-'+id+' > form').serializeArray();
				var fields = ['name','address','status','picture'];

				jQuery.each(fields,function(key,value){
					jQuery('#edit-location-'+id).find('#'+value).parent().parent().parent().removeClass('has-error');
					jQuery('#edit-location-'+id).find('#'+value).parent().find('span.help-block').remove();
					
				});

				var fm = new FormData();
				var pictureData =  (jQuery('#edit-location-'+id).find('#picture')[0].files[0]) ? jQuery('#edit-location-'+id).find('#picture')[0].files[0] : '';
				fm.append('action','bivt_edit_location');
				fm.append('picture',pictureData);

				jQuery.each(form,function(key,value){
					if(value.name!='address'){
						fm.append(value.name,value.value);
					}
					
				});
				fm.append('address',tinyMCE.activeEditor.getContent());

				(function repeat(){

					jQuery.ajax({

			          url: ajax_object_here.ajax_url_here + '?action=request',

			          data: fm,

			          processData: false,

			          contentType: false,

			          type: 'POST',

			          success: function(response){

			          	if(response==''){
			          		repeat();
			          	}else{
			          		jQuery(thisHere).attr('state',0);
			          		jQuery(thisHere).html(getValue);

			          		response = jQuery.parseJSON(response);

				          	if(response.success==1){
				          		window.location.reload();
				          	}

				          	if(response.errors!=''){
								jQuery.each(response.errors,function(key,value){
									jQuery('#edit-location-'+id).find('#'+key).parent().parent().parent().addClass('has-error');
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});
									jQuery('#edit-location-'+id).find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
								});

							}
			          	}
			          	

			          }
			      	});
				})();
			}else if(table=='assignment' && transaction=='add'){

				var id = parseInt(jQuery(this).attr('rowid'));
				jQuery(this).html('Huiswerk versturen...');

				var selectorName = "#assignment-data-"+id;
				
				// Clear error fields
				var fields = ['assignment_subject','content','files'];
				var tinymceFields = ['content'];

				jQuery.each(fields,function(key,value){
					if(jQuery.inArray(value, tinymceFields)==-1) {
						jQuery(selectorName + ' > form').find('#'+value).parent().parent().removeClass('has-error');
						jQuery(selectorName + ' > form').find('#'+value).parent().find('span.help-block').remove();						
					} else {
						jQuery(selectorName + ' > form').find('#'+value+'-'+id).parent().parent().removeClass('has-error');
						jQuery(selectorName + ' > form').find('#'+value+'-'+id).parent().find('span.help-block').remove();
					}

				});

				// Prepare load data info 				
				var formArray = jQuery(selectorName+ ' > form').serializeArray();
				var formData = new FormData (jQuery(selectorName + ' > form')) ;
				
				console.log (formArray);
				
				// Set action to take
				formData.append('action', 'bivt_stud_addHW');
				
				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					if(jQuery.inArray(value.name, tinymceFields) == -1) {
						formData.append(value.name, value.value);
					} else {
						formData.append(value.name, tinymce.get(value.name+'-'+id).getContent());
					}
				});
				
				// Load the file, if any
				var fileArray = jQuery(selectorName + " #files").prop("files");
				if (fileArray && fileArray.length > 0) {
					formData.append('files', jQuery(selectorName  + " #files").prop("files")[0]);
				}
				


				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
						if(response==''){
							repeat();
						}else{
				        	 console.log (response);

							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){
								

								jQuery.each(response.errors,function(key,value){

									console.log(response.data);
									var id = response.data.assignment_id;
									var tinymceFields = ['content'];
									var selectorName = "#assignment-data-"+id;


									if(jQuery.inArray(key, tinymceFields)==-1) {										
										jQuery(selectorName + ' > form').find('#'+key).parent().parent().addClass('has-error');
									} else {
										jQuery(selectorName + ' > form').find('#'+key+'-'+id).parent().parent().addClass('has-error');
									}
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									if(jQuery.inArray(key, tinymceFields)==-1) {
										jQuery(selectorName + ' > form').find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
									} else {
										jQuery(selectorName + ' > form').find('#'+key+'-'+id).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');										
									}
								});
							}
						}
			          }
					});
				})();
			}else if(table=='day_attendance' && transaction=='edit'){

				var id = parseInt(jQuery(this).attr('days_id'));
				jQuery(this).html('Aanwezigheid bijwerken...');

				var selectorName = "#day-attendance-"+id;
				
				// Clear error fields
				var fields = ['teacher_id','days_id','student_id','recored_id', 'attendance','remarks'];
				var tinymceFields = [];

				jQuery.each(fields,function(key,value){
					if(jQuery.inArray(value, tinymceFields)==-1) {
						jQuery(selectorName + ' > form').find('#'+value).parent().parent().removeClass('has-error');
						jQuery(selectorName + ' > form').find('#'+value).parent().find('span.help-block').remove();						
					} else {
						jQuery(selectorName + ' > form').find('#'+value+'-'+id).parent().parent().removeClass('has-error');
						jQuery(selectorName + ' > form').find('#'+value+'-'+id).parent().find('span.help-block').remove();
					}

				});

				// Prepare load data info 				
				var formArray = jQuery(selectorName+ ' > form').serializeArray();				
				var formData = new FormData (jQuery(selectorName + ' > form')) ;
				
				// Set action to take
				formData.append('action', 'bivt_teacher_attendance');
				
				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					if(jQuery.inArray(value.name, tinymceFields) == -1) {
						formData.append(value.name, value.value);
					} else {
						formData.append(value.name, tinymce.get(value.name+'-'+id).getContent());
					}
				});
				
				// Load the file, if any
				var fileArray = jQuery(selectorName + " #files").prop("files");
				if (fileArray && fileArray.length > 0) {
					formData.append('files', jQuery(selectorName  + " #files").prop("files")[0]);
				}

				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
						if(response==''){
							repeat();
						}else{
				        	 console.log (response);

							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){
								

								jQuery.each(response.errors,function(key,value){

									console.log(response.data);
									var id = response.data.instance_id;
									var tinymceFields = [];
									var selectorName = "#day-attendance-"+id;


									if(jQuery.inArray(key, tinymceFields)==-1) {										
										jQuery(selectorName + ' > form').find('#'+key).parent().parent().addClass('has-error');
									} else {
										jQuery(selectorName + ' > form').find('#'+key+'-'+id).parent().parent().addClass('has-error');
									}
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});

									if(jQuery.inArray(key, tinymceFields)==-1) {
										jQuery(selectorName + ' > form').find('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
									} else {
										jQuery(selectorName + ' > form').find('#'+key+'-'+id).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');										
									}
								});
							}
						}
			          }
					});
				})();
//			
			} else if(table=='user_meta' && transaction=='social'){
				var id = parseInt(jQuery(this).attr('row_id'));
				var state = jQuery(this).attr('state');
				jQuery(this).html('Social Media bijwerken...');
				var selectorName = ".social-media";

				// Clear error fields
				var fields = ['twitter_id','facebook_id','googleplus_id'];

				jQuery.each(fields,function(key,value){
					jQuery(selectorName + ' > form').find('#'+value).parent().parent().removeClass('has-error');
					jQuery(selectorName + ' > form').find('#'+value).parent().find('span.help-block').remove();

				});

				// Prepare load data info 				
				var formArray = jQuery(selectorName+ ' > form').serializeArray();				
				var formData = new FormData (jQuery(selectorName + ' > form')) ;

				// Set action to take
				formData.append('action', 'bivt_profile_social');
				formData.append('id', jQuery(this).attr('id'));

				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					formData.append(value.name, value.value);
				});

				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
						if(response==''){
							repeat();
						}else{

							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){
								jQuery.each(response.errors,function(key,value){
									console.log(key+"="+value);
									var selectorName = ".social-media";
										
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().parent().addClass('has-error');
									
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
									
								});
							}
						}
			          }
					});
				})();
			} else if(table=='wp_users' && transaction=='changepass'){
				var state = jQuery(this).attr('state');
				jQuery(this).html('Wachtwoord wijzigen bijwerken...');
				var selectorName = ".personal-info";
	            
	            // Clear error fields
				var fields = ['password','confirm_password'];

				jQuery.each(fields,function(key,value){
					jQuery(selectorName + ' > form').find('#'+value).parent().parent().removeClass('has-error');
					jQuery(selectorName + ' > form').find('#'+value).parent().find('span.help-block').remove();

				});

				// Prepare load data info 				
				var formArray = jQuery(selectorName+ ' > form').serializeArray();				
				var formData = new FormData (jQuery(selectorName + ' > form')) ;

				// Set action to take
				formData.append('action', 'bivt_profile_password');
				formData.append('id', jQuery(this).attr('id'));

				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					formData.append(value.name, value.value);
				});


				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
						if(response==''){
							repeat();
						}else{

							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								alert("Wachtwoord is bijgewerkt");
								window.location.href = location.protocol + "//" + location.host;
							}
							if(response.errors!=""){
								jQuery.each(response.errors,function(key,value){
									console.log(key+"="+value);
									var selectorName = ".personal-info";
										
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().parent().addClass('has-error');
									
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
									
								});
							}
						}
			          }
					});
				})();
			} else if(table=='docent' && transaction=='biography'){
				var state = jQuery(this).attr('state');
				jQuery(this).html('Opslaan bijwerken...');
				var selectorName = ".biography";
				var tinymceFields = ['bio'];
				var formArray = jQuery(selectorName+' > form').serializeArray();
				var formData = new FormData (jQuery(selectorName + ' > form')) ;

				jQuery(selectorName + ' > form').find('#bio').parent().parent().removeClass('has-error');
				jQuery(selectorName + ' > form').find('#bio').parent().find('span.help-block').remove();

				// Set action to take
				formData.append('action', 'bivt_profile_bio');
				formData.append('id', jQuery(this).attr('id'));

				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					formData.append(value.name, value.value);
				});

				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){
								jQuery.each(response.errors,function(key,value){
									console.log(key+"-"+value);
									var selectorName = ".biography";
									
									jQuery(selectorName + ' > form').find('#bio').parent().parent().addClass('has-error')
									jQuery(selectorName + ' > form').find('#bio').parent().append('<span class="help-block" style="font-weight:bold;">'+value+'</span>');	
									
								});
							}
						
			          }
					});
				})();
			} else if(table=='user_meta' && transaction=='address_profile'){
				var state = jQuery(this).attr('state');
				jQuery(this).html('Adres gegevens opslaan bijwerken...');
				var selectorName = ".address-profile";
	            
	            // Clear error fields
				var fields = ['personal_firstname','personal_lastname', 'personal_address', 'personal_birthdate', 'personal_birthplace', 'personal_telephone', 'personal_mobile', 'personal_zipcode','personal_city', 'personal_country', 'billing_company','billing_first_name','billing_last_name', 'billing_address_1','billing_postcode', 'billing_city', 'billing_country'];

				jQuery.each(fields,function(key,value){
					jQuery(selectorName + ' > form').find('#'+value).parent().parent().removeClass('has-error');
					jQuery(selectorName + ' > form').find('#'+value).parent().find('span.help-block').remove();

				});

				// Prepare load data info 				
				var formArray = jQuery(selectorName+ ' > form').serializeArray();				
				var formData = new FormData (jQuery(selectorName + ' > form')) ;

				// Set action to take
				formData.append('action', 'bivt_profile_addresses');
				formData.append('id', jQuery(this).attr('id'));

				// Load all fields, except pictures
				jQuery.each(formArray, function(key,value){
					formData.append(value.name, value.value);
				});

				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
							jQuery(thisHere).attr('state',0);
							jQuery(thisHere).html(getValue);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){
								jQuery.each(response.errors,function(key,value){
									console.log(key+"-"+value);
									var selectorName = ".address-profile";
									
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().parent().addClass('has-error');
									
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});
									jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');	
									
								});
							}
						
			          }
					});
				})();
			} else if(table=='user_meta' && transaction=='uploadProfile'){
				var state = jQuery(this).attr('state');
				jQuery(this).html('Opslaan bijwerken...');
				jQuery(this).attr('state',0);
				var selectorName = ".profile-image";
				
				var pictureData =  (document.querySelector('.profile-image > form #picture').files[0]) ? document.querySelector('.profile-image > form #picture').files[0] : '';
                
                var formData = new FormData();
                formData.append('picture', pictureData);
                formData.append('action', 'bivt_profile_image');
                formData.append('id', jQuery(this).attr('id'));

        		// Clear error fields

					jQuery(selectorName + ' > form').find('#picture').parent().parent().removeClass('has-error');
					jQuery(selectorName + ' > form').find('#picture').parent().find('span.help-block').remove();

					
              
				(function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
			          		jQuery(thisHere).html('Opslaan');
							jQuery(thisHere).attr('state',0);
							
							if(response.success==1 ){
								window.location.reload();
							}
							if(response.errors!=""){

								jQuery.each(response.errors,function(key,value){
									console.log(key+"-"+value);
									var selectorName = ".profile-image";
									
									jQuery(selectorName + ' > form').find('[name="picture"]').parent().parent().addClass('has-error');
									
									var accumErrors = '';
									jQuery.each(response.errors[key],function(key2,value2){
										accumErrors +=value2;
									});
									jQuery(selectorName + ' > form').find('[name="picture"]').parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');	
									
								});
							}
						
			          }
					});
				
				})();

			} else if(table=='import' && transaction=='importXls'){
				var xlsData =  (document.querySelector('#importXls').files[0]) ? document.querySelector('#importXls').files[0] : '';
                
                var formData = new FormData();
                formData.append('xls', xlsData);
                formData.append('action', 'bivt_import_xls');

                (function repeat(){

					jQuery.ajax({
			          url: ajax_object_here.ajax_url_here + '?action=request',
			          data: formData,
			          processData: false,
			          contentType: false,
			          dataType: 'json',
			          type: 'POST',
			          success:
			          function(response){
			          		console.log(response);
			          }
					});
				
				})();

			} else {
				console.log ('Default');
				var tables = {
					'teacher_homework' : {
						'edit' : { 
							'button_text' : 'Huiswerk opslaan...',
							'selector' : '#homework-body-',
							'fields' : ['teacher_id', 'row_id', 'remarks','passed'],
							'tinymceFields' : ['remarks'],
							'arrayFields' : [],
							'file' : [],
							'ajax_action' : 'bivt_teacher_homework',
						}
					},
					'teacher-courses' : {
						'edit' : {  // called for 'add' as well
							'button_text' : 'Opleiding opslaan...',
							'selector' : '#body-courses-',
							'fields' : ['introduction', 'meta_description', 'meta_description', 'meta_keywords','conditions','description'],
							'tinymceFields' : ['introduction','description','teacher'],
							'arrayFields' : [],
							'file' : [],
							'ajax_action' : 'bivt_teacher_courses',
						}
					},
					'teacher-modules' : {
						'edit' : {  // called for 'add' as well
							'button_text' : 'Opleiding opslaan...',
							'selector' : '#body-modules-',
							'fields' : ['introduction', 'meta_description', 'meta_description', 'meta_keywords','conditions','description'],
							'tinymceFields' : ['introduction', 'description','teacher'],
							'arrayFields' : [],
							'file' : [],
							'ajax_action' : 'bivt_teacher_courses',
						}
					},

					'materials' : {
						'edit' : {  // called for 'add' as well
							'button_text' : 'Materiaal opslaan...',
							'selector' : '#body-materials-',
							'fields' : ['name', 'files'],
							'tinymceFields' : [],
							'arrayFields' : [],
							'file' : '#file-upload-',
							'ajax_action' : 'bivt_admin_materials',
						},
					},
					'assignments' : {
						'edit' : {  // called for 'add' as well
							'button_text' : 'Opdracht opslaan...',
							'selector' : '#body-assignment-',
							'fields' : ['subject', 'content', 'files'],
							'tinymceFields' : ['content'],
							'arrayFields' : [],
							'file' : '#file-',
							'ajax_action' : 'bivt_admin_assignments',
						},
					},
					'users' : {
						'create' : {  // called for 'add' as well
							'button_text' : 'Gebruiker opslaan...',
							'selector' : '#body-add-user',
							'fields' : ['group', 'email'],
							'tinymceFields' : ['group'],
							'arrayFields' : [],
							'file' : '',
							'ajax_action' : 'bivt_admin_users_create',
						},
					}
				};
				
				if (typeof(tables[table]) != 'undefined' && tables[table] != null &&
						typeof(tables[table][transaction] != 'undefined' && tables[table][transaction] != null)) {
					var action = tables[table][transaction];
					var id = parseInt(jQuery(this).attr('row_id'));

					jQuery(this).html(action.button_text);


					var selectorName = (action.selector[action.selector.length-1] == '-') ? action.selector + id : action.selector;
					var fileSelectorName = (action.file[action.file.length-1] == '-') ? action.file + id : action.file;
					console.log(selectorName);
					console.log(fileSelectorName);

					// Clear error fields
					var fields = action.fields;
					var tinymceFields = action.tinymceFields;
					
					
					jQuery.each(fields,function(key,value){
						jQuery(selectorName + ' > form').find('[name="'+value+'"]').parent().parent().removeClass('has-error');
						jQuery(selectorName + ' > form').find('[name="'+value+'"]').parent().find('span.help-block').remove();						
					});

					// Prepare load data info 				
					var formArray = jQuery(selectorName+ ' > form').serializeArray();		
					console.log(jQuery(selectorName + ' > form'));
					var formData = new FormData (jQuery(selectorName + ' > form')) ;
					
					// Set action to take
					formData.append('action', action.ajax_action);
					// Used upon return (repeat()) to find right data
					formData.append('_table', table);
					formData.append('_transaction', transaction);
					
					// Load all fields, except pictures
					var teacher = [];

					jQuery.each(formArray, function(key,value){
						if(jQuery.inArray(value.name, action.tinymceFields) == -1) {
							formData.append(value.name, value.value);
						} else if (jQuery.inArray(value.name, action.arrayFields) == -1) {
							var current = formData.get(value.name);

							if (current) {
								var values = [];
								if (typeof current === 'string') {
									values.push(current);
								} else {
									values = current;
								}
								values.push(value.value);
								formData.set(value.name, values);
							} else if( value.name == "teacher"){
								teacher.push(value.value);

							} else {
								formData.append(value.name, value.value);
							}
						} else {
							formData.append(value.name, tinymce.get(value.name+'-'+id).getContent());
						}
					});
					formData.append('teacher',teacher);
					// Load the file, if any
					var fileArray = jQuery(selectorName + " " + fileSelectorName).prop("files");
					if (fileArray && fileArray.length > 0) {
						formData.append('files', jQuery(selectorName  + " " + fileSelectorName).prop("files")[0]);
					}

					(function repeat(){

						jQuery.ajax({
				          url: ajax_object_here.ajax_url_here + '?action=request',
				          data: formData,
				          processData: false,
				          contentType: false,
				          dataType: 'json',
				          type: 'POST',
				          success:
				          function(response){
							if(response==''){
								repeat();
							}else{
					        	 console.log (response);

								jQuery(thisHere).attr('state',0);
								jQuery(thisHere).html(getValue);
								
								if(response.success==1 ){
									window.location.reload();
								}
								if(response.errors!=""){

									console.log(response.data);
									var action = tables[response.data._table][response.data._transaction];
									var tinymceFields = action.tinymceFields;
									var selectorName = (action.selector[action.selector.length-1] == '-') ? action.selector + id : action.selector;
									
									jQuery.each(response.errors,function(key,value){

										jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().parent().addClass('has-error');
										console.log (jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().parent());
										var accumErrors = '';
										jQuery.each(response.errors[key],function(key2,value2){
											accumErrors +=value2;
										});

										jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
										console.log (jQuery(selectorName + ' > form').find('[name="'+key+'"]').parent());
									});
								}
							}
				          }
						});
					})();
					
				}	else {
					console.log ("Table: " + table +  " and transaction: " + transaction + " NOT implemented!");
				}		
			}			
		}

	});

});