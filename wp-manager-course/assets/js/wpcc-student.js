var student = (function () {
    
    var init = function () {
        $(document).ready(function () {


            $('body').on('click','.SubmitHomework', function(e) {

                e.preventDefault();
              
                var thisHere = this;

                var state = parseInt($(this).attr('state'));

                if(state == 0) {
                    //remove error class
                    $('#FormHomework > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $('#FormHomework > div:nth-child(1) > div > input').parent().find('span.help-block').remove();
                    
                    $('#FormHomework > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $('#FormHomework > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();

                    $(this).attr('state', 1);
                    $(this).html('Submitting Homework...');
                    $(this).attr('disabled','disabled');

                    var content = tinyMCE.activeEditor.getContent({format: 'raw'});

                    var formData = new FormData();

                    formData.append('subject', $('#subject').val());
                    formData.append('content', content);
                    var fileData =  (document.querySelector('#FormHomework > div:nth-child(3) > div > input').files[0]) ? document.querySelector('#FormHomework > div:nth-child(3) > div > input').files[0] : '';
                    formData.append('files', fileData);
                    formData.append('action', 'bivt_stud_addHW');
                    
                    student.AddHomeWork(formData);

                }

                

            });

            function readURL(input, imgTarget) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();


                    reader.onload = function (e) {
                        $(imgTarget).attr('src', e.target.result);

                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }     
                            
        });
    }

    var AddHomeWork = function(formData) {
        jQuery.ajax({

            url: ajax_object_here.ajax_url_here + '?action=request',

            data: formData,

            processData: false,

            contentType: false,

            type: 'POST',

            success: function(response){
                //window.location.reload();

                response = $.parseJSON(response);

                $('#FormHomework').attr('state',0);
                $('#FormHomework').removeAttr('disabled');
                
                if(response.errors) {


                    if(response.errors['subject']) {

                       $('#FormHomework > div:nth-child(1) > div > input').parent().addClass('has-error');
                       $('#FormHomework > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>' + response.errors['subject'] +'</strong></span>');

                    }

                    if(response.errors['content']) {

                        $('#FormHomework > div:nth-child(2) > div > textarea').addClass('has-error');
                        $('#FormHomework > div:nth-child(2) > div > textarea').append('<span class="help-block"><strong>' + response.errors['content'] + '</strong></span>');

                    }                 

                }
                
                if(response.success) {
                    window.location.reload();
                }



            }
        });


    }

    return {
        init: init,
        AddHomeWork : AddHomeWork
    };
    
})();

student.init();
