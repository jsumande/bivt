
var admin = (function () {
    
    var init = function () {

        $(document).ready(function () {

            $(".fancybox").fancybox({
                helpers : {
                    title: {
                        type: 'inside',
                        position: 'top'
                    }
                },
                nextEffect: 'fade',
                prevEffect: 'fade'

            });


            $('body').on('keyup','#fullname',function(e){
                if(e.which!=32){
                    var s = jQuery(this).val();
                    s = s.toLowerCase();
                    var splits = s.split(' ');

                    var joinSplits = '';
                    for(var x=0;x<splits.length;x++){
                        joinSplits+=splits[x];
                    }
                    jQuery('#username').val(joinSplits);

                }

            });

            $('body').on('click','.register-visitor',function(e){
                
                e.preventDefault();

                var state = $(this).attr('state');
                var valHere = $(this).val();
                var thisHere = this;
                if(state==0){

                    $(this).val('Registering...');
                    $(this).attr('state',1);

                    var form = $('form#visitorAdd').serialize();
                    var fields = ['username','fullname','email'];

                    $.each(fields,function(key,value){
                        $('#'+value).parent().parent().removeClass('has-error');
                        $('#'+value).parent().find('span.help-block').remove(); 
                    });

                    (function repeat(){

                        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_visitor_add', form : form }, function(response) {
                            if(response==''){
                                repeat();
                            }else{

                                console.log(response);
                                $(thisHere).attr('state',0);
                                $(thisHere).val(valHere);
                                if(response.errors!=''){

                                    console.log(response.errors);
                                    $.each(response.errors,function(key,value){
                                        $('#'+key).parent().parent().addClass('has-error');
                                        var accumErrors = '';
                                        $.each(response.errors[key],function(key2,value2){
                                            accumErrors +=value2;
                                        });

                                        $('#'+key).parent().append('<span class="help-block" style="font-weight:bold;">'+accumErrors+'</span>');
                                    });
                                }

                                if(response.status=='ok'){
                                    $.alert('Please check your email for confirmation');
                                }

                               // var link = $('#redirectCart').val()+data.id;
                                //window.location = link;
                               // 
                            }
                           

                        },'json');
                    })();

                }


                
               
                  
            });

            //initialize datepicker input elements

            $('.date-type').datepicker({
                dateFormat: 'dd-mm-yy'
            });


            //Listeners here......
            ///////////////// NEWS AREA /////////////////

            // Add News Items
            $('body').on('click','#NewsSubmit',function(e){

                e.preventDefault();
              
                var thisHere = this;


                var state = parseInt($(this).attr('state'));

                if(state==0) {
                    //remove error class
                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(1) > div > input').parent().find('span.help-block').remove();
                    
                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();

                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(4) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(4) > div > div > div > input').parent().find('span.help-block').remove();


                    $(this).attr('state',1);
                    $(this).html('Adding News...');
                    $(this).attr('disabled','disabled');

                    var content = tinyMCE.activeEditor.getContent({format: 'raw'});

                    var formData = new FormData();

                    formData.append('title', $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(1) > div > input').val());
                    formData.append('content', content);
                    var pictureData =  (document.querySelector('#AddNews > div.form-group.form-group-sm > div > div:nth-child(4) > div > div > div > input').files[0]) ? document.querySelector('#AddNews > div.form-group.form-group-sm > div > div:nth-child(4) > div > div > div > input').files[0] : '';
                    formData.append('picture',pictureData);
                    formData.append('action', 'bivt_admin_news');

                    admin.AddNewsData(formData);
                }
            });
        



            // Edit News Items
            $(".EditNewsSubmit").on('click', function (e) {
                

                e.preventDefault();
                var state = parseInt($(this).attr('state'));
                var newsid = $(this).attr('newsid');

                if(state==0) {

                    //remove error class

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find(' div.form-group > div > div:nth-child(5) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find(' div.form-group > div > div:nth-child(5) > div > input').parent().find('span.help-block').remove();
                   

                    $(this).attr('state',1);
                    $(this).html('Editing News...');
                    $(this).attr('disabled','disabled');


                    var pictureData =  (document.querySelector('#EditNews'+newsid+' > div.form-group.form-group-sm > div > div:nth-child(5) > div > input').files[0]) ? document.querySelector('#EditNews'+newsid+' > div.form-group.form-group-sm > div > div:nth-child(5) > div > input').files[0] : '';


                    var formData = new FormData();

                    formData.append('id',newsid);
                    formData.append('title',$(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').val());
                    formData.append('content',$(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').val());
                    formData.append('picture',pictureData);
                    formData.append('action','bivt_edit_news');

                    admin.EditNewsData(formData,this);
                }



            });

            // Delete News Items
            $(".delete-news").on('click',function () {                
                if ( confirm('Do you want to delete this news item?') ) {
                    var id = $(this).attr("delete-row");

                    $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_dnews', id : id }, function(data) {
                       window.location.reload();
                    }, 'json');
                }
            });

             /////////////////  ACCREDITATIES AREA ////////////////
            // Add Accreditaties item
            $('body').on('click','#AccreditatySubmit',function(e){
                
                e.preventDefault();

                var state = parseInt($('#AccreditatySubmit').attr('state'));

                if(state==0) {

                    //remove error class
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(1) > div > input').parent().find('span.help-block').remove();

                    $('#AddAccreditaties > div.form-group > div > div:nth-child(2) > div > input').parent().removeClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(2) > div > input').parent().find('span.help-block').remove();

                    $('#AddAccreditaties > div.form-group > div > div:nth-child(3) > div > input').parent().removeClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(3) > div > input').parent().find('span.help-block').remove();

                    $('#AddAccreditaties > div.form-group > div > div:nth-child(4) > div > select').parent().removeClass('has-error');
                     $('#AddAccreditaties > div.form-group > div > div:nth-child(4) > div > select').parent().find('span.help-block').remove();

                    $('#AddAccreditaties > div.form-group > div > div:nth-child(5) > div > select').parent().removeClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(5) > div > select').parent().find('span.help-block').remove();

                    $('#AccreditatySubmit').attr('state',1);
                    $('#AccreditatySubmit').html('Adding Accreditaty...');
                    $('#AccreditatySubmit').attr('disabled','disabled');
                    var data = {
                        points: $('#AddAccreditaties > div.form-group > div > div:nth-child(1) > div > input').val(),
                        valid_from: $('#AddAccreditaties > div.form-group > div > div:nth-child(2) > div > input').val(),
                        valid_till: $('#AddAccreditaties > div.form-group > div > div:nth-child(3) > div > input').val(),
                        module_id: $('#AddAccreditaties > div.form-group > div > div:nth-child(4) > div > select').val(),
                        professional_organizational_id: $('#AddAccreditaties > div.form-group > div > div:nth-child(5) > div > select').val()
                    }

                    admin.AddAccreditatiesData(data);

                }

               
            

            });


            ///////////////// COURSES AREA /////////////////
            $(".add-courses").on('click', function (e) {
                $("#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(10) > div > select").select2({placeholder: "Select a category"});
                $("#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(11) > div > select").select2({placeholder: "Select a teacher"});

            });
            $(".edit-courses").on('click', function (e) {
                var id = $(this).attr("course-id");
                $("#EditCourses"+ id +" > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(11) > div > select").select2({placeholder: "Select a category"});
                $("#EditCourses"+ id +" > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(12) > div > select").select2({placeholder: "Select a teacher"});

            });
            // Add Courses from selected user
            $("#CoursesSubmit").on('click', function (e) {
                
                e.preventDefault();

                var state = parseInt($(this).attr('state'));
                var thisHere = this;
                var valHere = $(this).html();

                if(state==0) {

                    $(this).attr('state',1);
                    $(this).html('Adding Courses...');
                    $(this).attr('disabled','disabled');

                
                    //remove error class
                    $('#name').parent().removeClass('has-error');
                    $('#name').parent().find('span.help-block').remove();

                    $('#introduction').parent().removeClass('has-error');
                    $('#introduction').parent().find('span.help-block').remove();

                    $('#meta_introduction').parent().removeClass('has-error');
                    $('#meta_introduction').parent().find('span.help-block').remove();

                    $('#meta_keywords').parent().removeClass('has-error');
                    $('#meta_keywords').parent().find('span.help-block').remove();

                    $('#conditions').parent().removeClass('has-error');
                    $('#conditions').parent().find('span.help-block').remove();

                    $('#description').parent().removeClass('has-error');
                    $('#description').parent().find('span.help-block').remove();

                    $('#sessions').parent().removeClass('has-error');
                    $('#sessions').parent().find('span.help-block').remove();

                    $('#meeting_days').parent().removeClass('has-error');
                    $('#meeting_days').parent().find('span.help-block').remove();


                    $('#price').parent().removeClass('has-error');
                    $('#price').parent().find('span.help-block').remove();

                    $('#sbu').parent().removeClass('has-error');
                    $('#sbu').parent().find('span.help-block').remove();

                    $('#incasso').parent().removeClass('has-error'); 
                    $('#incasso').parent().find('span.help-block').remove();

                    $('#status').parent().removeClass('has-error');
                    $('#status').parent().find('span.help-block').remove();

                    $('#teacher').parent().parent().removeClass('has-error');
                    $('#teacher').parent().parent().find('span.help-block').remove();


                    $('#category').parent().parent().removeClass('has-error');
                    $('#category').parent().parent().find('span.help-block').remove();


                    $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(9) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(9) > div > div > div > input').parent().find('span.help-block').remove();

                    var intro = $('#introduction_ifr');
                    var introduction = $('#tinymce[data-id="introduction"]', intro.contents()).html();

                    var desc = $('#description_ifr').contents();
                    var description = $('#tinymce[data-id="description"]', desc.contents()).html();

                    var meta_description = $('#meta_description').val();

                    var meta_keywords = $('#meta_keywords').val()






            
                    var formData = new FormData();
                    formData.append('action', 'bivt_admin_acourses');
                    formData.append('name', $('#name').val());
                    formData.append('introduction', introduction);        
                    formData.append('meta_description', meta_description);
                    formData.append('meta_keywords', meta_keywords);
                    formData.append('conditions', $('#conditions').val());
                    formData.append('description', description);
                    formData.append('sessions', $('#sessions').val());
                    formData.append('meeting_days', $('#meeting_days').val());
                    formData.append('price', $('#price').val());
                    formData.append('sbu', $('#sbu').val());
                    formData.append('incasso', $('#incasso').val());
                    formData.append('status', $('#status :selected').val());
                    formData.append('category', $('#category').val());
                    formData.append('teacher', $('#teacher').val());
                    var pictureData =  (document.querySelector('#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(9) > div > div > div > input').files[0]) ? document.querySelector('#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(9) > div > div > div > input').files[0] : '';
                    formData.append('picture', pictureData);



                   /* console.log($('#teacher').val());
                    $(this).attr('state',0);
                    $(this).html(valHere);
                    $(this).removeAttr('disabled');*/
                    admin.AddCoursesData(formData,thisHere);

                }

            });

            // Edit Courses from selected user
            $(".EditCourseSubmit").on('click', function (e) {

                e.preventDefault();

                var state = parseInt($(this).attr('state'));
                var courseid = $(this).attr('courseid');
                var thisHere = this;


                if(state==0) {

                    //remove error class
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group >div:nth-child(2) > div:nth-child(6) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().find('span.help-block').remove();


                    $('select[courseid="category-'+courseid+'"]').parent().parent().removeClass('has-error');
                    $('select[courseid="category-'+courseid+'"]').parent().parent().find('span.help-block').remove();


                    $('select[courseid="teacher-'+courseid+'"]').parent().parent().removeClass('has-error');
                    $('select[courseid="teacher-'+courseid+'"]').parent().parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > input').parent().find('span.help-block').remove();

                  
                    $(this).attr('state',1);
                    $(this).html('Editing Course...');
                    $(this).attr('disabled','disabled');


                    //get all tinyMCE values
                    var intro = $('#EditCourses'+courseid+' > div.form-group> div:nth-child(1) > div:nth-child(2) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    var introduction = $('#tinymce[data-id="introduction"]', intro.contents()).html();

                    var meta_description = $('#EditCourses'+courseid+' > #meta_keywords').val();
                    
                    var meta_keywords = $('#EditCourses'+courseid+' > #meta_keywords').val();

                    var description = $('#EditCourses'+courseid+' > div.form-group > div:nth-child(2) > div:nth-child(2) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                     description = $('#tinymce[data-id="description"]', description.contents()).html();

                    //get picture data
                    var pictureData =  (document.querySelector('#EditCourses'+courseid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(10) > div > input').files[0]) ? document.querySelector('#EditCourses'+courseid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(10) > div > input').files[0] : '';

                    var formData = new FormData();
                    formData.append('id',$(this).attr('courseid'));
                    formData.append('name', $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').val());
                    formData.append('introduction',introduction);
                    formData.append('meta_description',meta_description);
                    formData.append('meta_keywords',meta_keywords);
                    formData.append('conditions',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').val());
                    formData.append('description', description);
                    formData.append('sessions',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').val());
                    formData.append('meeting_days',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').val());
                    formData.append('price',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > input').val())
                    formData.append('sbu',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').val());
                    formData.append('incasso',$(this).parent().parent().find('div.form-group >div:nth-child(2) > div:nth-child(6) > div > input').val());
                    formData.append('status',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').val());

                    formData.append('picture',pictureData);
                    var category = $('#EditCourses'+ courseid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(11) > div > select').val();
                    formData.append('category',category);
                    formData.append('teacher', $('select[courseid="teacher-'+courseid+'"]').val());

                    formData.append('action','bivt_admin_ecourses');

                    admin.EditCoursesData(formData,this,courseid);
                }

            });

    

            // Delete Courses
            $('body').on('click','.delete-courses',function (e) {

                e.preventDefault();

                var state = $(this).attr('state');

                var thisHere = this;

                var r = confirm('Do you want to delete this course item?');

                if(r==true) {

                    if(state==0) {

                        $(this).attr('state',1);
                        var id = parseInt($(this).attr('delete-row'));

                        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_dcourses', id : id }, function(data) {
                            $(thisHere).attr('state',0);
                           window.location.reload();
                        }, 'json');
                    }
                    
                }
            });


            ///////////////// USERS AREA /////////////////

            $("#UsersSubmit").on('click', function () {
                var form = $('form#AddUsers').serialize(),
                    groups = $('form#AddUsers #groups').val();
                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_adusers', form : form, groups: groups }, function(data) {
                     window.location.reload();
                }, 'json');

            });

            //Return data from selected user
            $(".edit-users").on('click',function () {
                var id = $(this).attr("user-row");
                $("#edit_users #groups option").remove();
                $("#edit_users #groups").html($("#add_users #groups").html());

                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_vedit_user', id : id }, function(data) {
                    $('#edit_users #id').val(data.ID);
                    $('#edit_users #username').val(data.user_login);
                    $('#edit_users #fullname').val(data.display_name);
                    $('#edit_users #email').val(data.user_email);
                    $('#edit_users #status option').each(function(){
                          if( $(this).val() == data.user_status){ 
                            $(this).attr("selected", true);    
                          } else {
                            $(this).attr("selected", false);
                          }
                    });

                },'json');

                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_vgroups_edit_user', id : id }, function(data) {
                     $('#edit_users #groups option').attr("selected", false); 
 
                     for (i = 0; i < data.length; i++ ) {
                             $('#edit_users #groups option').each(function(){
                                   if( $(this).val() ==  data[i]){  
                                         $(this).attr("selected", true); 
                                          
                                   }
                                   $("#edit_users #groups").select2();
                             });
                      
                                        
                     }
                     
                     //$('#edit_users #groups').removeAttr('class');
                
                       
                     
                },'json');
            });

            //Updated data from selected user
            $("#UsersUpdateSubmit").on('click',function () {
               var form = $('form#EditUsers').serialize(),
                   groups = $('form#EditUsers #groups').val(); 

               $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_euser', form : form, groups : groups }, function(data) {
                    window.location.reload();
                }, 'json');

            });

            //Removed data from selected user
            $(".delete-users").on('click',function () {
                if ( confirm('Do you want to delete this user?') ) {
                   var id = $(this).attr("user-row");
                   $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_duser', id : id }, function(data) {
                       window.location.reload();
                    }, 'json');
                }

            });

        ///////////////// USERS AREA - Groups /////////////////
            $("#addGroupsSubmit").on('click', function () {
                var form = $('form#AddGroups').serialize();
                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_aduser_group', form : form }, function(data) {
                     window.location.reload();
                }, 'json');
            });

            //Return data from selected group
            $(".edit-groups").on('click',function () {
                var id = $(this).attr("user-row");
                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_veuser_group', id : id }, function(data) {
                    $('#edit_users #id').val(data.ID);
                    $('#edit_users #username').val(data.user_login);
                    $('#edit_users #fullname').val(data.display_name);
                    $('#edit_users #email').val(data.user_email);
                    $('#edit_users #status option').each(function(){
                          if( $(this).val() == data.user_status){ 
                            $(this).attr("selected", true);    
                          } else {
                            $(this).attr("selected", false);
                          }
                    });

                },'json');

            });

            ///////////////// MODULES AREA /////////////////
            $( "#multiple_module_select" ).on('click', function (e) {
              $("#add_modules #course").select2({placeholder: "Select a course"});
              $("#add_modules #category").select2({placeholder: "Select a category"});
            });
            $("#ModuleSubmit").on('click', function (e) {

                e.preventDefault();

                var state = parseInt($(this).attr('state'));
                var thisHere = this;
                if(state==0) {

                    $(this).attr('state',1);
                    $(this).html('Adding Modules...');
                    $(this).attr('disabled','disabled');
                                
                    var status = $("#status :selected").val();


                    //remove error class
                    $('#add_module #introduction').parent().removeClass('has-error');
                    $('#add_module #introduction').parent().find('span.help-block').remove();
 
                    $('#add_module #name').parent().removeClass('has-error');
                    $('#add_module #name').parent().find('span.help-block').remove();

                    $('#add_module #description').parent().removeClass('has-error');
                    $('#add_module #description').parent().find('span.help-block').remove();

                    $('#add_module #conditions').parent().removeClass('has-error');
                    $('#add_module #conditions').parent().find('span.help-block').remove();

                    $('#add_module #programme').parent().removeClass('has-error');
                    $('#add_module #programme').parent().find('span.help-block').remove();

                    $('#add_module #meta_description').parent().removeClass('has-error');
                    $('#add_module #meta_description').parent().find('span.help-block').remove();

                    $('#add_module #meta_keywords').parent().removeClass('has-error');
                    $('#add_module #meta_keywords').parent().find('span.help-block').remove();

                    $('#add_module #sessions').parent().removeClass('has-error');
                    $('#add_module #sessions').parent().find('span.help-block').remove();

                    $('#add_module #meeting_days').parent().removeClass('has-error');
                    $('#add_module #meeting_days').parent().find('span.help-block').remove();

                    $('#add_module #price').parent().removeClass('has-error');
                    $('#add_module #price').parent().find('span.help-block').remove();

                    $('#add_module #sbu').parent().removeClass('has-error');
                    $('#add_module #sbu').parent().find('span.help-block').remove();

                    $('#add_module #level').parent().removeClass('has-error');
                    $('#add_module #level').parent().find('span.help-block').remove();

                    $('#add_module #exam').parent().removeClass('has-error');
                    $('#add_module #exam').parent().find('span.help-block').remove();

                    $('#add_module #incasso').parent().removeClass('has-error');
                    $('#add_module #incasso').parent().find('span.help-block').remove();

                    $('#add_module #preliminary_knowledge').parent().removeClass('has-error');
                    $('#add_module #preliminary_knowledge').parent().find('span.help-block').remove();

                    $('#add_module #goal').parent().removeClass('has-error');
                    $('#add_module #goal').parent().find('span.help-block').remove();

                    $('#add_module #target_audience').parent().removeClass('has-error');
                    $('#add_module #target_audience').parent().find('span.help-block').remove();

                    $('#add_module #literature').parent().removeClass('has-error');
                    $('#add_module #literature').parent().find('span.help-block').remove();

                    $('#add_module #status').parent().removeClass('has-error');
                    $('#add_module #status').parent().find('span.help-block').remove();

                    $('#add_module #picture').parent().removeClass('has-error');
                    $('#add_module #picture').parent().find('span.help-block').remove();

                    $('#add_module #teacher').parent().removeClass('has-error');
                    $('#add_module #teacher').parent().find('span.help-block').remove();

                    var introduction = $('#introduction_ifr');
                    introduction = $('#tinymce[data-id="introduction"]', introduction.contents()).html();

                    var description = $('#description_ifr');
                    description = $('#tinymce[data-id="description"]',description.contents()).html();

                    var programme = $('#add_module #programme_ifr');
                    programme = $('#tinymce[data-id="programme"]',programme.contents()).html();

                    var meta_description = $('#meta_description').val;

                    var meta_keywords = $('#meta_keywords').val();

                    var preliminary_knowledge = $('#preliminary_knowledge_ifr');
                    preliminary_knowledge = $('#tinymce[data-id="preliminary_knowledge"]',preliminary_knowledge.contents()).html();

                    var goal = $('#goal_ifr');
                    goal = $('#tinymce[data-id="goal"]',goal.contents()).html();


                    var target_audience = $('#target_audience_ifr');
                    target_audience = $('#tinymce[data-id="target_audience"]',target_audience.contents()).html();

                    var literature = $('#literature_ifr');
                    literature = $('#tinymce[data-id="literature"]',literature.contents()).html();

                    var formData = new FormData();

                    formData.append('action','bivt_admin_modules');
                    formData.append('introduction',introduction);
                    formData.append('name',$('#add_module #name').val());
                    formData.append('description',description);
                    formData.append('conditions', $('#add_module #conditions').val());
                    formData.append('programme',programme);
                    formData.append('meta_description',meta_description);
                    formData.append('meta_keywords',meta_keywords);
                    formData.append('sessions',$('#add_module #sessions').val());
                    formData.append('meeting_days',$('#add_module #meeting_days').val());
                    formData.append('price',$('#add_module #price').val());
                    formData.append('sbu',$('#add_module #sbu').val());
                    formData.append('level',$('#add_module #level').val());
                    formData.append('exam',$('#add_module #exam').val());
                    formData.append('incasso',$('#add_module #incasso').val());
                    formData.append('preliminary_knowledge',preliminary_knowledge);
                    formData.append('goal',goal);
                    formData.append('target_audience',target_audience);
                    formData.append('literature',literature);
                    formData.append('status',$('#add_module #status').val());


                    var pictureData =  (document.getElementById('picture').files[0]) ? document.getElementById('picture').files[0] : '';
                    formData.append('picture',pictureData);

                    var course = $('#AddModules > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(13) > div > select').val();
                    formData.append('course',course);

                    var category = $('#AddModules > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(14) > div > select').val();
                    formData.append('category',category);

                    var teacher = $('#AddModules > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(15) > div > select').val();
                    formData.append('teacher',teacher);

                    admin.AddModulesData(formData);


                }



            });
            

            //Return data from selected INSTANCES
            $(".edit_instances").on('click',function () {
                var id = $(this).attr("edit-row");
                
                $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_v_editinstances', id : id }, function(data) {

                $('#EditInstances #module option').each(function(){
                      if( $(this).val() == data.module_id){ 
                        $(this).attr("selected", true);    
                      } else {
                        $(this).attr("selected", false);
                      }
                });
                tinyMCE.activeEditor.setContent(data.comments);
                $('#EditInstances #group').val(data.group);
                $('#EditInstances #full').val(data.full);
                console.log(data.comments);




                    // $('#edit_users #id').val(data.ID);
                    // $('#edit_users #username').val(data.user_login);
                    // $('#edit_users #fullname').val(data.display_name);
                    // $('#edit_users #email').val(data.user_email);
                    // $('#edit_users #status option').each(function(){
                    //       if( $(this).val() == data.user_status){ 
                    //         $(this).attr("selected", true);    
                    //       } else {
                    //         $(this).attr("selected", false);
                    //       }
                    // });

                },'json');
            });
    
            /*
            **  Start Add Category
            **
            */
            function readURL(input,imgTarget) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();


                    reader.onload = function (e) {
                        $(imgTarget).attr('src', e.target.result);

                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }


            $('body').on('change','#AddNews > div.form-group.form-group-sm > div > div:nth-child(4) > div > div > div > input',function(){
                $('#AddNews > div.form-group.form-group-sm > div > div:nth-child(3)').css('display','block');
                readURL(this,'.picture_preview');
            });

            $('body').on('change','#AddModules > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(12) > div > div > div > input',function(){
                $('#AddModules > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(11)').css('display','block');
                readURL(this,'.picture_preview');
            });


            $('body').on('change',' #AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(9) > div > div > div > input',function(){

                $('#AddCourses > div.form-group> div:nth-child(2) > div:nth-child(8)').css('display','block');
                readURL(this,'.picture_preview');
            });


            $('body').on('change','#AddCategories > div:nth-child(8) > div > div > div:nth-child(2) > input',function(){
                $('#AddCategories > div:nth-child(7)').css('display','block');
                readURL(this,'.picture_preview');
            });


            $('body').on('change','#AddCategories > div:nth-child(8) > div > div > div:nth-child(2) > input',function(){
                $('#AddCategories > div:nth-child(7)').css('display','block');
                readURL(this,'.picture_preview');
            });

            $('body').on('change','.edit-news > div.form-group.form-group-sm > div > div:nth-child(5) > div > input',function(){

                var newsid= $(this).attr('newsid');
                readURL(this,'.picture_preview_new');
                $('#EditNews' + newsid + ' > div.form-group > div > div:nth-child(4)').css('display','block');

            });

            $('body').on('change','.edit-categories > div.form-group.form-group-sm > div > div:nth-child(9) > div > input',function(){
                
                var catid= $(this).attr('catid');
                readURL(this,'.picture_preview_new');
                $('#EditCategories' + catid + ' > div.form-group > div > div:nth-child(8)').css('display','block');

            });

            $('body').on('change','.edit-module > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(13) > div > input',function(){
                var moduleid = $(this).attr('moduleid');
                readURL(this,'.picture_preview_new');
                $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(12)').css('display','block');

            });

            $('body').on('change','.edit-location > div:nth-child(7) > div > div > input',function(){

                var locationId = $(this).attr('locationid');
                readURL(this,'.picture_preview_new');
                $('#edit-location-'+locationId+'> div:nth-child(6)').css('display','block');
            });

            
            $('body').on('change','.edit-courses > div.form-group> div:nth-child(2) > div:nth-child(10) > div > input',function(){
                var courseid= $(this).attr('courseid');

                readURL(this,'.picture_preview_new');
                $('#EditCourses'+courseid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(9)').css('display','block');

            });


            //add mouseover/mouseout event in the category picture
            $('.specialPicture').parent().mouseover(function(e){
                $('.x-close').css('display','block');
            });


            $('.specialPicture').parent().mouseout(function(e){
                $('.x-close').css('display','none');
            });

            $('body').on('click','.removeTablePicture',function(e){

                e.preventDefault();

                var thisHere = this;
                var state = parseInt($(this).attr('state'));
                var requestid = $(this).attr('requestid');
                if(state==0) {
                    var tableItem = '';
                    if($(this).attr('table')=='categories')
                        tableItem = 'category';

                    if($(this).attr('table')=='courses')
                        tableItem = 'course';

                    if($(this).attr('table')=='newsitems')
                        tableItem = 'news';

                    if($(this).attr('table')=='modules')
                        tableItem = 'modules';

                    if($(this).attr('table')=='locations')
                        tableItem = 'locations';


                    //table="locations" requestid="<?= $v->id; ?>"

                    var r = confirm('Are you sure you want to remove this photo from this '+tableItem+' item?');

                    if(r==true) {
                        $(this).attr('state',1);

                        $(this).parent().append('<div class="remove-picture-protect" style="background-color:black;z-index:1000;height:100%;width:100%;position:absolute;top: 0px;left: 0px;opacity: 0.5;/* padding: 15px; */"></div>');

                        var formData = new FormData();
                        formData.append('table',$(this).attr('table'));
                        formData.append('id',requestid);
                        formData.append('action','bivt_admin_remove_picture_ajax');

                        $.ajax({

                          url: ajax_object_here.ajax_url_here + '?action=request',

                          data: formData,

                          processData: false,

                          contentType: false,

                          type: 'POST',

                          success: function(response){
                                                       
                            response = $.parseJSON(response);

                            if(response.status!='ok') {

                                alert('Error removing the photo. Please refresh the page and try again.');
                            }else{

                                if($(thisHere).attr('table')=='categories'){
                                    $(thisHere).parent().find('div.remove-picture-protect').remove();
                                    $(thisHere).parent().html('No Image found.');
                                    $(thisHere).remove();
                                    $('#categories-id-'+requestid + '>td:nth-child(6) > img').remove();
                                    $('#categories-id-'+requestid+ '>td:nth-child(6)').html('No Image found.');

                                }else if($(thisHere).attr('table')=='newsitems' || $(thisHere).attr('table')=='courses' || $(thisHere).attr('table')=='modules' || $(thisHere).attr('table')=='locations'){
                                    $(thisHere).parent().find('div.remove-picture-protect').remove();
                                    $(thisHere).parent().html('No Image found.');
                                    $(thisHere).remove();
                                }
                                
                            }

                          }
                        

                        });

                    }


                    


                }


               

            });

    
            //remove category picture via ajax
            $('body').on('click','.removeCategoryPicture',function(e){

                e.preventDefault();

                var thisHere = this;
                var state = parseInt($(this).attr('state'));
                var catid = $(this).attr('categoryid');
                if(state==0) {


                    var r = confirm('Are you sure you want to remove this photo from this category item?');

                    if(r==true) {

                        $(this).attr('state',1);



                        $(this).parent().append('<div class="remove-picture-protect" style="background-color:black;z-index:1000;height:100%;width:100%;position:absolute;top: 0px;left: 0px;opacity: 0.5;/* padding: 15px; */"></div>');



                        var formData = new FormData();


                        formData.append('table','categories');
                        formData.append('id',catid);
                        formData.append('action','bivt_admin_remove_picture_ajax');



                        $.ajax({

                          url: ajax_object_here.ajax_url_here + '?action=request',

                          data: formData,

                          processData: false,

                          contentType: false,

                          type: 'POST',

                          success: function(response){
                            
                           
                            response = $.parseJSON(response);


                            if(response.status!='ok') {

                                alert('Error removing the photo. Please refresh the page and try again.');
                            }else{
                                $(thisHere).parent().find('div.remove-picture-protect').remove();
                                $(thisHere).parent().html('No Image found.');
                                $(thisHere).remove();
                                $('#categories-id-'+catid + '>td:nth-child(6) > img').remove();
                                $('#categories-id-'+catid+ '>td:nth-child(6)').html('No Image found.');
                            }

                          }
                        

                        });

                    }


                    


                }


               

            });

             // Add Category item
            $('body').on('click','#CategorySubmit',function(e){
                
                e.preventDefault();

                var state = parseInt($('#CategorySubmit').attr('state'));

                if(state==0) {

                    //remove error class
                    $('#AddCategories > div:nth-child(1) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(1) > div > div > div > input').parent().find('span.help-block').remove();

                    $('#AddCategories > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();

                    $('#AddCategories > div:nth-child(3) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(3) > div > div > div > input').parent().find('span.help-block').remove();

                    $('#AddCategories > div:nth-child(4) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(4) > div > div > div > input').parent().find('span.help-block').remove();

                    $('#AddCategories > div:nth-child(5) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(5) > div > div > div > input').parent().find('span.help-block').remove();

                    $('#AddCategories > div:nth-child(6) > div > div > div > select').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(6) > div > div > div > select').parent().find('span.help-block').remove();


                    $('#AddCategories > div:nth-child(8) > div > div > div > input').parent().removeClass('has-error');
                    $('#AddCategories > div:nth-child(8) > div > div > div > input').parent().find('span.help-block').remove();


                    $('#CategorySubmit').attr('state',1);
                    $('#CategorySubmit').html('Adding Category...');
                    $('#CategorySubmit').attr('disabled','disabled');

                    var description = tinyMCE.activeEditor.getContent({format: 'raw'});
                    var formData = new FormData();

                    formData.append('name',$('#AddCategories > div:nth-child(1) > div > div > div > input').val());
                    // formData.append('description',$('#AddCategories > div:nth-child(2) > div > textarea').val());
                    formData.append('description', description);
                    formData.append('type',$('#AddCategories #type').val());
                    formData.append('meta_description',$('#AddCategories  #meta_description').val());
                    formData.append('meta_keyword',$('#AddCategories  #meta_keywords ').val());
                    formData.append('status',$('#AddCategories > div:nth-child(6) > div > div > div > select').val());


                    var pictureData =  (document.querySelector('#AddCategories > div:nth-child(8) > div > div > div > input').files[0]) ? document.querySelector('#AddCategories > div:nth-child(8) > div > div > div > input').files[0] : '';


                    formData.append('picture',pictureData);

                    admin.AddCategoriesData(formData);

                }
               

            });
    
            //edit category 

            $('body').on('click','#EditCategorySubmit',function(e){

                e.preventDefault();

                var state = parseInt($(this).attr('state'));
                var catid = $(this).attr('categoryid');


                if(state==0) {

                    //remove error class

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(4) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(4) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(5) > div > input').parent().removeClass('has-error');
                     $(this).parent().parent().find('div.form-group > div > div:nth-child(5) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(6) > div > select').parent().removeClass('has-error');
                     $(this).parent().parent().find('div.form-group > div > div:nth-child(6) > div > select').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(8) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(8) > div > input').parent().find('span.help-block').remove();

                    $(this).attr('state',1);
                    $(this).html('Editing Category...');
                    $(this).attr('disabled','disabled');

                    var pictureData =  (document.querySelector('#EditCategories'+catid+' > div.form-group.form-group-sm > div > div:nth-child(9) > div > input').files[0]) ? document.querySelector('#EditCategories'+catid+' > div.form-group.form-group-sm > div > div:nth-child(9) > div > input').files[0] : '';


                    var formData = new FormData();

                    formData.append('id',$(this).attr('categoryid'));
                    formData.append('name',$(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').val());
                    formData.append('description',$(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').val());

                    formData.append('type',$(this).parent().parent().find('#type').val());
                    formData.append('meta_description',$(this).parent().parent().find('#meta_description').val());
                    formData.append('meta_keyword',$(this).parent().parent().find(' #meta_keywords ').val());
                    formData.append('status',$(this).parent().parent().find('div.form-group > div > div:nth-child(6) > div > select').val());
                    formData.append('picture',pictureData);
                    formData.append('action','bivt_admin_edit_categories');
                    
                    admin.EditCategoriesData(formData,this);
                }

            });

            /*
            **  End Category
            **
            */




             /*
            **  Edit Accreditaty Submit 
            **
            */
            
            $('body').on('click','#EditAccreditatySubmit',function(e){

                e.preventDefault();

                var state = parseInt($('#EditAccreditatySubmit').attr('state'));

                if(state==0) {

                    //remove error class
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > input').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div > div:nth-child(4) > div > select').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(4) > div > select').parent().find('span.help-block');


                    $(this).parent().parent().find('div.form-group > div > div:nth-child(5) > div > select').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div > div:nth-child(5) > div > select').parent().find('span.help-block');


                    $(this).attr('state',1);
                    $(this).html('Editing Accreditaty...');
                    $(this).attr('disabled','disabled');

                    var data = {
                        id: $(this).attr('accreditatyid'),
                        points: $(this).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').val(),
                        valid_from: $(this).parent().parent().find('div.form-group > div > div:nth-child(2) > div > input').val(),
                        valid_till:$(this).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').val(),
                        module_id:$(this).parent().parent().find('div.form-group > div > div:nth-child(4) > div > select').val(),
                        professional_organizational_id: $(this).parent().parent().find('div.form-group > div > div:nth-child(5) > div > select').val()
                    }


                    admin.EditAccreditatiesData(data,this);
                }

            });
            

            //start
            // Edit Modules from selected user
            $('body').on('click','#EditModuleSubmit',function (e) {

                e.preventDefault();

                var state = parseInt($(this).attr('state'));
                var moduleid = $(this).attr('moduleid');
                var postID = $('#EditModules' + moduleid).attr('post-edit-id');
                var thisHere = this;

                if(state==0) {

                    //remove error class
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > textarea').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().find('span.help-block').remove();
                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().find('span.help-block').remove();


                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().find('span.help-block').remove();
                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().find('span.help-block').remove();

                   
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().find('span.help-block').remove();

                   
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().find('span.help-block').remove();

                    
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().find('span.help-block').remove();

                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().removeClass('has-error');
                    $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().find('span.help-block').remove();


                    $(this).attr('state',1);
                    $(this).html('Module opslaan...');
                    $(this).attr('disabled','disabled');

                    //get all tinyMCE values
                    var introduction = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(1) > div:nth-child(1) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    introduction = $('#tinymce[data-id="introduction"]', introduction.contents()).html();

                    var description = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(1) > div:nth-child(3) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    description = $('#tinymce[data-id="description"]', description.contents()).html();
                    
                    var programme = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(1) > div:nth-child(5) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    programme = $('#tinymce[data-id="programme"]', programme.contents()).html();


                    var meta_description = $('#EditModules'+moduleid+' > #meta_description').val();

                    var meta_keywords = $('#EditModules'+moduleid+' > #meta_keywords').val();


                    var preliminary_knowledge = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(6) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    preliminary_knowledge = $('#tinymce[data-id="preliminary_knowledge"]', preliminary_knowledge.contents()).html();


                    var goal = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(7) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    goal = $('#tinymce[data-id="goal"]', goal.contents()).html();
                    

                    var target_audience = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(8) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    target_audience = $('#tinymce[data-id="target_audience"]', target_audience.contents()).html();

                    var literature = $('#EditModules'+moduleid+' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(9) > div > div > div > div.mce-edit-area.mce-container.mce-panel.mce-stack-layout-item > iframe');
                    literature = $('#tinymce[data-id="literature"]', literature.contents()).html();


                    var formData = new FormData();
                    formData.append('id',moduleid);
                    formData.append('introduction',introduction);
                    formData.append('name',$(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > input').val());
                    formData.append('description',description);
                    formData.append('conditions',$(this).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').val());
                    formData.append('programme',programme);
                    formData.append('meta_description',meta_description);
                    formData.append('meta_keywords',meta_keywords);
                    formData.append('sessions', $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').val());
                    formData.append('meeting_days',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').val());

                    formData.append('price',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').val());

                    formData.append('sbu',$(this).parent().parent().find('div.form-group > di v:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').val());

                    formData.append('level',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').val());

                    formData.append('exam',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').val());

                    formData.append('incasso',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').val());

                    formData.append('preliminary_knowledge',preliminary_knowledge);
                    formData.append('goal',goal);
                    formData.append('status',$(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').val());
                    formData.append('target_audience',target_audience);
                    formData.append('literature',literature);
                    formData.append('post_id', postID);                       

                    var pictureData =  (document.querySelector('#EditModules'+ moduleid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(13) > div > input').files[0]) ? document.querySelector('#EditModules'+ moduleid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(13) > div > input').files[0] : '';
                    
                    formData.append('picture',pictureData);

                    var course = $('#EditModules'+ moduleid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(14) > div > select').val();
                    formData.append('course',course);

                    var category = $('#EditModules'+ moduleid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(15) > div > select').val();
                    formData.append('category',category);

                    var teacher = $('#EditModules'+ moduleid + ' > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(16) > div > select').val();
                    formData.append('teacher',teacher);

                    formData.append('action','bivt_admin_emodules');

                    admin.EditModulesData(formData,this);
                }

            });

            //end
            

            /*$(".EditModuleSubmit").on('click', function () {



                var id =  $( this ).parent().parent().attr('edit-row');
                console.log(id);
                admin.EditModulesData(id);
            });*/

            $('body').on('click','.delete-categories',function(e){

                e.preventDefault();

                var state = $(this).attr('state');

                var thisHere = this;

                var r = confirm('Do you want to delete this category item?');


                if(r==true) {

                    if(state==0) {

                        $(this).attr('state',1);

                        var id = parseInt($(this).attr('delete-row'));

                        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_dcategories', id : id }, function(data) {
                            $(thisHere).attr('state',0);
                           window.location.reload();
                        }, 'json');
                    }
                    
                }



            });




            $('body').on('click','.delete-accreditaties',function(e){

                e.preventDefault();

                var state = $(this).attr('state');

                var thisHere = this;

                var r = confirm('Do you want to delete this accreditaty item?');


                if(r==true) {

                    if(state==0) {

                        $(this).attr('state',1);

                        var id = parseInt($(this).attr('delete-row'));

                        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_daccreditaties', id : id }, function(data) {
                            $(thisHere).attr('state',0);
                           window.location.reload();
                        }, 'json');
                    }
                    
                }



            });

            // Delete modules
            $('body').on('click','.delete-modules',function(e){

                e.preventDefault();

                var thisHere = this;

                var state = $(this).attr('state');

                var r = confirm('Do you want to delete this module item?');

                if(r==true) {

                    if(state==0) {

                        $(this).attr('state',1);

                        var id = parseInt($(this).attr('delete-row'));

                        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_dmodules', id : id }, function(data) {
                            $(thisHere).attr('state',0);
                           window.location.reload();
                        }, 'json');
                    }
                    
                }
            });

            /*
            **  Material get extension from selected file 
            **
            */

            $('form#AddMaterials input[type="file"]').on('change', function(){
                var fileName = $(this).val();
                var ext = fileName.replace(/^.*\./, '');

                $("#file_type").val(ext);
            });

            $('.MaterialSubmit').on('click', function(e) {
                var form = $('#AddMaterials').serialize();
                var name = $('#name').val();
                var file_type = $('#file_type').val();
                var module_id = $('#module_id').val();
                var original_creator = $('#original_creator').val();
                var status = $('#status').val();

                var formData = new FormData();
                var input = document.getElementById('file');
                formData.append('action','bivt_admin_materials');
                formData.append("userfile", input.files[0]);
                formData.append("name", name);
                formData.append("file_type", file_type);
                formData.append("original_creator", original_creator);
                formData.append("module_id", module_id);
                formData.append("status", status);

                if ( name == '') {
                    $('#name').parent().parent().addClass('has-error');            
                } else {
                    $('#name').parent().parent().removeClass('has-error');
                }

                if ( original_creator == '') {
                    $('#original_creator').parent().parent().addClass('has-error');            
                } else {
                    $('#original_creator').parent().parent().removeClass('has-error');
                }

                if ( (name) && (original_creator) != '' )  {                    

                    jQuery.ajax({
                        url: ajax_object_here.ajax_url_here + '?action=request',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function(data){
                            window.location.reload();
                        }
                    });
                } else {
                    console.log('error');
                    e.preventDefault();
                }

            })
      
        });
        

    };

    var load_init = function() {
        $(window).load(function() {
            $('#bivt-table').DataTable();
        });
    }

    var EditCategoriesData = function(dataGet,thisHere) {

        //edit categories by issuing a call to a accreditaties corresponding url request and expecting JSON format
        jQuery.ajax({
            url: ajax_object_here.ajax_url_here + '?action=request',
            data: dataGet,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(dataHere){

                dataHere = $.parseJSON(dataHere);

                $(thisHere).attr('state',0);
                $(thisHere).removeAttr('disabled');
                $(thisHere).html('Edit Changes');

                if(dataHere.errors){

                    if(dataHere.errors['id'])
                        alert('Unexpected error occured. Please refresh the page.');

                    if(dataHere.errors['name']){
                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().addClass('has-error');
                         $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['name']+'</strong></span>');
                    }

                    if(dataHere.errors['description']){
                     
                       $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['description']+'</strong></span>');
                    }

                    if(dataHere.errors['type']){

                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().addClass('has-error');
                         $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['type']+'</strong></span>');

                    }

                    if(dataHere.errors['meta_description']){
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(4) > div > input').parent().addClass('has-error');
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(4) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_description']+'</strong></span>');

                    }

                    if(dataHere.errors['meta_keyword']){
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(5) > div > input').parent().addClass('has-error');
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(5) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_keyword']+'</strong></span>');
                    }

                    if(dataHere.errors['status']){
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(6) > div > select').parent().addClass('has-error');
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(6) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['status']+'</strong></span>');
                    }

                    if(dataHere.errors['picture']){

                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(8) > div > input').parent().addClass('has-error');
                      $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(8) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['picture']+'</strong></span>');
                    }


                }
                if(dataHere.success){
                   window.location.reload();
                }
            }
        });


    }


    var EditAccreditatiesData = function(dataGet,thisHere) {


        //add now the accreditaties by issuing a call to a accreditaties corresponding url request and expecting JSON format
        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_edit_accreditaties', dataPass: dataGet}, function(data) {
            $(thisHere).attr('state',0);
            $(thisHere).removeAttr('disabled');
            $(thisHere).html('Edit Changes');
            if(data.errors){


                if(data.errors['id'])
                    alert('Unexpected error occured. Please refresh the page.');

                if(data.errors['points']) {
                    $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().addClass('has-error');
                    $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['points']+'</strong>');
                }

                if(data.errors['valid_from']){
                   $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > input').parent().addClass('has-error');
                   $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['valid_from']+'</strong>');

                }

                if(data.errors['valid_till']){
                   $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().addClass('has-error');
                    $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(3) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['valid_till']+'</strong>');

                }
                if(data.errors['module_id']){
                    $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(4) > div > select').parent().addClass('has-error');
                    $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(4) > div > select').parent().append('<span class="help-block"><strong>'+data.errors['module_id']+'</strong>');
                }

                if(data.errors['professional_organizational_id']){
                  $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(5) > div > select').parent().addClass('has-error');
                  $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(5) > div > select').parent().append('<span class="help-block"><strong>'+data.errors['professional_organizational_id']+'</strong>');
                }

            }

            if(data.success){
               window.location.reload();
            }

        }, 'json');



    }


    var AddCategoriesData = function(formData) {


        formData.append('action','bivt_admin_categories');


        $.ajax({

          url: ajax_object_here.ajax_url_here + '?action=request',

          data: formData,

          processData: false,

          contentType: false,

          type: 'POST',

          success: function(response){

                response = $.parseJSON(response);

                $('#CategorySubmit').attr('state',0);
                $('#CategorySubmit').html('Add Category');
                $('#CategorySubmit').removeAttr('disabled');
                    
                if(response.errors) {
                    if(response.errors['name']) {
                        $('#AddCategories > div:nth-child(1) > div > div > div > input').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(1) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['name']+'</strong></span>');

                    }

                    if(response.errors['description']) {
                        $('#AddCategories > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+response.errors['description']+'</strong></span>');
                    }
                    if(response.errors['type']){
                        $('#AddCategories > div:nth-child(3) > div > div > div > input').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(3) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['type']+'</strong></span>');
                    }


                    if(response.errors['meta_description']){
                        $('#AddCategories > div:nth-child(4) > div > div > div > input').parent().addClass('has-error');
                         $('#AddCategories > div:nth-child(4) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['meta_description']+'</strong></span>');
                    }
                    if(response.errors['meta_keyword']){
                        $('#AddCategories > div:nth-child(5) > div > div > div > input').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(5) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['meta_keyword']+'</strong></span>');
                    }
                    if(response.errors['status']){
                        $('#AddCategories > div:nth-child(6) > div > div > div > select').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(6) > div > div > div > select').parent().append('<span class="help-block"><strong>'+response.errors['status']+'</strong></span>');
                    }
                    if(response.errors['picture']){
                        $('#AddCategories > div:nth-child(8) > div > div > div > input').parent().addClass('has-error');
                        $('#AddCategories > div:nth-child(8) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['picture']+'</strong></span>');
                    }

                }
                if(response.success) {
                    window.location.reload();
                }


          }   
        });

    }

    var AddAccreditatiesData = function(dataGet) {

        //add now the accreditaties by issuing a call to a accreditaties corresponding url request and expecting JSON format
        $.post(ajax_object_here.ajax_url_here + '?action=request', { action: 'bivt_admin_accreditaties', dataPass: dataGet}, function(data) {
            $('#AccreditatySubmit').attr('state',0);
            $('#AccreditatySubmit').removeAttr('disabled');
            $('#AccreditatySubmit').html('Add Accreditaty');
            if(data.errors){

                if(data.errors['points']) {
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(1) > div > input').parent().addClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['points']+'</strong></span>');
                }

                if(data.errors['valid_from']) {
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(2) > div > input').parent().addClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(2) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['valid_from']+'</strong></span>');
                }

                if(data.errors['valid_till']){
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(3) > div > input').parent().addClass('has-error');
                     $('#AddAccreditaties > div.form-group > div > div:nth-child(3) > div > input').parent().append('<span class="help-block"><strong>'+data.errors['valid_till']+'</strong></span>');
                }
                if(data.errors['module_id']){
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(4) > div > select').parent().addClass('has-error');
                    $('#AddAccreditaties > div.form-group > div > div:nth-child(4) > div > select').parent().append('<span class="help-block"><strong>'+data.errors['module_id']+'</strong></span>');
                }

                if(data.errors['professional_organizational_id']){
                   $('#AddAccreditaties > div.form-group > div > div:nth-child(5) > div > select').parent().addClass('has-error');
                   $('#AddAccreditaties > div.form-group > div > div:nth-child(5) > div > select').parent().append('<span class="help-block"><strong>'+data.errors['professional_organizational_id']+'</strong></span>');
                }
            }

            if(data.success){
               window.location.reload();
            }

        }, 'json');


    }



    var AddCoursesData = function (formData,thisHere) {

        jQuery.ajax({

          url: ajax_object_here.ajax_url_here + '?action=request',

          data: formData,

          processData: false,

          contentType: false,

          type: 'POST',

          success: function(response){

                response = $.parseJSON(response);

                $(thisHere).attr('state',0);
                $(thisHere).html('Add Course');
                $(thisHere).removeAttr('disabled');

                if(response.errors) {

                    if(response.errors['name']) {
                       $('#AddCourses > div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().addClass('has-error');
                       $('#AddCourses > div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+response.errors['name']+'</strong></span>');
                    }

                    if(response.errors['introduction']) {
                        $('#AddCourses > div.form-group> div:nth-child(1) > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                        $(' #AddCourses > div.form-group> div:nth-child(1) > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+response.errors['introduction']+'</strong></span>');
                    }

                    if(response.errors['meta_description']) {
                        $('#AddCourses > div.form-group> div:nth-child(1) > div:nth-child(3) > div > textarea').parent().addClass('has-error');
                        $(' #AddCourses > div.form-group> div:nth-child(1) > div:nth-child(3) > div > textarea').parent().append('<span class="help-block"><strong>'+response.errors['meta_description']+'</strong></span>');
                    }

                    if(response.errors['meta_keywords']) {
                        $('#AddCourses > div.form-group > div:nth-child(1) > div:nth-child(4) > div > textarea').parent().addClass('has-error');
                        $('#AddCourses > div.form-group> div:nth-child(1) > div:nth-child(4) > div > textarea').parent().append('<span class="help-block"><strong>'+response.errors['meta_keywords']+'</strong></span>');
                    }


                    if(response.errors['conditions']) {
                        
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+response.errors['conditions']+'</strong></span>');
                    }

                    if(response.errors['description']) {

                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+response.errors['description']+'</strong></span>');
                        
                    }

                    if(response.errors['sessions']) {

                        $('#AddCourses > div.form-group  > div:nth-child(2) > div:nth-child(3) > div > input').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').parent().append('<span class="help-block"><strong>'+response.errors['sessions']+'</strong></span>');
                        
                    }
                    

                    if(response.errors['meeting_days']) {

                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().append('<span class="help-block"><strong>'+response.errors['meeting_days']+'</strong></span>');
                        
                    }

                    if(response.errors['price']) {

                        $('#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(5) > div:nth-child(2)').addClass('has-error');
                        $('#AddCourses > div.form-group.form-group-sm > div:nth-child(2) > div:nth-child(5) > div:nth-child(2)').append('<span class="help-block"><strong>'+response.errors['price']+'</strong></span>');
                        
                    }

                    if(response.errors['sbu']) {

                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+response.errors['sbu']+'</strong></span>');
                        
                    }

                    if(response.errors['incasso']) {

                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(6) > div > input').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(6) > div > input').parent().append('<span class="help-block"><strong>'+response.errors['incasso']+'</strong></span>');
                        
                    }

                    if(response.errors['status']) {

                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().addClass('has-error');
                        $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().append('<span class="help-block"><strong>'+response.errors['status']+'</strong></span>');
                        
                    }

                    if(response.errors['category']){

                       $('#category').parent().parent().addClass('has-error');
                       $('#category').parent().parent().append('<span class="help-block"><strong>'+response.errors['category']+'</strong></span>');
                    }

                     if(response.errors['teacher']){

                       $('#teacher').parent().parent().addClass('has-error');
                       $('#teacher').parent().parent().append('<span class="help-block"><strong>'+response.errors['teacher']+'</strong></span>');
                    }


                    if(response.errors['picture']){

                       $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(9) > div > div > div > input').parent().addClass('has-error');
                       $('#AddCourses > div.form-group > div:nth-child(2) > div:nth-child(9) > div > div > div > input').parent().append('<span class="help-block"><strong>'+response.errors['picture']+'</strong></span>');
                    }

                }
                if(response.success) {
                    window.location.reload();
                }


            }
        });




    }

    var AddModulesData = function (formData) { 

        jQuery.ajax({

          url: ajax_object_here.ajax_url_here + '?action=request',

          data: formData,

          processData: false,

          contentType: false,

          type: 'POST',

          success: function(response){

                response = $.parseJSON(response);


                $('#ModuleSubmit').attr('state',0);
                $('#ModuleSubmit').html('Add Modules');
                $('#ModuleSubmit').removeAttr('disabled');
                

                if(response.errors) {


                    console.log(response.errors);

                    if(response.errors['introduction']) {
                        $('#introduction').parent().addClass('has-error');
                        $('#introduction').parent().append('<span class="help-block"><strong>'+response.errors['introduction']+'</strong></span>');
                    }

                    if(response.errors['name']) {
                        $('#name').parent().addClass('has-error');
                        $('#name').parent().append('<span class="help-block"><strong>'+response.errors['name']+'</strong></span>');
                    }

                    if(response.errors['description']) {
                        $('#description').parent().addClass('has-error');
                        $('#description').parent().append('<span class="help-block"><strong>'+response.errors['description']+'</strong></span>');
                    }

                    if(response.errors['conditions']) {
                        $('#conditions').parent().addClass('has-error');
                        $('#conditions').parent().append('<span class="help-block"><strong>'+response.errors['conditions']+'</strong></span>');
                    }


                    if(response.errors['programme']) {
                        $('#programme').parent().addClass('has-error');
                        $('#programme').parent().append('<span class="help-block"><strong>'+response.errors['programme']+'</strong></span>');
                    }

                    if(response.errors['meta_description']) {
                        $('#meta_description').parent().addClass('has-error');
                        $('#meta_description').parent().append('<span class="help-block"><strong>'+response.errors['meta_description']+'</strong></span>');
                    }

                    if(response.errors['meta_keywords']) {
                        $('#meta_keywords').parent().addClass('has-error');
                        $('#meta_keywords').parent().append('<span class="help-block"><strong>'+response.errors['meta_keywords']+'</strong></span>');
                    }

                    if(response.errors['sessions']) {
                        $('#sessions').parent().addClass('has-error');
                        $('#sessions').parent().append('<span class="help-block"><strong>'+response.errors['sessions']+'</strong></span>');
                    }

                    if(response.errors['meeting_days']) {
                        $('#meeting_days').parent().addClass('has-error');
                        $('#meeting_days').parent().append('<span class="help-block"><strong>'+response.errors['meeting_days']+'</strong></span>');
                    }

                    if(response.errors['price']) {
                        $('#price').parent().addClass('has-error');
                        $('#price').parent().append('<span class="help-block"><strong>'+response.errors['price']+'</strong></span>');
                    }

                    if(response.errors['sbu']) {
                        $('#sbu').parent().addClass('has-error');
                        $('#sbu').parent().append('<span class="help-block"><strong>'+response.errors['sbu']+'</strong></span>');
                    }

                    if(response.errors['level']) {
                        $('#level').parent().addClass('has-error');
                        $('#level').parent().append('<span class="help-block"><strong>'+response.errors['level']+'</strong></span>');
                    }

                    if(response.errors['exam']) {
                        $('#exam').parent().addClass('has-error');
                        $('#exam').parent().append('<span class="help-block"><strong>'+response.errors['exam']+'</strong></span>');
                    }

                    if(response.errors['incasso']) {
                        $('#incasso').parent().addClass('has-error');
                        $('#incasso').parent().append('<span class="help-block"><strong>'+response.errors['incasso']+'</strong></span>');
                    }

                    if(response.errors['preliminary_knowledge']) {
                        $('#preliminary_knowledge').parent().addClass('has-error');
                        $('#preliminary_knowledge').parent().append('<span class="help-block"><strong>'+response.errors['preliminary_knowledge']+'</strong></span>');
                    }

                    if(response.errors['goal']) {
                        $('#goal').parent().addClass('has-error');
                        $('#goal').parent().append('<span class="help-block"><strong>'+response.errors['goal']+'</strong></span>');
                    }

                    if(response.errors['target_audience']) {
                        $('#target_audience').parent().addClass('has-error');
                        $('#target_audience').parent().append('<span class="help-block"><strong>'+response.errors['target_audience']+'</strong></span>');
                    }

                    if(response.errors['literature']) {
                        $('#literature').parent().addClass('has-error');
                        $('#literature').parent().append('<span class="help-block"><strong>'+response.errors['literature']+'</strong></span>');
                    }

                    if(response.errors['status']) {
                        $('#status').parent().addClass('has-error');
                        $('#status').parent().append('<span class="help-block"><strong>'+response.errors['status']+'</strong></span>');
                    }

                    if(response.errors['picture']) {
                        $('#picture').parent().addClass('has-error');
                        $('#picture').parent().append('<span class="help-block"><strong>'+response.errors['picture']+'</strong></span>');
                    }

                    if(response.errors['teacher']) {
                        $('#teacher').parent().addClass('has-error');
                        $('#teacher').parent().append('<span class="help-block"><strong>'+response.errors['teacher']+'</strong></span>');
                    }


                }
                if(response.success) {
                    window.location.reload();
                }


            }
        });
    }


    var AddNewsData = function (formData) { 
        jQuery.ajax({

          url: ajax_object_here.ajax_url_here + '?action=request',

          data: formData,

          processData: false,

          contentType: false,

          type: 'POST',

          success: function(response){

                response = $.parseJSON(response);

                $('#NewsSubmit').attr('state',0);
                $('#NewsSubmit').html('Add News');
                $('#NewsSubmit').removeAttr('disabled');
                
                if(response.errors) {

                	if(dataHere.errors['id'])
                		alert('Unexpected error occured. Please refresh the page.');

                	//start
                	if(dataHere.errors['name']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['name']+'</strong></span>');
                	}

                	if(dataHere.errors['introduction']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['introduction']+'</strong></span>');
                	}

                	if(dataHere.errors['description']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['description']+'</strong></span>');
                	}

                	if(dataHere.errors['conditions']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['conditions']+'</strong></span>');
                	}

                	if(dataHere.errors['programme']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['programme']+'</strong></span>');
                	}

                	if(dataHere.errors['meta_description']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_description']+'</strong></span>');
                	}

                	if(dataHere.errors['meta_keywords']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_keywords']+'</strong></span>');
                	}

                	if(dataHere.errors['sessions']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sessions']+'</strong></span>');
                	}

                	if(dataHere.errors['meeting_days']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['meeting_days']+'</strong></span>');
                	}

                	if(dataHere.errors['price']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['price']+'</strong></span>');
                	}

                	if(dataHere.errors['sbu']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sbu']+'</strong></span>');
                	}


                	if(dataHere.errors['level']){
                		$(thisHere).parent().parent().find('div.form-group> div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group> div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['level']+'</strong></span>');
                	}

                	if(dataHere.errors['exam']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['exam']+'</strong></span>');
                	}

                	if(dataHere.errors['incasso']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['incasso']+'</strong></span>');
                	}

                	if(dataHere.errors['preliminary_knowledge']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['preliminary_knowledge']+'</strong></span>');
                	}

                	if(dataHere.errors['goal']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['goal']+'</strong></span>');
                	}

                	if(dataHere.errors['target_audience']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['target_audience']+'</strong></span>');
                	}

                	if(dataHere.errors['literature']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['literature']+'</strong></span>');
                	}

                	if(dataHere.errors['status']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['status']+'</strong></span>');
                	}

                	if(dataHere.errors['picture']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['picture']+'</strong></span>');
                	}

                	if(dataHere.errors['teacher']){
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().addClass('has-error');
                		$(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['teacher']+'</strong></span>');
                	}

                }
                if(response.success) {
                    window.location.reload();
                }


            }
        });
    }

    var EditNewsData = function (dataGet,thisHere) {
        
        jQuery.ajax({
            url: ajax_object_here.ajax_url_here + '?action=request',
            data: dataGet,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(dataHere){

                dataHere = $.parseJSON(dataHere);

                $(thisHere).attr('state',0);
                $(thisHere).removeAttr('disabled');
                $(thisHere).html('Edit Changes');

                if(dataHere.errors){

                    if(dataHere.errors['id'])
                        alert('Unexpected error occured. Please refresh the page.');

                    if(dataHere.errors['title']){

                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['title']+'</strong></span>');
                    }

                    if(dataHere.errors['content']){
                     
                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['content']+'</strong></span>');
                    }

                    if(dataHere.errors['picture']){

                      $(this).parent().parent().find(' div.form-group > div > div:nth-child(5) > div > input').parent().addClass('has-error');
                       $(this).parent().parent().find(' div.form-group > div > div:nth-child(5) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['picture']+'</strong></span>');
                    }



                }
                if(dataHere.success){
                   window.location.reload();
                }
            }
        });
    }


    var EditCoursesData = function(dataGet,thisHere,courseid) {

        jQuery.ajax({
            url: ajax_object_here.ajax_url_here + '?action=request',
            data: dataGet,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(dataHere){

                dataHere = $.parseJSON(dataHere);
                $(thisHere).attr('state',0);
                $(thisHere).removeAttr('disabled');
                $(thisHere).html('Edit Changes');
                
                if(dataHere.errors){

                    if(dataHere.errors['id'])
                        alert('Unexpected error occured. Please refresh the page.');

                    if(dataHere.errors['name']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['name']+'</strong></span>');
                    }

                    if(dataHere.errors['introduction']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                         $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['introduction']+'</strong></span>');
                    }

                    if(dataHere.errors['meta_description']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_description']+'</strong></span>');
                    }
                    
                    if(dataHere.errors['meta_keywords']){
                       $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > textarea').parent().addClass('has-error');
                       $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_keywords']+'</strong></span>');
                    }

                    if(dataHere.errors['conditions']){
                       $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['conditions']+'</strong></span>');
                    }

                    if(dataHere.errors['description']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().addClass('has-error');
                         $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['description']+'</strong></span>');
                    }

                    if(dataHere.errors['sessions']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').parent().addClass('has-error');
                        $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sessions']+'</strong></span>');
                    }

                    if(dataHere.errors['meeting_days']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['meeting_days']+'</strong></span>');
                    }

                    if(dataHere.errors['price']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(2) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['price']+'</strong></span>');
                    }
                    if(dataHere.errors['sbu']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().addClass('has-error');
                         $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sbu']+'</strong></span>');
                    }



                    if(dataHere.errors['incasso']){
                       $(thisHere).parent().parent().find('div.form-group >div:nth-child(2) > div:nth-child(6) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group >div:nth-child(2) > div:nth-child(6) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['incasso']+'</strong></span>');
                    }

                    if(dataHere.errors['status']){
                       $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['status']+'</strong></span>');
                    }


                    if(dataHere.errors['category']){
                      $('select[courseid="category-'+courseid+'"]').parent().parent().addClass('has-error');
                      $('select[courseid="category-'+courseid+'"]').parent().parent().append('<span class="help-block"><strong>'+dataHere.errors['category']+'</strong></span>');

                    }

                    if(dataHere.errors['teacher']){
                      $('select[courseid="teacher-'+courseid+'"]').parent().parent().addClass('has-error');

                      $('select[courseid="teacher-'+courseid+'"]').parent().parent().append('<span class="help-block"><strong>'+dataHere.errors['teacher']+'</strong></span>');

                    }


                    if(dataHere.errors['picture']){

                       $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > input').parent().addClass('has-error');
                        $(this).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['picture']+'</strong></span>');
                    }




                }
                if(dataHere.success){
                   window.location.reload();
                }
            }
        });
    }

    var EditModulesData = function(dataGet,thisHere) {


       jQuery.ajax({
            url: ajax_object_here.ajax_url_here + '?action=request',
            data: dataGet,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(dataHere){

                dataHere = $.parseJSON(dataHere);
                $(thisHere).attr('state',0);
                $(thisHere).removeAttr('disabled');
                $(thisHere).html('Edit Changes');
                
                if(dataHere.errors){

                    if(dataHere.errors['id'])
                        alert('Unexpected error occured. Please refresh the page.');

                    //start
                    if(dataHere.errors['introduction']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(1) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['introduction']+'</strong></span>');
                    }

                    if(dataHere.errors['name']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(2) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['name']+'</strong></span>');
                    }

                    if(dataHere.errors['description']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(3) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['description']+'</strong></span>');
                    }

                    if(dataHere.errors['conditions']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(4) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['conditions']+'</strong></span>');
                    }

                    if(dataHere.errors['programme']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(5) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['programme']+'</strong></span>');
                    }

                    if(dataHere.errors['meta_description']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(6) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_description']+'</strong></span>');
                    }

                    if(dataHere.errors['meta_keywords']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(1) > div:nth-child(7) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['meta_keywords']+'</strong></span>');
                    }

                    if(dataHere.errors['sessions']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(1) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sessions']+'</strong></span>');
                    }

                    if(dataHere.errors['meeting_days']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(2) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['meeting_days']+'</strong></span>');
                    }

                    if(dataHere.errors['price']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(2) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['price']+'</strong></span>');

                    }

                    if(dataHere.errors['sbu']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['sbu']+'</strong></span>');

                    }


                    if(dataHere.errors['level']){

                        $(thisHere).parent().parent().find('div.form-group> div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group> div:nth-child(2) > div:nth-child(4) > div:nth-child(2) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['level']+'</strong></span>');

                    }

                    if(dataHere.errors['exam']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(4) > div:nth-child(4) > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['exam']+'</strong></span>');

                    }

                    if(dataHere.errors['incasso']){

                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(5) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['incasso']+'</strong></span>');
                    }

                    if(dataHere.errors['preliminary_knowledge']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(6) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['preliminary_knowledge']+'</strong></span>');
                    }

                    if(dataHere.errors['goal']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(7) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['goal']+'</strong></span>');
                    }

                    if(dataHere.errors['target_audience']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(8) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['target_audience']+'</strong></span>');
                    }

                    if(dataHere.errors['literature']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(9) > div > textarea').parent().append('<span class="help-block"><strong>'+dataHere.errors['literature']+'</strong></span>');
                    }

                    if(dataHere.errors['status']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(10) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['status']+'</strong></span>');
                    }

                    if(dataHere.errors['picture']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(13) > div > input').parent().append('<span class="help-block"><strong>'+dataHere.errors['picture']+'</strong></span>');
                    }

                    if(dataHere.errors['teacher']){
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().addClass('has-error');
                        $(thisHere).parent().parent().find('div.form-group > div:nth-child(2) > div:nth-child(16) > div > select').parent().append('<span class="help-block"><strong>'+dataHere.errors['teacher']+'</strong></span>');
                    }


                }
                if(dataHere.success){
                   window.location.reload();
                }
            }
        });
    }

    // var AddMaterialsData = function() {        

        
    // }


    return {
        init: init,
        load_init : load_init,
        AddNewsData : AddNewsData,
        AddCoursesData: AddCoursesData,
        AddModulesData: AddModulesData,
        AddCategoriesData: AddCategoriesData,
        AddAccreditatiesData: AddAccreditatiesData,
        EditNewsData : EditNewsData,
        EditCoursesData : EditCoursesData,
        EditModulesData : EditModulesData,
        EditAccreditatiesData: EditAccreditatiesData,
        EditCategoriesData: EditCategoriesData
    };
    
})();

admin.init();
