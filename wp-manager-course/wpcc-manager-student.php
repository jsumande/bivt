<?php


/*
* Student Functionality
*
* Saving student data
*
*/

add_action( 'wp_ajax_bivt_stud_addHW', 'bivt_student_homework' );
add_action( 'wp_ajax_nopriv_bivt_stud_addHW', 'bivt_student_homework' );
function bivt_student_homework() {
	
	global $wpdb; // this is how you get access to the database

	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/HomeworkValidator.php');

	$shwv = new \wp_manager_course\HomeworkValidator();

	if (isset($_POST['form'])) {
		parse_str($_POST['form']); // In case jQuery is used for sending instead of ajax
	}
	$_POST['files'] = (isset($_FILES['files'])) ? $_FILES['files'] : '';

	$shwv->make($_POST,[
		'subject' => ['required'],
		'content' => ['required'],
		'files' => ['validateFileUpload','validateFileSize']
	]);


	if( $shwv->fails() ) {
		echo json_encode([
				'success'=>0,
				'errors'=>$shwv->errors(),
				'data'=>$_POST,
		]);
		} else {

		global $wpdb;


		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/homework_files/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['files']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $hwFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $hwFileName = md5(time()) . '.' . $imageFileType;
		//$hwFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $hwFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['files']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}

		$wpdb->insert(
			'homework', 
			array(
				'user_id' 		=> get_current_user_id(),
				'day_id'		=> isset($_POST['day_id']) ? $_POST['day_id'] : 0,
				'assignment_id'	=> isset($_POST['assignment_id']) ? $_POST['assignment_id'] : 0,
				'text_subject'	=> $_POST['subject'],
				'text_content'	=> $_POST['content'],
				'file_upload'	=> '/files/uploads/homework_files/' . $hwFileName,
				'created'		=> date('Y-m-d h:i:s'),
				'modified'		=> date('Y-m-d h:i:s')
			), 
			array( 
				'%d',
				'%d',
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s'
			) 
		);

		echo json_encode([
				'success'=>1,
				'errors'=>[],
				'data'=>$_POST,
		]);
		}
	wp_die();
	
}

