<?php
/**
 * News Functionality (Add, Edit and Delete)
 *
 *  WordPress Plugin name: WP Manager Course
 */

add_action( 'wp_ajax_bivt_teacher_profile', 'bivt_teacher_profile' );
add_action( 'wp_ajax_nopriv_bivt_teacher_profile', 'bivt_teacher_profile' );
function bivt_teacher_profile() {
	
	$id = get_current_user_id();

	global $wpdb;
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/TeacherValidator.php');

	$tv = new \wp_manager_course\TeacherValidator();


	$tv->make($_POST['dataPass'],[
		'name' =>['required'],
		'username' => ['required','validateUserName'],
		'bio' => ['required'],
		'password' => ['required','minPassword'],
		'confirm_password' => ['required','matchPassword']
	]);


	if($tv->fails()) {
		echo json_encode(['errors'=> $tv->errors()]);
	}
	else
	{
		$wpdb->update(
		'docents',
		array( 
			'name' 			=> $_POST['dataPass']['name'],
			'bio' 			=> $_POST['dataPass']['bio']			
		),
		array( 'user_id' => $id )
		);

		$wpdb->update(
			$wpdb->prefix.'users',
			array( 
				'user_login' 			=> $_POST['dataPass']['username'],
				'user_nicename' 		=> strtolower(str_replace(' ', '-', $_POST['dataPass']['username'])),
				'display_name' 			=> $_POST['dataPass']['username'],
				'password'              => 	wp_hash_password($_POST['dataPass']['password'])		
			),
			array( 'ID' => $id )
		);

		echo json_encode(['success'=>1]);
	}


	wp_die();
	
}



add_action( 'wp_ajax_teacher_profile_update_image', 'bivt_teacher_profile_update_image' );
add_action( 'wp_ajax_nopriv_teacher_profile_update_image', 'bivt_teacher_profile_update_image' );

function bivt_teacher_profile_update_image() {
	

	//start
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/TeacherValidator.php');


	$tv = new \wp_manager_course\TeacherValidator();
	
	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';

	$tv->make($_POST,[
		'picture' => ['validateFile','validateFileSize']
	]);


	if($tv->fails()) {
		echo json_encode(['errors'=>$tv->errors()]);
	}
	else
	{
		global $wpdb;

		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/profile_pictures/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}

		//updates or creates now the profile picture
		//profile_picture_original

		$currentUserId = get_current_user_id();


		$getUserMeta = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}usermeta WHERE user_id = $currentUserId AND meta_key='profile_picture_original'");


		if(count($getUserMeta)==1)
		{
			


			//delete the existing picture
			$str = $getUserMeta->meta_value;

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);

			$newPath = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);


			$targetFiles = [get_home_path() . 'wp-content/plugins/wp-manager-course'.$getUserMeta->meta_value,get_home_path() . 'wp-content/plugins/wp-manager-course'.$newPath];



			//delete main picture and corresponding thumbnail picture
			foreach($targetFiles as $targetFile)
			{
				if(file_exists($targetFile))
				{
					unlink($targetFile);
				}
			}

			$wpdb->update(
			$wpdb->prefix.'usermeta', 
			array(
				'meta_value' => '/files/uploads/profile_pictures/' . $newFileName
			),
			array( 'user_id' => $currentUserId,'meta_key'=>'profile_picture_original')
			);

		}
		else
		{
			$wpdb->insert(
			$wpdb->prefix . 'usermeta', 
			array(
				'meta_key' =>'profile_picture_original',
				'meta_value' => '/files/uploads/profile_pictures/' . $newFileName, 
				'user_id' => $currentUserId
			), 
			array( 
				'%s',
				'%s',
				'%d'
			) 
			);
		}

		//get now the thumbnail
		$str = '/files/uploads/profile_pictures/' . $newFileName;

		$result = explode('/', $str);

		$getLast = count($result) - 1;

		$limit = $getLast - 1;

		$getInitial = '';

		//get initial directory
		for($x=0;$x<=$limit;$x++)
		{
			$getInitial .=$result[$x] . '/';
		}

		$result = explode('.',$result[$getLast]);

		$pictureThumbnail = plugins_url('wp-manager-course'.sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]));
		
		echo json_encode(['success'=>1,'picture_thumbnail'=>$pictureThumbnail]);
	}
	//end
	

	wp_die();
}


add_action( 'wp_ajax_bivt_teacher_attendance', 'bivt_teacher_attendance' );
add_action( 'wp_ajax_nopriv_bivt_teacher_attendance', 'bivt_teacher_attendance' );
function bivt_teacher_attendance() {
	if(isset($_POST)) {
	
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/AttendanceValidator.php');
	
		$cv = new \wp_manager_course\AttendanceValidator();
		$dataValidation = [
				'attendance' => ['required','validateStatus'],
		];
	
		$cv->make($_POST,$dataValidation);
	
		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
			]);
			die ();
		}			

		global $wpdb;
					
		$teacher_id = isset($_POST['teacher_id']) ? $_POST['teacher_id'] : null;
		$days_id = isset($_POST['days_id']) ? $_POST['days_id'] : null;
		$student_id = isset($_POST['student_id']) ? $_POST['student_id'] : null;
		$record_id = isset($_POST['record_id']) ? $_POST['record_id'] : null;
		$present = isset($_POST['attendance']) ? $_POST['attendance'] : null;
		$remarks = isset($_POST['remarks']) ? $_POST['remarks'] : null;
		
		if (!($teacher_id && $days_id && $student_id && $present)) {
			echo json_encode([
					'success'=>0,
					'errors'=>['POST shall contain teacher_id, days_id, student_id, record_id, and attendance.'],
					'data'=>$_POST,
			]);
			die();
		}
		
		$present = $_POST['attendance'] == 'present' ? 1 : 0;
			
		$data = null;
		if($record_id > 0) {
			// Update
			$data = [
				'modified' => date('Y-m-d h:i:s'),
				'day_id' => $days_id,
				'docent_id' => $teacher_id,
				'student_id' => $student_id,
				'attended' => $present ? 1 : 0,
				'remarks' => isset($remarks) ? $remarks : "",
			];
		} else {
			// Insert
			$data = [
					'created' => date('Y-m-d h:i:s'),
					'modified' => date('Y-m-d h:i:s'),
					'day_id' => $days_id,
					'docent_id' => $teacher_id,
					'student_id' => $student_id,
					'attended' => $present ? 1 : 0,
					'remarks' => isset($remarks) ? $remarks : "",
			];
		}
	
		$result = 12345;
		if (isset($data['created'])) {
			$result =$wpdb->insert ('attendance_lists', $data, array('%s','%s','%d','%d','%d','%d','%s'));
		} else {
			$result = $wpdb->update ('attendance_lists', $data, array( 'id' => $record_id ), array('%s','%d','%d','%d','%d','%s'));
		}
	
		echo json_encode([
				'success'=>1,
				'errors'=>[],
				'result'=>$result,
				'record_id' => $record_id,
			]);
	}
	wp_die();
}


add_action( 'wp_ajax_bivt_teacher_homework', 'bivt_teacher_homework' );
add_action( 'wp_ajax_nopriv_bivt_teacher_homework', 'bivt_teacher_homework' );
function bivt_teacher_homework() {
	if(isset($_POST)) {

		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/HomeworkValidator.php');

		$cv = new \wp_manager_course\HomeworkValidator();
		$dataValidation = [
				'remarks' => ['required'],
				'passed' => ['required', 'validateStatus'],
		];

		$cv->make($_POST,$dataValidation);

		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
			]);
			die ();
		}
		
		global $wpdb;
		$table = 'homework';
		$data = [
				'correct_by' => $_POST['teacher_id'],
				'correct_on' => date('Y-m-d h:i:s'),
				'correction_remarks' => $_POST['remarks'],
				'passed' => $_POST['passed'] == 'passed' ? 1 : 0,
				'modified' => date('Y-m-d h:i:s'),
		];
		$format = ['%d','%s','%s','%d','%s'];
		$where = ['id' => $_POST['row_id']];
 		$wpdb->update($table, $data, $where, $format);		
	}
	echo json_encode([
			'success'=>1,
			'errors'=>[],
			'data'=>$_POST,
	]);
	die ();
}

add_action( 'wp_ajax_bivt_teacher_courses', 'bivt_teacher_courses' );
add_action( 'wp_ajax_nopriv_bivt_teacher_courses', 'bivt_teacher_courses' );
function bivt_teacher_courses() {
	if(isset($_POST)) {
		include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/CourseValidator.php');
		include( plugin_dir_path( __FILE__ ) . 'Validator/ModuleValidator.php');
		
		$source = isset($_POST['source']) ? $_POST['source'] : null;
		
		if($source == null) {
			echo json_encode([
					'success'=>0,
					'errors'=>['Variable "source" has not been posted. Not clear whether to deal with a course or module.'],
					'data'=>$_POST,
			]);
			die ();
		}
		
		
		$cv = ($source == 'courses') ? new \wp_manager_course\CourseValidator() : new \wp_manager_course\ModuleValidator();
		$dataValidation = [
			'introduction'=> ['required'],
			'description' => ['required'],
			'conditions' => ['required'],
			'meta_description' => ['required'],
			'meta_keywords' => ['required'],
		];

		$cv->make($_POST,$dataValidation);

		if($cv->fails()) {
			echo json_encode([
					'success'=>0,
					'errors'=>$cv->errors(),
					'data'=>$_POST,
			]);
			die ();
		}

		global $wpdb;
		$table = ($source == 'courses') ? 'courses' : 'modules';
		$data = [
				'introduction' => $_POST['introduction'],
				'description' => $_POST['description'],
				'meta_description' => $_POST['meta_description'],
				'meta_keywords' => $_POST['meta_keywords'],
				'conditions' => $_POST['conditions'],
				'modified' => date('Y-m-d h:i:s'),
		];
		//$format = "'%s','%s','%s','%s','%s', '%s'";
		$where = ['post_id' => $_POST['post_id']];
		$wpdb->update($table, $data, $where);

		if (isset($_POST['teacher'])) {
			$teachers = explode(",",$_POST['teacher']);

			if ( $source == 'courses') {
				$wpdb->delete( 'docents_courses',  array( 'course_id' => $_POST['post_id'] ));
				foreach ($teachers as $teacher) {
					$wpdb->insert(
						'docents_courses', 
						array( 
							'docent_id' 	=> $teacher,
							'course_id' 	=> $_POST['post_id']
						) 
					);
				}
			} else {
				$wpdb->delete( 'docents_modules',  array( 'module_id' => $_POST['post_id'] ));
				foreach ($teachers as $teacher) {
					$wpdb->insert(
						'docents_modules', 
						array( 
							'docent_id' 	=> $teacher,
							'module_id' 	=> $_POST['post_id']
						)
					);
				}
			}
		}
	}
	echo json_encode([
			'success'=>1,
			'errors'=>[],
			'data'=>$_POST,
			'table' => $table,
			'query_data' => $data,
			'query_format' => $format,
			'query_where' => $where,
	]);
	die ();
}

add_action( 'wp_ajax_bivt_profile_social', 'bivt_profile_social' );
add_action( 'wp_ajax_nopriv_bivt_profile_social', 'bivt_profile_social' );

function bivt_profile_social() {
	global $wpdb;
	$id = $_POST['id'];
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');
	$u = new \wp_manager_course\UserValidator();


	$u->make($_POST,[
		'twitter' => ['validateUrl'],
		'facebook' => ['validateUrl'],
		'googleplus' => ['validateUrl']
	]);


	if($u->fails()){
		$response['errors'] = $u->errors();
		
	}else{
		foreach ($_POST as $key => $value) {
			if ($key == 'twitter' || $key == 'facebook' || $key =='googleplus') {
				$havemeta = get_user_meta($id, $key, false);
					if ($havemeta) {
						update_user_meta( $id, $key, $value);
					} else {
						add_user_meta( $id, $key, $value);
					}
			}
		}
		$response['success'] = 1;
	}

	echo json_encode($response);
	wp_die();	

}

add_action( 'wp_ajax_bivt_profile_password', 'bivt_profile_password' );
add_action( 'wp_ajax_nopriv_bivt_profile_password', 'bivt_profile_password' );
function bivt_profile_password() {
	global $wpdb;
	$id = $_POST['id'];
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');
	$u = new \wp_manager_course\UserValidator();

	$u->make($_POST,[
		'password' => ['required', 'validateMinPassword'],
		'confirm_password' => ['validatePassword']
	]);

	if($u->fails()){
		$response['errors'] = $u->errors();
		
	}else{
		wp_set_password( $_POST['password'], $id );
		$response['success'] = 1;
	}

	echo json_encode($response);
	wp_die();
}

add_action( 'wp_ajax_bivt_profile_bio', 'bivt_profile_bio' );
add_action( 'wp_ajax_nopriv_bivt_profile_bio', 'bivt_profile_bio' );
function bivt_profile_bio() {
	global $wpdb;
	$id = $_POST['id'];
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');
	$u = new \wp_manager_course\UserValidator();

	$u->make($_POST,[
		'bio' => ['required']
	]);
	if($u->fails()){
		$response['errors'] = $u->errors();
	} else {
		foreach ($_POST as $key => $value) {
			if ($key == 'bio') {
			$wpdb->update('docents',
		        array(
		            'bio'            =>  $_POST['bio']
		        ),
		        array( 
		            'user_id'    => $id
		        )
		    );

			}
		}
		$response['success'] = 1;
	}
	echo json_encode($response);
	wp_die();
}

add_action( 'wp_ajax_bivt_profile_addresses', 'bivt_profile_addresses' );
add_action( 'wp_ajax_nopriv_bivt_profile_addresses', 'bivt_profile_addresses' );
function bivt_profile_addresses() {
	global $wpdb;
	$id = $_POST['id'];
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');
	$u = new \wp_manager_course\UserValidator();

	$u->make($_POST,[
		'personal_firstname' => ['required'],
		'personal_lastname' => ['required'],
		'personal_address' => ['required'],
		'personal_zipcode' => ['required'],
		'personal_birthdate' => ['required'],
		'personal_birthplace' => ['required'],
		'personal_telephone' => ['required'],
		'personal_mobile' => ['required'],
		'personal_city' => ['required'],
		'personal_country' => ['required'],
		'billing_company' => ['required'],
		'billing_first_name' => ['required'],
		'billing_last_name' => ['required'],
		'billing_address_1' => ['required'],
		'billing_postcode' => ['required'],
		'billing_city' => ['required'],
		'billing_country' => ['required']
	]);

	if($u->fails()){
		$response['errors'] = $u->errors();
		
	}else{
		foreach ($_POST as $key => $value) {
			if ($key == 'personal_firstname' || $key == 'personal_lastname' || $key =='personal_address' || $key =='personal_zipcode' || $key =='personal_city' || $key =='personal_mobile' || $key =='personal_telephone' || $key =='personal_birthplace' || $key =='personal_birthdate' || $key =='personal_country' || $key =='billing_company' || $key =='billing_first_name' || $key =='billing_last_name' || $key =='billing_address_1' || $key =='billing_postcode' || $key =='billing_city' || $key =='billing_country') {
				$havemeta = get_user_meta($id, $key, false);
					if ($havemeta) {
						update_user_meta( $id, $key, $value);
					} else {
						add_user_meta( $id, $key, $value);
					}
			}
		}

	$wpdb->update(
		'docents', 
		array( 
			'name' 	=> $_POST['personal_firstname'] ." ". $_POST['personal_lastname']
		),
		array( 'user_id' => $id )
	);

		 $response['success'] = 1;
	}

	echo json_encode($response);
	wp_die();
}

add_action( 'wp_ajax_bivt_profile_image', 'bivt_profile_image' );
add_action( 'wp_ajax_nopriv_bivt_profile_image', 'bivt_profile_image' );
function bivt_profile_image() {
	global $wpdb;
	$id = $_POST['id'];
	$_POST['picture'] = (isset($_FILES['picture'])) ? $_FILES['picture'] : '';
	include( plugin_dir_path( __FILE__ ) . 'Validator/Validator.php');
	include( plugin_dir_path( __FILE__ ) . 'Validator/UserValidator.php');

	$u = new \wp_manager_course\UserValidator();
	

	$u->make($_POST,[
		'picture' => ['validateFile','validateFileSize']
	]);
	
	if($u->fails()){
			$response['errors'] = $u->errors();
			
	} else {

		//begin upload scripts here
        $targetDirectory = get_home_path() . 'wp-content/plugins/wp-manager-course/files/uploads/profile_images/';

        if(!file_exists($targetDirectory))
            mkdir($targetDirectory, 0777, true);
		

        //upload the image
		$imageFileType = pathinfo(basename($_FILES['picture']['name']),PATHINFO_EXTENSION);
        //do not delete in case it will be used

        $newFileName = sprintf('%s.%s',md5(time()),$imageFileType);

        // $newFileName = md5(time()) . '.' . $imageFileType;
		//$newFileName = strtolower(str_replace(' ', '-',$_POST['name'])) . '.' . $imageFileType;
		$targetFile = sprintf('%s%s', $targetDirectory, $newFileName);

		//if successfully uploaded the file, create now the thumbnail
		if(move_uploaded_file($_FILES['picture']['tmp_name'],$targetFile))
		{

			//upload too the thumbnail picture
			include_once('easyphpthumbnail/PHP5/easyphpthumbnail.class.php');

			$thumb = new easyphpthumbnail;

			$thumb->Chmodlevel = '0777';
			
			$thumb->Thumblocation = $targetDirectory;
			
			$thumb->Thumbprefix = 'thumb_';
			
			$thumb -> Createthumb($targetFile,'file');

		}
		foreach ($_POST as $key => $value) {
			if ($key == 'picture') {
				$havemeta = get_user_meta($id, $key, false);
					if ($havemeta) {
						update_user_meta( $id, $key, '/files/uploads/profile_images/' . $newFileName);
						update_user_meta( $id, 'picture_thumb', '/files/uploads/profile_images/thumb_' . $newFileName);
					} else {
						add_user_meta( $id, $key, '/files/uploads/profile_images/' . $newFileName);
						add_user_meta( $id, 'picture_thumb', '/files/uploads/profile_images/thumb_' . $newFileName);

					}
			}
		}
		$response['success'] = 1;
	}

	echo json_encode($response);
	wp_die();
}