<?php
global $wpdb;
global $product;
global $woocommerce;


$slug = (isset($_GET['page'])) ? $_GET['page'] : null;

if ( null != $slug ) {
	
	$tab = (isset($_GET['tab'])) ? $_GET['tab'] : 'omschrijving' ;

	$is_module = true;
	$current_module = $wpdb->get_row ("SELECT * FROM modules WHERE slug = '".$slug."'" );
	if (!$current_module) {
		// This should be changed. However, it's not possible at all right now to enroll for a course,
		// only for single modules. There are no course instances.
		$is_module = false;
		$current_module = $wpdb->get_row ("SELECT * FROM courses WHERE slug = '".$slug."'" );
	}
	$categories =  $wpdb->get_results("SELECT c.name, c.slug, c.type FROM modules AS m LEFT JOIN categories_modules AS cm ON cm.module_id = m.id LEFT JOIN categories AS c ON c.id = cm.category_id WHERE m.slug = '".$slug."'");
	$vak_list = [];
	$vorm_list = [];
	
	foreach ($categories as $key => $category) {
		if ($category->type == 'vak') {
			$vak_list[] = (object)['name' => $category->name,
								   'slug' => $category->slug,
			];
		} else if ($category->type == 'vorm') {
			$vorm_list[] = (object)['name' => $category->name,
								    'slug' => $category->slug,
			];
		}		
	}
	$vak_list = (object)$vak_list;
	$vorm_list = (object)$vorm_list;
	
	
	// view_only is used to display the data from, for instance, the portfolio or financial view.
	// it should not be possible to add the item again to the basket, viewing only.
	$view_only = isset($_GET['view_only']) ? ($_GET['view_only'] == 'true' ? true : false) : false;
	
	?>
	<div class="inner-container">
		<div class="row">			
	        <div class="col-md-12">	
				<h3><?= $current_module->name ?></h3>
				<h4>Vorm:
				<?php 
					foreach($vorm_list AS $k=>$vorm) {
				?>
					<a href="<?= get_site_url()?>?tab=<?= $vorm->slug ?>"><?= $vorm->name ?></a>
				<?php
					}
				?>
				</h4>
				<h4>Vak:
				<?php 
					foreach($vak_list AS $k=>$vak) {
				?>
					<a href="<?= get_site_url()?>?tab=<?= $vak->slug ?>"><?= $vak->name ?></a>
				<?php
					}
				?>
				</h4>
			</div>
		</div>

		<div class="bivt-nav-news">
		<br>
			<div class="filter">
			    <div class="filter-categories">
					<a <?= ($tab=="omschrijving") ? 'class="show-all act"': '' ?> 
						href=" <?= get_site_url() ?>/news/?page=<?= $slug ?>&tab=omschrijving&view_only=<?=$view_only ? 'true' : 'false';?>">Omschrijving
					</a>
					<a <?= ($tab=="details") ? 'class="show-all act"': '' ?>
						href=" <?= get_site_url() ?>/news/?page=<?= $slug ?>&tab=details&view_only=<?=$view_only ? 'true' : 'false';?>">Details
					</a>
					<a <?= ($tab=="programma") ? 'class="show-all act"': '' ?>
						 href=" <?= get_site_url() ?>/news/?page=<?= $slug ?>&tab=programma&view_only=<?=$view_only ? 'true' : 'false';?>">Programma
						 </a>
					<a <?= ($tab=="planning") ? 'class="show-all act"': '' ?>
						href=" <?= get_site_url() ?>/news/?page=<?= $slug ?>&tab=planning&view_only=<?=$view_only ? 'true' : 'false';?>">Planning
					</a>
					<a <?= ($tab=="docent") ? 'class="show-all act"': '' ?>
						href=" <?= get_site_url() ?>/news/?page=<?= $slug ?>&tab=docent&view_only=<?=$view_only ? 'true' : 'false';?>">Docent(en)
					</a>
				</div>
			</div>
		<br>
		</div>
		
		<div class="inner-container">
			<div class="row">			
	            <div class="col-md-12 thumb">		
				<?php 	
					switch ($tab) {
						case 'omschrijving':
						?>
							<label>Omschrijving</label>
							<p> <?= isset($current_module->introduction) ? strip_tags($current_module->introduction) : '' ?></p>
							<p> <?= isset($current_module->description) ? strip_tags($current_module->description) : '' ?></p>
							<p class="goal"> <?= isset($current_module->goal) ? strip_tags($current_module->goal) : '' ?></p>
							<p> <?= isset($current_module->target_audience) ? strip_tags($current_module->target_audience) : '' ?></p>
						<?php 
						break;
						case 'details':
						?>
							<label>Details</label>
							<p>Hieronder worden de details van deze scholing aangegeven</p>
							<table class="grid" cellspacing="0">
								<tbody>
									<tr>
										<td>SBU:</td>
										<td><?= isset($current_module->SBU) ? $current_module->SBU : '&nbsp;' ?></td>
									</tr>
									<tr>
										<td>Niveau:</td>
										<td><?= isset($current_module->level) ? $current_module->level : '&nbsp;' ?></td>
									</tr>	
									<tr>
										<td>Contactdagen::</td>
										<td><?= isset($current_module->meeting_days) ? $current_module->meeting_days : '&nbsp;' ?></td>
									</tr>					
									<tr>
										<td>Dagdelen:</td>
										<td><?= isset($current_module->sessions) ? $current_module->sessions : '&nbsp;' ?></td>
									</tr>					
									<tr>
										<td>Prijs:</td>
										<td><?= isset($current_module->price) ? $current_module->price : '&nbsp;' ?></td>
									</tr>						
									<tr>
										<td>Examen:</td>
										<td><?= isset($current_module->exam) ? $current_module->exam : '&nbsp;' ?></td>
									</tr>
								</tbody>
					  		</table>
							<div>
								<label>Overige informatie</label>
								<p><?= isset($current_module->conditions) ? $current_module->conditions : '&nbsp;' ?></p>
							</div>
							<div>
								<label>Geaccrediteerd en/of erkend door</label>
							</div>
							<p class="disclaimer"><b>Disclaimer:</b> Als BivT doen we ons best om deze lijst zo compleet en up-to-date mogelijk te houden. Het kan echter voorkomen dat dit niet het geval is, we raden daarom altijd aan om bij je beroepsvereniging navraag te doen of je voor het volgen van de scholing punten zult ontvangen</p>
						<?php 
						break;
						case 'programma':
						?>
							<label>Programma</label>
							<p><?= isset($current_module->programme) ? $current_module->programme : "Nog niet bekend" ?></p>
							<label>Literatuur</label>
							<p><?= isset($current_module->literature) ? $current_module->literature : "Nog niet bekend" ?></p>
						<?php
						break;
						case 'planning':
							$instances = $wpdb->get_results("SELECT days.id, days.datum, instances.group, instances.full, locations.name AS location, docents.name AS teacher from instances LEFT JOIN days ON instances.id = days.instance_id LEFT JOIN locations ON days.location_id = locations.id LEFT JOIN docents_days ON days.id = docents_days.day_id LEFT JOIN docents ON docents_days.docent_id=docents.id WHERE instances.module_id='".$current_module->id."' AND instances.status='open'");
							if (null != $instances) {
						?>
							<label>Geplande groepen</label>
							<div id="open-huiswerk">
								<div class="col-sm-12">
			  						<table id="bivt-table">
										<tr>
											<th>Groep</th>
											<th>Data</th>
											<th>Locatie</th>
											<th>Docent</th>
											<th>Vol</th>
										</tr>
										<?php
										foreach ($instances AS $k=>$v) {
											$status = $v->full > 0 ? 'Ja' : 'Nee';
										?>
										<tr>
											<td><?= isset($v->group) ? $v->group : '&nbsp;'?></td>
											<td><?= isset($v->datum) ? $v->datum : '&nbsp;'?></td>
											<td><?= isset($v->location) ? $v->location : '&nbsp;'?></td>
											<td><?= isset($v->teacher) ? $v->teacher : '&nbsp;'?></td>
											<td><?= isset($v->status) ? $v->status : '&nbsp;'?></td>
										</tr>
										<?php 
										}
										?>
									</table>
								</div>
							</div>
						<?php 
							}
						break;
						case 'docent':
							$instances = null;
							if ($is_module) {
								$instances = $wpdb->get_results("SELECT modules.id, docents.name, docents.bio, docents.picture FROM modules LEFT JOIN docents_modules ON modules.id=docents_modules.module_id LEFT JOIN docents ON docents_modules.docent_id=docents.id WHERE docents.name IS NOT NULL AND modules.id='".$current_module->id."'");
							}
							if (null != $instances) {
						?>					
							<div id="open-huiswerk">
								<div class="col-sm-12">
			  						<table id="bivt-table">
										<tr>
											<th>Naam</th>
											<th>Bio</th>
											<th>Foto</th>
										</tr>
										<?php 
											foreach ($instances AS $k=>$v) {
												$picture = null;
												if(isset($v->picture)) {
													$picture = '<img src="'.$v->picture.'" class="img-circle" alt="'.$v->name.'">';
												}
										?>
										<tr>
											<td><?= isset($v->name) ? $v->name : '&nbsp;' ?>
											<td><?= isset($v->bio) ? $v->bio : '&nbsp;' ?>
											<td><?= isset($picture) ? $picture : '&nbsp;' ?>
										</tr>
										<?php
											}
										?>
									</table>
								</div>
							</div>
						<?php
							}
						break;
					}
					$postId = (isset($_GET['post_id'])) ? $_GET['post_id'] : 0;
					$instances = $wpdb->get_results("SELECT days.id, days.datum FROM `instances` as ins LEFT JOIN days ON days.instance_id = ins.id where ins.module_id='".$current_module->id."' AND ins.status='open' AND ins.full=0 GROUP BY ins.id ORDER BY days.datum ASC");

					// If first day is in the past, then skip this instance
					// NOTE: Should be set in back-end: status=closed
					foreach ($instances as $key => $instance) {
						if(isset($instances[$key])) {
							if (strtotime($instance->datum) < time()) {
								unset($instances[$key]);
							}
						}
					}

					$url = home_url() .'/registration/?reg=true&AddCartId=' .$postId."&days=";
	
				?>
				</div>
			</div>
		</div>
		<?php if (!$view_only) : ?>
		<div class="inner-container">
			<div class="row">			
				<?php if (count($instances)>0) { ?>
					<select id="days" name="days">
					<?php foreach ($instances as $instance) { ?>
						<option value="<?= $instance->id; ?>"><?= $current_module->name; ?> - <?= date("d-m-Y", strtotime( $instance->datum )); ?></option>
					<?php } ?>
					</select> 
					
					<a id="addCartBtn" class="button wc-forward ripple" href="<?= $url; ?>" > 
						Nu inschrijven - € <?= $current_module->price; ?>
					</a>	
				<?php } else { ?>
				<br>
					<div class="button wc-forward ripple"> Deze opleiding is nog niet ingepland</div>
				<?php } ?>							
			</div>
		</div>
		<?php endif; // view_only?>
	</div>
	<?php } ?>	
		<script>
			jQuery(document).ready(function() {	
				jQuery("#days").change(function(e){
					var date = jQuery("#days :selected").val();
			         jQuery("#addCartBtn").attr("href","<?= $url; ?>"+date);
			     }).change();
			});
		</script>

		