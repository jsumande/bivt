<?php

global $wpdb;

if ($_GET['location']) {


	$id = $_GET['location'];
	$rows = $wpdb->get_results("SELECT * FROM locations WHERE id ='$id'");	

if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}
?>


<div class="container">
	<div class="courses_wrapper">
		<table class="course_table">
		<?php foreach($rows as $k=> $v) : ?>
		<tr>
			<th>ID</th>
			<td><?= $v->id ?></td>
		</tr>
		<tr>
			<th>Name</th>
			<td><?= $v->name ?></td>
		</tr>
		<tr>
			<th>Address</th>
			<td><?= $v->address ?></td>
		</tr>
		<tr>
			<th>Parking</th>
			<td><?= $v->parking ?></td>
		</tr>
		<tr>
			<th>Description</th>
			<td><?= $v->description ?></td>
		</tr>
		<tr>
			<th>Picture</th>
			<td><img src="<?= plugins_url( 'wp-manager-course' ) . $v->picture ?>"></td>
		</tr>
		<tr>
			<th>status</th>
			<td><?= $v->status ?></td>
		</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>

<?php } ?>