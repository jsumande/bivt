<?php

global $wpdb;

if ($_GET['accreditaties_id']) {

	$accreditaties = '';
	$id = $_GET['accreditaties_id'];
	$rows = $wpdb->get_results(
			"SELECT accreditaties.created as created,
			 accreditaties.modified as modified,
			 accreditaties.id as id,
			 accreditaties.professional_organization_id AS ac_organization_id,
			 accreditaties.module_id AS ac_module_id,
			 professional_organizations.name AS professional_organizational_name,
			 modules.name AS module,valid_from,valid_till,Points 
			 FROM accreditaties LEFT JOIN professional_organizations ON
			 accreditaties.professional_organization_id = professional_organizations.id
			 LEFT JOIN modules ON accreditaties.module_id = modules.id 
			 WHERE accreditaties.id = ". $id ."
		");

	$accreditaties .= '<table class="accreditaties_table">';

	foreach ($rows as $k => $v) {
		$accreditaties .= '
			<tr>
				<th>ID</th>
				<td>'. $v->id .'</td>
			</tr>
			<tr>
				<th>Created</th>
				<td>'. $v->created .'</td>
			</tr>
			<tr>
				<th>Modified</th>
				<td>'. $v->modified .'</td>
			</tr>
			<tr>
				<th>Valid From</th>
				<td>'. $v->valid_from .'</td>
			</tr>
			<tr>
				<th>Valid Till</th>
				<td>'. $v->valid_till .'</td>
			</tr>
			<tr>
				<th>Points</th>
				<td>'. $v->Points .'</td>
			</tr>
			<tr>
				<th>Professional Organizations</th>
				<td>'. $v->professional_organizations .'</td>
			</tr>
			<tr>
				<th>Module</th>
				<td>'. $v->module .'</td>
			</tr>		
		';

	}
	$accreditaties .= "</table>";

	echo '
		<div class="container">
			<div class="accreditaties_wrapper">
				'. $accreditaties .'
			</div>
		</div>
	';
}

?>



	
