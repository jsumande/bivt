<?php
global $wpdb;
$user_id_login = get_current_user_id();

if ($user_id_login != 0) {
?>
	<div class="inner-container">
		<div class="row">
			<div class="col-md-12">
				<p>Uw account wordt geverifieerd ...</p>
				<?php
					$url = home_url() .'/cart/?add-to-cart=' . $_GET['AddCartId'] . '&quantity=1&user_id='.$user_id_login.'&days='.$_GET['days'] ;
					echo "<script>window.location = '" . $url ."';</script>";				
				?>
			</div>
		</div>
	</div>
<?php
} else {
?>
<div class="inner-container">
	<div class="row">
		<div class="col-md-12">
		    <form name="visitorAdd" id="visitorAdd" method="POST">
		    <input type="hidden" value="<?=$_GET['AddCartId']?>" id="AddCartId">

		        <div class="form-group form-group-sm">
		            <div class="col-sm-12">
		            	<h3>Registratie</h3>
		            </div>
		            &nbsp;
		           <input type="hidden" class="form-control" name="username" id="username" placeholder="Username" required/>
	                <div class="col-sm-12">
					Voer uw e-mail adres in om in te loggen of te registreren.
		            </div>
		            <br>
		            <div class="col-sm-12">   
		                <div class="form-group">
	                    	<label class="col-sm-2 control-label" for="email" >E-mail adres</label>
	                    	<div class="col-sm-10">
	                    		<input type="email" class="form-control" name="email" id="email" placeholder="Email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"/>
	                    	</div>
	                  	</div>
	                  	&nbsp;&nbsp;
	                </div>
	                <br><br>
	                <div class="col-sm-12">	                  		     
	                  	<input type="hidden" name="status" id="status" value="0">
	                  	<div class="form-group">
	                  	<?php
	                  		$url = home_url() .'/cart/?AddCartId='.$_GET['AddCartId'].'&quantity=1&days='.$_GET['days'];
	                  	?>
	                  		<input type="hidden" value="<?=$url?>" id="redirectCart">
	                  		<input type="hidden" value="<?=$_GET['AddCartId']?>" name="AddCartId" id="AddCartId">
	                  		<input type="hidden" value="<?=$_GET['days']?>" id="days" name="days">
	                    	<input type="submit" state=0 class="button wc-forward ripple register-visitor" value="Registreren">
	                    	
	                    	<a class="button wc-forward ripple" href="<?= wp_login_url() . '/?redirect_to=' . $url?>">Inloggen</a>
		               
		            </div>
		        </div>
		      
		    </form>

		</div>
	</div>

</div>
<?php
 }
?>
