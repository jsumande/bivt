<?php

global $wpdb;

if ($_GET['modules']) {


	$slug = $_GET['modules'];
	$rows = $wpdb->get_results("SELECT * FROM modules WHERE slug ='$slug'");	

if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}
?>


<div class="container">
	<div class="courses_wrapper">
		<table class="course_table">
		<?php foreach($rows as $k=> $v) : ?>
		<?php

			$str = $v->picture;

			$result = explode('/', $str);

			$getLast = count($result) - 1;

			$limit = $getLast - 1;

			$getInitial = '';
			//get initial directory
			for($x=0;$x<=$limit;$x++)
			{
				$getInitial .=$result[$x] . '/';
			}

			$result = explode('.',$result[$getLast]);

			$v->picture_thumbnail = sprintf('%s%s.%s',$getInitial,'thumb_'.$result[0], $result[1]);

		?>
		<tr>
			<th>ID</th>
			<td><?= $v->id ?></td>
		</tr>
		<tr>
			<th>Created</th>
			<td><?= $v->created ?></td>
		</tr>
		<tr>
			<th>Modified</th>
			<td><?= $v->modified ?></td>
		</tr>
		<tr>
			<th>Name</th>
			<td><?= $v->name ?></td>
		</tr>
		<tr>
			<th>Introduction</th>
			<td><?= strip_tags($v->introduction) ?></td>
		</tr>
		<tr>
			<th>Description</th>
			<td><?= strip_tags($v->description) ?></td>
		</tr>
		<tr>
			<th>SBU</th>
			<td><?= $v->SBU ?></td>
		</tr>
		<tr>
			<th>Picture</th>
			<td>
				<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture_thumbnail)) : ?>
					<a title="<?= $v->name; ?>" class="fancybox" rel="group" href="<?= plugins_url('wp-manager-course'.$v->picture); ?>"><img src="<?= plugins_url('wp-manager-course'.$v->picture_thumbnail); ?>" alt=""  height=200 width=200/></a>
				<?php else : ?>
				No Image found.
				<?php endif; ?>
			</td>
		</tr>
		<tr>
			<th>Goal</th>
			<td><?= strip_tags($v->goal) ?></td>
		</tr>
		<tr>
			<th>Target Audience</th>
			<td><?= strip_tags($v->target_audience) ?></td>
		</tr>
		<tr>
			<th>Literature</th>
			<td><?= strip_tags($v->literature) ?></td>
		</tr>
		<tr>
			<th>Programme</th>
			<td><?= strip_tags($v->programme) ?></td>
		</tr>
		<tr>
			<th>Level</th>
			<td><?= $v->level ?></td>
		</tr>
		<tr>
			<th>Meeting Days</th>
			<td><?= $v->meeting_days ?></td>
		</tr>
		<tr>
			<th>Sessions</th>
			<td><?= $v->sessions ?></td>
		</tr>
		<tr>
			<th>Preliminary Knowledge</th>
			<td><?= strip_tags($v->preliminary_knowledge) ?></td>
		</tr>
		<tr>
			<th>Price</th>
			<td><?= $v->price ?></td>
		</tr>
		<tr>
			<th>Incasso</th>
			<td><?= $v->incasso ?></td>
		</tr>
		<tr>
			<th>Conditions</th>
			<td><?= $v->conditions ?></td>
		</tr>
		<tr>
			<th>Meta Description</th>
			<td><?= strip_tags($v->meta_description) ?></td>
		</tr>
		<tr>
			<th>Meta Keywords</th>
			<td><?= strip_tags($v->meta_keywords) ?></td>
		</tr>
		<tr>
			<th>Status</th>
			<td><?= $v->status ?></td>
		</tr>
		<?php endforeach; ?>
		</table>
	</div>
</div>

<?php } ?>