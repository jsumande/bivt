<?php

global $wpdb;

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "SELECT COUNT(*) FROM changerequests" );
$num_of_pages = ceil( $total / $limit );

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

if($sort=='')
{
	$results = $wpdb->get_results("SELECT * FROM changerequests LIMIT $offset,$limit");
}
else
{
	$results = $wpdb->get_results("SELECT * FROM changerequests ORDER BY $sort $order LIMIT $offset,$limit");
}
class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=ver_menu&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=ver_menu&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color: inherit !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';
	}

}


get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">
		<div class="row">
			<h3>Vergaderverzoeken</h3>
			<div class="col-sm-12">
		  		<table id="bivt-table">
				<tr>
					<th><?= CustomSorter::sort('name','Name') ?></th>
					<th>Created</th>
					<th>Email</th>
					<th></th>
				</tr>
				<?php if(count($results)>0) : ?>
				<?php foreach($results as $v) : ?>
				<tr id="change-request-<?= $v->id; ?>">
					<td><?= $v->name; ?></td>
					<td><?= $v->created; ?></td>
					<td><?= $v->email; ?></td>
					<td>
						<button class="btn btn-link view-news" data-toggle="modal">
							<a href="<?php echo get_site_url() . '/admin/?action=ver_menu&ver_menu_id=' . $v->id ?>" target="_blank">
							<button class="btn btn-link view-ver">							
								<span class="glyphicon glyphicon-search"></span>
							</button>	
							</a>
						</button>
						<button class="btn btn-link">
							<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="changerequests" transaction="delete">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</button>
					</td>
				</tr>
				<?php endforeach; ?>
				<?php else : ?>
				<tr>
					<td colspan=3>No change request found.</td>
				</tr>
				<?php endif; ?>
				</table>
				<?php
				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
		            }
		           echo '</ul>';
        		}
				?>
			</div>

		</div>
		
	</div>
</div>
