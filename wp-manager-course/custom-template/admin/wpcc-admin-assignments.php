<?php

	global $wpdb;

	$id = get_current_user_id();
	$module_id = isset($_GET['module_id']) ? $_GET['module_id'] : null;
	$docent_id = isset($_GET['docent_id']) ? $_GET['docent_id'] : null;
	
	$role_name = get_perma_role();
	$header_file = null;
	switch ($role_name) {
		case 'admin':
			$header_file = 'wpcc-admin-header.php';
			break;
		case 'teacher':
			assert(docent_id); // Convenient for testing
				
			if ($docent_id == null) {
				// Not allowed to be here
				bivt_redirect ( home_url ());
				die ();
			}
				
			$header_file = plugin_dir_path( __FILE__ ) . '../teacher/wpcc-teacher-header.php';
			break;
		default:
			// You are not supposed to be here. Go home
			bivt_redirect ( home_url ());
			die ();
	}
	
	// Get the module name (if any)
	$module_name_query = "SELECT modules.name FROM modules WHERE modules.id = '" . $module_id . "'";
	$module_name = (module_id != null) ? $wpdb->get_var($module_name_query) : null;
	
	// Set the right query
	$query = "SELECT assignments.id, assignments.day_number AS available_on_day, assignments.text_subject, assignments.text_content, assignments.file, docents.name AS docent_name, docents.id AS docent_id, docents.user_id AS docent_user_id, modules.id AS module_id, modules.name AS module_name FROM assignments LEFT JOIN docents on assignments.docent_id=docents.id LEFT JOIN modules on modules.id = assignments.module_id";
	if ($module_id && $docent_id) {
		$query .= " WHERE assignments.module_id = '" . $module_id . "' AND assignments.docent_id = '" . $docent_id . "'";
	} elseif ($module_id) {
		$query .= " WHERE assignments.module_id = '" . $module_id . "'";
	} elseif ($docent_id) {
		$query .= " WHERE assignments.docent_id = '" . $docent_id . "'";
	}

	// Get the data
	$results = $wpdb->get_results($query);
	
// 	echo "<h4>Query: " . $query . "</h4>";
// 	echo "<h4>Results: " . json_encode($results) . "</h4>";
	
get_header();
?>

<div class="wrap">
	<?php include_once $header_file; ?>

	<div class="container">

		<h3>Opdrachten <?=isset($module_id) ? 'behorende bij de module ' . $module_name : '';?></h3>

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-default" data-toggle="modal" data-target="#modal-assignment-0">
					Opdracht toevoegen
				</button>
				<?php 
					$action = 'add';
					include( plugin_dir_path( __FILE__ ) . 'wpcc-admin-assignments-modal.php');
				?>	
			</div>
			
			<div class="col-md-6">
			<?php if( is_array( $page_links ) ) { ?>
				<ul class="pagination">					
				<?php foreach ( $page_links as $page_link ) { ?>
					<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
				<?php } ?>
				</ul>
			<?php } ?> 
			</div>
				
			<div class="col-md-4" style="padding-right:0px;">
				<form method="GET" action="">
					<input type="hidden" name="action" value="courses_courses" />
					<div class="input-group">
				        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
				            </button>
						</span>
					</div>
				</form>
	      	</div>
		</div>


		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<?php if ($docent_id == null) : ?><th>Docent</th><?php endif; ?>
					<?php if ($module_id == null) : ?><th>Module</th><?php endif;?>
					<th>Naam</th>
					<th>Dag</th>
					<th>Toelichting</th>
					<th>Bestand</th>
					<th>&nbsp;</th>
				</tr>

				<?php
					foreach ($results as $k => $v) {
						$content = isset($v->text_content) ? $v->text_content : 'Onbekend';
						if ($content.length > 25) {
							$content = substr($content, 0, 22) . '...';
						}
						
						$file_name = '&nbsp;';
						$file = '#';
						if (isset($v->file)) {
							$file_parts = explode('/', $v->file);
							$file_name = $file_parts[sizeof($file_parts) -1];
							$file = get_home_url() . '/wp-content/plugins/wp-manager-course' . $v->file;
						}
				?>

				<tr>					
					<?php if ($docent_id == null) : ?>
					<td><?= isset($v->docent_name) ? $v->docent_name : 'Onbekend';?></td>
					<?php endif; ?>
					<?php if ($module_id == null) : ?>
					<td><?= isset($v->module_name) ? $v->module_name : 'Onbekend';?></td>				
					<?php endif; ?>
					<td><?= isset($v->text_subject) ? $v->text_subject : 'Onbekend'; ?></td>
					<td><?= isset($v->available_on_day) ? ($v->available_on_day == 0 ? 'vooraf' : $v->available_on_day) : 'Onbekend'; ?></td>
					<td><?= $content; ?></td>
					<td>
						<a class="btn btn-link" href="<?= $file?>" target="_blank">
							<?=$file_name;?>
						</a>
						
					</td>
					<td class="action">
						<button class="btn btn-link" data-toggle="modal" data-target="#modal-assignment-<?=$v->id;?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>
						<?php 
							$action = 'edit';
							$modal_data = $v;
							include( plugin_dir_path( __FILE__ ) . 'wpcc-admin-assignments-modal.php');
						?>	

						<a href="#" class="transaction-button" rowid="" state=0 table="" transaction="delete">
							<button class="btn btn-link">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
						</a>

					</td>


				<?php 
					} // endforeach
				?>				
				</tr>
				
			</table>
		</div>
	</div>		
</div>

