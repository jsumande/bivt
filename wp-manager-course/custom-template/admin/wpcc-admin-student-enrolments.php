<?php

class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=student-enrolments&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=instance_menu&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}


global $wpdb;


//start
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;

$total = $wpdb->get_var( "SELECT COUNT(instances.id) FROM instances");

$num_of_pages = ceil( $total / $limit );


$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$base_query = "SELECT oii.order_item_id, oii.order_item_name, oii.order_id, days.datum, instances.id AS instance_id, locations.id AS location_id, locations.name AS location FROM wp_woocommerce_order_items AS oii LEFT JOIN wp_woocommerce_order_itemmeta AS oim ON oii.order_item_id=oim.order_item_id LEFT JOIN instances on oim.meta_value = instances.id LEFT JOIN days ON days.instance_id=instances.id LEFT JOIN locations ON days.location_id=locations.id WHERE meta_key='_instance_id' GROUP BY oii.order_item_id";


if($sort=='')
{
	$results = $wpdb->get_results($base_query . " ORDER BY oii.order_item_id DESC LIMIT $offset,$limit");
}
else
{
	$results = $wpdb->get_results($base_query . " ORDER BY $sort $order LIMIT $offset,$limit");
}

?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="inner-container">
		<div class="row pad-10">
			<h3>Inschrijvingen</h3>
		</div>
		<div class="row">
			<div class="col-sm-12">
		  		<table id="bivt-table">
					<tr>
						<th>Order</th>
						<th>Status</th>
						<th>Student</th>
						<th>Besteldatum</th>
						<th>Scholing</th>
						<th>Locatie</th>
						<th>Start datum</th>
						<th>&nbsp;</th>
					</tr>
					<?php 
						foreach ($results as $k => $v) {
							$order = new WC_Order($v->order_id);						
					?>
					<tr id="instance-<?= $v->id; ?>">
						<td><?= $v->order_id ?></td>
						<td><?= $order->post_status ?></td>
						<td>
							<a href="?action=user_profile&user_id=<?=$order->customer_user;?>">
								<?= $order->billing_first_name . " " . $order->billing_last_name ?>
							</a>
						</td>
						<td><?= date('d-m-Y', strtotime($order->order_date)) ?></td>
						<td>
						<a href="?action=instance_menu&instance=<?= $v->instance_id ?>"><?= $v->order_item_name ?></a>
						</td>
						<td>
							<?php if (isset($v->location)) :?>
							<a href="<?=get_site_url()?>/locatie/?locatie=<?=$v->location_id;?>"><?=$v->location;?></a>
							<?php else :?>
								Onbekend
							<?php endif;?>
						</td>
						<td><?= isset($v->datum) ? date('d-m-Y', strtotime($v->datum)) : 'Onbekend'; ?></td>
					</tr>
					<?php
						} // foreach
					?>
				</table>
			</div>
		</div>
	</div>
</div>