<?php 

global $current_user;
/*Error: Notice: get_currentuserinfo is deprecated since version 4.5! Use wp_get_current_user() instead. in D:\xampp\htdocs\bivt\wp-includes\functions.php on line 3658 */
// get_currentuserinfo();
wp_get_current_user();

$tab = $_GET['action'];

?>

<div class="row">
	<div class="col-sm-12" id="menuScroll">
		<div class="menu_wrapper">
			<form method="post" class="container">
			
				<ul id="menu-admin-menu">
					<li>
						<a href="<?= get_home_url(); ?>">BivT</a>
					</li>
					<li class="dropdown <?= $tab==substr('student',0,strlen('student')) ? 'active' : '' ?>">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="menu-item-text"><span class="menu-text" >Studenten</span></span><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=users_overview&group=3">Overzicht</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=planning_instances">Groepen</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=student_enrolments">Inschrijvingen</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=export&group=3">Exporteren</a></li>
						</ul>
					</li>
					<li class="dropdown <?= $tab==substr('docent',0,strlen('docent')) ? 'active' : '' ?>">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="menu-item-text"><span class="menu-text" >Docenten</span></span><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=users_overview&group=5">Overzicht</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=teacher_invoices">Facturen</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=export&group=5">Exporteren</a></li>
						</ul>
					</li>
					<li class="dropdown <?= $tab==substr('courses',0,strlen('courses')) ? 'active' : '' ?>">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="menu-item-text"><span class="menu-text" >Scholingen</span></span> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=courses_courses">Opleidingen</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=courses_modules">Modules</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=courses_categories">Categorieën</a></li>
						</ul>
					</li>
					<li class="dropdown <?= $tab==substr('planning',0,strlen('planning')) ? 'active' : '' ?>">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="menu-item-text"><span class="menu-text" >Planning</span></span> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=planning_instances">Groepen</a></li>
							<li><a href="<?=  get_home_url(). '/admin/'?>?action=planning_schedule">Rooster</a></li>
						</ul>
					</li>
					<li class=" <?= $tab=='import' ? 'active' : '' ?>">
						<a href="<?=  get_home_url(). '/admin/'?>?action=import">Import</a>
					</li>
					<li class=" <?= $tab=='news' ? 'active' : '' ?>">
						<a href="<?=  get_home_url(). '/admin/'?>?action=news">Nieuws</a>
					</li>
					<li class=" <?= $tab=='locations' ? 'active' : '' ?>">
						<a href="<?=  get_home_url(). '/admin/'?>?action=locations">Locaties</a>
					</li>
					<li class=" <?= $tab=='accreditations' ? 'active' : '' ?>">
						<a href="<?=  get_home_url(). '/admin/'?>?action=accreditations">Accreditaties</a>
					</li>
					<li class=" <?= $tab=='users' ? 'active' : '' ?>">
						<a href="<?=  get_home_url(). '/admin/'?>?action=users_overview">Gebruikers</a>
					</li>
					<li class="">
						<a href="<?= wp_logout_url(home_url()) ?>">Logout</a>
					</li>
				</ul>
			</form>
		</div>		
	</div>
</div>
<script type="text/javascript">
	$ = jQuery;
	$(window).load(function() {
		$('#phantom').html( $('#menuScroll').html() );
	});
</script>