<?php

global $wpdb;

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;

$module_id = isset($_GET['modules_id']) ? $_GET['modules_id'] : -1;

$total = 0;
$module_name = "";
if ($module_id > 0) {
	$total = $wpdb->get_var( "SELECT COUNT(`id`) FROM accreditaties WHERE modules.id=$module_id" );
	$module_name = $wpdb->get_var ("SELECT name FROM modules WHERE modules.id=$module_id");
} else {
	$total = $wpdb->get_var( "SELECT COUNT(`id`) FROM accreditaties" );
}
$num_of_pages = ceil( $total / $limit );


$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$base_query = "SELECT accreditaties.modified as modified,accreditaties.id as id,accreditaties.professional_organization_id AS ac_organization_id,accreditaties.module_id AS ac_module_id,professional_organizations.name AS professional_organizational_name,modules.name AS module,valid_from,valid_till,Points FROM accreditaties LEFT JOIN professional_organizations ON accreditaties.professional_organization_id = professional_organizations.id LEFT JOIN modules ON accreditaties.module_id = modules.id";
if($sort!='') {
	$base_query .= " ORDER BY $sort $order";
}

if ($module_id > 0) {
	$base_query .= " WHERE modules.id=$module_id";
}

$base_query .= " LIMIT $offset, $limit";

$results = $wpdb->get_results($base_query);

$countModules = $wpdb->get_var("SELECT COUNT(*) FROM modules");
$countOrganizations = $wpdb->get_var("SELECT COUNT(*) FROM professional_organizations");

$optionModules = '';
$optionOrganizations = '';

//modules
if($countModules>0){
	$getModules = $wpdb->get_results("SELECT * FROM modules",ARRAY_A);
}
else
	$optionModules = '<option value=0>No Modules found.</option>';

//organizations
if($countOrganizations>0){
	$getOrganizations = $wpdb->get_results("SELECT * FROM professional_organizations",ARRAY_A);
}
else
	$optionOrganizations = '<option value=0>No Professional Organization found.</option>';

class CustomSorter {

	public static function sort($item,$title) {


		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=acc_menu&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=acc_menu&sort='.$item;


		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}

get_header();
?>


<div class="wrap">
	<?php
		include 'wpcc-admin-header.php';
	?>

	<div class="container">

		<?php
			if ($module_id < 0) {
		?>
		<h3>Alle accreditaties</h3>
		<?php
			} else {
		?>
		<h3>Accreditaties voor module: <?=$module_name?></h3>
		<?php 
			}
		?>

		<div class="row">
			<div class="col-md-8"></div>
				<div class="col-md-4" style="padding-right:0px;">
					<form method="GET" action="">
						<input type="hidden" name="action" value="locs_menu" />
					    <div class="input-group">
					        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoek..." />
					        <span class="input-group-btn">
					            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
					                <i class="fa fa-search"></i>
					            </button>
					        </span>
				      	</div>
			      	</form>
		      	</div>
	      	</div>

			<div class="row">
		  		<table id="bivt-table">
					<tr>
						<th class="th_3"><?= CustomSorter::sort('professional_organizational_name','Professional Organization') ?></th>
						<th class="th_3">Modules</th>
						<th>Valid From</th>
						<th>Valid Till</th>
						<th>Points</th>
						<th><?= CustomSorter::sort('modified','Modified') ?></th>
						<th>&nbsp;</th>
					</tr>
					<?php
						foreach ($results as $k => $v):
					?>

					<tr>
						<td><?= $v->professional_organizational_name ?></td>
						<td><?= $v->module ?></td>
						<td><?= $v->valid_from ?></td>
						<td><?= $v->valid_till ?></td>
						<td><?= $v->Points ?></td>
						<td><?= $v->modified ?></td>
						<td>
							<a href="<?php echo get_site_url() . '/admin/?action=acc_menu&accreditaties_id=' . $v->id ?>" target="_blank">
								<button class="btn btn-link view-accreditaties" data-toggle="modal">
									<span class="glyphicon glyphicon-search"></span>
								</button>	
							</a>

							<button class="btn btn-link" data-toggle="modal" data-target="#edit_accreditaties<?= $v->id ?>">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
							<div class="modal fade" id="edit_accreditaties<?= $v->id ?>" tabindex="-1" role="dialog" aria-labelledby="accreditaties" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-80p">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												   <span aria-hidden="true">×</span>
												   <span class="sr-only">Close</span>
											</button>
											<h4 class="modal-title" id="acrreditaties">
												Acreditaties
											</h4>
										</div>
									
										<!-- Modal Body -->
										<div class="modal-body">
											<form id="EditAccreditaties" class="form-horizontal" method="POST">
												<div class="form-group form-group-sm">
													<!-- left column -->
													<div class="col-sm-12">
														<div class="form-group">
															<label  class="col-sm-2 control-label" for="points">Points</label>
															<div class="col-sm-10">
															<input type="text" class="form-control required" name="points" id="points" placeholder="Name" value="<?= $v->Points ?>" required/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="valid_from" >Valid from</label>
														<div class="col-sm-10">
															<input type="text" class="form-control required date-type date-type" name="valid_from" id="valid_from" value="<?= $v->valid_from ?>" placeholder="valid_from" required/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="valid_till" >Valid until</label>
														<div class="col-sm-10">
															<input type="text" class="form-control required date-type date-type" name="valid_till" id="valid_till" placeholder="valid_till" value="<?= $v->valid_till ?>" required/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="module_id" >Module</label>
														<div class="col-sm-10">
															<select class="form-control required" name="module_id">
															<?php foreach($getModules as $getModule) : ?>
															<?php if($getModule['id']==$v->ac_module_id) : ?>
															<option selected="selected" value="<?= $getModule['id'] ?>" ><?= $getModule['name']; ?></option>
															<?php else : ?>
															<option value="<?= $getModule['id']; ?>" ><?= $getModule['name']; ?></option>
															<?php endif; ?>
															<?php endforeach; ?>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="professional_organization_id" >Organization</label>
														<div class="col-sm-10">
															<select class="form-control required" name="professional_organization_id">
															<?php foreach($getOrganizations as $getOrganization) : ?>
															<?php if($getOrganization['id']==$v->ac_organization_id) : ?>
															<option selected="selected" value="<?= $getOrganization['id'] ?>" ><?= $getOrganization['name']; ?></option>
															<?php else : ?>
															<option value="<?= $getOrganization['id']; ?>" ><?= $getOrganization['name']; ?></option>
															<?php endif; ?>
															<?php endforeach; ?>
															</select>
														</div>
													</div>
												</div>
											</div>
										  	<!-- Modal Footer -->
											<div class="modal-footer">
												<button id="EditAccreditatySubmit" accreditatyid="<?= $v->id; ?>" type="button" state=0 class="btn btn-default">Edit changes</button>
											</div><!-- End Modal Footer -->
										  
										</form>
									</div> <!-- End modal body div -->
								</div> <!-- End modal content div -->
							</div> <!-- End modal dialog div -->
						</div> <!-- End modal div -->

						<button class="btn btn-link delete-accreditaties" delete-row="<?= $v->id; ?>" state=0>
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>

				<?php endforeach; ?>				
			</table>
			<?php

				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
						//echo "<li>$page_link</li>";
		            }
		           echo '</ul>';
        		}
			
					
			?>
			<div class="bivt_add_container">
		        <?= do_shortcode( "[bivt-accreditaties]"); ?>  <!--  Button "Accreditatie toevoegen" -->
			</div>			
		</div>
	</div>
		
</div>
