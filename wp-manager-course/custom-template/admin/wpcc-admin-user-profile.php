<?php

	global $wpdb;

	// Who am I?
	$currentUserId = get_current_user_id();

	// The user_id of the profile am I viewing
	$profileUserId = isset($_GET['user_id']) ? $_GET['user_id'] : $currentUserId;

	// Set edit mode or not.
	// NOTE: Do NOT use boolval() as it's available since PHP 5.5 and our server is not on that version
	$enable_edit = isset($_GET['edit']) ? $_GET['edit'] == 'true' ? true : false : false;
	
	$readonly = $enable_edit ? '' : 'readonly="readonly"';

	// Get current role
	$role_name = get_perma_role();
	$header_file = null;
	switch ($role_name) {
		case 'admin':
			$header_file = 'wpcc-admin-header.php';
			break;
		case 'teacher':
			$header_file = plugin_dir_path( __FILE__ ) . '../teacher/wpcc-teacher-header.php';
			$profileUserId = $currentUserId; // For those who try to edit the URL
			break;
		case 'student':
			$header_file = plugin_dir_path( __FILE__ ) . '../student/wpcc-student-header.php';
			$profileUserId = $currentUserId; // For those who try to edit the URL
			break;
		default:
			// You are not supposed to be here. Go home
			bivt_redirect ( home_url ());
			die ();
	}
		
	// Get the data
	$v = $wpdb->get_row('SELECT * FROM wp_users WHERE ID = '. $profileUserId .'');
	$meta_results = get_user_meta($profileUserId);
	$bio_results = $wpdb->get_row ('SELECT bio FROM docents WHERE user_id = '. $profileUserId .'');
	
	
	// Union memberschip
	$member_ships = [
			"Beroepsvereniging 1" => "1234567890",
			"Beroepsvereniging 2" => "0987654321",
	]; // Should come from DB!!
	$is_member = !empty($member_ships);
		

// 	echo "<h4>currentUserId: " . $currentUserId . "</h4>";
// 	echo "<h4>profileUserId: " . $profileUserId . "</h4>";
// 	echo "<h4>Bio: " . json_encode($bio_results) . "</h4>";

?>

<div class="wrap">
	<?php include_once $header_file; ?>

	<div class="container">
	    <h3><?= $profileUserId==$currentUserId ? 'Mijn gegevens' : 'Gegevens van ' . $v->display_name;?></h3>
	  	<hr>
		<div class="row">
	      	<!-- left column -->
	      	<div class="col-sm-2 profile-image">
	      		<form class="form-horizontal" role="form" method="POST">
		        	<div class="text-center">
		        		<h4>Profielfoto</h4>
		        		<?php
		        			if ( isset($meta_results["picture"][0]) && $meta_results["picture"][0] !="" ) {
		        				$picture = get_home_url().'/wp-content/plugins/wp-manager-course/'.$meta_results["picture_thumb"][0]; 
		        			} else {
		        				$picture = '//placehold.it/100';
		        			}
		        		?>
		          		<img src="<?= $picture ?>" class="avatar img-circle" alt="avatar">
		          		<?php if($enable_edit) : ?>

		          		<h6>Wijzig foto...</h6>	          		
		          		<input type="file" class="form-control" id="picture" name="picture" accept="image/*">
		          		<br>
						<button state=0 type="button"  table="user_meta" 
							class="btn btn-default transaction-button" transaction="uploadProfile" id="<?=$profileUserId?>" >
							Opslaan
						</button>
						<?php endif; ?>
		        	</div>
		        </form>
	      	</div>

	      	<!-- login form column -->
	      	<div class="col-sm-5 col-sm-offset-0 personal-info">
	      		<div class="col-sm-offset-5">	        	
	        		<h4>Login gegegevens</h4>
	        	</div>
	        
		        <form class="form-horizontal" role="form" method="POST">
		          	<div class="form-group">
		            	<label class="col-sm-5 control-label">Email:</label>
		            	<div class="col-sm-7">
		              		<input class="form-control" type="text" name="email" readonly="readonly" value="<?= $v->user_email ?>">
		            	</div>
		          	</div>

					<?php if ($enable_edit) : ?>		          	
		          	 <div class="form-group">
			            <label class="col-sm-5 control-label">Wachtwoord:</label>
			            <div class="col-sm-7">
			              	<input class="form-control" type="password" id="password" name="password" placeholder="password">
			            </div>
			        </div>
			        
			        <div class="form-group">
			            <label class="col-sm-5 control-label">Bevestig wachtwoord:</label>
			            <div class="col-sm-7">
			              	<input class="form-control" type="password" id="confirm_password" name="confirm_password" placeholder="password">
			            </div>
			        </div>

			        <div class="col-sm-2 col-sm-offset-5">
						<button state=0 type="button"  table="wp_users" 
							class="btn btn-default transaction-button" transaction="changepass" id="<?=$profileUserId?>" >
							Wachtwoord wijzigen
						</button>
					</div>
					<?php endif; ?>
				</form>
			</div>
	      	<div class="col-sm-4 col-sm-offset-1 social-media">	        	
	      		<div class="col-sm-offset-1">	        	
		        	<h4>Social Media</h4>
				</div>
				        
		        <form class="form-horizontal" role="form" method="POST">
		        
					<div class="form-group">
		            	<label class="col-sm-1 control-label">
		            		<span><i class="fa fa-twitter"></i></span>
		            	</label>
		            	<div class="col-sm-7">
		              		<input class="form-control" type="text" id="twitter_id" name="twitter" <?=$readonly;?> value="<?= ($meta_results["twitter"][0])?$meta_results["twitter"][0]:'' ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-sm-1 control-label">
		            		<span><i class="fa fa-facebook"></i></span>
		            	</label>
		            	<div class="col-sm-7">
		              		<input class="form-control" type="text" id="facebook_id" name="facebook" <?=$readonly;?> value="<?= ($meta_results["facebook"][0])?$meta_results["facebook"][0]:'' ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-sm-1 control-label">
		            		<span><i class="fa fa-google-plus"></i></span>
		            	</label>
		            	<div class="col-sm-7">
		              		<input class="form-control" type="text" id="googleplus_id" name="googleplus" <?=$readonly;?> value="<?= ($meta_results["googleplus"][0])?$meta_results["googleplus"][0]:'' ?>">
		            	</div>
		          	</div>

					<?php if($enable_edit) : ?>
			        <div class="col-sm-2 col-sm-offset-1">
						<button state=0 type="button" table="user_meta" 
							class="btn btn-default transaction-button" transaction="social" id="<?=$profileUserId?>" >
							Social Media opslaan
						</button>
					</div>
					<?php endif; ?>

		        </form>
			</div>
		</div>
		<?php if ($bio_results != null && $bio_results->bio != null) : ?>
		<div class="row">
			<hr>
		</div>
		<div class="row">
			<div class="col-sm-offset-0">	        	
				<h4>Biografie</h4>
	    	</div>
		
			<div class="col-sm-12 biography">
				<form class="form-horizontal" role="form" method="POST">
					<div class="form-group">
						<div class="col-sm-10">
						<textarea class="<?=$enable_edit ? 'mceEditor' : '';?>" name="bio" id="bio" rows="6" <?=$readonly;?>>
							<?= $bio_results->bio ?>
						</textarea>
						</div>
					</div>
				</form>
			</div>

			<?php if($enable_edit) : ?>
			<div class="col-sm-2">
				<button state=0 type="button" table="docent" 
						class="btn btn-default transaction-button" transaction="biography" id="<?=$profileUserId?>">
					Opslaan
				</button>

			</div>
			<?php endif; ?>
		</div>
		
		<?php endif; ?>
		
		<div class="row">
		<hr>
		</div>
		<div class="row address-profile">
			<form class="form-horizontal" role="form" method="POST">
				<div class="col-sm-5">
					<div class="col-sm-offset-4">	        	
						<h4>Persoonlijke gegevens</h4>
		    	    </div>

			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Voornamen:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_firstname" name="personal_firstname" <?=$readonly;?> value="<?= ($meta_results["personal_firstname"][0])?$meta_results["personal_firstname"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Achternaam:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_lastname" name="personal_lastname" <?=$readonly;?> value="<?= ($meta_results["personal_lastname"][0])?$meta_results["personal_lastname"][0]:'' ?>">
			            	</div>
			          	</div>
			          						
			          	 <div class="form-group">
			            	<label class="col-sm-4 control-label">Adres:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_address" name="personal_address" <?=$readonly;?> value="<?= ($meta_results["personal_address"][0])?$meta_results["personal_address"][0]:'' ?>">
			            	</div>
			          	</div>
			          	<?php if( $role_name == 'student') : ?>
			          	 <div class="form-group">
			            	<label class="col-sm-4 control-label">Geboortedatum:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control date-type" type="text" id="personal_birthdate" name="personal_birthdate" <?=$readonly;?> value="<?= ($meta_results["personal_birthdate"][0])?$meta_results["personal_birthdate"][0]:'' ?>">
			            	</div>
			          	</div>
			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Geboorteplaats:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control " type="text" id="personal_birthplace" name="personal_birthplace" <?=$readonly;?> value="<?= ($meta_results["personal_birthplace"][0])?$meta_results["personal_birthplace"][0]:'' ?>">
			            	</div>
			          	</div>
			          	<?php endif; ?>
			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Telefoon nummer:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control " type="text" id="personal_telephone" name="personal_telephone" <?=$readonly;?> value="<?= ($meta_results["personal_telephone"][0])?$meta_results["personal_telephone"][0]:'' ?>">
			            	</div>
			          	</div>
			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Mobiel nummer:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control " type="text" id="personal_mobile" name="personal_mobile" <?=$readonly;?> value="<?= ($meta_results["personal_mobile"][0])?$meta_results["personal_mobile"][0]:'' ?>">
			            	</div>
			          	</div>
			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Postcode:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_zipcode" name="personal_zipcode" <?=$readonly;?> value="<?= ($meta_results["personal_zipcode"][0])?$meta_results["personal_zipcode"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Woonplaats:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_city" name="personal_city" <?=$readonly;?> value="<?= ($meta_results["personal_city"][0])?$meta_results["personal_city"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-4 control-label">Land:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="personal_country" name="personal_country" <?=$readonly;?> value="<?= ($meta_results["personal_country"][0])?$meta_results["personal_country"][0]:'' ?>">
			            	</div>
			          	</div>

					<!-- </form> -->
				</div>
				<div class="col-sm-5 col-sm-offset-1">
					<div class="col-sm-offset-3">	        	
						<h4>Factuur gegevens</h4>
		        	</div>
				
					<!-- <form class="form-horizontal" role="form" method="POST"> -->
					
			          	<div class="form-group">
				          	<label class="col-sm-3 control-label">Praktijk:</label>
				          	<div class="col-sm-8">
					          	<input class="form-control" type="text" id="billing_company" name="billing_company" <?=$readonly;?> value="<?= ($meta_results["billing_company"][0])?$meta_results["billing_company"][0]:'' ?>">
			        	  	</div>
			          	</div>
			          	
			          	
			          	<div class="form-group">
			            	<label class="col-sm-3 control-label">Voornaam:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_firstname" name="billing_first_name" <?=$readonly;?> value="<?= ($meta_results["billing_first_name"][0])?$meta_results["billing_first_name"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-3 control-label">Achternaam:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_last_name" name="billing_last_name" <?=$readonly;?> value="<?= ($meta_results["billing_last_name"][0])?$meta_results["billing_last_name"][0]:'' ?>">
			            	</div>
			          	</div>
			          	
			          	 <div class="form-group">
			            	<label class="col-sm-3 control-label">Adres:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_address_1" name="billing_address_1" <?=$readonly;?> value="<?= ($meta_results["billing_address_1"][0])?$meta_results["billing_address_1"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-3 control-label">Postcode:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_postcode" name="billing_postcode" <?=$readonly;?> value="<?= ($meta_results["billing_postcode"][0])?$meta_results["billing_postcode"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-3 control-label">Woonplaats:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_city" name="billing_city" <?=$readonly;?> value="<?= ($meta_results["billing_city"][0])?$meta_results["billing_city"][0]:'' ?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-sm-3 control-label">Land:</label>
			            	<div class="col-sm-8">
			              		<input class="form-control" type="text" id="billing_country" name="billing_country" <?=$readonly;?> value="<?= ($meta_results["billing_country"][0])?$meta_results["billing_country"][0]:'' ?>">
			            	</div>
			          	</div>				

					<!-- </form> -->
				</div>
			
			<?php if($enable_edit) : ?>
			<div class="col-sm-4 col-sm-offset-4">
				<button state=0 type="button" table="user_meta" 
						class="btn btn-default transaction-button" transaction="address_profile" id="<?=$profileUserId?>">
					Adres gegevens opslaan
				</button>
			</div>
			<?php endif;?>
		</form>		
		</div>

		<div class="row">
			<hr>
		</div>
		<div class="row address-profile">
			<form class="form-horizontal" role="form" method="POST">
				<div class="col-sm-12">
					<div class="col-sm-offset-4">
						<h4>Beroepsverenigingen</h4>
					</div>

					<?php $is_member=false;?>
					<div class="form-group">
						<label class="col-sm-3 control-label">Geen lid van een beroepsvereniging</label>
						<div class="col-sm-1">
							<input class="form-control" type="checkbox" <?=$is_member ? '' : 'checked="checked"';?>
								id="not-member" name="not_memeber" <?=$readonly;?> value="">
						</div>
					</div>
					<?php if ($is_member) : ?>
					<div class="table-wrapper">
						<table id="bivt-table">
							<thead>
								<tr>
									<th>Beroepsvereniging</th>
									<th>Lidmaatschapsnummer</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td></td>
									<td></td>
								</tr>
							</tbody>
						</table>
					</div>
					<?php endif; ?>
				</div>
			</form>
		</div>

	</div>
</div>		
