<?php

global $wpdb;

$results = $wpdb->get_results('SELECT r.*, l.name, l.address, l.parking FROM reservations r LEFT JOIN locations l ON l.id = r.location_id');


get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Reservations</h3>

		<div class="col-sm-3">
	  		<div class="bivt_wrapper">
	  			<div class="bivt_action_wrapper">
	  				<label>Actions</label>
	  			</div>
				<div class="bivt_add_container">
					<a href="#">
		          		<span class="glyphicon glyphicon-plus bivt-plus-icon"> Add Reservation </span>
		        	</a>
				</div>			
	  		</div>
		</div>
		<div class="col-sm-9">
	  		<table id="bivt-table">
				<tr>
					<th>Locatie</th>
					<th>Zaal</th>
					<th>Starttijd</th>
					<th>Eindtijd</th>
					<th>Gebruiker</th>
					<th>&nbsp;</th>
				</tr>
				<?php 
					foreach ($results as $k => $v):
				?>

				<tr>
					<td>Location</td>
					<td><?= $v->room ?></td>
					<td><?= $v->starttime ?></td>
					<td><?= $v->endtime ?></td>
					<td>user</td>
					<td>&nbsp;</td>
				</tr>

				<?php endforeach; ?>				
			</table>
		</div>
	</div>
		
</div>
