<?php 
	assert(isset($action));
	
	$modal_data = isset($modal_data) ? $modal_data : null;
	$course_id = isset($modal_data) ? $modal_data->id : null;
	$action = isset($course_id) ? 'edit' : 'add';
	$label = $action . '-course' . ($course_id ? '-' . $course_id : '');
	
	$statuses = ['close'=>'Close','open'=>'Open'];

	$categories = $wpdb->get_results('SELECT id, id AS selected, name FROM categories');
	$selected_categories = null;
	if ($course_id) {
		$selected_categories = $wpdb->get_results ( "SELECT c.id as id FROM `categories_courses` as cm LEFT JOIN categories c ON c.id = cm.category_id WHERE cm.course_id = '" . $course_id . "'" );
		$tmp = [];
		foreach ($selected_categories AS $sc_key=>$sc_value) {
			$tmp[] = $sc_value->id;
		}
		$selected_categories = $tmp;
	}
	
	if(!function_exists('is_category_selected'))
	{
		function is_category_selected($cat, $selected_categories) {
			return ($selected_categories && in_array($cat->id, $selected_categories));
		}
	}
	
	if ($action == 'edit') {
		$teachers = $wpdb->get_results ( "SELECT docents.id, docents.name, docents_courses.course_id AS cid FROM docents LEFT JOIN docents_courses ON docents.id = docents_courses.docent_id WHERE docents_courses.course_id='".$course_id."' ORDER BY name" );
	} else {
		$teachers = $wpdb->get_results ("SELECT docents.id, docents.name FROM docents ORDER BY name");
	}
?>

<div class="modal fade" style="margin-top: 65px;" id="<?= $label; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-80p">
		<div class="modal-content">
		
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span> <span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="<?= $label; ?>">
					<?= (null != $course_id) ? "Opleiding: " . $modal_data->name : "Nieuwe opleiding";?>
				</h4>
			</div>
			
			
			<!-- Modal Body -->
			<div class="modal-body <?= $action?>-courses"id="<?= $label; ?>">
				<form class="form-horizontal" method="POST">
					<input type="hidden" name="course_id" id="course_id" value="<?= $course_id; ?>">
					<input type="hidden" name="post_id" id="post_id" value="<?= $modal_data->post_id; ?>">
					<div class="form-group form-group-sm">

						<!-- left column -->
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="name">Naam</label>
								<div class="col-sm-9">
									<input type="text" value="<?= $course_id ? $modal_data->name : ''; ?>"
										class="form-control required" name="name" id="name"
										placeholder="Naam" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="introduction">Introductie</label>
								<div id="introduction_div" class="col-sm-9">
									<textarea class="form-control required mceEditor" rows="6"
										name="introduction" id="introduction-<?= $label; ?>" required>
										<?= $course_id ? $modal_data->introduction : ''; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="meta_description">
									Meta Description </label>
								<div class="col-sm-9">
									<textarea class="form-control required " rows="6"
										name="meta_description" id="meta_description-<?= $label; ?>" required>
										<?= $course_id ? $modal_data->meta_description : ''; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="meta_keywords"> Meta
									Keywords </label>
								<div class="col-sm-9">
									<textarea class="form-control required" rows="6"
										name="meta_keywords" id="meta_keywords-<?= $label; ?>" required>
										<?= $course_id ? $modal_data->meta_keywords : ''; ?>
									</textarea>
								</div>
							</div>
						</div>


						<!-- right column -->
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="conditions">Voorwaarden</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required"
										name="conditions" id="conditions" placeholder="Voorwaarden"
										required value="<?= $course_id ? $modal_data->conditions : ''; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="description">Omschrijving</label>
								<div class="col-sm-9">
									<textarea class="form-control required mceEditor" name="description"
										id="description-<?= $label; ?>" required>
										<?= $course_id ? $modal_data->description : ''; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="sessions" class="col-sm-3 control-label">Sessies</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required"
										name="sessions" id="sessions" placeholder="Sessions" required
										value="<?= $course_id ? $modal_data->sessions : ''; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="meeting_days" class="col-sm-3 control-label">
									Bijeenkomsten </label>
								<div class="col-sm-9">
									<input type="text" class="form-control required"
										name="meeting_days" id="meeting_days"
										placeholder="Bijeenkomsten" required
										value="<?= $course_id ? $modal_data->meeting_days : ''; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="new_bol_require" class="col-sm-3 control-label">Prijs</label>
								<div class="col-sm-3">
									<input type="text" class="form-control required" name="price"
										id="price" placeholder="$0.00" required
										value="<?= $course_id ? $modal_data->price : ''; ?>" />
								</div>
								<label for="new_pod_require" class="col-sm-3 control-label">SBU</label>
								<div class="col-sm-3">
									<input type="text" class="form-control required" name="sbu"
										id="sbu" placeholder="0" required
										value="<?= $course_id ? $modal_data->SBU : ''; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="incasso" class="col-sm-3 control-label">Incasso</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required" name="incasso"
										id="incasso" placeholder="Incasso" required
										value="<?= $course_id ? $modal_data->incasso : ''; ?>" />
								</div>
							</div>
							<div class="form-group">
								<label for="status" class="col-sm-3 control-label">Status</label>
								<div class="col-sm-9">
									<select class="form-control" name="status" id="status">
										<?php foreach($statuses as $key => $status) : ?>
											<?php if($key==$modal_data->status) : ?>
												<option selected="selected" value="<?= $key ?>"><?= $status; ?></option>
											<?php else : ?>
												<option value="<?= $key; ?>"><?= $status; ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="picture_preview">
									Huidige afbeelding </label>
								<div class="col-sm-8">
									<div class="x-close removeTablePicture" table="courses"
										requestid="<?= $course_id ? $modal_data->id : ''; ?>" state=0>
									</div>
									<?php if($course_id && $modal_data->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $modal_data->picture)) : ?>
										<img src="<?= plugins_url('wp-manager-course'.$modal_data->picture); ?>"
										class="img-responsive coursePicture specialPicture" />
									<?php else : ?>
										<p>Geen afbeelding beschikbaar.</p>
										<br>
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group" style="display: none;">
								<label class="col-sm-3 control-label" for="picture_preview_new">
									Voorbeeld afbeelding
								</label>
								<div class="col-sm-9">
									<img class="form-control picture_preview_new img-responsive" />
								</div>
							</div>
							<br>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="picture">Afbeelding</label>
								<div class="col-sm-9">
									<input type="file" courseid="<?= $course_id ? $modal_data->id : ''; ?>"
										class="form-control required" name="picture" id="picture" required />
								</div>
							</div>
							<div class="form-group">
								<label for="course" class="col-sm-3 control-label">Categorieën</label>
								<div class="col-sm-9">
									<select courseid="category-<?= $course_id ? $modal_data->id : '' ?>"
										class="form-control cat-edit-multiple" multiple="multiple" name="category" id="category">
										<?php
											foreach ( $categories as $k => $cat ) {
												$selected = is_category_selected( $cat, $selected_categories ) ? 'selected="selected"' : '';
										?>
												<option <?= $selected ?> value="<?=$cat->id ?>"> <?=$cat->name ?> </option>
										<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="teacher" class="col-sm-3 control-label"> Docent</label>
								<div class="col-sm-9">
									<select teacherid="teacher-<?= $course_id; ?>"
										class="form-control cat-edit-multiple" multiple="multiple" name="teacher" id="teacher">
										<?php foreach ( $teachers as $teacher ) : ?>
										<?php $selected = !is_null($teacher->cid) && $modal_data->id==$teacher->cid ? 'selected="selected"' : ''; ?>
										?>
										<option value="<?= $teacher->id; ?>"<?= $selected; ?>><?= $teacher->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<button state=0 type="button" rowid="<?= $course_id; ?>"
							transaction="<?=$action?>" table="courses"
							class="btn btn-default transaction-button">
											<?= $course_id ? 'Wijzigingen opslaan' : 'Opleiding opslaan';?>
										</button>
					</div>
					<!-- End Modal Footer -->
				</form>
			</div>
			<!-- End modal body div -->
		</div>
		<!-- End modal content div -->
	</div>
	<!-- End modal dialog div -->
</div>
<!-- End modal div -->
