<?php
	
	global $wpdb;


	$currentUserId = get_current_user_id();


	$getOrders = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key = '_useraccount' AND meta_value='{$currentUserId}'");


	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

	$limit = 10; // number of rows in page
	$offset = ( $pagenum - 1 ) * $limit;

	$total = $wpdb->get_var("SELECT COUNT(meta_id) FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key = '_useraccount' AND meta_value='{$currentUserId}' LIMIT $offset,$limit");


	$num_of_pages = ceil( $total / $limit );


	if(count($getOrders)>0){

		foreach($getOrders as $key=>$getOrder){
			//check day 
			$getDay = $wpdb->get_var("SELECT meta_value FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE order_item_id = {$getOrder->order_item_id} AND meta_key = '_days_instance'");

			if($getDay!=null){

				$getInfo= $wpdb->get_row("SELECT instances.id AS instance_id, modules.name AS module_name,modules.sessions AS module_session,modules.price AS price,days.datum AS day_datum,modules.status AS status FROM days LEFT JOIN instances ON days.instance_id = instances.id LEFT JOIN modules  ON instances.module_id = modules.id WHERE days.id = {$getDay}");

				if ( !empty($getInfo->instance_id) ) {
					$getOrders[$key]->instance_id = $getInfo->instance_id;
					$instance_days = $wpdb->get_results("SELECT days.datum AS datum, locations.name AS location_name, docents.name AS docent_name FROM days LEFT JOIN locations ON days.location_id=locations.id LEFT JOIN docents_days ON days.id = docents_days.day_id LEFT JOIN docents ON docents.id = docents_days.docent_id WHERE instance_id='$getInfo->instance_id' AND days.datum >= NOW() ORDER BY days.datum ASC");
					if ( !empty($instance_days) ) {
						$getOrders[$key]->instance_days = $instance_days;
					}
				}
				if ( !empty($getInfo->module_name) ){
					$getOrders[$key]->module_name = $getInfo->module_name;
				}
				if ( !empty($getInfo->module_session) ){
					$getOrders[$key]->module_session = $getInfo->module_session;
				}
				if ( !empty($getInfo->price) ){
					$getOrders[$key]->module_price = $getInfo->price;	
				}
				if ( !empty($getInfo->day_datum) ){ 
					$getOrders[$key]->datum = $getInfo->day_datum;
				}
				if ( !empty($getInfo->status) ){ 
						$getOrders[$key]->status = $getInfo->status;
				}

			}
			
		}	
	}


	//modules.name modules.description, module.session, module.price, days.datum,modules.Status

?>


<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Rooster</h3>
		
		<div class="row">
			<div class="col-md-8"></div>
				<div class="col-md-4" style="padding-right:0px;">
					<form method="GET" action="">
						<input type="hidden" name="action" value="locs_menu" />
					    <div class="input-group">
					        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoek..." />
					        <span class="input-group-btn">
					            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
					                <i class="fa fa-search"></i>
					            </button>
					        </span>
				      	</div>
			      	</form>
		      	</div>
	      	</div>
		
		<div class="row">
	  		<table id="bivt-table">
				<tr>
					<th>Onderdeel nummer</th>		<!-- Instance number for correspondence -->
					<th>Onderdeel</th>		 		<!-- Description -->
					<th>Datum</th>
					<th>Locatie</th>
					<th>Docent</th>
					<!-- <th>Actions</th> -->
				</tr>
				<?php
					foreach ($getOrders as $key=>$order) {
						if ( !empty($order->module_name) ) {
							foreach ($order->instance_days as $k=>$day) {
							?>
							<tr>
								<td><?= $order->instance_id; ?></td>
								<td><?= $order->module_name; ?></td>
								<td><?= $day->datum; ?></td>
								<td><?= $day->location_name; ?></td>
								<td><?= $day->docent_name; ?></td>	
							</tr>						
							<?php
							}
						}
					}
				?>
			</table>
			<?php
				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
		            }
		           echo '</ul>';
        		}
			?>
		</div>
	</div>
</div>
