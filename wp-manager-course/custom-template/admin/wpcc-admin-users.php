<?php

global $wpdb;

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
$order = (isset($_GET['order'])) ? $_GET['order'] : '';
$group = isset($_GET['group']) ? $_GET['group'] : '0';
$pagenum = isset($_GET['pagenum']) ? $_GET['pagenum'] : 1;

$limit = 25; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;

$group_name = $group > 0 ? $wpdb->get_var("SELECT groups.name AS group_name FROM groups WHERE groups.id=$group") : "Alle gebruikers";

$base_query = "SELECT * FROM wp_users LEFT JOIN wp_usermeta ON wp_users.id = wp_usermeta.user_id LEFT JOIN users_groups ON wp_users.ID = users_groups.user_id";
if ($group > 0) {
	$base_query .= " WHERE users_groups.group_id = $group";
}

if($sort=='')
{
	$raw_results = $wpdb->get_results($base_query . " ORDER BY wp_users.ID DESC");
}
else
{
	$raw_results = $wpdb->get_results($base_query . " ORDER BY $sort $order");
}

// echo $base_query . " ORDER BY wp_users.ID DESC";

$raw_groups = $wpdb->get_results("SELECT * from groups");
$user_groups = [];
foreach ($raw_groups AS $k=>$v) {
 	$user_groups[$v->id] = $v->name;
}

$results = [];

$current_id = null;
foreach ($raw_results AS $k=>$v) {
 if ($v->ID == $current_id) {
  $results[$current_id] = (object) array_merge( (array)$results[$current_id], [$v->meta_key => $v->meta_value] );
 } else {
  $current_id = $v->ID;
  $element = ["id" => $v->ID,
    "display_name" => $v->display_name,
    $v->meta_key => $v->meta_value,
    "group_id" => isset($v->group_id) ? $v->group_id : 0,
    "group_name" => isset($v->group_id) ? $user_groups[$v->group_id] : "Onbekend",
  ];
  $results[$current_id] =(object)$element;
 }
}
$num_of_pages = ceil( sizeof($results) / $limit );

class CustomSorter {

	public static function sort($group, $item,$title) {
		$generateLink = '/admin/?action=users_overview&group='.$group.'&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}
}

$page_links = paginate_links( array(
		'base' => add_query_arg( 'pagenum', '%#%' ),
		'format' => '',
		'current' =>$pagenum,
		'total' => $num_of_pages,
		'type'  => 'array',
		'prev_next'   => TRUE,
		'prev_text'    => __('Previous'),
		'next_text'    => __('Next'),
));

$pattern = '/current/';

get_header();

$searchLocation='';
if(isset($_GET['search-location'])) {
	$searchLocation = $_GET['search-location'];
}

?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3><?=$group_name?></h3>
		
			<div class="row">
				<div class="col-md-2">
					<button class="btn btn-default" data-toggle="modal" data-target="#add-user" onclick="$('#body-add-user .cat-edit-multiple').select2({ allowClear: true })">
					<?php
						switch ($group) {
							case '1':
								echo "Administrator toevoegen";
								break;
							case '3':
								echo "Student toevoegen";
								break;
							case '4':
								echo "Zaalhuurder toevoegen";
								break;
							case '5':
								echo "Docent toevoegen";
								break;
							default:
								echo "Gebruiker toevoegen";
								break;
						}
					?> 
					</button>
					<?php 
						include( plugin_dir_path( __FILE__ ) . 'wpcc-admin-user-add-modal.php');
					?>						
				</div>
			
				<div class="col-md-6">
				<?php if( is_array( $page_links ) ) { ?>
					<ul class="pagination">					
					<?php foreach ( $page_links as $page_link ) { ?>
						<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
					<?php } ?>
					</ul>
				<?php } ?> 
				</div>
				
				<div class="col-md-4" style="padding-right:0px;">
					<form method="GET" action="">
						<input type="hidden" name="action" value="locs_menu" />
					    <div class="input-group">
					        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
					        <span class="input-group-btn">
					            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
					                 <i class="fa fa-search"></i>
					            </button>
					        </span>
				      	</div>
			      	</form>
		      	</div>
	      	</div>
		
		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th><?= CustomSorter::sort($group, 'wp_users.display_name','Gebruikersnaam') ?></th>
					<th>Voornaam</th>	
					<th>Achternaam</th>	
					<th>Adres</th>	
					<th>Postcode</th>
					<th>Woonplaats</th>
					<th>Land</th>
					<?php if($group == 0) { ?><th>Groep</th><?php };?>
					<th>&nbsp;</th>
				</tr>
				<?php 
					$counter = 0;
					foreach ($results as $k => $v) {
						if ($counter >= $offset && $counter < ($offset + $limit)) {
							if (!isset($v->shipping_address_1)) {
								$array_v = (array)$v;								
								$array_v['first_name'] = isset($array_v['first_name']) ? $array_v['first_name'] : 
									isset($array_v['personal_firstname']) ? $array_v['personal_firstname'] : 
									isset($array_v['billing_first_name']) ? $array_v['billing_first_name'] : '';
								$array_v['last_name'] = isset($array_v['last_name']) ? $array_v['last_name'] :
									isset($array_v['personal_firstname']) ? $array_v['personal_firstname'] :
									isset($array_v['billing_last_name']) ? $array_v['billing_last_name'] : '';
								$array_v['personal_address'] = isset($v->billing_address_1) ? $v->billing_address_1 : '';
								$array_v['personal_zipcode'] = isset($v->billing_postcode) ? $v->billing_postcode : '';
								$array_v['personal_city'] = isset($v->billing_city) ? $v->billing_city : '';
								$array_v['personal_country'] = isset($v->billing_country) ? $v->billing_country : '';
								$v = (object)$array_v;
							}
				?>

				<tr>
					<td><?= isset($v->display_name) ? $v->display_name : '' ?></td>
					<td><?= isset($v->personal_firstname) ? $v->personal_firstname : '' ?></td>
					<td><?= isset($v->personal_lastname) ? $v->personal_lastname : '' ?></td>
					<td><?= isset($v->personal_address) ? $v->personal_address : '' ?></td>
					<td><?= isset($v->personal_zipcode) ? $v->personal_zipcode : '' ?></td>
					<td><?= isset($v->personal_city) ? $v->personal_city : '' ?></td>
					<td><?= isset($v->personal_country) ? $v->personal_country : '' ?></td>
					<?php
						if($group == 0) {
					?>
						<td><?= isset($v->group_name) ? $v->group_name : '' ?></td>
					<?php
						};
					?>
					<td>
						<a href="<?php echo get_site_url() . '/admin/?action=user_profile&user_id=' . $v->id?>">
							<button class="btn btn-link">
								<span class="glyphicon glyphicon-search"></span>
							</button>						
						</a>

						<a href="<?php echo get_site_url() . '/admin/?action=user_profile&user_id=' . $v->id . '&edit=true'?>">
							<button class="btn btn-link">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
						</a>

						<?php
							if ($group == '3') { // Add portfolio buttons in Student view
						?>
							<a href="<?=get_site_url()?>/admin/?action=student_info&id=<?=$v->id?>">
								<button class="btn btn-link" >
									<span class="glyphicon glyphicon-info-sign"></span>
								</button>	
							</a>
						<?php
							}
						?>					
						
						<?php
							if ($group == '5') { // Add invoices buttons in Teacher view
						?>
							<a href="<?=get_site_url()?>/admin/?action=teacher_invoices&id=<?=$v->id?>">
								<button class="btn btn-link">
									<span class="glyphicon glyphicon-tasks"></span>
								</button>	
							</a>
						<?php
							}
						?>					

						<button class="btn btn-link">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>

				<?php
						} // endif
					$counter++;
					} // end foreach
				?>				
			</table>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-2">
			<?php if( is_array( $page_links ) ) { ?>
					<ul class="pagination">					
					<?php foreach ( $page_links as $page_link ) { ?>
						<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
					<?php } ?>
					</ul>
			<?php } ?> 
			</div>
		</div>
	</div>
</div>
