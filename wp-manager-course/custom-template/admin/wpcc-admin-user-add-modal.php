<?php 
	global $wpdb;

	$group = isset($group) ? $group : 0;
	
	$user_id = get_current_user_id();
	
	$groups = $wpdb->get_results("SELECT groups.id, groups.name FROM groups");
	
	$group_name = 'Onbekend';
	if ($group > 0) {
		// Specific user group already selected
		foreach ($groups AS $k=>$g) {
			if ($g->id == $group) {
				$group_name = $g->name;
			}
		}
	}	
?>

<div class="modal fade" style="margin-top: 65px;" id="add-user" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-80p">
		<div class="modal-content">
		
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span> <span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="add-user-title">
				<?php switch ($group) {
					case '1' :
						echo "Nieuwe admin";
						break;
					case '3' :
						echo "Nieuwe student";
						break;
					case '4' :
						echo "Nieuwe zaalhuurder";
						break;
					case '5' :
						echo "Nieuwe docent";
						break;
					default:
						echo "Nieuwe gebruiker";
				}?>
				</h4>
			</div>
			
			
			<!-- Modal Body -->
			<div class="modal-body body-user-add" id="body-add-user">
				<form class="form-horizontal" method="POST">
					<div class="form-group form-group-sm">
						<div class="col-sm-12">
							<div class="form-group">
								<div class="row">
									<?php 	if ($group > 0) :?>
										<input type="hidden" name="group" id="group" value="<?=$group;?>"/>							
									<?php 	else : ?>
									<div class="row">
										<label class="col-sm-2 control-label" name="group_label" for="group">Groepen</label>
										<div class="col-sm-4">
											<select class="form-control cat-edit-multiple" multiple="multiple" name="group" id="group">
											<?php foreach($groups AS $k=>$g) :
													$selected = (($group > 0) && $g->id==$group ? 'selected="selected"' : ''); ?>
												<option value="<?=$g->id;?>" <?= $selected; ?>>
													<?= $g->name ?>
												</option>
											<?php endforeach; ?>
											</select>
										</div>
									</div>
									<?php endif;?>
								</div>
							</div>							
							<div class="form-group">
								<div class="row">
									<label class="col-sm-2 control-label" for="email">Email</label>
									<div class="col-sm-6">
										<input type="text" 
											class="form-control required" name="email" id="email"
											placeholder="Email adres" required />
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<!-- NOTE: transaction is always 'edit'. row_id 0 will indicate 'add' -->
						<button state=0 type="button" row_id="0"
							transaction="create" table="users"
							class="btn btn-default transaction-button">
							Gebruiker aanmaken
						</button>
					</div>
					<!-- End Modal Footer -->
				</form>
			</div>
			<!-- End modal body div -->
		</div>
		<!-- End modal content div -->
	</div>
	<!-- End modal dialog div -->
</div>
<!-- End modal div -->
