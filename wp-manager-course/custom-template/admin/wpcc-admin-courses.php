<?php

global $wpdb;
$searchLocation ='';
if (isset($_GET['search-location'])) {
	$searchLocation = $_GET['search-location'];
}
//start
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "SELECT COUNT(`id`) FROM courses" );
$num_of_pages = ceil( $total / $limit );

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$sort_order = (''==$sort) ? "" : "ORDER BY $sort $order";
$query = "SELECT courses.* FROM courses LEFT JOIN wp_posts on courses.post_id = wp_posts.id $sort_order ORDER BY courses.modified DESC LIMIT $offset, $limit";
$results = $wpdb->get_results($query);


class CustomSorter {

	public static function sort($item,$title) {


		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=courses_courses&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=courses_courses&sort='.$item;


		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}


if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}

$page_links = paginate_links( array(
		'base' => add_query_arg( 'pagenum', '%#%' ),
		'format' => '',
		'current' =>$pagenum,
		'total' => $num_of_pages,
		'type'  => 'array',
		'prev_next'   => TRUE,
		'prev_text'    => __('Previous'),
		'next_text'    => __('Next'),
));

$pattern = '/current/';

get_header();

?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Opleidingen</h3>

		<div class="row">
			<div class="col-md-2">
				<button class="btn btn-default" data-toggle="modal" data-target="#add-course" onclick="$('#add-course .groups-edit-multiple').select2({ allowClear: true });$('#add-course .cat-edit-multiple').select2({ allowClear: true })">
					Opleiding toevoegen
				</button>
				<?php 
					$action = 'add';
					include( plugin_dir_path( __FILE__ ) . 'wpcc-admin-courses-modal.php');
				?>		
			</div>
			
			<div class="col-md-6">
			<?php if( is_array( $page_links ) ) { ?>
				<ul class="pagination">					
				<?php foreach ( $page_links as $page_link ) { ?>
					<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
				<?php } ?>
				</ul>
			<?php } ?> 
			</div>
				
			<div class="col-md-4" style="padding-right:0px;">
				<form method="GET" action="">
					<input type="hidden" name="action" value="courses_courses" />
					<div class="input-group">
				        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
				            </button>
						</span>
					</div>
				</form>
	      	</div>
		</div>


		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th><?= CustomSorter::sort('post_id','Artikelnummer') ?></th>
					<th><?= CustomSorter::sort('name','Naam') ?></th>
					<th>Omschrijving</th>
					<th>Contactdagen</th>
					<th>Sessies</th>
					<th>Prijs</th>
					<th>Status</th>					
					<th>&nbsp;</th>
				</tr>

				<?php
					foreach ($results as $k => $v) {
						$price = $v->price;
						if ( function_exists('money_format') ) {
							$v->price = money_format('%.2i', $v->price);
						}
					
						$description = substr($v->description, 0, 50);
						if ( $description == "") {
							$description = $v->description;
						}
				?>

				<tr>					
					<td><?= sprintf("%'.06d",$v->post_id) ?></td>
					<td><?= $v->name ?></td>
					<td><?=  $description. '...'; ?></td>
					<td><?= $v->meeting_days ?></td>
					<td><?= $v->sessions ?></td>
					<td><?= $price?></td>
					<td><?= $v->status ?></td>
					<td class="action">
						<a href="<?php echo get_site_url() . '/news/?page=' . $v->slug .'&post_id='. $v->post_id?>">
							<button class="btn btn-link view-courses" data-toggle="modal">								
								<span class="glyphicon glyphicon-search"></span>								
							</button>
						</a>		

						<button class="btn btn-link" data-toggle="modal" data-target="#edit-course-<?= $v->id ?>" onclick="$('#edit-course-<?= $v->id ?> .groups-edit-multiple').select2({ allowClear: true });$('#edit-course-<?= $v->id ?> .cat-edit-multiple').select2({ allowClear: true })">
							<span class="glyphicon glyphicon-edit"></span>
						</button>
						<?php 
							$action = 'edit';
							$modal_data = $v;
							include( plugin_dir_path( __FILE__ ) . 'wpcc-admin-courses-modal.php');
								?>

						<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="courses" transaction="delete">
							<button class="btn btn-link">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
						</a>

					</td>


				<?php 
					} // endforeach
				?>				
				</tr>
				
			</table>
		</div>
	</div>		
</div>

