
<?php
	
	global $wpdb;

	$getId = (isset($_GET['ver_menu_id'])) ? abs($_GET['ver_menu_id']) : 0;
	if($getId==0)
		return false;
	else{

		$getVerInfo = $wpdb->get_row("SELECT changerequests.*,modules.name AS module_name FROM changerequests LEFT JOIN modules ON changerequests.modulename = modules.id WHERE changerequests.id ={$getId}");

		if(count($getVerInfo)==0)
			return false;

		

		$getVerInfo->startinglocation = $wpdb->get_var("SELECT name FROM locations WHERE id = {$getVerInfo->startinglocation}");
		$getVerInfo->changetolocation = $wpdb->get_var("SELECT name FROM locations WHERE id = {$getVerInfo->changetolocation}");
		

	}
	

	get_header();
?>
<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">
		<h3>Vergaderverzoeken</h3>

		<div class="col-sm-3">
	  		<div class="bivt_wrapper">
	  			<div class="bivt_action_wrapper">
	  				<label>Actions</label>
	  			</div>
				<div class="bivt_add_container">
					<button class="btn btn-link">
						<a href="#" class="transaction-button" rowid="<?= $getVerInfo->id; ?>" state=0 table="changerequests_view" transaction="delete">
		    				<span class="glyphicon glyphicon-trash bivt-plus-icon">Delete Veranderverzoek</span>
		    			</a>
		    		</button>
				</div>			
	  		</div>
		</div>
		<div class="col-sm-9">
			<div class="courses_wrapper">
				<table class="course_table">
					<tr>
						<th>Name</th>
						<td><?= $getVerInfo->name; ?></td>
					</tr>
					<tr>
						<th>Created</th>
						<td><?= $getVerInfo->created; ?></td>
					</tr>
					<tr>
						<th>E-mail</th>
						<td><?= $getVerInfo->email; ?></td>
					</tr>
					<tr>
						<th>Module</th>
						<td><?= $getVerInfo->module_name; ?></td>
					</tr>
					<tr>
						<th>Starting Date</th>
						<td><?= $getVerInfo->startingdate; ?></td>
					</tr>
					<tr>
						<th>Starting Location</th>
						<td><?= $getVerInfo->startinglocation; ?></td>
					</tr>
					<tr>
						<th>Change from Date</th>
						<td><?= $getVerInfo->changefromdate; ?></td>
					</tr>
					<tr>
						<th>Change to Date</th>
						<td><?= $getVerInfo->changetodate; ?></td>
					</tr>
					<tr>
						<th>Change to Location</th>
						<td><?= $getVerInfo->changetolocation; ?></td>
					</tr>
					<tr>
						<th>Remarks</th>
						<td><?= $getVerInfo->details; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>