<?php 
	assert(isset($action));
	if ($action == 'edit') {
		assert(isset($modal_data));
	}
	
	global $wpdb;
	
	$user_id = get_current_user_id();
	
	$assignment_id = isset($modal_data) ? $modal_data->id : 0;
	$label = $action . '-assignment' . (($assignment_id > 0) ? '-' . $assignment_id : '');
	$statuses = ['open'=>'Open', 'close'=>'Close'];
	
	$query = "SELECT modules.id, modules.name FROM modules";
	if (isset($docent_id) && $docent_id != null) {
		$query .= " LEFT JOIN docents_modules ON docents_modules.module_id = modules.id WHERE docents_modules.docent_id = '" . $docent_id . "' AND modules.id IS NOT NULL";
	}
	$all_modules = $wpdb->get_results($query);
	
// 	echo "<h4>Query: " . $query . "</h4>";
// 	echo "<h4>Results: " . json_encode($all_modules) . "</h4>";
?>

<div class="modal fade" style="margin-top: 65px;" id="modal-assignment-<?=$assignment_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-80p">
		<div class="modal-content">
		
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span> <span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title" id="<?= $label; ?>">
					<?= (null != $assignment_id) ? "Opdrachten voor de module " . $modal_data->name : "Nieuwe opdrachten";?>
				</h4>
			</div>
			
			
			<!-- Modal Body -->
			<div class="modal-body body-materials" id="body-assignment-<?=$assignment_id;?>">
				<form class="form-horizontal" method="POST">
					<input type="hidden" name="assignment_id" id="assignment-id-<?=$assignment_id;?>" value="<?= $assignment_id; ?>">
					<input type="hidden" name="docent_id" id="docent-id" value="<?=$docent_id;?>">						
					<div class="form-group form-group-sm">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="module_name" class="col-sm-3 control-label">Module</label>
								<div class="col-sm-9">
									<select class="form-control cat-edit-multiple" name="module_id" id="module-name-<?=$assignment_id?>">
										<?php
											foreach ( $all_modules as $module ) {
												$selected = $module_id==$module->id ? 'selected="selected"' : '';
										?>
										<option value="<?=$module->id;?>" <?= $selected; ?>><?=$module->name;?></option>
										<?php
										} // foreach
										?>
									</select>
								</div>
							</div>							
							<div class="form-group">
								<label class="col-sm-3 control-label" for="subject">Onderwerp</label>
								<div class="col-sm-9">
									<input type="text" value="<?= $assignment_id ? $modal_data->text_subject : ''; ?>"
										class="form-control required" name="subject" id="subject-<?=$assignment_id;?>"
										placeholder="Onderwerp" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="content">Omschrijving</label>
								<div class="col-sm-9">
									<textarea class="form-control required mceEditor" rows="6"
										name="content" id="content-<?=$assignment_id;?>" required>
										<?= $modal_data->text_content; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
							<?php 
								if($modal_data->file) {
									$file = get_home_url() . '/wp-content/plugins/wp-manager-course' . $modal_data->file;
									$filename = array_pop(explode('/', $modal_data->file));
							?>
									<label for="file" class="col-sm-3 control-label">Huidige opdracht</label>
									<div class="col-sm-9">
										<a class="btn btn-default" href="<?= $file?>" target="_blank">
											<span class="<?= $icons["download"] ?>"></span>
										<?=$filename;?>	
										</a>
									</div>
							<?php 
								} // if file 
							?>		
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="file" >
									<?=($action=='edit' ? 'Vervang document' : 'Upload document')?>
								</label>
								<div class="col-sm-4">
									<input type="file" class="form-control" name="files" id="file-<?=$assignment_id;?>" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="available_on_day">Beschikbaar vanaf dag</label>
								<div class="col-sm-1">
									<input type="number" value="<?= $assignment_id ? $modal_data->day_number : 0; ?>"
										class="form-control required" name="available_on_day" id="avaialble-on-day-<?=$assignment_id;?>"
										placeholder="Dagnummer" required />
								</div>
								<div>
									<p>Opdrachten die vooraf beschikbaar zijn, worden aangegeven met dag 0</p>
								</div>
							</div>							
							<div class="form-group">
								<label for="status" class="col-sm-3 control-label">Status</label>
								<div class="col-sm-2">
									<select class="form-control" name="status" id="status-<?=$assignment_id;?>">
										<?php foreach($statuses as $key => $status) : ?>
											<?php if($key==$modal_data->status) : ?>
												<option selected="selected" value="<?= $key ?>"><?= $status; ?></option>
											<?php else : ?>
												<option value="<?= $key; ?>"><?= $status; ?></option>
											<?php endif; ?>
										<?php endforeach; ?>
									</select>
								</div>
							</div>


						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<!-- NOTE: transaction is always 'edit'. row_id 0 will indicate 'add' -->
						<button state=0 type="button" row_id="<?=$assignment_id;?>"
							transaction="edit" table="assignments"
							class="btn btn-default transaction-button">
								<?= $assignment_id > 0 ? 'Wijzigingen opslaan' : 'Opdracht opslaan';?>
						</button>
					</div>
					<!-- End Modal Footer -->
				</form>
			</div>
			<!-- End modal body div -->
		</div>
		<!-- End modal content div -->
	</div>
	<!-- End modal dialog div -->
</div>
<!-- End modal div -->
