<?php
	include (plugin_dir_path (__FILE__ ) . '../../classes/TableSorter.php');

	use \wp_manager_course\TableSorter;

	global $wpdb;

	$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
	$order = (isset($_GET['order'])) ? $_GET['order'] : '';

	$statuses = ['close'=>'Close','open'=>'Open'];

	$query = "SELECT * FROM categories";
	$tableSorter = new TableSorter ($query);
	$data = $tableSorter->getData();
	
	if (null == $data || sizeof($data) < 1) {
		// Did not retrieve a single result. Let's quit
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
		die();
	}
	
	if(!function_exists('get_home_path'))
	{
		function get_home_path()
		{
			$home    = set_url_scheme( get_option( 'home' ), 'http' );
			$siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
			if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
				$wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
				$pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
				$home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
				$home_path = trailingslashit( $home_path );
			} else {
				$home_path = ABSPATH;
			}
	
			return str_replace( '\\', '/', $home_path );
		}
	}
	
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">
		<h3>Categorieën</h3>
		<div class="col-md-2" >
		    	<?= do_shortcode( "[bivt-categories]"); ?>		    

		</div>
		<div class="col-md-6"></div>
			<div class="col-md-4">
				<form method="GET" action="">
					<input type="hidden" name="action" value="planning_instances" />
					<div class="input-group">
				        <input type="text" id="search-instance" name="search-instance" value="" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
							<button class="btn btn-default" type="submit" style="margin-top:-10px;">
								<i class="fa fa-search"></i>
							</button>
						</span>
					</div>
				</form>
			</div>			

			<div class="row">
				<div class="col-md-12">
					<div class="table-wrapper">
				  		<table id="bivt-table">
							<tr>
								<th><?= $tableSorter->getSortLink('name','Name') ?></th>
								<th><?= $tableSorter->getSortLink('type','Type') ?></th>
								<th><?= $tableSorter->getSortLink('status','Status') ?></th>
								<th>&nbsp;</th>
							</tr>
							<?php foreach ($data as $k => $v) : ?>
							<tr id="categories-id-<?= $v->id; ?>">
								<td><?= $v->name ?></td>
								<td><?= $v->type ?></td>
								<td><?= $v->status ?></td>
								<td>
									<button class="btn btn-link" data-toggle="modal" data-target="#view_categories<?= $v->id ?>">
										<span class="glyphicon glyphicon-search"></span>
									</button>

									<div class="modal fade" id="view_categories<?= $v->id ?>" tabindex="-1" role="dialog" aria-labelledby="categories" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-80p">
											<div class="modal-content">
												<!-- Modal Header -->
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														   <span aria-hidden="true">×</span>
														   <span class="sr-only">Close</span>
													</button>
													<h4 class="modal-title" id="categories">
														Categories
													</h4>
												</div>
												
												<!-- Modal Body -->
												<div class="modal-body">
													<form id="ViewCategories<?= $v->id; ?>" class="form-horizontal edit-categories" method="POST">
														<div class="form-group form-group-sm">
															<div class="col-sm-12">
																<div class="form-group">
																	<label  class="col-sm-2 control-label" for="name">Naam</label>
																	<div class="col-sm-10">
																		<input type="text" readonly="readonly" class="form-control required" name="name" id="name" placeholder="Naam" value="<?= $v->name ?>" required/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="description" >Omschrijving</label>
																	<div class="col-sm-10">
																		<input type="text" readonly="readonly" class="form-control required" name="description" id="description"value="<?= $v->description ?>" required/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="type" >Type</label>
																	<div class="col-sm-10">
																		<select class="form-control" name="type" id="type" disabled="disabled">
																			<option value="vak" <?= isset($v->type) ? ($v->type == 'vak' ? 'selected="selected"' : '' ): '';?>>vak</option>
																			<option value="vorm" <?= isset($v->type) ? ($v->type == 'vorm' ? 'selected="selected"' : '' ): '';?>>vorm</option>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="meta_description" >Meta Omschrijving</label>
																	<div class="col-sm-10">
																		<input type="text" readonly="readonly" class="form-control required" name="meta_description" id="meta_description"value="<?= $v->meta_description ?>" required/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="meta_keyword" >Meta Keywords</label>
																	<div class="col-sm-10">
																		<input type="text" readonly="readonly" class="form-control required" name="meta_keywords" id="meta_keywords"value="<?= $v->meta_keywords ?>" required/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="status" >Status</label>
																	<div class="col-sm-10">
																		<select class="form-control required" name="status" disabled="disabled">
																		<?php foreach($statuses as $key => $status) : ?>
																		<?php if($key==$v->status) : ?>
																		<option selected="selected" value="<?= $key ?>" ><?= $status; ?></option>
																		<?php else : ?>
																		<option value="<?= $key; ?>" ><?= $status; ?></option>
																		<?php endif; ?>
																		<?php endforeach; ?>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="picture_preview" >Huidige afbeelding</label>
																	<div class="col-sm-4">	
																		<div class="x-close removeTablePicture" table="categories" requestid="<?= $v->id; ?>" state=0></div>
																		<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
																		<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive categoryPicture specialPicture" />
																		<?php else : ?>
																		Geen afbeelding gevonden
																		<?php endif; ?>
																	</div>
																</div>			
															</div>
														</div>

													</form>
												</div> <!-- End modal body div -->
											</div> <!-- End modal content div -->
										</div> <!-- End modal dialog div -->
									</div> <!-- End modal div -->	






									<button class="btn btn-link" data-toggle="modal" data-target="#edit_categories<?= $v->id ?>">
										<span class="glyphicon glyphicon-edit"></span>
									</button>

									<div class="modal fade" id="edit_categories<?= $v->id ?>" tabindex="-1" role="dialog" aria-labelledby="categories" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-80p">
											<div class="modal-content">
												<!-- Modal Header -->
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														   <span aria-hidden="true">×</span>
														   <span class="sr-only">Close</span>
													</button>
													<h4 class="modal-title" id="categories">
														Categorieën
													</h4>
												</div>
												
												<!-- Modal Body -->
												<div class="modal-body">
													<form id="EditCategories<?= $v->id; ?>" class="form-horizontal edit-categories" method="POST">
														<div class="form-group form-group-sm">
															<div class="col-sm-12">
																<div class="form-group">
																	<label  class="col-sm-2 control-label" for="name">Naam</label>
																	<div class="col-sm-10">
																		<input type="text" class="form-control required" name="name" id="name" placeholder="Naam" value="<?= $v->name ?>" required/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="description" >Omschrijving</label>
																	<div class="col-sm-10">
																		<textarea class="form-control required mceEditor" rows="6" name="description" placeholder="Omschrijving" id="description" required ><?= $v->description ?></textarea>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="type" >Type</label>
																	<div class="col-sm-10">
																		<select class="form-control" name="type" id="type">
																			<option value="vak" <?= isset($v->type) ? ($v->type == 'vak' ? 'selected="selected"' : '' ): '';?>>vak</option>
																			<option value="vorm" <?= isset($v->type) ? ($v->type == 'vorm' ? 'selected="selected"' : '' ): '';?>>vorm</option>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="meta_description" >Meta Omschrijving</label>
																	<div class="col-sm-10">
																		<textarea class="form-control required" name="meta_description" id="meta_description" required>
																			<?= $v->meta_description; ?>
																		</textarea> 
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="meta_keyword" >Meta Keywords</label>
																	<div class="col-sm-10">
																		<textarea class="form-control required" name="meta_keywords" id="meta_keywords" required>
																			<?= $v->meta_keywords; ?>
																		</textarea> 
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="status" >Status</label>
																	<div class="col-sm-10">
																		<select class="form-control required" name="status">
																		<?php foreach($statuses as $key => $status) : ?>
																		<?php if($key==$v->status) : ?>
																		<option selected="selected" value="<?= $key ?>" ><?= $status; ?></option>
																		<?php else : ?>
																		<option value="<?= $key; ?>" ><?= $status; ?></option>
																		<?php endif; ?>
																		<?php endforeach; ?>
																		</select>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="picture_preview" >Huidige afbeelding</label>
																	<div class="col-sm-4">	
																		<div class="x-close removeTablePicture" table="categories" requestid="<?= $v->id; ?>" state=0></div>
																		<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
																		<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive categoryPicture specialPicture" />
																		<?php else : ?>
																		Geen afbeelding gevonden
																		<?php endif; ?>
																	</div>
																</div>
																<div class="form-group" style="display:none;">
																	<label class="col-sm-2 control-label" for="picture_preview_new" >Voorbeeld afbeelding</label>
																	<div class="col-sm-4">	
																		<img class="picture_preview_new img-responsive"/>
																	</div>
																</div>
																<div class="form-group">
																	<label class="col-sm-2 control-label" for="picture" >Afbeelding</label>
																	<div class="col-sm-10">
																		<input type="file" catid="<?= $v->id; ?>" class="form-control required" name="picture" id="picture" required/>
																	</div>
																</div>
			
															</div>
														</div>
												  		<!-- Modal Footer -->
														<div class="modal-footer">
															<button id="EditCategorySubmit" categoryid="<?= $v->id; ?>" type="button" state=0 class="btn btn-default">Categorie wijzigen</button>
														</div><!-- End Modal Footer -->
											  
													</form>
												</div> <!-- End modal body div -->
											</div> <!-- End modal content div -->
										</div> <!-- End modal dialog div -->
									</div> <!-- End modal div -->	

									<button class="btn btn-link delete-categories" delete-row="<?= $v->id; ?>" state=0>
										<span class="glyphicon glyphicon-trash"></span>
									</button>

								</td>
							</tr>
	
							<?php endforeach; ?>				
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
		
</div>
