<?php

global $wpdb;

//start
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
$limit = 25; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;

$total = $wpdb->get_var( "SELECT COUNT(instances.id) FROM instances LEFT JOIN modules ON modules.id = instances.module_id");

$num_of_pages = ceil( $total / $limit );

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$base_query = "SELECT ins.id, ins.comments, ins.group, ins.full, ins.status, ins.group_size, mods.name AS name,mods.id AS mods_id, mods.slug AS slug, days.datum, days.id AS days_id, locations.name AS location, locations.id AS location_id FROM instances as ins LEFT JOIN modules AS mods ON mods.id = ins.module_id LEFT JOIN days ON days.instance_id = ins.id LEFT JOIN locations ON days.location_id=locations.id GROUP BY ins.id";

if($sort=='')
{
	$results = $wpdb->get_results($base_query . " ORDER BY ins.id DESC LIMIT $offset,$limit");
}
else
{
	$results = $wpdb->get_results($base_query . " ORDER BY $sort $order LIMIT $offset,$limit");
}

class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=planning_instances&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=planning_instances&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}
$page_links = paginate_links( array(
		'base' => add_query_arg( 'pagenum', '%#%' ),
		'format' => '',
		'current' =>$pagenum,
		'total' => $num_of_pages,
		'type'  => 'array',
		'prev_next'   => TRUE,
		'prev_text'    => __('Previous'),
		'next_text'    => __('Next'),
));

$pattern = '/current/';

// Ensure that ONLY 1 modal dialog for Add is created.
$has_add_modal = 0;

?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="inner-container">
		<div class="row pad-10">
			<h3>Groepen</h3>
		</div>
		<div class="row pad-10">
			<div class="col-md-2">
			    <?php if( isset($v->id) ) {
			    		unset($v->id); 
			    	}
			    ?>
			    <a href="?action=instance_menu&add=true">
					<button class="btn btn-default">
						Groep toevoegen
					</button>
				</a>			    
			</div>

			<div class="col-md-6">
			<?php if( is_array( $page_links ) ) { ?>
				<ul class="pagination">					
				<?php foreach ( $page_links as $page_link ) { ?>
					<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
				<?php } ?>
				</ul>
			<?php } ?> 
			</div> 
			
			<div class="col-md-4">
				<form method="GET" action="">
					<input type="hidden" name="action" value="planning_instances" />
					<div class="input-group">
				        <input type="text" id="search-instance" name="search-instance" value="" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
				            </button>
						</span>
					</div>
				</form>
	      	</div>			
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="table-wrapper">		
			  		<table id="bivt-table">
						<tr>
							<th><?= CustomSorter::sort('ins.id','Id') ?></th>
							<th><?= CustomSorter::sort('name','Module') ?></th>
							<th><?= CustomSorter::sort('ins.group','Groep') ?></th>
							<th><?= CustomSorter::sort('locations.name','Locatie') ?></th>
							<th><?= CustomSorter::sort('days.datum','Datum') ?></th>
							<th>Vol</th>
							<th>Status</th>
		 					<th>Plaatsen</th>
							<th>Inschrijvingen</th>
							<th>Opmerkingen</th>
		 					<th>&nbsp;</th>
						</tr>
						<?php foreach ($results as $k => $v) : ?>
						<tr id="instance-<?= $v->id; ?>">
							<td><?= $v->id ?></td>
							<td><a href="<?=get_home_url() . '/news/?page=' . $v->slug . '&view_only=true'?>"><?=$v->name;?></a></td>
							<td><?= $v->group; ?></td>
							<td><a href="<?=get_home_url() . '/locatie/?locatie=' . $v->location_id ?>"><?=$v->location;?></a></td>
							<td><?= date('d-m-Y', strtotime($v->datum)); ?></td>
							<td><?= $v->full > 0 ? 'Ja' : 'Nee' ?></td>
							<td><?= $v->status == 'open' ? 'open' : 'gesloten' ?></td>
							<td><?= $v->group_size ?></td>
							<td><?= count(get_students_by_instance_id($v->id)); ?></td>
							<td><?= isset($v->comments) ? substr($v->comments,0, 20) : '' ?>
							<td>

								<a href="?action=instance_menu&instance=<?= $v->id ?>">
									<button class="btn btn-link">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</a>						

								<a href="?action=instance_menu&instance=<?= $v->id ?>&edit=true">
									<button class="btn btn-link">
									<span class="glyphicon glyphicon-edit"></span>
									</button>
								</a>						

								<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="instance" transaction="delete">
									<button class="btn btn-link">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
								</a>
							</td>
						<?php 
							endforeach;
						?>										
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>