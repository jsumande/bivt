<?php

global $wpdb;

$results = $wpdb->get_results('SELECT * FROM groups');
get_header();
?>
<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Groups</h3>
		<div class="col-sm-12">
			<div class="admin-user-nav pull-left">
				<ul>
					<li>
						<a href="<?=  get_site_url(). '/admin/?action=users_menu';?> "> 
							<i class="fa fa-user-secret" aria-hidden="true"></i> User Manager
						</a>
					</li>
					<li class="active">
						<a href="<?=  get_site_url(). '/admin/?action=users_menu&tab=groups';?>">
							<i class="fa fa-users" aria-hidden="true"></i> Groups
						</a>
					</li>
				</ul>
			</div>
			<div class="bivt_add_container pull-right">
		    	<!-- Button trigger modal -->
				<button class="btn btn-link" data-toggle="modal" data-target="#add_groups">
		    		<span class="glyphicon glyphicon-plus bivt-plus-icon"> Add groups </span>
		    	</button>
			</div>
				<div class="modal fade" id="add_groups" tabindex="-1" role="dialog" aria-labelledby="users" aria-hidden="true">
				    <div class="modal-dialog modal-lg modal-80p">
				        <div class="modal-content">
				            <!-- Modal Header -->
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">
				                       <span aria-hidden="true">×</span>
				                       <span class="sr-only">Close</span>
				                </button>
				                <h4 class="modal-title" id="users">
				                    Add Groups
				                </h4>
				            </div>
				            
				            <!-- Modal Body -->
				            <div class="modal-body">
							    <form id="AddGroups" class="form-horizontal" method="POST">

							        <div class="form-group form-group-sm">
							            <!-- left column -->
							            <div class="col-sm-12">

						                  	<div class="form-group">
						                    	<label class="col-sm-2 control-label" for="name" >Name</label>
						                    	<div class="col-sm-10">
						                    		<input type="text" class="form-control" name="name" id="name" placeholder="name" required/>
						                    	</div>
						                  	</div>
				               
							            </div>
							        </div>

							      <!-- Modal Footer -->
						            <div class="modal-footer">
						                <button id="addGroupsSubmit"type="button" class="btn btn-default">Add </button>
						            </div><!-- End Modal Footer -->
							      
							    </form>
							</div> <!-- End modal body div -->
						</div> <!-- End modal content div -->
					</div> <!-- End modal dialog div -->
				</div> <!-- End modal div -->

		  		<table id="bivt-table">
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Actions</th>
					</tr>

					<?php 
						foreach ($results as $k => $v):
					?>

					<tr>
						<td><?= $v->id ?></td>
						<td><?= $v->name ?></td>
						<td class="action" >
							<button class="btn btn-link edit-groups" data-toggle="modal" user-row="<?= $v->id ?>" data-target="#edit_groups">
								<span class="glyphicon glyphicon-edit"></span>
							</button>
							<button class="btn btn-link delete-groups" user-row="<?= $v->id ?>">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
						</td>
					</tr>

					<?php endforeach; ?>				
				</table>
				<!-- MODAL EDIT GROUP -->
				<div class="modal fade" id="edit_groups" tabindex="-1" role="dialog" aria-labelledby="users" aria-hidden="true">
				    <div class="modal-dialog modal-lg modal-80p">
				        <div class="modal-content">
				            <!-- Modal Header -->
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">
				                       <span aria-hidden="true">×</span>
				                       <span class="sr-only">Close</span>
				                </button>
				                <h4 class="modal-title" id="users">
				                    Edit Groups
				                </h4>
				            </div>
				            
				            <!-- Modal Body -->
				            <div class="modal-body">
							    <form id="AddGroups" class="form-horizontal" method="POST">

							        <div class="form-group form-group-sm">
							            <!-- left column -->
							            <div class="col-sm-12">
							           		<input type="hidden" id="id"/>
						                  	<div class="form-group">
						                    	<label class="col-sm-2 control-label" for="name" >Name</label>
						                    	<div class="col-sm-10">
						                    		<input type="text" class="form-control" name="name" id="name" placeholder="name" required/>
						                    	</div>
						                  	</div>
				               
							            </div>
							        </div>

							      <!-- Modal Footer -->
						            <div class="modal-footer">
						                <button id="addGroupsSubmit"type="button" class="btn btn-default">Add </button>
						            </div><!-- End Modal Footer -->
							      
							    </form>
							</div> <!-- End modal body div -->
						</div> <!-- End modal content div -->
					</div> <!-- End modal dialog div -->
				</div> <!-- End modal div -->
			</div>	

		</div>
</div>