<?php

global $wpdb;

$results = $wpdb->get_results('SELECT * FROM docents');


get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Docents</h3>

		<div class="col-sm-3">
	  		<div class="bivt_wrapper">
	  			<div class="bivt_action_wrapper">
	  				<label>Actions</label>
	  			</div>
				<div class="bivt_add_container">
					<a href="#">
		          		<span class="glyphicon glyphicon-plus bivt-plus-icon"> Add Docents </span>
		        	</a>
				</div>			
	  		</div>
		</div>
		<div class="col-sm-9">
	  		<table id="bivt-table">
				<tr>
					<th>Name</th>
					<th>Picture</th>					
					<th>&nbsp;</th>
				</tr>
				<?php 
					foreach ($results as $k => $v):
				?>

				<tr>
					<td><?= $v->name ?></td>
					<td><?= $v->picture ?></td>
					<td>
						<button class="btn btn-link view-news" data-toggle="modal">
							<a href="#" target="_blank">
								<span class="glyphicon glyphicon-search"></span>
							</a>
						</button>						

						<button class="btn btn-link" data-toggle="modal" data-target="#edit_news<?= $v->id ?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>

						<button class="btn btn-link">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>

				<?php endforeach; ?>				
			</table>
		</div>
	</div>
		
</div>
