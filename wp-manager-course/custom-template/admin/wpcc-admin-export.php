<?php

global $wpdb;
$group = isset($_GET['group']) ? $_GET['group'] : '0';
$group_name = $group > 0 ? $wpdb->get_var("SELECT groups.name AS group_name FROM groups WHERE groups.id=$group") : "Alle gebruikers";

?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">

		<h3>Exporteren</h3>

		<div class="col-sm-9">
		Export van <?=$group_name?> gegevens is momenteel niet beschikbaar.
		</div>
	</div>
</div>