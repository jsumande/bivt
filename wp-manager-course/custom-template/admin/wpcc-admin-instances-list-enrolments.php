<?php

$instance = abs($_GET['instance']);
$dayId = abs($_GET['day_id']);

global $wpdb;

//start
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;


$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;


$total = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key = '_days_instance' AND meta_value = '{$dayId}'");

$num_of_pages = ceil( $total / $limit );



$orders = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key = '_days_instance' AND meta_value = '{$dayId}'");


if(count($orders)>0){
	
	foreach ($orders as $keyOrig => $order) {

		$getUserId = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key = '_useraccount' AND order_item_id = {$order->order_item_id}");

		if(count($getUserId)>0){

			$usersPrefix = $wpdb->prefix.'users';
			$user = $wpdb->get_row("SELECT display_name FROM {$usersPrefix} WHERE id = {$getUserId->meta_value}");
			$getModule = $wpdb->get_row("SELECT modules.name as module_name FROM days LEFT JOIN instances ON days.instance_id = instances.id LEFT JOIN modules ON instances.module_id = modules.id WHERE days.id = {$dayId}");
			$getDays = $wpdb->get_row("SELECT datum,locations.name AS location_name FROM days LEFT JOIN locations ON days.location_id = locations.id WHERE days.id = {$dayId}");

			if(count($getDays)>0){
				$orders[$keyOrig]->display_name = $user->display_name;
				$orders[$keyOrig]->datum = $getDays->datum;
				$orders[$keyOrig]->location_name = $getDays->location_name;
			}


			if(count($getModule)>0){
				$orders[$keyOrig]->module_name = $getModule->module_name;
			}
			
		}
	}
}
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">
		<h3>List of Enrolments</h3>
		<div class="row">
			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<thead>
					<tr>
						<th>Order Id</th>
						<th>Username</th>
						<th>Module Name</th>
						<th>Datum</th>
						<th>Location</th>
						<th class="actions"></th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($orders)>0) : ?>
					<?php foreach ($orders as $key => $value) : ?>
					<tr>
						<td><?= $value->order_item_id; ?></td>
						<td><?= $value->display_name; ?></td>
						<td><?= $value->module_name; ?></td>
						<td><?= $value->datum; ?></td>
						<td><?= $value->location_name; ?></td>
					</tr>
					<?php endforeach; ?>
					<?php else : ?>
					<tr>
						<td colspan=5 style="text-align:center;">No enrolment found.</td>
					</tr>
					<?php endif; ?>
				</tbody>
			</table>
			<?php
				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
		            }
		           echo '</ul>';
        		}
			?>
		</div>
	</div>
</div>