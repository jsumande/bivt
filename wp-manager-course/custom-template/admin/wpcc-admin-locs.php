<?php

global $wpdb;


$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "SELECT COUNT(*) FROM locations" );
$num_of_pages = ceil( $total / $limit );

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';


$searchLocation = (isset($_GET['search-location'])) ? $_GET['search-location'] : '';



$where = ($searchLocation!='') ? "WHERE name LIKE '%$searchLocation%' OR address LIKE '%$searchLocation%' OR parking LIKE '%$searchLocation%' OR comments LIKE '%$searchLocation%'" : '';



if($sort=='')
{
	$results = $wpdb->get_results("SELECT * FROM locations $where ORDER BY id DESC LIMIT $offset,$limit");
}
else
{
	$results = $wpdb->get_results("SELECT * FROM locations $where ORDER BY $sort $order LIMIT $offset,$limit");
}


class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=locs_menu&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=locs_menu&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color: inherit !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}

if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}


get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">
	
		<h3>Locaties</h3>

		<div class="row">
			<div class="col-md-8"></div>
			<div class="col-md-4" style="padding-right:0px;">
				<form method="GET" action="">
					<input type="hidden" name="action" value="locs_menu" />
					<div class="input-group">
				        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
					        </button>
					    </span>
				    </div>
			   </form>
			</div>
		</div>
		<div class="row">
			<table id="bivt-table">
				<tr>
					<th><?= CustomSorter::sort('id','Id') ?></th>
					<th>Naam</th>
					<th>Adres</th>
					<th>Afbeelding</th>
					<th>Status</th>
					<th>&nbsp;</th>
				</tr>
				<?php if(count($results)>0) : ?>
				<?php
					foreach ($results as $k => $v):
				?>
				<tr id="location-<?= $v->id; ?>">
					<td><?= $v->id ?></td>
					<td><?= $v->name ?></td>
					<td><?= $v->address ?></td>
					<td>
						<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
							<a class="fancybox" title="<?= $v->name; ?>" rel="group" href="<?= plugins_url('wp-manager-course'.$v->picture); ?>"><img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" alt=""  height=200 width=200/></a>
						<?php else : ?>
							No Image found.
						<?php endif; ?>
					</td>
					<td><?= $v->status ?></td>
					<td>
						<button class="btn btn-link" data-toggle="modal" data-target="#edit_location_<?= $v->id ?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>
						<button class="btn btn-link">
							<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="location" transaction="delete">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</button>
					</td>
					<div class="modal fade" style="margin-top:65px;" id="edit_location_<?= $v->id; ?>" tabindex="-1" role="dialog" aria-labelledby="edit_locations_<?= $v->id; ?>" aria-hidden="true">
					    <div class="modal-dialog modal-lg modal-80p">
					        <div class="modal-content">
					            <!-- Modal Header -->
					            <div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">
										<span aria-hidden="true">×</span>
					                       <span class="sr-only">Close</span>
						                </button>
						                <h4 class="modal-title" id="edit_days_<?= $day->id; ?>">
						                    Location
						                </h4>
						            </div>
						            <!-- Modal Body -->
						            <div class="modal-body edit-location" id="edit-location-<?= $v->id; ?>">
									    <form class="form-horizontal" method="POST">
									    	<input type="hidden" name="location_id" id="location_id" value="<?= $v->id; ?>" />
									        <div class="form-group form-group-sm">
							                    <div class="row">
								                    <label for="name" class="col-sm-2 control-label">Name</label>
								                    <div class="col-sm-10">
								                        <input type="text" class="form-control required" name="name" id="name" placeholder="Name" required value="<?= $v->name; ?>"/>                      
								                    </div>
							                	</div>
							                </div>
							               	<div class="form-group form-group-sm">
							               		<div class="row">
								                    <label for="address" class="col-sm-2 control-label">Address</label>
								                    <div class="col-sm-10">
								                    	<textarea class="form-control mceEditor" rows="6" name="address" id="address" ><?= $v->address ?></textarea>               
								                    </div>
							                	</div>
							                </div>
							                <div class="form-group">
							                	<div class="row">
													<label class="col-sm-2 control-label" for="picture_preview" >Current Picture</label>
													<div class="col-sm-10">	
														<div class="x-close removeTablePicture" table="locations" requestid="<?= $v->id; ?>" state=0></div>
														<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
														<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive newsPicture specialPicture" />
														<?php else : ?>
														No Image found.
														<?php endif; ?>
													</div>
												</div>
											</div>
											<div class="form-group" style="display:none;">
												<div class="row">
													<label class="col-sm-2 control-label" for="picture_preview_new" >Picture Preview</label>
													<div class="col-sm-10">	
														<img class="picture_preview_new img-responsive"/>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="row">
													<label class="col-sm-2 control-label" for="picture" >Picture</label>
													<div class="col-sm-10">
														<input type="file" locationid="<?= $v->id; ?>" class="form-control required" name="picture" id="picture" required/>
													</div>
												</div>
											</div>
											<div class="form-group form-group-sm">
												<div class="row">
								                    <label for="status" class="col-sm-2 control-label">Status</label>
								                    <div class="col-sm-10">
								                    <?php
								                    	$statuses = ['open','close'];
								                    ?>
								                    	<select id="status" name="status" class="form-control">
								                    	<?php foreach ($statuses as $key => $value) : ?>
								                    	<option value="<?= $value ?>" <?php if($value==$v->status) : ?>selected="selected"<?php endif; ?>><?= $value; ?></option>
								                    	<?php endforeach; ?>
								                    	</select>
								                    </div>
							                    </div>                    
							                </div>
									      	<!-- Modal Footer -->
								            <div class="modal-footer">
								                <button state=0 type="button" rowid="<?= $v->id; ?>" transaction="edit" table="location" class="btn btn-default transaction-button">Edit Location</button>
								            </div><!-- End Modal Footer -->
									    </form>
									</div> <!-- End modal body div -->
								</div> <!-- End modal content div -->
							</div> <!-- End modal dialog div -->
						</div> <!-- End modal div -->
					</tr>
					<?php endforeach; ?>
					<?php else : ?>
					<tr>
						<td style="text-align:center;" colspan=6><?php if($searchLocation!='') : ?>No result found for "<?= $searchLocation; ?>"<?php else : ?>No location found.<?php endif; ?></td>
					</tr>
					<?php endif; ?>
					<div class="modal fade" style="margin-top:65px;" id="add_location" tabindex="-2" role="dialog" aria-labelledby="add_location" aria-hidden="true">
					    <div class="modal-dialog modal-lg modal-80p">
					        <div class="modal-content">
					            <!-- Modal Header -->
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal">
				                       <span aria-hidden="true">×</span>
				                       <span class="sr-only">Close</span>
					                </button>
					                <h4 class="modal-title">
					                    Location
					                </h4>
					            </div>
					            <!-- Modal Body -->
					            <div class="modal-body add-location" id="add-location">
								    <form class="form-horizontal" method="POST">
								        <div class="form-group form-group-sm">
						                    <div class="row">
							                    <label for="name" class="col-sm-2 control-label">Name</label>
							                    <div class="col-sm-10">
							                        <input type="text" class="form-control required" name="name" id="name" placeholder="Name" required value=""/>                      
							                    </div>
						                	</div>
						                </div>
						               	<div class="form-group form-group-sm">
						               		<div class="row">
							                    <label for="address" class="col-sm-2 control-label">Address</label>
							                    <div class="col-sm-10">
							                    	<textarea class="form-control mceEditor" rows="6" name="address" id="address" ></textarea>
							                    </div>
						                	</div>
						                </div>
										<div class="form-group form-group-sm">
											<div class="row">
												<label class="col-sm-2 control-label" for="picture" >Picture</label>
												<div class="col-sm-10">
													<input type="file" locationid="<?= $v->id; ?>" class="form-control required" name="picture" id="picture" required/>
												</div>
											</div>
										</div>
										<div class="form-group form-group-sm">
											<div class="row">
							                    <label for="status" class="col-sm-2 control-label">Status</label>
							                    <div class="col-sm-10">
							                    <?php
							                    	$statuses = ['open','close'];
							                    ?>
							                    	<select id="status" name="status" class="form-control">
							                    	<?php foreach ($statuses as $key => $value) : ?>
							                    	<option value="<?= $value ?>"><?= $value; ?></option>
							                    	<?php endforeach; ?>
							                    	</select>
							                    </div>
						                    </div>                    
						                </div>
								      	<!-- Modal Footer -->
							            <div class="modal-footer">
							                <button state=0 type="button" transaction="add" table="location" class="btn btn-default transaction-button">Add Location</button>
							            </div><!-- End Modal Footer -->
								    </form>
								</div> <!-- End modal body div -->
							</div> <!-- End modal content div -->
						</div> <!-- End modal dialog div -->
					</div> <!-- End modal div -->
				</table>
			</div>
			<?php
				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
		            }
		           echo '</ul>';
        		}
			?>
			<div class="bivt_add_container">
				<button state=0 type="button" transaction="add" table="" class="btn btn-default transaction-button">
				Locatie toevoegen
				</button>		
			</div>			
		</div>
		
</div>
