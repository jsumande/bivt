<?php
	include (plugin_dir_path (__FILE__ ) . '../../classes/TableSorter.php');

	use \wp_manager_course\TableSorter;

	global $wpdb;

	$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
	$order = (isset($_GET['order'])) ? $_GET['order'] : '';
	$instance_id = isset($_GET['instance_id']) ? $_GET['instance_id'] : null;
	$day_id = isset($_GET['day_id']) ? $_GET['day_id'] : null;
	
	if (null == $instance_id) {
		// Cannot display without having a specific instance ID
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
		die();
	}

	if (null == $day_id) {
		// Cannot display without having a specific day ID
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
		die();
	}
	
	// Get the current instace and all related days with their location
	$query = "SELECT instances.id, instances.comments, instances.group, instances.full, instances.status, instances.group_size, days.datum, days.id AS day_id, locations.id AS location_id, locations.name AS location, modules.name AS module_name, docents.user_id AS docent_user_id, docents.id AS docent_id, docents.name AS docent_name FROM `instances` LEFT JOIN modules ON modules.id=instances.module_id LEFT JOIN days ON days.instance_id=instances.id LEFT JOIN locations ON locations.id=days.location_id LEFT JOIN docents_days ON docents_days.day_id = days.id LEFT JOIN docents ON docents.id = docents_days.docent_id WHERE instances.id='".$instance_id."'";
	$tableSorter = new TableSorter ($query);
	$data = $tableSorter->getData();

	if (null == $data || sizeof($data) < 1) {
		// Did not retrieve a single result. Let's quit
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
		die();
	}

	$students = get_attendance_by_day_id($day_id);
	
	$instance = $data[0]; // All rows have same info about the instance, just pick the first one.
	$datum = (null != $day_id) ? $wpdb->get_var("SELECT days.datum FROM days WHERE days.id='" . $day_id . "'") : null;
	$datum = (null != $datum) ? date('d-m-Y',strtotime($datum)) : '';
	
?>

<div class="wrap">

	<?php include_once 'wpcc-admin-header.php'; ?>
	
	<div class="container">
		<h3>Groep: <?=$instance->group; ?></h3>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Module:</div>
				<div class="col-sm-2"><?=$instance->module_name;?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Status:</div>
				<div class="col-sm-2"><?=$instance->status;?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Vol:</div>
				<div class="col-sm-2"><?=$instance->full > 0 ? 'Ja' : 'Nee';?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Aantal plaatsen:</div>
				<div class="col-sm-2"><?=$instance->group_size;?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Aantal studenten (groep):</div>
				<div class="col-sm-2"><?=sizeof (get_students_by_instance_id($instance_id));?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-2">Aantal studenten (dag):</div>
				<div class="col-sm-2"><?=sizeof (get_students_by_day_id($day_id));?></div>
			</div>
		</div>
				
		<div class="related row">
			<div class="col-md-12">
				<h3>Studenten op <?= $datum; ?></h3>
				<div class="table-wrapper">
					<table id="bivt-table">
						<thead>
							<tr>
								<th>Naam</th>
								<th>Email</th>
								<th>Aanwezigheid</th>
								<th>Opmerkingen</th>
								<th class="actions"></th>
							</tr>
						</thead>

						<tbody>
						<?php foreach($students AS $k=>$v) : ?>
							<tr>
								<td><?=$v->display_name;?></td>
								<td><?=$v->user_email;?></td>
								<td><?=isset($v->attendance_days[$day_id]) ? ($v->attendance_days[$day_id]->attended ? 'Aanwezig' : 'Afwezig') : 'Onbekend'; ?></td>
								<td><?=isset($v->attendance_days[$day_id]) ? $v->attendance_days[$day_id]->remarks : 'Onbekend'; ?></td>
								<td>
									<button class="btn btn-link" data-toggle="modal" data-target="#modal-edit-attendance-<?=$day_id;?>">
										<span class="glyphicon glyphicon-edit"></span>
									</button>
									&nbsp;
							
									<div class="modal fade" id="modal-edit-attendance-<?=$day_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-80p">
											<div class="modal-content">
												<!-- Modal Header -->
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														<span aria-hidden="true">×</span>
														<span class="sr-only">Sluiten</span>
													</button>
													<h4 class="modal-title" id="modules">
														Aanwezigheid voor de groep <?= $instance->group ?> op <?= $datum; ?>
													</h4>
												</div>
										
												<!-- Modal Body -->
												<div class="modal-body" id="day-attendance-<?=$day_id;?>">
													<form id="enrolment-form-<?=$day_id ?>" class="form-horizontal edit-module" method="POST">
														<div class="table-wrapper">
															<table id="bivt-table">
																<tr>
																	<th>Student</th>
																	<th>Aanwezigheid</th>
																	<th>Opmerkingen</th>
																</tr>
																<?php foreach ($students AS $ks=>$vs) :
																?>
																<tr>															
																	<td>
																		<input id="teacher-id" name="teacher_id" type="hidden"  value="<?=$instance->docent_id;?>">
																		<input id="days-id" name="days_id" type="hidden"  value="<?=$day_id;?>">
																		<input id="student-id" name="student_id" type="hidden"  value="<?=$vs->ID;?>">
																		<input id="record-id" name="record_id" type="hidden"  value="<?=isset($vs->attendance_days[$day_id]) ? $vs->attendance_days[$day_id]->id : -1;?>">
																		<?=$vs->display_name;?>
																	</td>
																	<td>
																		<div>
																			<select class="form-control" name="attendance" id="attendance">
																				<option value="present" <?= isset($vs->attendance_days[$day_id]) ? ($vs->attendance_days[$day_id]->attended ? 'selected="selected"' : '' ): '';?>>Aanwezig</option>
																				<option value="absent" <?= isset($vs->attendance_days[$day_id]) ? ($vs->attendance_days[$day_id]->attended ? '' : 'selected="selected"' ): '';?> >Afwezig</option>
																			</select>
																		</div>
																	</td>
																	<td>
																		<input id="remarks" name="remarks" class="form-control" type="text"  value="<?=isset($vs->attendance_days[$day_id]) ? $vs->attendance_days[$day_id]->remarks : ''; ?>">
																	</td>
																</tr>
																<?php endforeach; ?>											
															</table>
														</div>
														<!-- Modal Footer -->
														<div class="modal-footer">
															<button state=0 type="button" days_id="<?= $day_id; ?>" 
																	transaction="edit" table="day_attendance"
																	class="btn btn-default transaction-button">
																Wijzigingen opslaan
															</button>
														</div><!-- End Modal Footer -->
													</form>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>			
			</div>
		</div>
	</div>
</div>			
