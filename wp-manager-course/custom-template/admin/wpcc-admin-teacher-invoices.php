<?php

class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=teacher_invoices&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=student_overview&sort='.$item;
		$generateLink = '/admin/?action=teachers_invoices&sort='.$item;
		if (isset($_GET['pagenum'])) {
			$generateLink .= '&pagenum='.$_GET['pagenum'];
		}

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}
}

global $wpdb;

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$num_of_pages = ceil( $wpdb->get_var("SELECT COUNT(id) FROM docent_invoices") / $limit );


$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$base_query = "SELECT display_name AS teacher_name, docent_invoices.docent_invoice_reference AS invoice_ref, docent_invoices.amount AS invoice_amount modules.name AS course, instances.group AS group_name, days.datum as course_day FROM docent_invoices LEFT JOIN docents_days on docent_invoices.docents_days_id = docents_days.id LEFT JOIN days ON days.id = docents_days.day_id LEFT JOIN instances ON instances.id = days.instance_id LEFT JOIN modules ON modules.id=instances.module_id LEFT JOIN wp_users on wp_users.ID = docents_days.docent_id";

if($sort!='') {
	$base_query .= " ORDER BY $sort $order";
}
$base_query .= " LIMIT $offset,$limit";

$results = $wpdb->get_results($base_query);

if (sizeof($results) < 1) {
	$element = [
				"teacher_name" => "Een geweldige docent",
				"invoice_ref" => "201700005",
				"invoice_amount" => "999,99",
				"course" => "Goede scholing",
				"group_name" => "clubje",
				"course_day" => "2016-12-25",
	];
	$results["dummy"] =(object)$element;
}
get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">

		<h3>Overzicht docent facturen</h3>

		<div class="col-md-2 pull-right">
			<div>
				<form role="search" method="get" id="searchform"
						class="searchform" action="">
					<div>
						<label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
						<input type="text" value="" name="s" id="s" />
						<input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Zoeken', 'submit button' ); ?>" />
					</div>
				</form>
			</div>
		</div>
		
		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th>Docent</th>
					<th>Factuurnummer</th>	
					<th>Factuurbedrag</th>	
					<th>Scholing</th>
					<th>Groep</th>
					<th>Lesdag</th>
					<th>Factuur</th>
					<th>&nbsp;</th>
				</tr>
				<?php 
					foreach ($results as $k => $v) {
				?>

				<tr>
					<td><?=$v->teacher_name?></td>
					<td><?=$v->invoice_ref?></td>	
					<td><?=$v->invoice_amount?></td>	
					<td><?=$v->course?></td>
					<td><?=$v->group_name?></td>
					<td><?=$v->course_day?></td>
					<td><i>link to PDF</i></td>
					<td>
						<button class="btn btn-link view-news" data-toggle="modal">
							<a href="#">
								<span class="glyphicon glyphicon-search"></span>
							</a>
						</button>						

						<button class="btn btn-link" data-toggle="modal" data-target="#edit_news<?= $v->id ?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>

						<button class="btn btn-link">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>

				<?php
					} // end foreach
				?>				
			</table>
			
				<?php

					$page_links = paginate_links( array(
			            'base' => add_query_arg( 'pagenum', '%#%' ),
			            'format' => '',
			            'current' =>$pagenum,
			            'total' => $num_of_pages,
			            'type'  => 'array',
			            'prev_next'   => TRUE,
						'prev_text'    => __('Previous'),
						'next_text'    => __('Next'),
	        		));

					$pattern = '/current/';

					if( is_array( $page_links ) ) {

			            echo '<ul class="pagination">';
			            foreach ( $page_links as $page_link ) {
			            	if(preg_match($pattern, $page_link,$matches)){
			            		echo "<li class='active'>$page_link</li>";
							}else{
								echo "<li>$page_link</li>";
							}
			            }
			           echo '</ul>';
	        		}
			
					
				?>
			
			
			<div>
				<button state=0 type="button" transaction="add" table="" class="btn btn-default transaction-button">
					Factuur toevoegen
				</button>		
			</div>
		</div>
	</div>
</div>