<?php
	include (plugin_dir_path (__FILE__ ) . '../../classes/TableSorter.php');

	use \wp_manager_course\TableSorter;
	
	global $wpdb;

	// View is used in 'view', 'edit', and 'add' mode
	const MODE_VIEW = 1;
	const MODE_EDIT = 2;
	const MODE_ADD = 3;
	
	$mode = MODE_VIEW;
	$mode = isset($_GET['edit']) ? ($_GET['edit'] == 'true' ? MODE_EDIT : $mode): $mode;
	$mode = isset($_GET['add']) ? ($_GET['add'] == 'true' ? MODE_ADD : $mode): $mode;
	
	$instance_id = (MODE_ADD != $mode) ? ($instance_id = isset($_GET['instance']) ? $_GET['instance'] : 0) : 0;	

	if ((MODE_ADD != $mode) && (null == $instance_id)) {
		// Cannot display without having a specific instance ID while not in 'add' mode
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
		die();
	}

	// Initialize $instance for convenience
	$instance = [
			'id' => -1,
			'comments' => '',
			'group' => '',
			'full' => 0,
			'status' => 'open',
			'group_size' => 0,
	];
	
	// Get the current instace and all related days with their location
	$query = "SELECT instances.id, instances.comments, instances.group, instances.full, instances.status, instances.group_size, days.datum, days.id AS day_id, locations.id AS location_id, locations.name AS location, modules.id AS module_id, modules.name AS module_name, docents.user_id AS docent_user_id, docents.id AS docent_id, docents.name AS docent_name FROM `instances` LEFT JOIN modules ON modules.id=instances.module_id LEFT JOIN days ON days.instance_id=instances.id LEFT JOIN locations ON locations.id=days.location_id LEFT JOIN docents_days ON docents_days.day_id = days.id LEFT JOIN docents ON docents.id = docents_days.docent_id WHERE instances.id='".$instance_id."'";
	$tableSorter = new TableSorter ($query, TableSorter::SORT_3);
	$data = $tableSorter->getData();

	if ((MODE_ADD != $mode) && (null == $data || sizeof($data) < 1)) {
		// Did not retrieve a single result. Let's quit while not in 'add' mode
		die();
		bivt_redirect(get_home_url() . '/admin/?action=planning_instances');
	} else {
		// instance info is same for all records retrieved.
		$instance = $data[0];
	}
	
	// Retreive all locations used in modal dialog
	function getLocations() {
		global $wpdb;
		
		$query = "SELECT id, name FROM locations";
		return $wpdb->get_results($query);
	}
	
	function getDocents($module_id) {
		global $wpdb;
		
		$query = "SELECT docents.id, docents.user_id, docents.name FROM docents LEFT JOIN docents_modules ON docents_modules.docent_id = docents.id LEFT JOIN modules ON modules.id = docents_modules.module_id WHERE modules.id = '". $module_id ."'";
		return $wpdb->get_results($query);
	}
	
	// Retrieve modules for add operation, otherwise only related module
	$query = "SELECT modules.id, modules.name FROM modules";
	if (MODE_ADD != $mode) {
		$query .= " WHERE modules.id = '" . $instance->module_id . "'";
	}
	$modules = $wpdb->get_results($query); 
	
	$dialog_title = (MODE_ADD != $mode) ? 'Groep: ' . $instance->group : 'Nieuwe groep';
	$read_only = (MODE_VIEW == $mode) ? 'readonly="readonly"' : '';
	$number_enroled = (MODE_ADD != $mode) ? sizeof (get_students_by_instance_id($instance_id)) : 0;
	$button_action = (MODE_ADD == $mode) ? 'add' : 'edit';
	$status_values = ['open'=>'open', 'close'=>'gesloten'];
	$full_values = [0=>'beschikbaar', 1=>'vol'];
?>

<div class="wrap">

	<?php include_once 'wpcc-admin-header.php'; ?>
	
	<div class="container" id="<?=(MODE_ADD == $mode) ? 'add-instance' : 'edit-instance-' .$instance->id?>">
	
		<h3><?=$dialog_title;?>
		</h3>
		
		<form method="GET" action="">
		
			<input type="hidden" value="<?=$instance->id; ?>" name="instance_id" id="instance-id"/>
		
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="module">Module</label>
						<div class="col-sm-4">
							<select class="form-control" name="module_id" id="module" <?=$readonly;?>>
								<?php foreach ($modules AS $k_modules=>$module) : ?>
									<?php $selected = (($module->id == $instance->module_id) ? 'selected="selected"' : '');?>
									<option <?=$selected;?> value="<?=$module->id?>"><?=$module->name;?></option>
								<?php endforeach; ?>
							</select>
						
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="group">Groep</label>
						<div class="col-sm-4">
							<input type="text" value="<?=$instance->group; ?>" <?=$readonly;?>
								class="form-control required" name="group" id="group"
								placeholder="Groep" required />
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="status">Status</label>
						<div class="col-sm-4">
							<select class="form-control" name="status" id="status" <?=$readonly;?>>
								<?php foreach ($status_values AS $state_key=>$state_value) : ?>
									<?php if ((MODE_VIEW != $mode) || ($state_key == $instance->status)) : ?>
										<?php $selected = (($state_key == $instance->status) ? 'selected="selected"' : '');?>
										<option <?=$selected;?> value="<?=$state_key?>"><?=$state_value;?></option>
									<?php endif;?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="full">Vol</label>
						<div class="col-sm-4">
							<select class="form-control" name="full" id="full" <?=$readonly;?>>
								<?php foreach ($full_values AS $state_key=>$state_value) : ?>
									<?php if ((MODE_VIEW != $mode) || ($state_key == $instance->full)) : ?>
									<?php $selected = (($state_key == $instance->full) ? 'selected="selected"' : '');?>
										<option <?=$selected;?> value="<?=$state_key?>"><?=$state_value;?></option>
									<?php endif;?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="seats_available">Aantal plaatsen</label>
						<div class="col-sm-4">
							<input type="number" value="<?=$instance->group_size; ?>" <?=$readonly;?>
								class="form-control required" name="seats_available" id="seats-available"
								placeholder="1" required />
						</div>
					</div>
				</div>
			</div>

			<?php if (MODE_ADD != $mode) : ?>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="seats">Aantal ingeschreven</label>
						<div class="col-sm-4">
							<input type="number" value="<?=$number_enroled; ?>" <?=$readonly;?>
								class="form-control required" name="seats" id="seats"
								placeholder="1" required />
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			
		</form>

		<?php if(MODE_VIEW != $mode) :?>
		<div class="row">
			<button state=0 type="button" rowid="<?= $instance_id ?>" transaction="<?=$button_action;?>" table="instance" class="btn btn-default transaction-button">
				Groep opsplaan
			</button>
		</div>		
		<?php endif; ?>
		
					
		<?php if(MODE_ADD != $mode) : ?>
		<div class="related row">
			<div class="col-md-12">
				<h3>Opleidingsdagen</h3>
				
				<div class="row">
					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-add-day"> 
						<span class="glyphicon glyphicon-plus"></span>
						Opleidingsdag toevoegen
					</button>
				</div>		
				
				
				<div class="table-wrapper">
					<table id="bivt-table">
						<thead>
							<tr>
								<th>Dag</th>
								<th><?= (null != $tableSorter) ? $tableSorter->getSortLink('datum', 'Datum') : 'Datum';?></th>
								<th><?= (null != $tableSorter) ? $tableSorter->getSortLink('location', 'Locatie') : 'Locatie';?></th>
								<th><?= (null != $tableSorter) ? $tableSorter->getSortLink('docent_name', 'Docent') : 'Docent';?></th>
								<th>Aantal studenten</th>
								<th class="actions"></th>
							</tr>
						</thead>

						<tbody>
						<?php foreach($data AS $k=>$v) : ?>
						<?php if (isset($v->datum)) : ?>
						<tr>
								<td><?=$k+1;?></td>
								<td><?=date('d-m-Y', strtotime($v->datum));?></td>
								<td><a href="<?=get_home_url() . '/locatie/?locatie='. $v->location_id;?>"><?=$v->location;?></a></td>
								<td><a href="<?='?action=user_profile&user_id=' . $v->docent_user_id;?>"><?=$v->docent_name;?></a></td>
								<td><?=sizeof(get_students_by_day_id($v->day_id));?></td>
								<td>
								
									<a href="?action=enrolment&instance_id=<?=$instance_id;?>&day_id=<?=$v->day_id;?>">
										<button class="btn btn-link view-news">
											<span class="glyphicon glyphicon-search"></span>
										</button>
									</a>

									<button class="btn btn-link" data-toggle="modal" data-target="#edit-day-<?=$v->day_id;?>">
										<span class="glyphicon glyphicon-edit"></span>
									</button>
									
									<a href="#" class="transaction-button" rowid="" state=0 table="" transaction="delete">
										<button class="btn btn-link">
											<span class="glyphicon glyphicon-trash"></span>
										</button>
									</a>
							
									<!-- Create modal dialog for editing day -->
									<div class="modal fade" style="margin-top:65px;" id="edit-day-<?= $v->day_id; ?>" tabindex="-1" role="dialog" aria-labelledby="edit_days_<?= $v->day_id; ?>" aria-hidden="true">
									    <div class="modal-dialog modal-lg modal-80p">
									        <div class="modal-content">
									            <!-- Modal Header -->
									            <div class="modal-header">
									                <button type="button" class="close" data-dismiss="modal">
						    		                   <span aria-hidden="true">×</span>
						        		               <span class="sr-only">Close</span>
							        		        </button>
							            		    <h4 class="modal-title" id="title-edit-days-<?= $v->day_id; ?>">
							                		    Opleidingsdag
								                	</h4>
									            </div>
									            <!-- Modal Body -->
									            <div class="modal-body" id="edit-day-<?= $v->day_id; ?>">
												    <form class="form-horizontal" method="POST">
												    	<input type="hidden" name="day_id" id="day_id" value="<?= $v->day_id; ?>" />
												    	<input type="hidden" name="instance_id" id="instance_id" value="<?=$instance_id; ?>" />
	
												        <div class="form-group form-group-sm">
										                    <label for="datum" class="col-sm-2 control-label">Datum</label>
										                    <div class="col-sm-10">
										                        <input type="text" class="form-control required date-type" name="datum" id="datum" placeholder="Datum" required value="<?= date('d-m-Y', strtotime($v->datum)); ?>"/>                      
										                    </div>
										                </div>
		
										                <div class="form-group form-group-sm">
										                    <label for="location_id" class="col-sm-2 control-label">Locatie</label>
										                    <div class="col-sm-10">
										                    	<select id="location_id" name="location_id" class="form-control">
										              	      	<?php foreach (getLocations() as $key => $location) : ?>
										                    	<option value="<?= $location->id; ?>" <?php if($location->id==$v->location_id) : ?>selected="selected"<?php endif; ?>><?= $location->name; ?></option>
										                    	<?php endforeach; ?>
								                		    	</select>        
								                    		</div>              
										                </div>
									                
										                <div class="form-group form-group-sm">
										                    <label for="docent_id" class="col-sm-2 control-label">Docent</label>
										                    <div class="col-sm-10">
										                    	<select id="docent_id" name="docent_id" class="form-control">
										                    	<?php foreach (getDocents($instance->module_id) as $key => $docent) : ?>
										                    	<option value="<?= $docent->id; ?>" <?php if($docent->id==$v->docent_id) : ?>selected="selected"<?php endif; ?>><?= $docent->name; ?></option>
									    	                	<?php endforeach; ?>
								                	    		</select>
								                    		</div>			                    
									                	</div>
								                
												      	<!-- Modal Footer -->
											            <div class="modal-footer">
											                <button state=0 type="button" rowid="<?= $v->day_id; ?>" transaction="edit" table="related_day" class="btn btn-default transaction-button">Wijzigingen opslaan</button>
											            </div><!-- End Modal Footer -->
												    </form>
												</div> <!-- End modal body div -->
											</div> <!-- End modal content div -->
										</div> <!-- End modal dialog div -->
									</div> <!-- End modal div -->

								</td>							
							</tr>
						<?php endif; ?>
						<?php endforeach;?>
						</tbody>
					</table>
				</div>
				
				
				
				<div class="modal fade" style="margin-top:65px;" id="modal-add-day" tabindex="-1" role="dialog" aria-labelledby="days" aria-hidden="true">
				    <div class="modal-dialog modal-lg modal-80p">
				        <div class="modal-content">
				            <!-- Modal Header -->
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">
			                       <span aria-hidden="true">×</span>
			                       <span class="sr-only">Close</span>
				                </button>
				                <h4 class="modal-title" id="days">
				                    Opleidingsdag
				                </h4>
				            </div>
				            <!-- Modal Body -->
				            <div class="modal-body">
							    <form id="add-day" class="form-horizontal" method="POST">
							    	<input type="hidden" name="instance_id" id="instance-id" value="<?= $instance->id; ?>" />
							        <div class="form-group form-group-sm">
					                    <label for="datum" class="col-sm-2 control-label">Datum</label>
					                    <div class="col-sm-10">
					                        <input type="text" class="form-control required date-type" name="datum-add" id="datum-add" placeholder="Datum" required value=""/>			                        
					                    </div>
					                </div>
					                <div class="form-group form-group-sm">
					                    <label for="location_id" class="col-sm-2 control-label">Locatie</label>
					                    <div class="col-sm-10">
					                    	<select id="location_id" name="location_id" class="form-control">
					                    	<?php foreach (getLocations() as $key_location => $location) : ?>
						                    	<option value="<?= $location->id; ?>"><?= $location->name; ?></option>
					                    	<?php endforeach; ?>
					                    	</select>             
					                    </div>
					                </div>
					                <div class="form-group form-group-sm">
					                    <label for="docent_id" class="col-sm-2 control-label">Docent</label>
					                    <div class="col-sm-10">
					                    	<select id="docent-id" name="docent_id" class="form-control">
					                    	<?php foreach (getDocents($instance->module_id) as $key => $docent) : ?>
						                    	<option value="<?= $docent->id; ?>"><?= $docent->name; ?></option>
					                    	<?php endforeach; ?>
					                    	</select>
					                    </div>			                    
					                </div>
							      	<!-- Modal Footer -->
						            <div class="modal-footer">
						                <button state=0 type="button" transaction="add" table="related_day" class="btn btn-default transaction-button">Voeg dag toe</button>
						            </div><!-- End Modal Footer -->
							    </form>
							</div> <!-- End modal body div -->
						</div> <!-- End modal content div -->
					</div> <!-- End modal dialog div -->
				</div> <!-- End modal div -->
					
					
					
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>			
