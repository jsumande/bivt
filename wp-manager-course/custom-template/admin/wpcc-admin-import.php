<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<input type="file" class="form-control" id="importXls" name="xls" accept=".xls,xlsx">
			</div>
			<div class="col-md-4">
				<button state="0" type="button" transaction="importXls" table="import" class="btn btn-default transaction-button" >Import</button>
			</div>
		</div>
	</div>

</div>