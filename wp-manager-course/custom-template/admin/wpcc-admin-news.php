<?php

global $wpdb;

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "SELECT COUNT(`id`) FROM newsitems" );
$num_of_pages = ceil( $total / $limit );

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
$order = (isset($_GET['order'])) ? $_GET['order'] : '';


if($sort=='')
{
	$result = $wpdb->get_results("SELECT * FROM newsitems LIMIT $offset, $limit");
}
else
{
	$result = $wpdb->get_results("SELECT * FROM newsitems ORDER BY $sort $order LIMIT $offset, $limit");
}

class CustomSorter {

	public static function sort($item,$title) {


		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=news&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=news&sort='.$item;


		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}

if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}


get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Nieuwsberichten</h3>

		<div class="row">
			<div class="col-md-8"></div>
				<div class="col-md-4" style="padding-right:0px;">
					<form method="GET" action="">
						<input type="hidden" name="action" value="locs_menu" />
					    <div class="input-group">
					        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoek..." />
					        <span class="input-group-btn">
					            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
					                <i class="fa fa-search"></i>
					            </button>
					        </span>
				      	</div>
			      	</form>
		      	</div>
	      	</div>

		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th><?= CustomSorter::sort('id','ID') ?></th>
					<th><?= CustomSorter::sort('title','Titel') ?></th>
					<th>Auteur</th>
					<th>Laatste wijziging</th>
					<th>Datum</th>
					<th>&nbsp;</th>
				</tr>
				<?php 
					foreach ($result as $k => $v):
				?>

				<tr id="news-id-<?= $v->id; ?>">
					<td><?= $v->id ?></td>
					<td><?= $v->title ?></td>
					<td><?= $v->author ?></td>
					<td><?= $v->last_modifier ?></td>
					<td><?= $v->date ?></td>
					<td class="action">
						<a href="<?php echo get_site_url() . '/admin/?action=news&newsitems=' . $v->slug ?>">
							<button class="btn btn-link view-news" data-toggle="modal">							
								<span class="glyphicon glyphicon-search"></span>
							</button>	
						</a>					

						<button class="btn btn-link edit-news" data-toggle="modal" data-target="#edit_news<?= $v->id ?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>

						<!-- Edit Courses-->					
						<div class="modal fade" id="edit_news<?= $v->id ?>" tabindex="-1" role="dialog" aria-labelledby="news" aria-hidden="true">
						    <div class="modal-dialog modal-lg modal-80p">
						        <div class="modal-content">
						            <!-- Modal Header -->
						            <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal">
						                       <span aria-hidden="true">×</span>
						                       <span class="sr-only">Close</span>
						                </button>
						                <h4 class="modal-title" id="courses">
						                    News
						                </h4>
						            </div>
						            
						            <!-- Modal Body -->
						            <div class="modal-body">
									    <form id="EditNews<?= $v->id ?>" class="form-horizontal edit-news" method="POST" edit-row="<?= $v->id ?>">
											<div class="form-group form-group-sm">
												<!-- left column -->
												<div class="col-sm-10">
													<div class="form-group">
														<label  class="col-sm-2 control-label" for="title">Title</label>
														<div class="col-sm-10">							                    		
															<input type="text" class="form-control" name="title" id="title" value="<?= $v->title ?>"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="content" >Content</label>
														<div class="col-sm-10">
															<textarea class="form-control mceEditor" rows="6" name="content" id="content" ><?= $v->content ?></textarea>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="picture_preview" >Current Picture</label>
														<div class="col-sm-4">	
															<div class="x-close removeTablePicture" table="newsitems" requestid="<?= $v->id; ?>" state=0></div>
															<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
															<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive newsPicture specialPicture" />
															<?php else : ?>
															No Image found.
															<?php endif; ?>
														</div>
													</div>
													<div class="form-group" style="display:none;">
														<label class="col-sm-2 control-label" for="picture_preview_new" >Picture Preview</label>
														<div class="col-sm-4">	
															<img class="picture_preview_new img-responsive"/>
														</div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="picture" >Picture</label>
														<div class="col-sm-10">
															<input type="file" newsid="<?= $v->id; ?>" class="form-control required" name="picture" id="picture" required/>
														</div>
													</div>
												</div>
											</div>
										  <!-- Modal Footer -->
											<div class="modal-footer">
												<button state=0 type="button" newsid="<?= $v->id; ?>" class="btn btn-default EditNewsSubmit">Save Changes</button>
											</div><!-- End Modal Footer -->
										  
										</form>
									</div> <!-- End modal body div -->
								</div> <!-- End modal content div -->
							</div> <!-- End modal dialog div -->
						</div> <!-- End modal div -->

						<button class="btn btn-link delete-news" delete-row="<?= $v->id ?>">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<?php

				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
						//echo "<li>$page_link</li>";
		            }
		           echo '</ul>';
        		}
			?>
			<div class="bivt_add_container">
				<?= do_shortcode( "[bivt-news]"); ?>
			</div>			
		</div>
	</div>	
</div>