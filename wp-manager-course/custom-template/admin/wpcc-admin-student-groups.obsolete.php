<?php

global $wpdb;


//start
$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
$limit = 10; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;

$total = $wpdb->get_var( "SELECT COUNT(instances.id) FROM instances");

$num_of_pages = ceil( $total / $limit );


$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';

$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$base_query = "SELECT ins.*, mods.name as name,mods.id AS mods_id, days.datum, days.id AS days_id, locations.name AS location FROM instances as ins LEFT JOIN modules AS mods ON mods.id = ins.module_id LEFT JOIN days ON days.instance_id = ins.id LEFT JOIN locations ON days.location_id=locations.id GROUP BY ins.id";

if($sort=='')
{
	$results = $wpdb->get_results($base_query . " ORDER BY ins.modified DESC LIMIT $offset,$limit");
}
else
{
	$results = $wpdb->get_results($base_query . " ORDER BY $sort $order LIMIT $offset,$limit");
}

class CustomSorter {

	public static function sort($item,$title) {

		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=student-groups&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=instance_menu&sort='.$item;

		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>
	<div class="container">

		<h3>Groepen</h3>

		<div class="col-sm-9">
	  		<table id="bivt-table">
				<tr>
					<th>Groep</th>
					<th>Scholing</th>
					<th>Locatie</th>
					<th>Start datum</th>
					<th>Vol</th>
					<th>Status</th>
					<th>Plaatsen</th>
					<th>Inschrijvingen</th>
					<th>Opmerkingen</th>
					<th>&nbsp;</th>
				</tr>
				<?php 
					foreach ($results as $k => $v):
				?>
				<tr id="instance-<?= $v->id; ?>">
					<td><?= $v->group ?></td>
					<td><?= $v->name ?></td>
					<td><?= $v->location ?></td>
					<td><?= $v->datum ?></td>
					<td><?= $v->full > 0 ? 'Ja' : 'Nee' ?></td>
					<td><?= $v->status == 'open' ? 'open' : 'gesloten' ?></td>
					<td><?= $v->group_size ?></td>
					<td><?= $wpdb->get_var("SELECT COUNT(meta_id) FROM wp_woocommerce_order_itemmeta WHERE meta_key = '_days_instance' AND meta_value=$v->days_id") ?></td>
					<td><?= isset($v->comments) ? substr($v->comments,0, 20) : '' ?>
					<td>
						<button class="btn btn-link view-news">
							<a href="?action=instance_menu&instance=<?= $v->id ?>">
								<span class="glyphicon glyphicon-search"></span>
							</a>
						</button>

						<button class="btn btn-link" data-toggle="modal" data-target="#edit_instance-<?= $v->id ?>">
							<span class="glyphicon glyphicon-edit"></span>
						</button>

						<button class="btn btn-link">
							<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="instance" transaction="delete">
								<span class="glyphicon glyphicon-education"></span>
							</a>
						</button>

						<button class="btn btn-link">
							<a href="#" class="transaction-button" rowid="<?= $v->id; ?>" state=0 table="instance" transaction="delete">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</button>
					</td>
					<div class="modal fade" style="margin-top:65px;" id="edit_instance-<?= $v->id; ?>" tabindex="-1" role="dialog" aria-labelledby="edit_instances_<?= $v->id; ?>" aria-hidden="true">
					    <div class="modal-dialog modal-lg modal-80p">
					        <div class="modal-content">
					            <!-- Modal Header -->
					            <div class="modal-header">
					                <button type="button" class="close" data-dismiss="modal">
				                       <span aria-hidden="true">×</span>
				                       <span class="sr-only">Close</span>
					                </button>
					                <h4 class="modal-title" id="edit_instance_<?= $v->id; ?>">
					                    Instance
					                </h4>
					            </div>
					            <!-- Modal Body -->
					            <div class="modal-body edit-instance" id="edit-instance-<?= $v->id; ?>">
								    <form class="form-horizontal" method="POST">
								    	<input type="hidden" name="instance_id" id="instance_id" value="<?= $v->id; ?>">
								    	<div class="form-group form-group-sm">
						                    <div class="row">
						                    	<label for="status" class="col-sm-2 control-label">Modules</label>
							                    <div class="col-sm-10">
							                    	<select id="module_id" name="module_id" class="form-control">
							                    	<?php foreach ($getModules as $key => $vs) : ?>
							                    	<option value="<?= $vs->id ?>" <?php if($vs->id==$v->mods_id) : ?>selected="selected"<?php endif; ?>><?= $vs->name; ?></option>
							                    	<?php endforeach; ?>
							                    	</select>
							                    </div>
					                    	</div>
						                </div>
						               	<div class="form-group form-group-sm">
						                    <div class="row">
							                    <label for="group" class="col-sm-2 control-label">Group</label>
							                    <div class="col-sm-10">
							                        <input type="text" class="form-control required" name="group" id="group" placeholder="Group" required value="<?= $v->group; ?>"/>                      
							                    </div>
						                	</div>
					                	</div>
					                	<div class="form-group form-group-sm">
						                    <div class="row">
							                    <label for="full" class="col-sm-2 control-label">Full</label>
							                    <div class="col-sm-10">
							                    	<?php $fulls = array(0=>'Not Full',1=>'Full'); ?>
							                    	<select id="full" name="full" class="form-control">
							                    	<?php foreach ($fulls as $key => $vs) : ?>
							                    	<option <?php if($key==$v->full) : ?>selected="selected"<?php endif; ?> value="<?= $key ?>"><?= $vs; ?></option>
							                    	<?php endforeach; ?>
							                    	</select>           
							                    </div>
						                	</div>
					                	</div>
					                	<div class="form-group form-group-sm">
						                    <div class="row">
							                    <label for="status" class="col-sm-2 control-label">Status</label>
							                    <div class="col-sm-10">
							                    	<?php
							                    		$statuses = array('open','close');
							                    	?>
							                    	<select id="status" name="status" class="form-control">
							                    	<?php foreach ($statuses as $key => $vs) : ?>
							                    	<option <?php if($vs==$v->status) : ?>selected="selected"<?php endif; ?> value="<?= $vs ?>"><?= ucfirst($vs); ?></option>
							                    	<?php endforeach; ?>
							                    	</select>           
							                    </div>
						                	</div>
					                	</div>
								      	<!-- Modal Footer -->
							            <div class="modal-footer">
							                <button state=0 type="button" rowid="<?= $v->id; ?>" transaction="edit" table="instance" class="btn btn-default transaction-button">Edit Instance</button>
							            </div><!-- End Modal Footer -->
								    </form>
								</div> <!-- End modal body div -->
							</div> <!-- End modal content div -->
						</div> <!-- End modal dialog div -->
					</div> <!-- End modal div -->
				</tr>

				<?php endforeach; ?>	
			<div class="modal fade" style="margin-top:65px;" id="add_instance" tabindex="-2" role="dialog" aria-labelledby="add_instance" aria-hidden="true">
				    <div class="modal-dialog modal-lg modal-80p">
				        <div class="modal-content">
				            <!-- Modal Header -->
				            <div class="modal-header">
				                <button type="button" class="close" data-dismiss="modal">
			                       <span aria-hidden="true">×</span>
			                       <span class="sr-only">Close</span>
				                </button>
				                <h4 class="modal-title">
				                    Instance
				                </h4>
				            </div>
				            <!-- Modal Body -->
				            <div class="modal-body add-instance" id="add-instance">
							    <form class="form-horizontal" method="POST">
					               	<div class="form-group form-group-sm">
										<div class="row">
						                    <label for="status" class="col-sm-2 control-label">Modules</label>
						                    <div class="col-sm-10">
						                    	<select id="module_id" name="module_id" class="form-control">
						                    	<?php foreach ($getModules as $key => $v) : ?>
						                    	<option value="<?= $v->id ?>"><?= $v->name; ?></option>
						                    	<?php endforeach; ?>
						                    	</select>
						                    </div>
					                    </div>                    
				                	</div>
				                	<div class="form-group form-group-sm">
					                    <div class="row">
						                    <label for="group" class="col-sm-2 control-label">Group</label>
						                    <div class="col-sm-10">
						                        <input type="text" class="form-control required" name="group" id="group" placeholder="Group" required value=""/>                      
						                    </div>
					                	</div>
					                </div>
					                <div class="form-group form-group-sm">
					                    <div class="row">
						                    <label for="full" class="col-sm-2 control-label">Full</label>
						                    <div class="col-sm-10">
						                    	<?php
						                    		$fulls = array(0=>'Not Full',1=>'Full');
						                    	?>
						                    	<select id="full" name="full" class="form-control">
						                    	<?php foreach ($fulls as $key => $v) : ?>
						                    	<option value="<?= $key ?>"><?= $v; ?></option>
						                    	<?php endforeach; ?>
						                    	</select>           
						                    </div>
					                	</div>
					                </div>
					                <div class="form-group form-group-sm">
					                    <div class="row">
						                    <label for="status" class="col-sm-2 control-label">Status</label>
						                    <div class="col-sm-10">
						                    	<?php
						                    		$statuses = array('open','close');
						                    	?>
						                    	<select id="status" name="status" class="form-control">
						                    	<?php foreach ($statuses as $key => $v) : ?>
						                    	<option value="<?= $v ?>"><?= ucfirst($v); ?></option>
						                    	<?php endforeach; ?>
						                    	</select>           
						                    </div>
					                	</div>
					                </div>
							      	<!-- Modal Footer -->
						            <div class="modal-footer">
						                <button state=0 type="button" transaction="add" table="instance" class="btn btn-default transaction-button">Add Instance</button>
						            </div><!-- End Modal Footer -->
							    </form>
							</div> <!-- End modal body div -->
						</div> <!-- End modal content div -->
					</div> <!-- End modal dialog div -->
				</div> <!-- End modal div -->			
			</table>
			<?php

				$page_links = paginate_links( array(
		            'base' => add_query_arg( 'pagenum', '%#%' ),
		            'format' => '',
		            'current' =>$pagenum,
		            'total' => $num_of_pages,
		            'type'  => 'array',
		            'prev_next'   => TRUE,
					'prev_text'    => __('Previous'),
					'next_text'    => __('Next'),
        		));

				$pattern = '/current/';

				if( is_array( $page_links ) ) {

		            echo '<ul class="pagination">';
		            foreach ( $page_links as $page_link ) {
		            	if(preg_match($pattern, $page_link,$matches)){
		            		echo "<li class='active'>$page_link</li>";
						}else{
							echo "<li>$page_link</li>";
						}
		            }
		           echo '</ul>';
        		}
			?>
		</div>
	</div>
</div>