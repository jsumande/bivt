<?php

global $wpdb;

$student_id = isset($_GET['id']) ? $_GET['id'] : get_current_user_id();
$base_query = "SELECT * FROM wp_users WHERE wp_users.id=$student_id";

$results = $wpdb->get_results($base_query);

get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Studenten Informatie</h3>
		<?php 
			foreach($results as $k=>$v) {
		?>
		Student: <?= $v->display_name ?>
		Id: <?= $v->ID ?>
		<?php 
			} // end foreach
		?>
		<h4>Financiën</h4>
		<button class="btn btn-link" data-toggle="modal" data-target="#edit_student-<?= $v->ID ?>">
			<span class="glyphicon glyphicon-edit"></span>
		</button>
		

		<h4>Portofilio</h4>      
		
		<h4>NAW gegevens</h4>
		
		<h4>Opmerkingen</h4>              
	</div>
	
	<div class="modal fade" style="margin-top:65px;" id="edit_student-<?= $v->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="edit_instances_<?= $v->id; ?>" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-80p">
			<div class="modal-content">
				<!-- Modal Header -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="edit_instance_<?= $v->ID; ?>">
					                    Gebruiker
					</h4>
					<?php include $path . '/custom-template/admin/wpcc-admin-students.php'?>
				</div>
				<!-- Modal Body -->
				<div class="modal-body edit-instance" id="edit-instance-<?= $v->id; ?>">
				</div> <!-- End modal body div -->
			</div> <!-- End modal content div -->
		</div> <!-- End modal dialog div -->
	</div> <!-- End modal div -->		
</div>
