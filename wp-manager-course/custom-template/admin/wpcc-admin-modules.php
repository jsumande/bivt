<?php

$id = get_current_user_id();
$searchLocation = '';
if (isset($_GET['search-location'])){
	$searchLocation = $_GET['search-location']; 
}
//start
global $wpdb;

$sort = (isset($_GET['sort'])) ? $_GET['sort']: '';
$order = (isset($_GET['order'])) ? $_GET['order'] : '';

$query = "SELECT modules.* FROM modules LEFT JOIN wp_posts ON wp_posts.ID = modules.post_id";
$query .= $sort=='' ? " ORDER BY modules.id DESC" : " ORDER BY $sort $order";
$results = $wpdb->get_results($query);

class CustomSorter {

	public static function sort($item,$title) {


		$generateLink = (isset($_GET['pagenum'])) ? '/admin/?action=courses_modules&sort='.$item.'&pagenum='.$_GET['pagenum'] : '/admin/?action=courses_modules&sort='.$item;


		$order = (isset($_GET['order']) && $_GET['order']=='ASC') ? 'DESC' : 'ASC';

		$generateLink .='&order='.$order;

		return '<a style="color:white !important;" href="'.get_site_url(null,$generateLink).'">'.$title.'</a>';

	}

}
$statuses = ['open'=>'Open','close'=>'Close'];
//end


if(!function_exists('get_home_path'))
{
	function get_home_path()
	{
		$home    = set_url_scheme( get_option( 'home' ), 'http' );
	    $siteurl = set_url_scheme( get_option( 'siteurl' ), 'http' );
	    if ( ! empty( $home ) && 0 !== strcasecmp( $home, $siteurl ) ) {
	        $wp_path_rel_to_home = str_ireplace( $home, '', $siteurl ); /* $siteurl - $home */
	        $pos = strripos( str_replace( '\\', '/', $_SERVER['SCRIPT_FILENAME'] ), trailingslashit( $wp_path_rel_to_home ) );
	        $home_path = substr( $_SERVER['SCRIPT_FILENAME'], 0, $pos );
	        $home_path = trailingslashit( $home_path );
	    } else {
	        $home_path = ABSPATH;
	    }
	 
	    return str_replace( '\\', '/', $home_path );
	}
}

$courses = $wpdb->get_results('SELECT id, name FROM courses');
$categories = $wpdb->get_results('SELECT id, name FROM categories');
	
?>

<div class="wrap">
	<?php include_once 'wpcc-admin-header.php'; ?>

	<div class="container">

		<h3>Modules</h3>

		<div class="row">
			<div class="col-md-2">
			    <?php if( isset($v->id) ) {
			    		unset($v->id); 
			    	}
			    ?>
				<?= do_shortcode( "[bivt-modules]" ); ?> <!--  Button "Module toevoegen" -->
			</div>
			
			<div class="col-md-6">
			</div>
			
			<div class="col-md-4" style="padding-right:0px;">
				<form method="GET" action="">
					<input type="hidden" name="action" value="courses_modules" />
					<div class="input-group">
				        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
				            </button>
						</span>
					</div>
				</form>
	      	</div>			
		</div>

		<div class="col-sm-12">
			<div class="table-wrapper">
				<table id="bivt-table">
					<tr>
						<th><?= CustomSorter::sort('post_id','Artikelnummer'); ?>
						<th><?= CustomSorter::sort('name','Naam'); ?></th>
						<th>Bijeenkomsten</th>
						<th>Sessies</th>
						<th>Prijs</th>
						<th>Status</th>
						<th>&nbsp;</th>
					</tr>
					<?php
						foreach ($results as $k => $v):
					?>					

					<tr>
						<td><?= isset($v->post_id) ? $v->post_id : ""; ?></td>
						<td><?= isset($v->name) ? $v->name : ""; ?></td>
						<td><?= isset($v->meeting_days) ? $v->meetings_days : ""; ?></td>
						<td><?= isset($v->sessions) ? $v->sessions : ""; ?></td>
						<td><?= isset($v->price) ? $v->price : ""; ?></td>
						<td><?= isset($v->status) ? $v->status : ""; ?></td>
						<td>
							<a href="<?php echo get_site_url() . '/news/?page=' . $v->slug .'&post_id='. $v->id?>">
								<button class="btn btn-link view-news" data-toggle="modal">								
									<span class="glyphicon glyphicon-search"></span>								
								</button>
							</a>		
											
							<button class="btn btn-link" data-toggle="modal" data-target="#edit_modules<?= $v->id ?>" onclick="$('#edit_modules<?= $v->id ?> .groups-edit-multiple').select2({ allowClear: true });$('#edit_modules<?= $v->id ?> .cat-edit-multiple').select2({ allowClear: true })">
								<span class="glyphicon glyphicon-edit"></span>
							</button>

							<div class="modal fade" id="edit_modules<?= $v->id ?>" tabindex="-1" role="dialog" aria-labelledby="modules" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-80p">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												   <span aria-hidden="true">×</span>
												   <span class="sr-only">Sluiten</span>
											</button>
											<h4 class="modal-title" id="modules">
												Module &mdash; <?= $v->name ?>										
											</h4>
										</div>
										<!-- Modal Body -->
										<div class="modal-body">
											<form id="EditModules<?= $v->id ?>" post-edit-id="<?= $v->post_id ?>" class="form-horizontal edit-module" edit-row="<?= $v->id ?>" method="POST">

												<div class="form-group form-group-sm">
													<!-- left column -->
													<div class="col-sm-6">
														<div class="form-group">
															<label  class="col-sm-2 control-label" for="introduction">Introductie</label>
															<div class="col-sm-10">
																<textarea class="form-control mceEditor required" rows="6" name="introduction" id="introduction" required><?= $v->introduction ?></textarea>
															</div>
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="name" >Naam</label>
															<div class="col-sm-10">
																<input type="text" class="form-control required" name="name" id="name" value="<?= $v->name ?>" required/>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-sm-2 control-label" for="description" >Omschrijving</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="description" id="description" required><?= $v->description ?></textarea>
															</div>
														</div>		                  	

														<div class="form-group">
															<label class="col-sm-2 control-label" for="conditions" >Voorwaarden</label>
															<div class="col-sm-10">
																<input type="text" class="form-control required" name="conditions" id="conditions" value="<?= $v->conditions ?>" required/>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-sm-2 control-label" for="programme" >Programma</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="programme" id="programme" required><?= $v->programme ?></textarea>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-sm-2 control-label" for="meta_description" >Meta omschrijving</label>
															<div class="col-sm-10">
																<textarea class="form-control required" rows="6" name="meta_description" id="meta_description" required><?= $v->meta_description ?></textarea>
															</div>
														</div>	

														<div class="form-group">
															<label class="col-sm-2 control-label" for="meta_keywords" >Meta Keywords</label>
															<div class="col-sm-10">
																<textarea class="form-control required" rows="6" name="meta_keywords" id="meta_keywords" required><?= $v->meta_keywords ?></textarea>
															</div>
														</div>
													   
													</div>

													<!-- right column -->
													<div class="col-sm-6">
														<div class="form-group">
															<label for="sessions" class="col-sm-2 control-label">Sessies</label>
															<div class="col-sm-10">
																<input type="text" class="form-control required" name="sessions" id="sessions" value="<?= $v->sessions ?>" required/>			                        
															</div>			                    
														</div>

														<div class="form-group">
															<label for="meeting_days" class="col-sm-2 control-label">Lesdagen</label>
															<div class="col-sm-10">
																<input type="text" class="form-control required" name="meeting_days" id="meeting_days" value="<?= $v->meeting_days ?>" required/>		                        
															</div>			                    
														</div>

														<div class="form-group">
															<label for="new_bol_require" class="col-sm-2 control-label">Prijs</label>
															<div class="col-sm-4">
																<input type="text" class="form-control required" name="price" id="price" value="<?= $v->price ?>" required/>	
															</div>
															<label for="new_pod_require" class="col-sm-2 control-label">SBU</label>
															<div class="col-sm-4">
																<input type="text" class="form-control required" name="sbu" id="sbu" value="<?= $v->SBU ?>" required/>	
															</div>
														</div>
														
														<div class="form-group">
															<label for="level" class="col-sm-2 control-label">Niveau</label>
															<div class="col-sm-4">
																<input type="text" class="form-control required" name="level" id="level" value="<?= $v->level ?>" required/>	
															</div>
															<label for="exam" class="col-sm-2 control-label">Examen</label>
															<div class="col-sm-4">
																<input type="text" class="form-control required" name="exam" id="exam" value="<?= $v->exam ?>" required/>	
															</div>
														</div>

														<div class="form-group">
															<label for="incasso" class="col-sm-2 control-label">Incasso</label>
															<div class="col-sm-10">
																<input type="text" class="form-control required" name="incasso" id="incasso" value="<?= $v->incasso ?>" required/>			                        
															</div>			                    
														</div>
														
														<div class="form-group">
															<label for="preliminary_knowledge" class="col-sm-2 control-label">Vereiste voorkennis</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="preliminary_knowledge" id="preliminary_knowledge" required><?= $v->preliminary_knowledge ?></textarea>			                        
															</div>			                    
														</div>

														<div class="form-group">
															<label class="col-sm-2 control-label" for="goal" >Doel</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="goal" id="goal" required><?= $v->goal ?></textarea>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-sm-2 control-label" for="target_audience" >Doelgroep</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="target_audience" id="target_audience" required><?= $v->target_audience ?></textarea>
															</div>
														</div>
														
														<div class="form-group">
															<label class="col-sm-2 control-label" for="literature" >Literatuur</label>
															<div class="col-sm-10">
																<textarea class="form-control required mceEditor" name="literature" id="literature" required><?= $v->literature ?></textarea>
															</div>
														</div>

														<div class="form-group">
															<label for="status" class="col-sm-2 control-label">Status</label>
															<div class="col-sm-4">
																<select class="form-control" name="status" id="status">
																	<option value="open" <?= $v->status == 'open' ? 'selected' : ''?>>Open</option>
																	<option value="close" <?= $v->status == 'open' ? '' : 'selected' ?> >Gesloten</option>
																</select>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="picture_preview" >Huidige afbeelding</label>
															<div class="col-sm-4">	
																<div class="x-close removeTablePicture" table="modules" requestid="<?= $v->id; ?>" state=0></div>
																<?php if($v->picture!='' && file_exists(get_home_path() . 'wp-content/plugins/wp-manager-course' . $v->picture)) : ?>
																<img src="<?= plugins_url('wp-manager-course'.$v->picture); ?>" class="img-responsive modulePicture specialPicture" />
																<?php else : ?>
																No Image found.
																<?php endif; ?>
															</div>
														</div>
														<div class="form-group" style="display:none;">
															<label class="col-sm-2 control-label" for="picture_preview_new" >Voorbeeld afbeelding</label>
															<div class="col-sm-4">	
																<img class="picture_preview_new img-responsive"/>
															</div>
														</div>
														<div class="form-group">
															<label class="col-sm-2 control-label" for="picture" >Afbeelding</label>
															<div class="col-sm-10">
																<input type="file" moduleid="<?= $v->id; ?>" class="form-control required" name="picture" id="picture" required/>
															</div>
														</div>
													<div class="form-group">
													    <label for="status" class="col-sm-2 control-label">Scholing</label>
													    <div class="col-sm-10">
															<select class="groups-edit-multiple" multiple="multiple" name="course[]" id="course">
															<?php 
															$courses_results = $wpdb->get_results( "SELECT c.id as id FROM courses_modules as cm LEFT JOIN courses c ON c.id = cm.course_id WHERE cm.module_id = '" . $v->id . "'");
															$courses_results_arry = array();
															
															foreach ($courses_results as $k => $val) {
																$courses_results_arry[] = $val->id;

															}

															foreach ($courses as $k => $val):
																	
																if ( in_array($val->id, $courses_results_arry) ) {
																	echo '<option selected value="'. $val->id. '"> '. $val->name. ' </option>';
																} else {
																	echo '<option value="'. $val->id. '"> '. $val->name. ' </option>';
																}	
															endforeach;

															?>
															</select>
													    </div>
													</div>
													<div class="form-group">
														<label class="col-sm-2 control-label" for="category" >Categorieën</label>
														<div class="col-sm-10">
															<select class="form-control cat-edit-multiple" multiple="multiple" name="category[]" id="category">
																<?php 
																$category_results = $wpdb->get_results( "SELECT c.id as id FROM categories_modules as cm LEFT JOIN categories c ON c.id = cm.category_id WHERE cm.module_id = '".$v->id."'");
																$category_results_arry = array();
																
																foreach ($category_results as $k => $val) {
																	$category_results_arry[] = $val->id;
																}
																foreach ($categories as $k => $val):
																	
																if ( in_array($val->id, $category_results_arry) ) {
																	echo '<option selected value="'. $val->id. '"> '. $val->name. ' </option>';
																} else {
																	echo '<option value="'. $val->id. '"> '. $val->name. ' </option>';
																}	
															endforeach;

																?>
															</select>
														</div>
													</div>	
													<div class="form-group">
														<label for="teacher" class="col-sm-3 control-label"> Docent</label>
														<div class="col-sm-9">
															<select class="form-control cat-edit-multiple" multiple="multiple" name="teacher" id="teacher">
															<?php
																$teachers = $wpdb->get_results ( "SELECT docents.id, docents.name, docents_modules.module_id AS mid FROM docents LEFT JOIN docents_modules ON docents.id = docents_modules.docent_id WHERE docents_modules.module_id='".$v->id."' ORDER BY name" );																
																foreach ( $teachers as $teacher ) {
																	$selected = ((null != $teacher->mid) && ($v->id==$teacher->mid)) ? 'selected="selected"' : '';
															?>
																	<option <?= $selected; ?> value="<?= $teacher->id; ?>">
																		<?= $teacher->name ?>
																	</option>
															<?php
															}
															?>
															</select>
														</div>
													</div>
													
													</div>
												</div>
											  <!-- Modal Footer -->

												<div class="modal-footer">
													<button id="EditModuleSubmit" moduleid="<?= $v->id; ?>" post-edit-id="<?= $v->post_id ?>" type="button" state=0 class="btn btn-default">Wijzigingen opslaan</button>
												</div><!-- End Modal Footer -->
											  
											</form>
										</div> <!-- End modal body div -->
									</div> <!-- End modal content div -->
								</div> <!-- End modal dialog div -->
							</div> <!-- End modal div -->

							<a href="<?php echo get_site_url() . '/admin/?action=accreditations&modules_id=' . $v->id ?>">
								<button class="btn btn-link view-news" data-toggle="modal">								
									<span class="glyphicon glyphicon-certificate"></span>								
								</button>
							</a>	
							
							<button state=0 class="btn btn-link delete-modules" delete-row="<?= $v->id; ?>">
								<span class="glyphicon glyphicon-trash"></span>
							</button>

							<a href="<?php echo get_site_url() . '/admin/?action=materials&module_id=' . $v->id;?>">
								<button class="btn btn-link">								
									<span class="glyphicon glyphicon-book"></span>								
								</button>
							</a>
					
							<a href="<?php echo get_site_url() . '/admin/?action=assignments&module_id=' . $v->id;?>">
								<button class="btn btn-link">								
									<span class="glyphicon glyphicon-pencil"></span>								
								</button>
							</a>
						</td>
					</tr>

					<?php endforeach; ?>				
				</table>
			</div>
		</div>
	</div>	
</div>
