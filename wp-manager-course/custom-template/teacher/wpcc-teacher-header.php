<?php 

$tab = (isset($_GET['action'])) ? $_GET['action'] : 'profile';

?>
<div class="row">
	<div class="col-sm-12">
		<div class="menu_wrapper">
			<form method="post" class="container">
				<ul id="menu-teacher-menu">
					<li>  
						<a href="<?= get_home_url(); ?>">BivT</a>
					</li>
					<li class="<?= $tab=='profile' ? 'active' : '' ?>">  
						<a href="teacher?action=profile">Mijn gegevens</a>
					</li>	
					<li class="<?= $tab=='courses' ? 'active' : '' ?>">  
						<a href="teacher?action=courses">Scholingen</a>
					</li>	
					<li class="<?= $tab=='schedule' ? 'active' : '' ?>">  
						<a href="teacher?action=schedule">Rooster</a>
					</li>	
					<li class="<?= $tab=='homework' ? 'active' : '' ?>">  
						<a href="teacher?action=homework">Openstaand huiswerk</a>
					</li>	
					<li class="">
						<a href="<?= wp_logout_url(home_url()) ?>">Logout</a>
					</li>					
				</ul>
			</form>
		</div>		
	</div>
</div>
