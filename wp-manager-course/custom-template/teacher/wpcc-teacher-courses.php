<?php
	global $wpdb;
	$id = get_current_user_id();


	$query_modules = "SELECT modules.name, modules.slug, modules.id AS module_id, wp_posts.id AS post_id, modules.introduction AS introduction, modules.meta_description AS meta_description, modules.meta_keywords AS meta_keywords, modules.conditions AS conditions, modules.description AS description, modules.meeting_days AS meeting_days, modules.sessions AS sessions, modules.price AS price, modules.status AS status, docents_modules.docent_id AS docent_id FROM docents_modules LEFT JOIN modules ON modules.id=docents_modules.module_id LEFT JOIN docents ON docents.id = docents_modules.docent_id LEFT JOIN wp_posts on modules.post_id = wp_posts.id WHERE docents.user_id = '" .$id ."' AND modules.id IS NOT NULL";
	$results_modules = $wpdb->get_results($query_modules);
	$query_courses = "SELECT courses.name, courses.slug, courses.id AS course_id, wp_posts.id AS post_id, courses.introduction AS introduction, courses.meta_description AS meta_description, courses.meta_keywords AS meta_keywords, courses.conditions AS conditions, courses.description AS description, courses.meeting_days AS meeting_days, courses.sessions AS sessions, courses.price AS price, courses.status AS status, docents_courses.docent_id AS docent_id FROM docents_courses LEFT JOIN courses ON courses.id=docents_courses.course_id LEFT JOIN docents ON docents.id = docents_courses.docent_id LEFT JOIN wp_posts on courses.post_id = wp_posts.id WHERE docents.user_id = '" .$id ."' AND courses.id IS NOT NULL";
	$results_courses = $wpdb->get_results($query_courses);
	
// 	echo "<h4>Query courses: " . $query_courses . "</h4>";
// 	echo "<h4>Results courses: " . json_encode($results_courses) . "</h4>";
// 	echo "<h4>Query modules: " . $query_modules . "</h4>";
// 	echo "<h4>Results modules: " . json_encode($results_modules) . "</h4>";
	
	$educations = [
			'courses' => (object)[
					'cat_name' => 'Opleidingen',
					'name' => 'Opleiding',
					'edit_link' => '#edit-course-',  // CHECK!!
					'materials_link' => '#',
					'assignments_link' => '#',
					'data' => $results_courses,
			],
			'modules' => (object)[
					'cat_name' => 'Modules',
					'name' => 'Module',
					'edit_link' => '#edit-module-', // CHECK!!
					'materials_link' => '#',
					'assignments_link' => '#',
					'data' => $results_modules,
			],
	];
	
	$icons = [
			'view' => 'glyphicon glyphicon-search',
			'edit' => 'glyphicon glyphicon-edit',
			'materials' => 'glyphicon glyphicon-book',
			'assignments' => 'glyphicon glyphicon-pencil',
	];
	
?>

<div class="wrap">
	<?php include_once 'wpcc-teacher-header.php'; ?>

	
	<div class="container">

		<h3>Opleidingen en Modules</h3>

		<div class="sm-col-12">
			<!-- Pagination is not relevant as a teacher won't have that many courses / modules -->
			<div class="col-md-4">
			<?php if( is_array( $page_links ) ) { ?>
				<ul class="pagination">					
				<?php foreach ( $page_links as $page_link ) { ?>
					<li class=<?= (preg_match($pattern, $page_link,$matches)) ? 'active' : ''?>><?=$page_link?></li>
				<?php } ?>
				</ul>
			<?php } ?> 
			</div>
				
			<!-- OK for now, to be done later -->
			<div class="col-md-4" style="padding-right:0px;">
				<form method="GET" action="">
					<input type="hidden" name="action" value="courses_courses" />
					<div class="input-group">
				        <input type="text" id="search-location" name="search-location" value="<?= $searchLocation; ?>" class="form-control" placeholder="Zoeken..." />
				        <span class="input-group-btn">
				            <button class="btn btn-default" type="submit" style="margin-top:-10px;">
				                <i class="fa fa-search"></i>
				            </button>
						</span>
					</div>
				</form>
	      	</div>
		</div>

		<?php
			foreach ($educations AS $education_key=>$education) {
		?>
		
		<div class="col-sm-12">
			<h5><?= $eduction->cat_name; ?></h5>
	  		<table id="bivt-table">
				<tr>
					<th>Naam</th>				<!-- Sortable -->
					<th>Contactdagen</th>
					<th>Sessies</th>
					<th>Prijs</th>
					<th>Status</th>					
					<th>&nbsp;</th>				<!-- Actions -->
				</tr>

				<?php
					foreach ($education->data AS $data) {
						$source = isset($data->module_id) ? 'modules' : isset($data->course_id) ? 'courses' : null;	
						$label = $source . '-' . $data->post_id;
					?>
				<tr>
					<td><?=isset($data->name) ? $data->name : "Onbekend";?></td>					
					<td><?=isset($data->meeting_days) ? $data->meeting_days : "Onbekend";?></td>					
					<td><?=isset($data->sessions) ? $data->sessions : "Onbekend";?></td>					
					<td><?=isset($data->price) ? $data->price : "Onbekend";?></td>					
					<td><?=isset($data->status) ? $data->status : "Onbekend";?></td>					
					<td class="action">
						<a href="<?php echo get_site_url() . '/news/?page=' . $data->slug .'&post_id='. $data->post_id?>">
							<button class="btn btn-link">								
								<span class="<?=$icons['view'];?>"></span>								
							</button>
						</a>						

						<button class="btn btn-link" data-toggle="modal" data-target="#<?=$label;?>">
							<span class="<?=$icons['edit'];?>"></span>
						</button>
						<?php
							$modal_data = $data;
							include( plugin_dir_path( __FILE__ ) . 'wpcc-teacher-courses-modal.php');
						?>

						<?php
							if(isset($data->module_id)) {
								// Only modules have materials and assignments
						?>
								<a href="<?php echo get_site_url() . '/teacher/?action=materials&module_id=' . $data->module_id . '&docent_id=' . $data->docent_id;?>">
									<button class="btn btn-link">								
										<span class="<?=$icons['materials'];?>"></span>								
									</button>
								</a>
						
								<a href="<?php echo get_site_url() . '/teacher/?action=assignments&module_id=' . $data->module_id . '&docent_id=' . $data->docent_id;?>">
									<button class="btn btn-link">								
										<span class="<?=$icons['assignments'];?>"></span>								
									</button>
								</a>
						<?php
							} // endif
						?>			
					</td>
				<?php 
					} // endforeach data
				?>				
				</tr>				
			</table>
		</div>
		<?php 
			} // endforeach education
		?>				
				
	</div>		
</div>
