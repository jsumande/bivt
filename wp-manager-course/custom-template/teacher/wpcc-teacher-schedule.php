<?php
	global $wpdb;
	$currentUserId = get_current_user_id();
	$query = "SELECT docents.user_id, docents.id AS docent_id, instances.id as group_id, instances.group, modules.name, days.datum, days.id AS days_id, locations.address, locations.name AS location, locations.id AS location_id FROM docents LEFT JOIN docents_days ON docents.id = docents_days.docent_id LEFT JOIN days ON docents_days.day_id = days.id LEFT JOIN instances ON instances.id = days.instance_id LEFT JOIN locations ON locations.id = days.location_id LEFT JOIN modules ON modules.id = instances.module_id WHERE docents.user_id = '" . $currentUserId . "' ORDER BY days.datum ASC";
	$results = $wpdb->get_results($query);
	
	usort($results, function($a, $b) {
		return strtotime($a->datum) - strtotime($b->datum);
	});
?>
<div class="wrap">
	<?php require 'wpcc-teacher-header.php'; ?>
	<div class="container">

		<h3>Rooster</h3>

		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th>Onderdeel nummer</th>		<!-- Group -->
					<th>Onderdeel</th>				<!-- Course title -->
					<th>Datum</th>					<!-- Date -->
					<th>Locatie</th>				<!-- Location -->
					<th>&nbsp;</th>					<!-- Icons -->
				</tr>
				<?php 
					foreach ($results as $k => $v):
				?>
					<tr>
						<td><?= isset($v->group) ? $v->group : '&nbsp;'; ?></td>
						<td><?= isset($v->name) ? $v->name : '&nbsp;'; ?></td>
						<td><?= isset($v->datum) ? date("d-m-Y", strtotime($v->datum)) : '&nbsp;'; ?></td>
						<td>
							<?php
								$location = isset($v->location) ? $v->location : 'Onbekend';
								if(isset($v->location_id)) {
									$location = '<a href="' . get_site_url() . '/locatie/?locatie=' . $v->location_id .'">' . $location . '</a>';
								}
							?>						
							<?= $location; ?>
						</td>
						<td>
							<button class="btn btn-link" data-toggle="modal" data-target="#enrolments-<?= $v->days_id ?>">
								<span class="glyphicon glyphicon-education"></span>&nbsp;&nbsp;&nbsp;
							</button>						
						
							<div class="modal fade" id="enrolments-<?= $v->days_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-lg modal-80p">
									<div class="modal-content">
										<!-- Modal Header -->
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">×</span>
												<span class="sr-only">Sluiten</span>
											</button>
											<h4 class="modal-title" id="modules">
												Aanwezigheid voor de groep <?= $v->group ?> op <?= date("d-m-Y", strtotime($v->datum)); ?>
											</h4>
										</div>
										
										<!-- Modal Body -->
										<div class="modal-body" id="day-attendance-<?=$v->days_id;?>">
											<form id="enrolment-form-<?=$v->days_id ?>" class="form-horizontal edit-module" method="POST">
												<div class="table-wrapper">
													<table id="bivt-table">
														<tr>
															<th>Student</th>
															<th>Aanwezigheid</th>
															<th>Opmerkingen</th>
														</tr>
														<?php
															$students = get_attendance_by_day_id($v->day_id);
														?>
														<?php foreach ($students AS $ks=>$vs) :?>
														<tr>															
															<td>
																<input id="teacher-id" name="teacher_id" type="hidden"  value="<?=$v->docent_id; ?>">
																<input id="days-id" name="days_id" type="hidden"  value="<?=$v->days_id; ?>">
																<input id="student-id" name="student_id" type="hidden"  value="<?=  $vs->ID; ?>">
																<input id="record-id" name="record_id" type="hidden"  value="<?=isset($vs->attendance_days[$v->days_id]) ? $vs->attendance_days[$v->days_id]->id : -1;?>">
																<?=$vs->display_name;?>
															</td>
															<td>
																<div>
																	<select class="form-control" name="attendance" id="attendance">
																		<option value="present" <?= isset($vs->attendance_days[$v->days_id]) ? ($vs->attendance_days[$v->days_id]->attended ? 'selected="selected"' : '' ): '';?>>Aanwezig</option>
																		<option value="absent" <?= isset($vs->attendance_days[$v->days_id]) ? ($vs->attendance_days[$v->days_id]->attended ? '' : 'selected="selected"' ): '';?> >Afwezig</option>
																	</select>
																</div>
															</td>
															<td>
																<input id="remarks" name="remarks" class="form-control" type="text"  value="<?=isset($vs->attendance_days[$v->days_id]) ? $vs->attendance_days[$v->days_id]->remarks : ''; ?>">
															</td>
														</tr>
														<?php endforeach; ?>											
													</table>
												</div>
												<!-- Modal Footer -->
												<div class="modal-footer">
													<button state=0 type="button" days_id="<?= $v->days_id; ?>" 
															transaction="edit" table="day_attendance"
															class="btn btn-default transaction-button">
														Wijzigingen opslaan
													</button>
												</div><!-- End Modal Footer -->
											  
											</form>
										</div> <!-- End modal body div -->
									</div> <!-- End modal content div -->
								</div> <!-- End modal dialog div -->
							</div> <!-- End modal div -->
														
						</td>
					</tr>
				<?php endforeach; ?>		
			</table>
		</div>
	</div>
</div>
