<?php 
	global $wpdb;
	
	$currentUserId = get_current_user_id();
	
	$query = "SELECT homework.*, instances.group, modules.name as module_name, assignments.text_subject AS subject, assignments.file AS assignment_file, docents.id AS teacher_id FROM `homework` LEFT JOIN days on days.id = homework.day_id LEFT JOIN docents_days ON docents_days.day_id = days.id LEFT JOIN docents ON docents.id = docents_days.docent_id LEFT JOIN instances ON instances.id = days.instance_id LEFT JOIN modules ON modules.id = instances.module_id LEFT JOIN assignments ON assignments.id = homework.assignment_id WHERE correct_on = '0000-00-00 00:00:00' AND docents.user_id='" . $currentUserId . "'";
	$data = $wpdb->get_results($query);
	
?>
<div class="wrap">
	<?php include_once 'wpcc-teacher-header.php'; ?>
	<div class="container">
		<h3>Openstaand huiswerk</h3>
		
		<div id="open-huiswerk">
			<div class="col-sm-12">
		  		<table id="bivt-table">
					<tr>
						<th>Onderdeel nummer</th>		<!-- Instance number for correspondence -->
						<th>Onderdeel</th>		 		<!-- Description -->
						<th>Student</th>				<!-- Name of student -->
						<th>Huiswerk</th>				<!-- Unchecked homework -->
					</tr>
					<tr>
						<?php
							foreach($data AS $k_row=>$v_row) {
								$subject = isset($v_row->subject) ? $v_row->subject : 'Onbekend';
								$subject = $subject.length > 30 ? substr($subject, 0, 30) . '...' : $subject;
								
								$user = get_user_by ('id', $v_row->user_id);
								$student_name = isset($user->first_name) ? $user->first_name : '' . '&nbsp;' . isset($user->last_name) ? $user->last_name : '';
								$student_name = $student_name.length > 1 ? $student_name : $user->user_nicename;
						?>
								<td><?= isset($v_row->group) ? $v_row->group : 'Onbekend';?></td>
								<td><?= isset($v_row->module_name) ? $v_row->module_name : 'Onbekend';?></td>
								<td><?= $student_name; ?></td>
								<td>
									<button class="btn btn-default" data-toggle="modal" data-target="#homework<?= $v_row->id; ?>">
				                       <?= $subject;?>
									</button>						
						
									<div class="modal fade" id="homework<?= $v_row->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
										<div class="modal-dialog modal-lg modal-80p">
											<div class="modal-content">
												<!-- Modal Header -->
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">
														<span aria-hidden="true">×</span>
														<span class="sr-only">Sluiten</span>
													</button>
													<h4 class="modal-title" id="modules">Huiswerk</h4>
													<h5>Module: <?= $v_row->module_name; ?></h5>
													<h5>Groep: <?= $v_row->group; ?></h5>
												</div>
										
										<!-- Modal Body -->
										<div class="modal-body" id="homework-body-<?=$v_row->id;?>">
											<form id="homework-form-<?=$v_row->id ?>" class="form-horizontal edit-module" method="POST">
												<input type="hidden" name="teacher_id" value="<?=$v_row->teacher_id;?>">
												<input type="hidden" name="row_id" value="<?=$v_row->id;?>">
												<div class="form-group">
													<label class="col-sm-3 control-label" for="assignment_text">Opdracht</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" readonly="readonly"
															name="assignment_text" id="assignment-text-<?=$v_row->id;?>"
															value="<?= $v_row->subject; ?>" />
													</div>
												</div>
												<div>
												<?php if($v_row->assignment_file) {
													$file = get_home_url() . '/wp-content/plugins/wp-manager-course';
													$file = $v_row->assignment_file ? $file . $v_row->assignment_file : '#';
												?>
													<a class="btn btn-default" href="<?= $file?>" target="_blank">
														<span class="<?= $icons["download"] ?>"></span>
														Download opdracht
													</a>
												<?php } // if file ?>		
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="student">Student</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" readonly="readonly"
															name="student" id="student-<?=$v_row->id;?>"
															value="<?= $student_name; ?>" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="assignment">Opdracht</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" readonly="readonly"
															name="assignment" id="assignment-<?=$v_row->id;?>"
															value="<?= $v_row->text_subject ?>" />
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="message">Bericht</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" readonly="readonly"
															name="message" id="message-<?=$v_row->id;?>"
															value="<?= $v_row->text_content; ?>" />
													</div>
												</div>
												<div>
												<?php if($v_row->file_upload) {
													$file = get_home_url() . '/wp-content/plugins/wp-manager-course';
													$file = $v_row->file_upload ? $file . $v_row->file_upload : '#';
												?>
													<a class="btn btn-default" href="<?= $file?>" target="_blank">
														<span class="<?= $icons["download"] ?>"></span>
														Download huiswerk
													</a>
												<?php } // if file ?>		
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label" for="remarks">Correctie opmerkingen</label>
													<div class="col-sm-9">
														<textarea class="mceEditor" name="remarks" id="remarks-<?=$v_row->id;?>">
															<?= isset($v_row->correction_remarks) ? $v_row->correction_remarks : ''; ?>
														</textarea>
													</div>
												</div>
												<div>
													<select class="form-control" name="passed" id="passed">
														<option value="passed" <?= $v_row->passed > 0 ? 'selected="selected"' : ''?>>Voldoende</option>
														<option value="failed" <?= $v_row->passed > 0 ? '' : 'selected="selected"' ?> >Onvoldoende</option>
													</select>
												</div>
												




												<!-- Modal Footer -->
												<div class="modal-footer">
													<button state=0 type="button" row_id="<?= $v_row->id; ?>" 
															transaction="edit" table="teacher_homework"
															class="btn btn-default transaction-button">
														Wijzigingen opslaan
													</button>
												</div><!-- End Modal Footer -->
											  
											</form>
										</div> <!-- End modal body div -->
									</div> <!-- End modal content div -->
								</div> <!-- End modal dialog div -->
							</div> <!-- End modal div -->
								
						</td>
					</tr>
					<?php 
							} // end foreach
					?>
				</table>
			</div>
		</div>
				
	</div>
</div>
