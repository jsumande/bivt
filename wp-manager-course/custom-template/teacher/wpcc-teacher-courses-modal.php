<?php 
	assert(isset($modal_data));
	assert(isset($source)); // Either edit course or module
	assert(isset($label)); // ID for dialogs
	
	$statuses = ['close'=>'Close','open'=>'Open'];

?>

<div class="modal fade" style="margin-top: 65px;" id="<?=$label;?>" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-80p">
		<div class="modal-content">
		
			<!-- Modal Header -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span> <span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title">
					<?= (($source == 'courses') ? "Opleiding: " : "Module: ") . $modal_data->name;?>
				</h4>
			</div>
			
			
			<!-- Modal Body -->
			<div class="modal-body" id="body-<?=$label;?>">
				<form class="form-horizontal" method="POST">
					<input type="hidden" name="post_id" id="post-id" value="<?=$modal_data->post_id;?>">
					<input type="hidden" name ="source" id="source" value="<?=$source;?>">
					<div class="form-group form-group-sm">

						<!-- left column -->
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="introduction">Introductie</label>
								<div id="introduction_div" class="col-sm-9">
									<textarea class="form-control required mceEditor" rows="6"
										name="introduction" id="introduction-<?=$modal_data->post_id;?>" required>
										<?= $modal_data->introduction; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="meta_description">
									Meta Description </label>
								<div class="col-sm-9">
									<textarea class="form-control required " rows="6"
										name="meta_description" id="meta_description-<?=$modal_data->post_id;?>" required>
										<?= $modal_data->meta_description; ?>
									</textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="meta_keywords"> Meta
									Keywords </label>
								<div class="col-sm-9">
									<textarea class="form-control required" rows="6"
										name="meta_keywords" id="meta_keywords-<?=$modal_data->post_id;?>" required>
										<?= $modal_data->meta_keywords; ?>
									</textarea>
								</div>
							</div>
						</div>


						<!-- right column -->
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="conditions">Voorwaarden</label>
								<div class="col-sm-9">
									<input type="text" class="form-control required"
										name="conditions" id="conditions-<?=$label;?>" placeholder="Voorwaarden"
										required value="<?= $modal_data->conditions; ?>" />
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-3 control-label" for="description">Omschrijving</label>
								<div class="col-sm-9">
									<textarea class="form-control required mceEditor" name="description"
										id="description-<?=$modal_data->post_id;?>" required>
										<?= $modal_data->description; ?>
									</textarea>
								</div>
							</div>
						</div>
					</div>
					<!-- Modal Footer -->
					<div class="modal-footer">
						<button state=0 type="button" row_id="<?= $modal_data->post_id; ?>"
							transaction="edit" table="teacher-<?=$source;?>"
							class="btn btn-default transaction-button">
							Wijzigingen opslaan
						</button>
					</div>
					<!-- End Modal Footer -->
				</form>
			</div>
			<!-- End modal body div -->
		</div>
		<!-- End modal content div -->
	</div>
	<!-- End modal dialog div -->
</div>
<!-- End modal div -->
