<?php
	global $wpdb;
	$currentUserId = get_current_user_id();
	
	$order_items = $wpdb->get_results("SELECT wp_woocommerce_order_items.* FROM wp_woocommerce_order_itemmeta LEFT JOIN wp_woocommerce_order_items ON wp_woocommerce_order_itemmeta.order_item_id = wp_woocommerce_order_items.order_item_id WHERE meta_key = '_useraccount' AND meta_value = '" . $currentUserId . "'");

	$data = array();
	foreach($order_items AS $key=>$order_item) {

		$order = new WC_Order( $order_item->order_id );
		$line_items = $order->get_items();
		$item_counter = 0;
		foreach ($line_items AS $line_id=>$line_item) {
			
			// Avoid old products, non-courses
			if(isset($line_item["item_meta"]["_days_instance"])) {
				$days = $line_item["item_meta"]["_days_instance"][0];
				// As soon as all order items are stored with all days, the line below becomes obsolete and a simple jscon_decode) will do
				$day = (is_array(json_decode($days)) ? json_decode($days)[0] : $days);
				$day_module = $wpdb->get_results("SELECT days.datum AS start_date, modules.slug AS slug FROM days LEFT JOIN instances ON instances.id = days.instance_id LEFT JOIN modules on modules.id = instances.module_id WHERE days.id = '".$day."'");
			
				$payment_methods = ["mollie_wc_gateway_banktransfer" => "Overschrijving",
						"mollie_wc_gateway_ideal" => "Ideal",
				];
		
				$item_counter++;
				$data[] = (object)["order_number" => $order_item->order_id . '-' . $item_counter,
									"module_name" => $line_item["name"],
									"slug" => $day_module[0]->slug,
									"start_date" => $day_module[0]->start_date,
									"status" => $order->get_status(),
									"amount" => $order->get_item_total($line_item),
									"amount_open" => 0,
									"payment_method" => $payment_methods[get_post_meta( $order_item->order_id, '_payment_method', true )],
									"next_payment_term" => "",
				];
			}
		}
	}
	
	$data=(object)$data;
	
?>
<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>

	<div class="container">

		<h3>Invoices</h3>
		
		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th>Inschrijfnummer</th> <!-- Inschrijfnummer == order number -->
					<th>Factuurnummer </th>
					<th>Onderdeel </th>
					<th>Startdatum</th>
					<th>Status</th>
					<th>Bedrag</th>
					<th>Openstaand bedrag</th>
					<th>Betalingsmethode</th>
					<th>Volgende incasso</th>
					<th></th>
				</tr>
				<?php 
					foreach($data AS $k=>$v) {
				?>

						<tr>
							<td><?= isset($v->order_number) ? $v->order_number : 'Onbekend'; ?></td>
							<td></td> <!--  Invoice number not yet available --> 
							<td>
								<?php if (isset($v->module_name)) : ?>
									<a href="<?=get_home_url() . '/news/?page=' . $v->slug . '&view_only=true'?>"><?=$v->module_name;?></a>
								<?php else : ?>
										Onbekend
								<?php endif; ?>
							</td>
							<td><?= isset($v->start_date) ? date('d-m-Y', strtotime($v->start_date)) : 'Onbekend'; ?></td>
							<td><?= isset($v->status) ? $v->status : 'Onbekend' ?></td>
							<td><?= isset($v->amount) ? '€ ' . number_format($v->amount,2,',','.') : ''; ?></td>
							<td><?= isset($v->amount_open) ? '€ ' . number_format($v->amount_open,2,',','.')  : '' ?></td>
							<td><?= isset($v->payment_method) ? $v->payment_method : 'Onbekend'; ?></td>
							<td><?= isset($v->next_payment_term) ? $v->next_payment_term : 'n.v.t.'; ?></td>
						</tr>
				<?php
					} 
				?>
			</table>
		</div>
	</div>
</div>
