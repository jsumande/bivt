<?php

global $wpdb;

$id = get_current_user_id();

$results = $wpdb->get_results('SELECT * FROM wp_users WHERE ID = '. $id .'');
$meta_results = get_user_meta($id);

if (isset($_POST['submit']) ) {
	$fullname = $_POST['fullname'];
	$username = $_POST['username'];
	$nicename = strtolower(str_replace(' ', '-', $fullname));
	
	$query = $wpdb->update(
			    'wp_users',
			    array(
			        'display_name' => $fullname,
			        'user_login' => $username,
			        'user_nicename' => $nicename
			    ),
			    array( 'ID' => $id ),
			    array( '%s', '%s', '%s' ),
			    array( '%d' )
			);
	
	$wpdb->query($query);
}




?>

<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>

	<div class="container">
	<?php foreach ($results as $k => $v) : ?>

	    <h3>Mijn gegevens</h3>
	  	<hr>
		<div class="row">
	      	<!-- left column -->
	      	<div class="col-md-2">
	        	<div class="text-center">
	        		<h4>Profielfoto</h4>
	          		<img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
	          		<h6>Wijzig foto...</h6>
	          		
	          		<input type="file" class="form-control">
	        	</div>
	      	</div>
	      	<div class="col-md-1"></div>
	      	<!-- edit form column -->
	      	<div class="col-md-9 personal-info">	        	
	        	<h4>Persoonlijke informatie</h4>
	        
	        	<div id="user-profile-<?= $v->ID ?>">
		        <form class="form-horizontal" role="form" method="POST">
		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Email:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="email" id="email" disabled value="<?= $v->user_email ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Volledige naam:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="fullname" value="<?= $v->display_name ?>">
		            	</div>
		          	</div>
		          	
		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Voornaam:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="firstname" value="<?= $meta_results["first_name"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Achternaam:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="lastname" value="<?= $meta_results["last_name"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Adres:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="address" value="<?= $meta_results["shipping_address_1"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Postcode:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="zipcode" value="<?= $meta_results["shipping_postcode"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Woonplaats:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="city" value="<?= $meta_results["shipping_city"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Land:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="country" value="<?= $meta_results["shipping_country"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Bedrijf:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="company" value="<?= $meta_results["billing_company"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Voornaam contactpersoon:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="firstname_c" value="<?= $meta_results["billing_first_name"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Achternaam contactpersoon:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="lastname_c" value="<?= $meta_results["billing_last_name"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Bedrijfsadres:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="address_c" value="<?= $meta_results["billing_address_1"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Postcode:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="zipcode_c" value="<?= $meta_results["billing_postcode"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Woonplaats:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="city_c" value="<?= $meta_results["billing_city"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Land:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="country_c" value="<?= $meta_results["billing_country"][0] ?>">
		            	</div>
		          	</div>

					<div class="form-group">
		            	<label class="col-lg-3 control-label">Twitter:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="twitter" value="<?= $meta_results["twitter"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Facebook:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="facebook" value="<?= $meta_results["facebook"][0] ?>">
		            	</div>
		          	</div>

		          	<div class="form-group">
		            	<label class="col-lg-3 control-label">Google+:</label>
		            	<div class="col-lg-8">
		              		<input class="form-control" type="text" name="googleplus" value="<?= $meta_results["googleplus"][0] ?>">
		            	</div>
		          	</div>

			        <div class="form-group">
			            <label class="col-md-3 control-label">Wachtwoord:</label>
			            <div class="col-md-8">
			              	<input class="form-control" type="password" placeholder="password">
			            </div>
			        </div>
			        
			        <div class="form-group">
			            <label class="col-md-3 control-label">Bevestig wachtwoord:</label>
			            <div class="col-md-8">
			              	<input class="form-control" type="password" placeholder="password">
			            </div>
			        </div>
			        
			        <div class="form-group">
			            <label class="col-md-3 control-label"></label>
			            <div class="col-md-8">
			 				 <button state=0 type="button" transaction="edit" table="user_profile" rowid="<?= $v->ID?>" class="btn btn-default transaction-button">Opslaan</button>
			 				 <button state=0 type="button" transaction="ignore" table="user_profile" class="btn btn-default transaction-button" onClick="window.location.reload()">Annuleren</button>
			            </div>
			        </div>
		        </form>
		        </div>
	      	</div>
	  	</div>
	</div>

	<?php endforeach; ?>

	<hr>


</div>