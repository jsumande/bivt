<?php 

$tab = (isset($_GET['action'])) ? $_GET['action'] : 'profile';

?>

<div class="row">
	<div class="col-sm-12">
		<div class="menu_wrapper">
			<form method="post" class="container">
				<ul id="menu-teacher-menu">
					<li>  
						<a href="<?= get_home_url(); ?>">BivT</a>
					</li>
					<li class="<?= $tab=='profile' ? 'active' : '' ?>">  
						<a href="student?action=profile">Mijn gegevens</a>
					</li>	
					<li class="<?= $tab=='financial' ? 'active' : '' ?>">  
						<a href="student?action=financial">Financieel overzicht</a>
					</li>
					<li class="<?= $tab=='portfolio' ? 'active' : '' ?>">  
						<a href="student?action=portfolio">Portfolio</a>
					</li>		
					<li class="<?= $tab=='schedule' ? 'active' : '' ?>">  
						<a href="student?action=schedule">Rooster</a>
					</li>							
					<li class="">
						<a href="<?= wp_logout_url(home_url()) ?>">Logout</a>
					</li>
				</ul>
			</form>
		</div>		
	</div>
</div>
