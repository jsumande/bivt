<?php

	global $wpdb;

	$currentUserId = get_current_user_id();
	
	$order_items_query = "SELECT wp_woocommerce_order_items.* FROM wp_woocommerce_order_itemmeta LEFT JOIN wp_woocommerce_order_items ON wp_woocommerce_order_itemmeta.order_item_id = wp_woocommerce_order_items.order_item_id WHERE meta_key = '_useraccount' AND meta_value = '" . $currentUserId . "'";
	$order_items = $wpdb->get_results($order_items_query);
	
	$not_enrolled_statuses = ['cancelled', 'refunded', 'failed'];
	
	$result = [];
	foreach ($order_items AS $key=>$order_item) {	
	
		$order = new WC_Order( $order_item->order_id );
		
		$order_status = $order->get_status();
		if (!in_array($order_status, $not_enrolled_statuses)) {
			$line_items = $order->get_items();

			foreach ($line_items AS $line_id=>$line_item) {
			
				$instance_day_ids = json_decode($line_item["item_meta"]["_days_instance"][0]);
				$query = "SELECT days.id, days.datum from days WHERE days.id IN ('" .implode("','",$instance_day_ids) ."')";
				$instance_days = $wpdb->get_results($query);
			
				$in_past = true;
				$days_past = 0;
				foreach ($instance_days AS $instance_day_id => $instance_day) {
					$past = (strtotime($instance_day->datum) < time());
					$in_past = $in_past && $past;
					if ($past) {
						$days_past++;
					}
				}
				
				$query = "SELECT instances.id AS instance_id, instances.group AS group_name, modules.name AS module_name, modules.id AS module_id, modules.slug AS slug FROM instances LEFT JOIN days ON instances.id = days.instance_id LEFT JOIN modules ON modules.id = instances.module_id WHERE days.id IN ('" .implode("','",$instance_day_ids) ."') GROUP by modules.id";
	            $instance_info = $wpdb->get_row($query);
	            
				$data[] = (object) ["instance_id" => $instance_info->instance_id,
									"days"=>$line_item["item_meta"]["_days_instance"][0],
									"group_name" => $instance_info->group_name,
									"module_name" => $instance_info->module_name,
									"slug" => $instance_info->slug,
									"days_past" => $days_past,
									"completed" => $days_past >= sizeof($instance_day_ids),
									"end_date" => $instance_data[sizeof($instance_data)-1]->course_date,
				];
			}
		}
	}
				
	$data = (object)$data;
	
	$icons = [
		"lezen" => "glyphicon glyphicon-book",
		"opdracht" => "glyphicon glyphicon-tasks",
		"huiswerk" => "glyphicon glyphicon-pencil",
		"download" => "glyphicon glyphicon-download",
		"upload" => "glyphicon glyphicon-upload",
		"todo" => "glyphicon glyphicon-tasks",
		"done" => "glyphicon glyphicon-ok",
		"overig" => "glyphicon glyphicon-question-sign",
		"passed" => "glyphicon glyphicon-ok",
		"failed" => "glyphicon glyphicon-remove",
		"open" => "glyphicon glyphicon-flag",
		"present" => "glyphicon glyphicon-ok",
		"absent" => "glyphicon glyphicon-remove",
		"remarks" => "glyphicon glyphicon-pencil",
	]
	
?>

<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>
	<div class="container">

		<h3>Portfolio</h3>
		
		<div id="open-portfolio">
			<?php
				$is_completed = [false, true];
				foreach ($is_completed AS $completed) {
			?>
			<h4><?= $completed ? "Gevolgde scholing" : "Lopende scholing" ?></h4>
			<div class="col-sm-10">
				<div class="table-wrapper">
			  		<table id="bivt-table">
						<tr>
							<th>Onderdeel nummer</th>		<!-- Instance number for correspondence -->
							<th>Onderdeel</th>		 		<!-- Description -->
						</tr>
						<?php
							foreach ($data AS $k=>$v) {
								if ($v->completed == $completed) {
						?>
								<tr>
									<td>
										<a href="<?='student?action=portfolio-item&instance_id=' . $v->instance_id . '&days=' . $v->days; ?>"><?= $v->group_name ?></a>
									</td>
									<td>
										<a href="<?=get_home_url() . '/news/?page=' . $v->slug . '&view_only=true'?>"><?=$v->module_name;?></a>
									</td>
								</tr>
						<?php 
								}
							}
						?>
					</table>
				</div>
				<?php 
					}
				?>
			</div>
		</div>
	</div>
</div>
