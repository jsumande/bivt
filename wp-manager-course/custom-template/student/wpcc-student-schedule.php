<?php
	
global $wpdb;


$currentUserId = get_current_user_id();

$order_items = $wpdb->get_results("SELECT order_item_id FROM wp_woocommerce_order_itemmeta WHERE meta_key = '_useraccount' AND meta_value = '" . $currentUserId . "'");

$results = [];
foreach ($order_items AS $key=>$order_item) {
		
	$days = $wpdb->get_results("SELECT meta_value AS day FROM wp_woocommerce_order_itemmeta WHERE meta_key='_days_instance' AND order_item_id='".$order_item->order_item_id."'");
	
	foreach ($days AS $k=>$v) {
		
		$course_days = json_decode($v->day);
		
		foreach ($course_days AS $course_day) {
			$schedules = $wpdb->get_results ("SELECT instances.group AS group_name, modules.id AS module_id, modules.name as module_name, modules.slug AS slug, days.datum AS datum, days.id AS day_id, locations.id AS location_id, locations.name AS location, docents.name AS docent FROM days LEFT JOIN instances ON instances.id = days.instance_id LEFT JOIN locations on locations.id = days.location_id LEFT JOIN docents_days ON docents_days.day_id = days.id LEFT JOIN docents ON docents.id = docents_days.docent_id LEFT JOIN modules ON modules.id = instances.module_id WHERE days.id='".$course_day."'");
				
			foreach ($schedules as $schedule_key => $schedule) {
				if(isset($schedule->datum)) {
					$schedule->datum = date('d-m-Y', strtotime($schedule->datum));
				}
				$results[$course_day] = $schedule;
					}
		}
	}
}

usort($results, function($a, $b) {
	return strtotime($a->datum) - strtotime($b->datum);
});
?>


<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>

	<div class="container">

		<h3>Rooster</h3>
		<div class="col-sm-12">
	  		<table id="bivt-table">
				<tr>
					<th>Groep</th>		<!-- Instance number for correspondence -->
					<th>Onderdeel</th>		 		<!-- Description -->
					<th>Datum</th>
					<th>Locatie</th>
					<th>Docent</th>
					<th>Actions</th>
				</tr>
				<?php foreach ($results as $key=>$day_schedule) : ?>
					<?php if(strtotime($day_schedule->datum) > time()) :?>
						<tr>
							<td><?= $day_schedule->group_name; ?></td>
							<td>
								<a href="<?=get_home_url() . '/news/?page=' . $day_schedule->slug . '&view_only=true'?>"><?= $day_schedule->module_name; ?></a>
							</td>
							<td><?= $day_schedule->datum; ?></td>
							<td>
								<a href="<?=get_home_url() . '/locatie/?locatie=' . $day_schedule->location_id ?>"><?= $day_schedule->location; ?></a>
							</td>
							<td>
								<a href="<?=get_home_url() . '/news/?page=' . $day_schedule->slug . '&tab=docent&view_only=true'?>"><?= $day_schedule->docent; ?></a>
							</td>	
							<td>
								<button class="btn btn-link" data-toggle="modal" data-target="#change-request-<?=$day_schedule->day_id; ?>">
									<span class="glyphicon glyphicon-refresh"></span>
								</button>
							</td>
							
							<div class="modal fade" style="margin-top:65px;" id="change-request-<?=$day_schedule->day_id; ?>" tabindex="-1" role="dialog"
									aria-labelledby="<?// $arialabel; ?>" aria-hidden="true">
							    <div class="modal-dialog modal-lg modal-80p">
							        <div class="modal-content">
							            <!-- Modal Header -->
							            <div class="modal-header">
							                <button type="button" class="close" data-dismiss="modal">
						                       <span aria-hidden="true">×</span>
						                       <span class="sr-only">Sluiten</span>
							                </button>
											<h4 class="modal-title" id="titel-<?=$day_schedule->day_id;?>">
												Wijzigingsverzoek
											</h4>
							            </div>
							            <!-- Modal Body -->
							            <div class="modal-body change-request" id="change-request-body-<?=$day_schedule->day_id;?>">
										    <form class="form-horizontal" method="POST">
										    
										    	<input type="hidden" name="instance_id" id="instance_id" value="">
										    	
										    	<div class="form-group form-group-sm">
								                    <div class="row">
								                    	<label for="status" class="col-sm-2 control-label">Groep</label>
									                    <div class="col-sm-10">
									                    </div>
							                    	</div>
								                </div>

										      	<!-- Modal Footer -->
									            <div class="modal-footer">
									                <button state=0 type="button" rowid="<?=$day_schedule->day_id; ?>" transaction="" table="" class="btn btn-default transaction-button">
														Verzenden
									                </button>
									            </div><!-- End Modal Footer -->

										    </form>
										</div> <!-- End modal body div -->
									</div> <!-- End modal content div -->
								</div> <!-- End modal dialog div -->
							</div> <!-- End modal div -->
							
							
						</tr>			
					<?php endif; ?>
				<?php endforeach; ?>
			</table>
		</div>
	</div>
</div>
