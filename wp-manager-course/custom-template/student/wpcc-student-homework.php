<?php

global $wpdb;

$id = get_current_user_id();

$results = $wpdb->get_results("SELECT * FROM homework WHERE user_id = $id");

get_header();
?>

<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>

	<div class="container">

		<h3>Homework</h3>

		<div class="col-sm-3">
	  		<div class="bivt_wrapper">
	  			<div class="bivt_action_wrapper">
	  				<label>Actions</label>
	  			</div>
				<div class="bivt_add_container">

		        	<button class="btn btn-link add-courses" data-toggle="modal" data-target="#add_homework">
			    		<span class="glyphicon glyphicon-plus bivt-plus-icon"> Submit Homework </span>
			    	</button>				

					<!-- Modal -->
					<div class="modal fade" id="add_homework" tabindex="-1" role="dialog" aria-labelledby="homework" aria-hidden="true">
					    <div class="modal-dialog modal-lg modal-80p">
					        <div class="modal-content">
					            <!-- Modal Header -->
					            <div class="modal-header">
					                <button type="button" class="close"  data-dismiss="modal">
					                       <span aria-hidden="true">&times;</span>
					                       <span class="sr-only">Close</span>
					                </button>
					                <h4 class="modal-title" id="homework">Homework</h4>
					            </div>
					            
					            <!-- Modal Body -->
					            <div class="modal-body">
					                <form id="FormHomework" class="form-horizontal" role="form">
										<div class="form-group">
											<label class="control-label col-sm-2 form-text" for="subject">Subject</label>
											<div class="col-sm-10">
												<input type="text" class="form-control" id="subject" placeholder="Subject">
											</div>
										</div>

										<div class="form-group">
											<label class="control-label col-sm-2 form-text" for="content">Content</label>
											<div class="col-sm-10">
												<textarea class="mceEditor" name="content"></textarea>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-2 control-label" for="file_upload" >File Upload</label>
											<div class="col-sm-10">
												<input type="file" class="form-control" name="file_upload" id="file_upload" required/>
											</div>
										</div>
									</form>
					            </div>
					            
					            <!-- Modal Footer -->
					            <div class="modal-footer">
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
											<button state=0 type="submit" class="btn btn-default SubmitHomework">Submit</button>
										</div>
									</div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
	  		</div>
		</div>
		<div class="col-sm-9">
	  		<table id="bivt-table">
				<tr>
					<th>Subject</th>
					<th>Content</th>
					<th>File Upload</th>
					<th>Created</th>
				</tr>
				<?php 
					foreach ($results as $k => $v):
				?>

				<tr>
					<td><?= $v->text_subject ?></td>
					<td><?= $v->text_content ?></td>
					<td><?= $v->file_upload ?></td>
					<td><?= $v->created ?></td>
				</tr>

				<?php endforeach; ?>				
			</table>
		</div>
	</div>
		
</div>