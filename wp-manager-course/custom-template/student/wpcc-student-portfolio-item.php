<?php

	global $wpdb;

	$currentUserId = get_current_user_id();
	
	$instance_day_ids = isset($_GET['days']) ? json_decode($_GET['days']) : [];
	$instance_id = isset($_GET['instance_id']) ? intval($_GET['instance_id']) : 0;
	
	if ((sizeof($instance_day_ids) < 1) || $instance_id < 1) {
		// Unacceptable input;
		return bivt_redirect(get_home_url() . '/student/?action=portfolio');
	}
	
	$query = "SELECT modules.id, modules.name, instances.group FROM instances LEFT JOIN modules ON modules.id = instances.module_id WHERE instances.id='" . $instance_id . "'";
	$page_info = $wpdb->get_results($query);
	
	$result = [];
	$query = "SELECT days.id, days.datum from days WHERE days.id IN ('" .implode("','",$instance_day_ids) ."')";
	$instance_days = $wpdb->get_results($query);
	$module_id = (isset($page_info[0]) && isset($page_info[0]->id)) ? $page_info[0]->id : 0;
	$module_name = (isset($page_info[0]) && isset($page_info[0]->name)) ? $page_info[0]->name : 'Onbekend';
	$group_name = (isset($page_info[0]) && isset($page_info[0]->group)) ? $page_info[0]->group : 'Onbekend';
	
			
	$query = "SELECT id, name, available_on_day, file, type FROM materials WHERE module_id = '" . $module_id . "' AND status = 'open'";
	$materials = $wpdb->get_results ($query);
            
	$query = "SELECT assignments.id, assignments.day_number AS available_on_day, docents.name AS docent, text_subject, text_content, file FROM assignments LEFT JOIN docents on assignments.docent_id=docents.id WHERE module_id = '" . $module_id . "'";
	$assignments = $wpdb->get_results ($query);
            
	$query = "SELECT homework.id, day_id, assignment_id, docents.name AS docent, homework.text_subject, homework.text_content, homework.file_upload, homework.correct_by, homework.correct_on, homework.correction_remarks, homework.passed, assignments.day_number AS available_on_day FROM `homework` LEFT JOIN docents ON homework.correct_by=docents.id LEFT JOIN assignments ON assignments.id = homework.assignment_id WHERE homework.user_id = '". $currentUserId. "' AND day_id IN ('". implode("','",$instance_day_ids) ."')";
	$homework = $wpdb->get_results ($query);

	$query = "SELECT * FROM days LEFT JOIN attendance_lists ON attendance_lists.day_id = days.id WHERE days.id IN ('". implode("','",$instance_day_ids) ."') AND attendance_lists.student_id='" .$currentUserId . "'";
	$attendance = $wpdb->get_results ($query);
	

	// Create the data object. As 'before start' is not a day, an 'empty' day will be created first.
	$data[] = (object)[
			"datum" => null,
			"day_id" => null,
			"materials" => [],
			"assignments" => [],
			"homework" => [],
			"attendance" => [],
	];
	foreach ($instance_days AS $k=>$v) {
		$data[] = (object)[
				"datum" => $v->datum,
				"day_id" => $v->id,
				"materials" => [],
				"assignments" => [],
				"homework" => [],
				"attendance" => [],
		];
	}
	
	foreach ($materials AS $k=>$v) {
		array_push ($data[$v->available_on_day]->materials,$v);
	}
	
	foreach ($assignments AS $k=>$v) {
		array_push ($data[$v->available_on_day]->assignments,$v);
	}
	
	foreach ($homework AS $k=>$v) {
		array_push ($data[$v->available_on_day]->homework,$v);
	}
	
	foreach ($attendance AS $k=>$v) {
		for ($i=0, $j=sizeof($data); $i<$j; $i++) {
			if ($data[$i]->day_id == $v->day_id) {
				array_push ($data[$i]->attendance,$v);
			}
		}
	}

// 	foreach ($data AS $k=>$v) {
// 		echo "<h5>Data: " . $k . " => " . json_encode($v) . "</h5>";
// 	}
	
	function get_homework_submitted_status ($assignment, $homework) {
		$day_number = $assignment->available_on_day;

		$submitted = false;
		foreach ($homework AS $k=>$v) {
			$submitted = $submitted || ($v->assignment_id == $assignment->id);
		}
		
		return $submitted;		
	}
	
	$icons = [
		"lezen" => "glyphicon glyphicon-book",
		"opdracht" => "glyphicon glyphicon-tasks",
		"huiswerk" => "glyphicon glyphicon-pencil",
		"download" => "glyphicon glyphicon-download",
		"upload" => "glyphicon glyphicon-upload",
		"todo" => "glyphicon glyphicon-tasks",
		"done" => "glyphicon glyphicon-ok",
		"overig" => "glyphicon glyphicon-question-sign",
		"passed" => "glyphicon glyphicon-ok",
		"failed" => "glyphicon glyphicon-remove",
		"open" => "glyphicon glyphicon-flag",
		"present" => "glyphicon glyphicon-ok",
		"absent" => "glyphicon glyphicon-remove",
		"remarks" => "glyphicon glyphicon-pencil",
	]
?>

<div class="wrap">
	<?php include_once 'wpcc-student-header.php'; ?>
	<div class="container">

		<h3>Portfolio</h3>
		<h4>Onderdeel: <?=$module_name;?></h4>
		<h4>Groep: <?=$group_name;?></h4>
		
		<div id="portfolio-item">
			<div class="col-sm-10">
				<div class="table-wrapper">
			  		<table id="bivt-table">
						<tr>
							<th>Datum</th>		 			<!-- Date -->
							<th>Materialen</th>				<!-- Cource materials availble upon start / day 1 -->
							<th>Huiswerk inleveren</th>		<!-- Submit homework -->
							<th>Huiswerk status</th>		<!-- Previously submitted homework and status -->
							<th>Aanwezigheid</th>			<!-- Attendance -->
						</tr>
						<?php foreach ($data AS $k=>$v) :?>
							<?php $visible_day = isset($v->datum) ? strtotime($v->datum) < time() : true;?>
						<tr>
							<td>
								<?=isset($v->datum) ? date('d-m-Y', strtotime($v->datum)) : 'Vooraf';?>
							</td>
							<td>
								<ul>
								<?php if ($visible_day) : ?>	
									<?php foreach($v->materials as $materials_key=>$material) : ?>
										<li class="list-unstyled">
											<?php if($material->file) :?>
											<a class="btn btn-link" href="<?=plugins_url('/wp-manager-course' . $material->file);?>" target="_blank"><?=$material->name;?></a>
											<?php else : ?>
											<span class="<?= isset($icons[$material->type]) ? $icons[$material->type] : $icons["overig"] ?>"></span>
											<?=$material->name;?>
											<?php endif; ?>
										</li>
									<?php endforeach; // materials loop ?>
								<?php endif; // visible_day ?>
								</ul> 
							</td>
							<td>
								<ul>
								<?php if ($visible_day) : ?>
									<?php foreach ($v->assignments as $a_key => $assignment) : ?>
										<li class="list-unstyled">
											<button class="btn btn-link" data-toggle="modal" data-target="#assignment-<?=$assignment->id ?>">
												<span class="<?= $icons[get_homework_submitted_status($assignment, $v->homework) ? "done" : "todo"] ?>"></span>
												<?= $assignment->text_subject ?>
											</button>
												
											<div class="modal fade" style="margin-top:65px;" id="assignment-<?= $assignment->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
											    <div class="modal-dialog modal-lg modal-80p">
					        						<div class="modal-content">
											            <!-- Modal Header -->
											            <div class="modal-header">
					        						        <button type="button" class="close" data-dismiss="modal">
										                       <span aria-hidden="true">×</span>
				           						           		<span class="sr-only">Sluiten</span>
										                	</button>
															<h4>
																<?= $assignment->text_subject; ?>
															</h4>
				            							</div>
											            <!-- Modal Body -->
				            							<div class="modal-body assignment" id="modal-body-<?= $assignment->id; ?>">
				            								<div>
				            									<div>
				            									<p><?= isset($assignment->text_content) ? $assignment->text_content : "";?></p>
																<?php if($assignment->file) {
																	$file = get_home_url() . '/wp-content/plugins/wp-manager-course';
																	$file = $assignment->file ? $file . $assignment->file : '#';
																?>
																	<a class="btn btn-default pull-right" href="<?= $file?>" target="_blank">
																		<span class="<?= $icons["download"] ?>"></span>
																		Download opdracht
																	</a>
																<?php } // if file ?>		
					            								</div>
					            								
					            								<div>
						            								<?php if(get_homework_submitted_status($assignment, $homework)) { ?>
						           										<p>Dit huiswerk is al ingeleverd.</p>
						           									<?php } else {
						           										$index = $assignment->available_on_day > 0 ? $assignment->available_on_day - 1 : 0;
						           										$day_id = $instance_days[$index];
						           									?>
						           									<h4>Huiswerk inleveren</h4>
					           									</div>
					            									
					           									<div id="assignment-data-<?=$assignment->id; ?>">
							   										 <form class="form-horizontal" method="POST">
																    	<input type="hidden" name="user_id" id="user_id" value="<?= $currentUserId ?>">
																    	<input type="hidden" name="day_id" id="day_id" value="<?= $day_id->id ?>">
																    	<input type="hidden" name="assignment_id" id="assignment_id" value="<?= $assignment->id ?>">
																    	<input type="hidden" name="subject" id="subject" value="<?= $assignment->text_subject ?>">
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="content">Bericht</label>
																			<div class="col-sm-10">
																				<textarea class="mceEditor" name="content" id="content-<?=$assignment->id?>"></textarea>
																			</div>
																		</div>
																		<div class="form-group">
																			<label class="col-sm-2 control-label" for="file_upload" >Huiswerk bestand </label>
																			<div class="col-sm-10">
																				<input type="file" class="form-control" name="file_upload" id="files" required/>
																			</div>
																		</div>
						           									</form>
						           									<div class="form-group">
																		<div class="col-sm-offset-2 col-sm-10">
																			<button state=0 type="button" rowid="<?=$assignment->id?>" transaction="add" table="assignment" class="btn btn-default transaction-button">
																				Verzenden
																			</button>
																		</div>
																	</div>						            									
				            										<?php } // end homework to be submitted ?>
				            									</div>
					            							</div>
														    <!-- Modal Footer -->
													        <div class="modal-footer">
							        					        <button type="button" class="btn btn-default" data-dismiss="modal">
						            				           		Sluiten
											                	</button>
												        	</div><!-- End Modal Footer -->
														</div> <!-- End modal body div -->
													</div> <!-- End modal content div -->
												</div> <!-- End modal dialog div -->
											</div> <!-- End modal div -->															    
										</li>
									<?php endforeach; // foreach ?>
								<?php endif; // visible?>
								</ul>
							</td>
							<td>
								<ul>
									<?php 
										foreach ($v->homework as $h_key => $work) {
											$status = isset($work->docent) ? ($work->passed ? 'passed' : 'failed') : 'open';
									?>
											<li class="list-unstyled">
												<button class="btn btn-link" data-toggle="modal" data-target="#homework-<?= $work->id?>">
													<span class="<?= $icons[$status] ?>"></span>
													<?= $work->text_subject ?>
												</button>
												
												<div class="modal fade" style="margin-top:65px;" id="homework-<?=$work->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
												    <div class="modal-dialog modal-lg modal-80p">
					        							<div class="modal-content">
												            <!-- Modal Header -->
												            <div class="modal-header">
					        							        <button type="button" class="close" data-dismiss="modal">
											                       <span aria-hidden="true">×</span>
				            						           		<span class="sr-only">Sluiten</span>
											                	</button>
																<h4>
																	<?= $work->text_subject; ?>
																</h4>
					            							</div>
												            <!-- Modal Body -->
					            							<div class="modal-body assignment" id="modal-body-homework-<?= $work->id; ?>">
					            								<div id="assignment-data-<?=$work->id; ?>">
								   									 <form class="form-horizontal" method="POST">
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="content">Jouw bericht</label>
																	    	<input name="content" id="subject" readonly="readonly"
																	    			value="<?= $work->text_content ?>">
																    	</div>
																    	<?php 
																	    	$file = get_home_url() . '/wp-content/plugins/wp-manager-course';
																	    	$file = $work->file_upload ? $file . $work->file_upload : null;
																	    	?>
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="content">Jouw huiswerk</label>
																				<?php if ($file) : ?>																					
																	    		<a href="<?= $file ?>" target="_blank"><?= $work->text_subject ?></a>
																				<?php endif; ?>
																				
																    	</div>
																		<br>
																		<br>
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="docent">Docent</label>
																	    	<input name="docent" id="docent" readonly="readonly"
																	    			value="<?= isset($work->docent) ? $work->docent : 'Nog niet bekend'; ?>">
																	    </div>
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="correct_on">Beoordeeld op</label>
																	    	<input name="corrected_on" id="correct_on" readonly="readonly"
																	    			value="<?= isset($work->correct_on) ? $work->correct_on : 'n.v.t.'; ?>">
																	    </div>
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="correction_remarks">Opmerkingen docent</label>
																	    	<input name="corrected_remarks" id="corrected_remarks" readonly="readonly"
																	    			value="<?= isset($work->correction_remarks) ? $work->correction_remarks : ''; ?>">
																	    </div>
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="passed">Resultaat</label>
																	    	<input name="passed" id="passed" readonly="readonly"
																	    			value="<?= (isset($work->passed) && $work->passed > 0) ? 'Afgesloten' : 'Openstaand'; ?>">
																	    </div>
																	    <div>
																	    </div>
					            									</form>
					            								</div>
															    <!-- Modal Footer -->
														        <div class="modal-footer">
							        						        <button type="button" class="btn btn-default" data-dismiss="modal">
						            					           		Sluiten
												                	</button>
													        	</div><!-- End Modal Footer -->
															</div> <!-- End modal body div -->
														</div> <!-- End modal content div -->
													</div> <!-- End modal dialog div -->
												</div> <!-- End modal div -->															    
												
											</li>
									<?php 
										}
									?>
								</ul>
							</td>
							<td>
								<ul>
									<?php foreach ($v->attendance as $att_key => $att) : ?>
										<?php if(strtotime($att->datum) <= time()) :?>
											<li class="list-unstyled">
												<?php if (isset($att->remarks)) : ?>
												<button class="btn btn-link" data-toggle="modal" data-target="#attendance-<?=$att->id?>">
													<span class="<?= $icons["remarks"] ?>"></span>
													<?=date("d-m-Y", strtotime( $att->datum ));?>
												</button>
												<?php else : ?>
												<p>
													<span class="<?= $icons[isset($att->student_id) ? "present" : "absent"] ?>"></span>
													<?=date("d-m-Y", strtotime( $att->datum ));?>
												</p>
												<?php endif; ?>
												
												<div class="modal fade" style="margin-top:65px;" id="attendance-<?= $att->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
												    <div class="modal-dialog modal-lg modal-80p">
					        							<div class="modal-content">
												            <!-- Modal Header -->
												            <div class="modal-header">
					        							        <button type="button" class="close" data-dismiss="modal">
											                       <span aria-hidden="true">×</span>
				            						           		<span class="sr-only">Sluiten</span>
											                	</button>
																<h4>Aanwezigheid op <?=date("d-m-Y", strtotime( $att->datum ));?></h4>
					            							</div>
												            <!-- Modal Body -->
					            							<div class="modal-body assignment" id="modal-body-attendance-<?= $att->id; ?>">
					            								<div id="attendance-data-<?=$att->id; ?>">
								   									 <form class="form-horizontal" method="POST">
																		<div class="form-group">
																			<label class="control-label col-sm-2 form-text" for="remarks">Opmerkingen</label>
																	    	<input name="remarks" id="remarks" readonly="readonly"
																	    			value="<?= isset($att->remarks) ? $att->remarks : 'Geen'; ?>">
																	    </div>
					            									</form>
					            								</div>
															    <!-- Modal Footer -->
														        <div class="modal-footer">
							        						        <button type="button" class="btn btn-default" data-dismiss="modal">
						            					           		Sluiten
												                	</button>
													        	</div><!-- End Modal Footer -->
															</div> <!-- End modal body div -->
														</div> <!-- End modal content div -->
													</div> <!-- End modal dialog div -->
												</div> <!-- End modal div -->															    
											</li>
										<?php endif;?>
									<?php endforeach;?>
								</ul>
							</td>
						</tr>
						<?php endforeach; // loop on $data ?> 
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
